#include <iostream>
#include "game.h"
#include "lib/Color.h"
#include "lib/Image.h"
#include "lib/AppData.h"
#include "lib/Console.h"
#include "lib/Application.h"
#include "lib/ImageFont.h"
#include "lib/ImageAtlas.h"
#include <unistd.h>
#include <V3.h>
#include <Matrix.h>
#include "game/dat/Serializer.h"

#include "Window.h"
#include "Input.h"
#include "Testing.h"
#include "Camera.h"
#include "Shader.h"
#include "Pipeline.h"
#include "rInterface.h"
#include "Model.h"
#include "PipelineStage.h"
#include "GameManager.h"
#include "rWindow.h"
#include "lib/Timer.h"

#include "game/uni/ContourObject.h"
#include "game/uni/HermiteObject.h"
#include "WorldManager.h"
#include "ResourceManager.h"

#include "FontIndex.h"
#include "TextureAtlas.h"
#include "FontWriter.h"

#include "ai/Naming.h"

#include "field/WorldFields.h"
#include "client/UniformBlock.h"
#include "soul/Plant.h"
#include "soul/Rock.h"
#include <lib/System.h>
#include <thread>
#include <client/ParticleSystem.h>

#include <client/Skydome.h>
#include "gen/landform/Landform.h"

#include "gen/continent/Overmap.h"

#include "edit/EditView.h"

#include "import/ModelImport.h"
#include "ui/UIResources.h"
#include "ui/UIBar.h"
#include "ui/UIWindow.h"

#include "entity/Entity.h"
#include <fstream>

#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wshadow"
namespace GameManager {
  
  Window *window; Camera *camera;
  RenderPipeline *pipeline;
  Input *input;
  Model *model;
  Shader *opaque, *lineTest, *parTest;
  Testing *testing;
  
  IdealContour testContour, toContour, exContour, fxContour, txContour, testPath, subPath, subContour[2];
  ContourObject testObj;
  ContourObject subObj;
  HermiteObject testHObj;
  
  //Universal
  void Init() {//Called immediatly on execution
    isServer = isClient = true;
    Application::BeginInitialization();
    //System::SetrLimit(1024*1024*1024);
    Application::SetDataDir("data/");
    Application::SetName("RPG Beta");
    Application::SetCodename("rpg2beta");
    Application::SetCompanyName("Delta III Tech");
    Application::SetVersion("0.0r1X");
    Application::SetDebug(true);
    Application::OnExit(&GameManager::Cleanup);
    //System::SetStackLimit(1024*1024*256);
    Application::EndInitialization();
    
    ImageFont::Initialize();
    Input::Initialize();
    ProgramStat::StartRecorderThread("prstat/main");
  }
  
  void addPost(V3 center, V3 delta) { _prsguard(); /*
    ContourObject *c = new ContourObject();
    IdealContour *testContour, *toContour, *tofContour, *testPath;
    testContour = new IdealContour(); toContour = new IdealContour(); tofContour = new IdealContour(); testPath = new IdealContour();
    
    IdealFeature tmpFeature;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 0.5f, 0.f, 0.f); tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); 
    tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; testContour->addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-1.f, 0.f, 0.f); tmpFeature.end = V3(1.f, 0.f, 0.f); testContour.addFeature(tmpFeature);
    testContour->prefab = true;
    testContour->quadratic = 1.f;
    testContour->parametricize();
    
    toContour->prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3(0.5f, 0.f, 0.f);  tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); 
    tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; toContour->addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-2.f, 0.f, 0.f); tmpFeature.end = V3(2.f, 0.f, 0.f); toContour.addFeature(tmpFeature);
    toContour->quadratic = 1.f;
    toContour->parametricize();
    
    tofContour->prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3(0.3f, 0.f, 0.f);  tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); 
    tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; tofContour->addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-2.f, 0.f, 0.f); tmpFeature.end = V3(2.f, 0.f, 0.f); tofContour.addFeature(tmpFeature);
    tofContour->quadratic = 1.f;
    tofContour->parametricize();
    
    c->extrusionLength = 30.f;
    c->addContour(testContour);
    c->addContour(toContour);
    c->addContour(tofContour);
    
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0); tmpFeature.end = delta; tmpFeature.level = 0; c->path.addFeature(tmpFeature);

    //tmpFeature.type = IdealFeature::Line;
    //tmpFeature.start = V3(0); tmpFeature.end = V3(2.0f, 25.f, 0.0f); tmpFeature.level = 1; c->path.addFeature(tmpFeature);
    c->path.parametricize(true);
    
    //c->path = testPath;
    c->transform.position = center;
    c->subfield.setDefault("wood_rough.pure");
    WorldManager::Emplace(c);*/
    ContourObject *c = new ContourObject();
    IdealContour *testContour, *toContour, *testPath;
    testContour = new IdealContour(); toContour = new IdealContour(); testPath = new IdealContour();
    
    IdealFeature tmpFeature;
    tmpFeature.type = IdealFeature::Line;
    float rad = 1.2f;
    tmpFeature.start = V3(  0.f, 0.f, 0.f); tmpFeature.end = V3(  0.f,  rad, 0.f);
    tmpFeature.smoothness = 1.f;
    tmpFeature.setHermite(V3(-1.f, 0.f, 0.f)); testContour->addFeature(tmpFeature);
    
    tmpFeature.start = V3(  0.f, rad, 0.f); tmpFeature.end = V3(  rad,  rad, 0.f);
    tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); testContour->addFeature(tmpFeature);
    
    tmpFeature.start = V3(  rad, rad, 0.f); tmpFeature.end = V3(  rad,  0.f, 0.f);
    //tmpFeature.smoothness = 1.f;
    tmpFeature.setHermite(V3(1.f, 0.f, 0.f)); testContour->addFeature(tmpFeature);
    
    tmpFeature.start = V3(  rad, 0.f, 0.f); tmpFeature.end = V3(  0.f,  0.f, 0.f);
    //tmpFeature.smoothness = 1.f;
    tmpFeature.setHermite(V3(0.f, -1.f, 0.f)); testContour->addFeature(tmpFeature);
    
    testContour->prefab = true;
    testContour->quadratic = 1.f;
    testContour->parametricize();
    
    toContour->prefab = true;

        tmpFeature.start = V3(  0.f, 0.f, 0.f); tmpFeature.end = V3(  0.f,  rad, 0.f);
    tmpFeature.smoothness = 1.f;
    tmpFeature.setHermite(V3(-1.f, 0.f, 0.f)); toContour->addFeature(tmpFeature);
    
    tmpFeature.start = V3(  0.f, rad, 0.f); tmpFeature.end = V3(  rad,  rad, 0.f);
    tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); toContour->addFeature(tmpFeature);
    
    //tmpFeature.smoothness = 0.f;
    tmpFeature.start = V3(  rad, rad, 0.f); tmpFeature.end = V3(  rad,  0.f, 0.f);
    tmpFeature.setHermite(V3(1.f, 0.f, 0.f)); toContour->addFeature(tmpFeature);
    
    //tmpFeature.smoothness = 0.f;
    tmpFeature.start = V3(  rad, 0.f, 0.f); tmpFeature.end = V3(  0.f,  0.f, 0.f);
    tmpFeature.setHermite(V3(0.f, -1.f, 0.f)); toContour->addFeature(tmpFeature);
    
    toContour->quadratic = 1.f;
    toContour->parametricize();
    
    c->extrusionLength = glm::length(delta);
    c->addContour(testContour);
    c->addContour(toContour);
    
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0); tmpFeature.end = delta; tmpFeature.level = 0; c->path.addFeature(tmpFeature);
    c->path.parametricize(true);
    
    //c->path = testPath;
    c->transform.position = center;
    c->subfield.setDefault("wood_rough.pure");
    WorldManager::Emplace(c);
   }
  
  WorldObjectPtr addtri(V3 center) { _prsguard();
    ContourObject *c = new ContourObject();
    c->llLock();
    IdealContour *testContour, *toContour, *testPath;
    testContour = new IdealContour(); toContour = new IdealContour(); testPath = new IdealContour();
    
    float df = 1.f;
    
    IdealFeature tmpFeature;
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(  0.f, 0.f, 0.f); tmpFeature.end = V3(  0.f,  df, 0.f);
    tmpFeature.smoothness = 0.f;
    tmpFeature.setHermite(V3(-1.f, 0.f, 0.f)); testContour->addFeature(tmpFeature);
    
    tmpFeature.start = V3(  0.f, df, 0.f); tmpFeature.end = V3(  df,  df, 0.f);
    tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); testContour->addFeature(tmpFeature);
    
    tmpFeature.start = V3(  df, df, 0.f); tmpFeature.end = V3(  df,  0.f, 0.f);
    //tmpFeature.smoothness = 1.f;
    tmpFeature.setHermite(V3(1.f, 0.f, 0.f)); testContour->addFeature(tmpFeature);
    
    tmpFeature.start = V3(  df, 0.f, 0.f); tmpFeature.end = V3(  0.f,  0.f, 0.f);
    //tmpFeature.smoothness = 1.f;
    tmpFeature.setHermite(V3(0.f, -1.f, 0.f)); testContour->addFeature(tmpFeature);
    
    testContour->prefab = true;
    testContour->quadratic = 1.f;
    testContour->parametricize();
    
    toContour->prefab = true;

        tmpFeature.start = V3(  0.f, 0.f, 0.f); tmpFeature.end = V3(  0.f,  df, 0.f);
    tmpFeature.smoothness = 1.f;
    tmpFeature.setHermite(V3(-1.f, 0.f, 0.f)); toContour->addFeature(tmpFeature);
    
    tmpFeature.start = V3(  0.f, df, 0.f); tmpFeature.end = V3(  df,  df, 0.f);
    tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); toContour->addFeature(tmpFeature);
    
   // tmpFeature.smoothness = 0.f;
    tmpFeature.start = V3(  df, df, 0.f); tmpFeature.end = V3(  0.f,  0.f, 0.f);
    tmpFeature.setHermite(V3(1.f, 0.f, 0.f)); toContour->addFeature(tmpFeature);
    
    //tmpFeature.smoothness = 0.f;
    //tmpFeature.start = V3(  df, 0.f, 0.f); tmpFeature.end = V3(  0.f,  0.f, 0.f);
    //tmpFeature.setHermite(V3(0.f, -1.f, 0.f)); toContour->addFeature(tmpFeature);
    
    toContour->quadratic = 1.f;
    toContour->parametricize();
    
    c->extrusionLength = 12.f;
    c->addContour(testContour);
    c->addContour(toContour);
    
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 12.f, 0.0f); tmpFeature.level = 0; c->path.addFeature(tmpFeature);
    c->path.parametricize(true);
    
    //c->path = testPath;
    c->transform.position = center;
    c->transform.lookAt(center+V3(0.f,0.f,1.f));
    c->subfield.setDefault("brick.pure");
    c->llUnlock();
    return WorldManager::Emplace(c);
  }
  
   void addiptr(V3 center) {
    ContourObject *c = new ContourObject();
    IdealContour *testContour, *toContour, *testPath;
    testContour = new IdealContour(); toContour = new IdealContour(); testPath = new IdealContour();
    
    IdealFeature tmpFeature;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(9.f, 5.f, 0.f); tmpFeature.start = V3( 0.f, 5.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f);
    tmpFeature.setHermite(V3(0.f, -1.f, 0.f)); tmpFeature.theta = 180.f; testContour->addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-1.f, 0.f, 0.f); tmpFeature.end = V3(1.f, 0.f, 0.f); testContour.addFeature(tmpFeature);
    tmpFeature.type = IdealFeature::Line;
    //tmpFeature.start = V3( 10.f, 10.f, 0.f); tmpFeature.end = V3(  0.f, 10.f, 0.f); tmpFeature.setHermite(V3( 0.f, 1.f, 0.f)); toContour->addFeature(tmpFeature);
    tmpFeature.start = V3(  18.f, 5.f, 0.f); tmpFeature.end = V3(  0.f,  5.f, 0.f);
    tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); testContour->addFeature(tmpFeature);
    
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(  5.f, 3.f, 0.f); tmpFeature.start = V3(  5.f,  2.4f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f);
    //tmpFeature.setHermite(V3(0.f, -1.f, 0.f)); tmpFeature.theta = 360.f; testContour->addFeature(tmpFeature);
    testContour->prefab = true;
    testContour->quadratic = 1.f;
    testContour->parametricize();
    
    toContour->prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(  0.f,  0.f, 0.f); tmpFeature.end = V3( 10.f,  0.f, 0.f); tmpFeature.setHermite(V3( 0.f,-1.f, 0.f)); toContour->addFeature(tmpFeature);
    tmpFeature.start = V3( 10.f,  0.f, 0.f); tmpFeature.end = V3( 10.f, 10.f, 0.f); tmpFeature.setHermite(V3( 1.f, 0.f, 0.f)); toContour->addFeature(tmpFeature);
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid =  V3(5.f, 10.f, 0.f); tmpFeature.start = V3( 10.f, 10.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f);
    tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); tmpFeature.theta = 180.f; toContour->addFeature(tmpFeature);
    tmpFeature.type = IdealFeature::Line;
    //tmpFeature.start = V3( 10.f, 10.f, 0.f); tmpFeature.end = V3(  0.f, 10.f, 0.f); tmpFeature.setHermite(V3( 0.f, 1.f, 0.f)); toContour->addFeature(tmpFeature);
    tmpFeature.start = V3(  0.f, 10.f, 0.f); tmpFeature.end = V3(  0.f,  0.f, 0.f); tmpFeature.setHermite(V3(-1.f, 0.f, 0.f)); toContour->addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-2.f, 0.f, 0.f); tmpFeature.end = V3(2.f, 0.f, 0.f); toContour.addFeature(tmpFeature);
    
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(  5.f, 3.f, 0.f); tmpFeature.start = V3(  5.f,  2.2f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f);
   // tmpFeature.setHermite(V3(0.f, -1.f, 0.f)); tmpFeature.theta = 360.f; toContour->addFeature(tmpFeature);
    
    toContour->quadratic = 1.f;
    toContour->parametricize();
    
    c->extrusionLength = 12.f;
    c->addContour(testContour);
    c->addContour(toContour);
    
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 12.f, 0.0f); tmpFeature.level = 0; c->path.addFeature(tmpFeature);
    c->path.parametricize(true);
    
    //c->path = testPath;
    c->transform.position = center;
    c->subfield.setDefault("brick_old.pure");
    WorldManager::Emplace(c);
  }
  
  void addGround(V3 center) {
    ContourObject *c = new ContourObject();
    IdealContour *testContour, *toContour, *testPath;
    //testContour = new IdealContour(); toContour = new IdealContour();
    testPath = new IdealContour();
    const Uint16 sliceTot = 1;
    
    
    IdealFeature tmpFeature;
    c->extrusionLength = 10.f;
    //tmpFeature.type = IdealFeature::Arc;
    //tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 4.5f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; testContour->addFeature(tmpFeature);
    for(int i = 0; i <= sliceTot; i++) {
      float ci = float(i)/float(sliceTot)*10.f + center.z;
      testContour = new IdealContour;
      tmpFeature.type = IdealFeature::Line;
      tmpFeature.smoothness = 0.f;
      tmpFeature.start = V3(  0.f,  0.f, 0.f); tmpFeature.end = V3( 10.f,  0.f, 0.f); tmpFeature.setHermite(V3( 0.f,-1.f, 0.f)); testContour->addFeature(tmpFeature);
      tmpFeature.start = V3( 10.f,  0.f, 0.f); tmpFeature.end = V3( 10.f, 10.f, 0.f); tmpFeature.setHermite(V3( 1.f, 0.f, 0.f));
      //tmpFeature.end = tmpFeature.end = V3( 10.f, 10.f+std::sin(M_PI*float(ci+center.x)/float(sliceTot)*.f), 0.f);
      testContour->addFeature(tmpFeature);
      //tmpFeature.start = V3( 10.f, 10.f, 0.f); tmpFeature.end = V3(  0.f, 10.f, 0.f); tmpFeature.setHermite(V3( 0.f, 1.f, 0.f)); testContour->addFeature(tmpFeature);
      V3 le = tmpFeature.end ;
      for(int j = 0; j < sliceTot; j++) {
        float cj = float(j)/float(sliceTot)*10.f + center.x;
        float s = float(j)/float(sliceTot), e = float(j+1)/float(sliceTot);
        float v = 0.0;//0.2 + 0.05*sin((M_PI*s + M_PI * (i/float(sliceTot)))*6.f) + 0.1f*float(rand()%100)/100.f;//std::sin(M_PI*float(cj+ci)/float(sliceTot)*1.f);
        tmpFeature.start = le; tmpFeature.end = V3(  10.f-e*10.f, 10.f+v, 0.f);
        if(i != sliceTot && i != 0)tmpFeature.smoothness = 0.4f;
        le = tmpFeature.end;
        tmpFeature.setHermite(V3( 0.f, 1.f, 0.f)); testContour->addFeature(tmpFeature);
      }
      tmpFeature.smoothness = 0.f;
      tmpFeature.start = le; tmpFeature.end = V3(  0.f,  0.f, 0.f); tmpFeature.setHermite(V3(-1.f, 0.f, 0.f)); testContour->addFeature(tmpFeature);
      //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-1.f, 0.f, 0.f); tmpFeature.end = V3(1.f, 0.f, 0.f); testContour->addFeature(tmpFeature);
      testContour->prefab = true;
      testContour->quadratic = 1.f;
      testContour->parametricize();
      c->addContour(testContour);
    }
    
    /*toContour->prefab = true;
    //tmpFeature.type = IdealFeature::Arc;
    //tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3(4.5f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; toContour->addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-2.f, 0.f, 0.f); tmpFeature.end = V3(2.f, 0.f, 0.f); toContour.addFeature(tmpFeature);
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(  0.f,  0.f, 0.f); tmpFeature.end = V3( 10.f,  0.f, 0.f); tmpFeature.setHermite(V3( 0.f,-1.f, 0.f)); toContour->addFeature(tmpFeature);
    tmpFeature.start = V3( 10.f,  0.f, 0.f); tmpFeature.end = V3( 10.f, 10.f, 0.f); tmpFeature.setHermite(V3( 1.f, 0.f, 0.f)); toContour->addFeature(tmpFeature);
    tmpFeature.start = V3( 10.f, 10.f, 0.f); tmpFeature.end = V3(  0.f, 10.f, 0.f); tmpFeature.setHermite(V3( 0.f, 1.f, 0.f)); toContour->addFeature(tmpFeature);
    tmpFeature.start = V3(  0.f, 10.f, 0.f); tmpFeature.end = V3(  0.f,  0.f, 0.f); tmpFeature.setHermite(V3(-1.f, 0.f, 0.f)); toContour->addFeature(tmpFeature);
    toContour->quadratic = 1.f;
    toContour->parametricize();*/
    //c->addContour(testContour);
    //c->addContour(toContour);
    
    tmpFeature.type = IdealFeature::Line;
    float tx = 0.f, ty = 0.f;
    for(int i = 0; i < sliceTot; i++) {
      float s = float(i)/float(sliceTot), e = float(i+1)/float(sliceTot);
      tmpFeature.start = V3(s*tx, s*ty, s*10.f); tmpFeature.end = V3(e*tx,e*ty,e*10.f); 
      //std::cout << "from " << glm::to_string(tmpFeature.start) << " to " << glm::to_string(tmpFeature.end) << "\n";
      tmpFeature.level = i; c->path.addFeature(tmpFeature);
    }
    c->path.parametricize(true);
    c->subfield.setDefault("brick_old.pure", 0.5f,"grass.pure", 0.5f);
    
    //c->path = testPath;
    c->transform.position = center;
    WorldManager::Emplace(c);
  }
  /*
  void addTree(V3 center) {
    ContourObject *c = new ContourObject();
    IdealContour *testContour, *toContour, *tofContour, *testPath;
    testContour = new IdealContour(); toContour = new IdealContour(); tofContour = new IdealContour(); testPath = new IdealContour();
    
    IdealFeature tmpFeature;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 5.2f, 0.f, 0.f); tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); 
    tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; testContour->addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-1.f, 0.f, 0.f); tmpFeature.end = V3(1.f, 0.f, 0.f); testContour.addFeature(tmpFeature);
    testContour->prefab = true;
    testContour->quadratic = 1.f;
    testContour->parametricize();
    
    toContour->prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3(3.1f, 0.f, 0.f);  tmpFeature.setHermite(V3(0.f, 1.f, 0.f)); 
    tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; toContour->addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-2.f, 0.f, 0.f); tmpFeature.end = V3(2.f, 0.f, 0.f); toContour.addFeature(tmpFeature);
    toContour->quadratic = 1.f;
    toContour->parametricize();
    
    c->extrusionLength = 10.f;
    c->addContour(testContour);
    c->addContour(toContour);
    
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 9.3f, 0.0f); tmpFeature.level = 0; c->path.addFeature(tmpFeature);
    c->path.parametricize(true);
    
    //c->path = testPath;
    c->transform.position = V3(0.f);
    c->subfield.setDefault("wood_bark.pure");
    
    WorldObject *par = new WorldObject();
    par->emplace(c);
    par->transform.position = center;
    par->soul = new Soul::PlantSoul(par);
    WorldManager::Emplace(par);
  }*/
  
  void addRock(V3 center) {
    
    ContourObject *par = new ContourObject();
    par->transform.position = center;
    par->soul = new Soul::Rock(par);
    par->soul->genesis();
    WorldManager::Emplace(par);
  }
  
  //Shader *txtShader;
  Skydome *sky; ParticleSystem *pts, *txt; TextureAtlas *txtAtlas; EditView *editView;
  Shader *rawModelShader, *rawModelLinesShader; ModelImport *imp = nullptr;
  UIBar *testBar = nullptr; UIRect *testRect;
  
  Entity *player;
  void PrimaryLoad() { _prsguard();//Load required assets (for low-level, ie pre loading screen)
    window = new Window(Application::GetName(), 800, 600);
    input = new Input();
    testing = new Testing();

    
    ResourceManager::Initialize();
    
    txtAtlas = new TextureAtlas(2048, 2048, 2);
    txtAtlas->setName("uiAtlas");
//    /txtAtlas->setMipmap(true);
    txtAtlas->useCache();
    
    Font::sdfResolution = 48;
    FontIndex *fidx = new FontIndex(txtAtlas,128);
    fidx->setSDF(false);
    fidx->loadFont("ui/matsura.ttf");
    
    EditView::Initialize(fidx, txtAtlas);
    
    opaque = Shader::Load("opaqueTest.prgm");
    lineTest = Shader::Load("lineTest.prgm");
    parTest = Shader::Load("particle.prgm");
    camera = new Camera();
    camera->transform.position = V3(2.f, 0.f, -2);
    camera->setPerspective(76.f, 0.12f, 8000.f);
    camera->Activate();
    sky = new Skydome(8000.f);
    
    //std::cout << "ALLOC PARTICLE SYS\n";
    pts = new ParticleSystem(256);
    pts->setShader(parTest);
    pts->setAtlas(ResourceManager::GetParticles());
    //pts->setSorted(true);
    for(unsigned int i = 0; i < 1024; i++) {
      ParticleVert *v = pts->addVert();
      v->pos = V3(float(rand()%1000) / 20.f, 0.4f,  float(rand()%1000) / 20.f);
      v->size = V2(5.f, 8.f);
      v->normal = glm::normalize(V3(float(rand()%1000) / 100.f - 5.f, float(rand()%1000) / 100.f * 0.1f ,float(rand()%1000) / 100.f - 5.f));
      v->figureID = 0;
    }
    pts->upload();
    //std::cout << "DOING SHIT text\n";
    txt = new ParticleSystem(4096);
    //txt->setSorted(true);
    FontWriter::Write(fidx, txt, "does this work...? test!", 0.1f, V3(0.f, 40.f, -5.f), V3(1.f, 0.f, 0.f));
    FontWriter::Write(fidx, txt, "A Second Test :)", 0.1f, V3(30.f, 48.f, -5.f), V3(1.f, 1.f, 0.6f));
    txt->setShader(parTest);
    txt->setAtlas(txtAtlas);
    
    UIResources::Initialize(txtAtlas);
    UIResources::setDefaultFont(fidx);
    UIWindow::SetDefaults(txtAtlas, parTest);
    
    testBar = new UIBar("ui/window.png","ui/simpleBar.png");
    testBar->setSystem(txt);
    testBar->setPosition(V3(0.f, -3.f, 0.f));
    testBar->setTint(ColorRGBA(255,255,255,196));
    testBar->setSize(V2(50.f, 6.f));
    testBar->setValue(86.2f);
    testBar->setColor(ColorRGBA(20, 180, 20, 196));
    testBar->setLabelMode(UIBar::LabelMode::Full);
    testBar->setMaximum(100.f);
    testBar->draw();
    
    testRect = new UIRect("ui/window.png");
    testRect->setSystem(txt);
    testRect->setTint(ColorRGBA(255,255,255,220));
    testRect->setPosition(V3(0.f, -3.f, 0.f));
    testRect->setSize(V2(6.f, 6.f));
    testRect->setNormal(glm::normalize(V3_FORWARD));
    testRect->draw();
    
    txtAtlas->upload();
    txtAtlas->saveCache();
    txt->upload();
    
    player = new Entity();
    player->stats.setLevel(19);
    player->calculateAll();
    {
    std::ofstream os("player.bin");
    ///cereal::XMLOutputArchive archive(os);
    
    //archive(*player);
    char* raw; unsigned int len;
    Serializer::FromEntity(player, raw, len, Serializer::Type::Binary);
    os.write(raw, len);
    delete[] raw;
    
    }
    
    //std::cout << "END PARTICLE SYS\n";
    /*
    Naming::Init();
    for(int i = 0; i < 50; i++) {
      Naming::Specs spec;
      if((i%2) == 0) spec.gender = 200;
      else spec.gender = -200;
      spec.harsh = rand()%256 - 128;
      spec.intel = 0;//rand()%256 - 128;
      spec.high = rand()%256 - 128;
      spec.myst = 0;//rand()%256 - 128;
      spec.actFlags = 0xFF;
      Name name = Naming::GetName();
      
      std::cout << name.classify() << " -> \t\t\t" << name << "\n";
    }*/
  }
  
  void populateWorld();
  
  WorldFields *wf = NULL;
  UniformBlock *blk = NULL;
  WMBufferRegionTransform *transfs = NULL;
  void SecondaryLoad() { _prsguard();//Loads assets(run aschronously with loading screen)
    wf = new WorldFields();
    wf->load();
    //wf->loadCompoundResources();
    //wf->saveGraph("field.png");
    ResourceManager::Commit();
    
    pipeline = new RenderPipeline();
    pipeline->load("pipeline/MainRP.xml");
    pipeline->Allocate();
    
    //transfs = new WMBufferRegionTransform[128];
    //transfs[0].tint = V4(1.f, 1.f, 1.f, 1.f);
    //transfs[0].tmat = glm::translate(V3(0.f, 0.f, 3.f));
    //blk = new UniformBlock();
    //blk->allocate();
    //blk->upload((char*)transfs, sizeof(transfs)*128, true);
    
    IdealFeature tmpFeature;/*
    //tmpFeature.type = IdealFeature::Line;
    //tmpFeature.start = V3(-1.f,-1.f, 0.f); tmpFeature.end = V3( 1.f,-1.f, 0.f); testContour.addFeature(tmpFeature);
    //tmpFeature.start = V3( 1.f,-1.f, 0.f); tmpFeature.end = V3( 1.f, 1.f, 0.f); testContour.addFeature(tmpFeature);
    //tmpFeature.start = V3( 1.f, 1.f, 0.f); tmpFeature.end = V3(-1.f, 1.f, 0.f); testContour.addFeature(tmpFeature);
    //tmpFeature.start = V3(-1.f, 1.f, 0.f); tmpFeature.end = V3(-1.f,-1.f, 0.f); testContour.addFeature(tmpFeature);
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 2.f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; testContour.addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-1.f, 0.f, 0.f); tmpFeature.end = V3(1.f, 0.f, 0.f); testContour.addFeature(tmpFeature);
    testContour.prefab = true;
    testContour.quadratic = 2.f;
    testContour.parametricize();
    
    toContour.prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3(2.9f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; toContour.addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-2.f, 0.f, 0.f); tmpFeature.end = V3(2.f, 0.f, 0.f); toContour.addFeature(tmpFeature);
    toContour.quadratic = 2.f;
    toContour.parametricize();
    
    exContour.prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 1.8f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta =360.f; exContour.addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line;  tmpFeature.end = V3(1.f, 0.f, 0.f); exContour.addFeature(tmpFeature);
    exContour.quadratic = 2.f;
    exContour.parametricize();
    
    fxContour.prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 2.2f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta = 360.f; fxContour.addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Line;  tmpFeature.end = V3(1.f, 0.f, 0.f); fxContour.addFeature(tmpFeature);
    fxContour.quadratic = 1.f;
    fxContour.parametricize();
    
    txContour.prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 2.2f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta =360.f; txContour.addFeature(tmpFeature);
   // tmpFeature.type = IdealFeature::Line;  tmpFeature.end = V3(1.f, 0.f, 0.f); txContour.addFeature(tmpFeature);
    txContour.quadratic = 1.f;
    txContour.parametricize();
    
    testObj.extrusionLength = 12.f;
    testObj.addContour(&testContour);
    testObj.addContour(&toContour);
    testObj.addContour(&exContour);
    testObj.addContour(&fxContour);
    testObj.addContour(&txContour);
    
    Uint8 sci = 0;
    subContour[sci].prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 2.01f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta =360.f; subContour[sci].addFeature(tmpFeature);
    subContour[sci].quadratic = 1.f;
    subContour[sci].parametricize();
    subObj.addContour(&subContour[sci]);
    sci++;
    
    
    subContour[sci].prefab = true;
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.mid = V3(0.f, 0.f, 0.f); tmpFeature.start = V3( 1.8f, 0.f, 0.f); tmpFeature.axis = V3(0.f, 0.f, 1.f); tmpFeature.theta =360.f; subContour[sci].addFeature(tmpFeature);
    subContour[sci].quadratic = 1.f;
    subContour[sci].parametricize();
    subObj.addContour(&subContour[sci]);
    sci++;
    
    subObj.extrusionLength = 10.f;
    */
    //tmpFeature.start = V3(0.f, 0.f, 0.f); tmpFeature.end = V3(0.f, 0.f, 4.f); testPath.addFeature(tmpFeature);
    /*tmpFeature.type = IdealFeature::Arc;
    tmpFeature.start = V3(0); tmpFeature.phi = 90.f;
    tmpFeature.mid = V3(0.f, 5.f, 0.f); tmpFeature.axis = V3(1.f, 0.f, 0.f); tmpFeature.theta = 180.f; testPath.addFeature(tmpFeature);
    
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0.f, 10.f, 0.f); tmpFeature.end = V3(0.f, 10.f, 10.f); testPath.addFeature(tmpFeature);
    
    tmpFeature.type = IdealFeature::Arc;
    tmpFeature.start = V3(0.f, 10.f, 10.f); tmpFeature.phi = 90.f;
    tmpFeature.mid = V3(0.f, 5.f, 10.f); tmpFeature.axis = V3(1.f, 0.f, 0.f); tmpFeature.theta = 180.f; testPath.addFeature(tmpFeature);
    
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0.f, 0.f, 10.f); tmpFeature.end = V3(0.f, 0.f, 0.f); testPath.addFeature(tmpFeature);*/
    /*tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 0.2f, 0.0f); tmpFeature.level = 0; testPath.addFeature(tmpFeature);
    tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 2.2f, 0.0f); tmpFeature.level = 1; testPath.addFeature(tmpFeature);
    tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 5.6f, 0.0f); tmpFeature.level = 2; testPath.addFeature(tmpFeature);
    tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 6.0f, 0.0f); tmpFeature.level = 3; testPath.addFeature(tmpFeature);
    tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 6.2f, 0.0f); tmpFeature.level = 4; testPath.addFeature(tmpFeature);
    //tmpFeature.start = V3(0); tmpFeature.end = V3(0.0f, 6.f, 0.0f); tmpFeature.level = 0; testPath.addFeature(tmpFeature);
    //tmpFeature.type = IdealFeature::Arc;
    //tmpFeature.start = V3(0); tmpFeature.phi = 0.f;
    //tmpFeature.mid = V3(0.f, 6.f, 3.f); tmpFeature.axis = V3(1.f, 0.f, 0.f); tmpFeature.level = 2; tmpFeature.theta = 270.f; testPath.addFeature(tmpFeature);
    testPath.parametricize(true);
    
    testObj.path = testPath;
    
    tmpFeature.type = IdealFeature::Line;
    //tmpFeature.start = V3(-3.f, 0.f, 0.f); tmpFeature.end =V3(-3.f, 0.1f, 1.f); tmpFeature.level = 0; subPath.addFeature(tmpFeature);
    tmpFeature.start = V3(2.f, 2.f, -2.f); tmpFeature.end = V3(0.f, 0.f,  9.f); tmpFeature.level = 1; subPath.addFeature(tmpFeature);
    //tmpFeature.start = V3(1.0f, 1.f, -1.f); tmpFeature.end = V3( 1.f, 1.f,  7.f); tmpFeature.level = 1; subPath.addFeature(tmpFeature);
    //tmpFeature.start = V3(3.f, 6.f, 3.f); tmpFeature.end = V3( 3.f, 6.f,  3.f); tmpFeature.level = 1; subPath.addFeature(tmpFeature);
    subPath.parametricize();
    subObj.path = subPath;
    subObj.flags.set(WorldObject::Subtractive);
    */
    //testObj.addChild(&subObj);
    //subObj.construct(0.1f, NULL);
    Timer testTT;
    /*
    testTT.start();
    subObj.construct(0.1f);
    testTT.stop();
    std::cout << "time to construct subtractive contour: "<< testTT.getElapsedMilli() << "ms\n";
    
    
    testTT.start();
    testObj.construct(0.1f);
    testTT.stop();
    std::cout << "time to construct contour: "<< testTT.getElapsedMilli() << "ms\n";
    
    testTT.start();
    testHObj.construct(0.1f);
    testTT.stop();
    std::cout << "time to construct DC/Voxel: "<< testTT.getElapsedMilli() << "ms\n";
    */
    //testObj.subfield.setDefault("ceramic.pure");
    WorldManager::Initialize();
    //WorldManager::Emplace(&testObj);
    //WorldManager::Emplace(&subObj);
     
    Soul::Rock::Initialize();
    
    populateWorld();
    //std::thread popt(populateWorld);
    //popt.detach();
    //Camera::activeCamera = camera;
    WorldManager::Update();
    
    if(true&&false) {
      rawModelShader = Shader::Load("rawModel.prgm");
      rawModelLinesShader = Shader::Load("rawModelLines.prgm");
      imp = new ModelImport("wt_teapot.obj");//
      //imp = new ModelImport("teddy.obj"); imp->model->scale(1.f/100.f*4.f);
      //imp = new ModelImport("pumpkin_tall_10k.obj"); imp->model->scale(1/25.f);
      //imp = new ModelImport("testImportBody.obj"); imp->model->scale(1.f/100.f*4.f);
      //imp = new ModelImport("yugifull.obj"); imp->model->scale(1.f/100.f*4.f);
      //imp = new ModelImport("test/f2.obj"); imp->model->scale(1/5.f);
      //imp = new ModelImport("sponza.obj"); imp->model->scale(1.f/100.f*4.f);
      //imp->model->scale(4.f);
      imp->model->shader = rawModelShader;
      
      imp->useVectors(3);
      imp->process();
    }
    
    if(false&&true) {
      Landform::LoadAll();
      Continent::Overmap *om = new Continent::Overmap(V2T<Uint16>(512), V2(1000.f), 921);
      om->generate();
      om->WriteImage("omTest");
      delete om;
    }
  }
  
  WorldObjectPtr triPtr;
  void populateWorld() {
    const int maxX =0, maxY =9;
    for(int x = 0; x < maxX; x++) {
      for(int y = 0; y < maxY; y++) {
        addGround(V3(x*10.f, -10.f, y*10.f));
        if(x == maxX - 1 || y == maxY - 1 || x == 1 || y == 1) addtri(V3(x*10.f - 1.5f, 0.f, y*10.f + 1.5f));
        if(x == 3 && y == 3) continue;
        
        //if(rand() % 20 == 0) addRock(V3(x*10.f, 10.f, y*10.f));
        //std::cout << "ADDING POST";
        for(int up = 0; up < ((x/6)%1 == 0 && x%6 == 0)? 6 : 0; up++) {
          addPost(V3(x*10.f, up*10.f+0.f, y*10.f), V3(0.f, 10.f, 0.f));
          //if(x < maxX-1&&x != 2) addPost(V3(x*10.f, up*10.f+10.f, y*10.f), V3(10.f, 0.f, 0.f));
          if(y < maxY-1&&y != 2) addPost(V3(x*10.f, up*10.f+10.f, y*10.f), V3(0.f, 0.f, 10.f));
        }
        //addRock(V3(x*10.f+(rand()%10), 0.f, y*10.f+(rand()%10)));
      }
    }
    
   addtri(V3(20.f, 0.f, 20.f));
    //addGround(V3(5.f, -2.f, -20.f));
    //addTree(V3(10.f, 0.f, 10.f));
    
    /*triPtr = addtri(V3(-5.f, 0.f, 5.f));
    {
    std::ofstream os("tri.bin");
    //cereal::XMLOutputArchive archive(os);
    
    //archive(*triPtr);
    char* raw; unsigned int len;
    Serializer::FromWorldObject(triPtr, raw, len, Serializer::Type::Binary);
    os.write(raw, len);
    delete raw;
    }*/
    //editView = new EditView();
    //editView->setObject(triPtr);
    //addRock(V3(15.f, 0.f, 0.f));
    //addPost(V3(-20.f, 0.f, -20.f),V3(-25.f, 10.f, -20.f));
  }
  
  //Low-level client
  void ClientSetup() {//Called after SecondaryLoad is completed
  
  }
  void WindowChanged() {//Called at window change
  
  }
  
  bool toggleWireframe = true, toggleShaded = false;
  //Client-side stuff, called in order listed
  V3 camForward = V3_FORWARD; V2 camAxisRot = V2(0.f);
  void Update(float newDeltaTime) { _prsguard(); _unused(newDeltaTime);
    { _prsguard();
    Application::UpdateGlobals();
    input->zero();
    window->update();
    Application::UpdateDelta();
    input->update(1);
    }
    
    { _prsguard();
    float moveSpeed = 6.f;
    float rotateSpeed = 220.0f;
    
    if(input->getKeyDown(Input::Key::LShift)) moveSpeed *= 14.f;

    V3 deltaPos = V3(0.0f), deltaPosPr = V3(0.f);
    if(input->getKeyDown('w'))	deltaPos.z += moveSpeed * _deltaTime;
    if(input->getKeyDown('s'))	deltaPos.z -= moveSpeed * _deltaTime;
    if(input->getKeyDown('d'))	deltaPos.x -= moveSpeed * _deltaTime;
    if(input->getKeyDown('a'))	deltaPos.x += moveSpeed * _deltaTime;
    if(input->getKeyDown('r'))	deltaPosPr.y += moveSpeed * _deltaTime;
    if(input->getKeyDown('f'))	deltaPosPr.y -= moveSpeed * _deltaTime;
    if(input->getKeyPress('t')) toggleWireframe = !toggleWireframe;
    if(input->getKeyPress('y')) toggleShaded = !toggleShaded;
    camera->transform.translate(deltaPos * camera->transform.rotation + deltaPosPr);
    
    if(input->getKeyPress('h') && triPtr) triPtr->physical.applyAccel(-V3_UP + V3_RIGHT, 90.f); 
    
    camAxisRot.x +=  input->getAxisDelta(Input::Axis::MouseX)* rotateSpeed * _deltaTime;
    camAxisRot.y += -input->getAxisDelta(Input::Axis::MouseY)* rotateSpeed * _deltaTime;
    camAxisRot.x = wrapAngle(camAxisRot.x);
    camAxisRot.y = clamp((float)wrapAngle(camAxisRot.y), -70.f, 70.f);
    camForward = V3_FORWARD;
    //V3 forward = camera->transform.forward();
    //forward = glm::normalize(forward + camera->transform.right() * float(input->getAxisDelta(Input::Axis::MouseX) * rotateSpeed) + camera->transform.up() * float(input->getAxisDelta(Input::Axis::MouseY) * rotateSpeed) );
    camForward = glm::rotate(camForward, camAxisRot.y*DEG_TO_RAD, camera->transform.up());
    camForward = glm::rotate(camForward, camAxisRot.x*DEG_TO_RAD, camera->transform.right());
    camForward = glm::normalize(camForward);
    
    Quat cRot = Quat(V3(camAxisRot.y * DEG_TO_RAD,0.f, 0.f)) * Quat(V3(0.f,camAxisRot.x*DEG_TO_RAD,0.f));;//quatFromToRotation(V3_FORWARD, camForward);
    //if(forward != V3_FORWARD && forward != -V3_FORWARD)camera->transform.rotate(cRot);
    camera->transform.rotation = cRot;//.rotate(cRot);
    //camera->transform.rotate(Quat(V3(input->getAxisDelta(Input::Axis::MouseY) * -rotateSpeed * _deltaTime, input->getAxisDelta(Input::Axis::MouseX) * rotateSpeed * _deltaTime, 0.f)));
    if(_globalFrame % 60 == 0)  std::cout << glm::to_string(camera->transform.position) << "\n";
    }
    
    WorldManager::Update();
    ResourceManager::CommitDiffUB();
  
  ProgramStat::SubmitFrame();
  }
  
  Transform* GetCameraTransform() {
    return (&camera->transform);
  }
  
  REND_DRAW_R renderScene REND_DRAW_D {
    if(stage->hasFlag(RenderStage::DrawOpaque)) {
      OnOpaque3D(camera, stage);
    }
    if(stage->hasFlag(RenderStage::DrawTransparent)) {
      OnTransparent3D(camera, stage);
    }
    if(stage->hasFlag(RenderStage::DrawUI)) {
      OnUI3D(camera, stage);
    }
    return 1;
  }
  
  static double tt = 0.f;
  void OnOpaque3D(Camera *const camera, RenderStage *const stage) { _prsguard();
    _unused(camera); _unused(stage);
    
    //WorldManager::Render();
    //blk->upload((char*)transfs, sizeof(transfs)*128, true);
    //model->draw(stage);
    riEnsureState();
    if(toggleShaded) {
      opaque->activate();
      riEnsureState();
      opaque->bindTextureArray(ResourceManager::GetDiffuse(), 1, 1);
      opaque->bindTexture(Skydome::noise, 4, 4);
      opaque->bindV3(camera->forward(), 27);
      
      opaque->bindV3(camera->transform.position, 28);
      opaque->bindFloat(tt, 25);
      
      ResourceManager::GetDiffuseUB()->bindTo(41);
      tt += _deltaTime;
      riEnsureState();
      WorldManager::Render();
      riEnsureState();
      
      if(imp != nullptr) {
        imp->drawRaw();
      }
      
    sky->render(V3(camera->transform.position));
      glDisable(GL_CULL_FACE);
      //glDepthMask(false);
      //pts->draw();
      //glDepthMask(true);
      glEnable(GL_CULL_FACE);
     // std::cout << "Drawing stage\n";
      //testing->DrawSphere();
      //testObj.draw();
      //subObj.draw();
      //testHObj.draw();
      opaque->deactivate();
      riEnsureState();
    }
      
      if(toggleWireframe) {
        //glEnable(GL_BLEND);
        lineTest->activate();
        WorldManager::RenderDebug();
        lineTest->deactivate();
        
        
        if(imp != nullptr) {
          rawModelLinesShader->activate();
          imp->drawDebug();
          rawModelLinesShader->deactivate();
        }
        
        //glDisable(GL_BLEND);
      }
    
  }
  void OnTransparent3D(Camera *const camera, RenderStage *const stage) {_prsguard();
    _unused(camera); _unused(stage);
      glDisable(GL_CULL_FACE);
      glDepthMask(false);
    if(toggleShaded) {
      //pts->draw();
      //txt->draw();
      
    }
      
      if(toggleWireframe) {
        
      }
      glDepthMask(true);
    //editView->render();
    glEnable(GL_CULL_FACE);
  }
  void OnUI3D(Camera *const camera, RenderStage *const stage) {_prsguard();
    _unused(camera); _unused(stage);
      glDisable(GL_CULL_FACE);
      glDepthMask(false);
    if(toggleShaded) {
      //pts->draw();
      V3 uip = camera->transform.position + camera->forward()*80.f;
      //std::cout << "drawing at " << glm::to_string(camera->transform.position + camera->transform.mkNormal(V3(-1.f, -1.f, 6.f))) << "\n";
    //testBar->setPosition(uip);
    //testBar->setNormal(glm::normalize(V3_UP + V3_FORWARD*float(sin(_globalTimef))));
      testBar->setNormal(camera->forward());
      testBar->draw();
      
      V3 delCam = glm::normalize(camera->transform.position - testRect->workplane.center);
      static float theta = 0.f;
      theta = fmod(theta + _deltaTime * M_PI/6.f, M_PI);
      V3 rotVec = V3(glm::rotate(theta, V3_FORWARD) * V4(V3_UP, 0.f));
      V3 rotVec2 = V3(glm::rotate(theta, V3_FORWARD) * V4(V3_RIGHT, 0.f));
    testRect->setNormal(rotVec);
    testRect->draw();
    txt->upload();
      txt->draw();
      
    }
      
      if(toggleWireframe) {
        
      }
      glDepthMask(true);
    //editView->render();
    glEnable(GL_CULL_FACE);
  }
  void OnOrtho() {
    
  }
  
  void Render() { _prsguard();
     pipeline->Activate();
    //camera->transform.position.z = -0.1f;
    //camera->transform.lookAt(V3(0,0,0));
    camera->Activate();
    //opaque->activate();
    
    //testing->DrawPlane();
    //testing->DrawSphere();
    //model->draw();
    pipeline->Execute(renderScene);
    //testing->DrawTriangle();
    //opaque->deactivate();
    
    pipeline->Blit();
    pipeline->Deactivate();
    //std::cout << "new frame\n";
    
    //window->update();
    
    camera->Deactivate();
  }
  
  unsigned short GetDistortionCount() {
    return 0;
  }
  void OnDistortion(unsigned short id) {
    _unused(id);
  }
  
  //Server-side stuff
  void OnTick() {
    
  }
  
  void Cleanup() {
    WorldManager::Cleanup();
    Landform::Cleanup();
    ResourceManager::Cleanup();
    ProgramStat::StopRecorderThread();
  }
  
  bool isServer, isClient;
};
#pragma GCC diagnostic pop

int main(int argc, char** argv) {
  _unused(argc); _unused(argv);
  GameManager::Init();
  
  GameManager::PrimaryLoad();
  GameManager::SecondaryLoad();
  
  GameManager::ClientSetup();
  
  int exitCode = 0;
  try {
    while(true) { _prsguard();
      GameManager::Render();

      GameManager::Update(1.f);
      //if(_globalFrame % 60 == 0) std::cout << "FPS: " << (1.f/_deltaTime) << "\n";
      //usleep(std::max(0, int((0.014f-_deltaTime)*1400)));
    }
  } catch(const Application::Exiting &ex) {
    exitCode = ex.code;
  } catch (const std::exception& ex) {
    Console::Fatal(ex.what());
    return 1;
  } catch(...) {
    std::cout << "Caught something...\n";
  }
  /*
  pipeline.Deallocate();
  
  delete model;
  delete camera; delete testing;
  delete input;
  delete window;*/
  
  return exitCode;
}