###export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LIBRARY_PATH

ARKE_PATH = ./engine/
CC_SYS_FLAGS = -DAPP_VERSION="\"0.0r1-X\"" -DUSEGL -DCLIENT

GS_DIR = game/
OBJ_DIR = objects/
GS_CLASSES = 
GS_CLASSES += comp/LineSet.cpp comp/VectorSet.cpp
GS_CLASSES += energy/EnergyType.cpp energy/EnergyInstance.cpp energy/EnergyStore.cpp
GS_CLASSES += mat/PhysMat.cpp mat/MatManager.cpp
GS_CLASSES += field/WorldFields.cpp field/PhaseField.cpp field/FieldVector.cpp field/DeltaVector.cpp field/FieldND.cpp field/FieldVolume.cpp field/ElementClass.cpp field/Element.cpp field/ElementVolume.cpp  field/ElementVector.cpp field/CompoundRenderInfo.cpp field/Compound.cpp field/CompoundList.cpp field/Subfield.cpp
GS_CLASSES += phys/Physical.cpp phys/Surface.cpp phys/PhysicalProperties.cpp
GS_CLASSES += store/WMBootstrap.cpp store/WMWorld.cpp store/WPtr.cpp
#GS_CLASSES += gen/continent/Overmap.cpp gen/continent/Generator.cpp
GS_CLASSES += $(patsubst game/entity/%.cpp,entity/%.cpp,$(shell find game/entity/ -type f -name '*.cpp'))
GS_CLASSES += $(patsubst game/gen/%.cpp,gen/%.cpp,$(shell find game/gen/ -type f -name '*.cpp'))
GS_CLASSES += $(patsubst game/edit/%.cpp,edit/%.cpp,$(shell find game/edit/ -type f -name '*.cpp'))
GS_CLASSES += $(patsubst game/import/%.cpp,import/%.cpp,$(shell find game/import/ -type f -name '*.cpp'))
GS_CLASSES += $(patsubst game/gui/%.cpp,gui/%.cpp,$(shell find game/gui/ -type f -name '*.cpp'))
GS_CLASSES += $(patsubst game/instr/%.cpp,instr/%.cpp,$(shell find game/instr/ -type f -name '*.cpp'))
GS_CLASSES += $(patsubst game/dat/%.cpp,dat/%.cpp,$(shell find game/dat/ -type f -name '*.cpp'))
GS_CLASSES += soul/Soul.cpp soul/Plant.cpp soul/Rock.cpp
GS_CLASSES += ai/Naming.cpp ai/AICritic.cpp ai/AIDrive.cpp ai/AIEntity.cpp ai/AIGoal.cpp ai/AIEPredicate.cpp ai/AIEmotions.cpp ai/AIPersonality.cpp
GS_CLASSES += rep/ContourPlane.cpp rep/BakedContour.cpp rep/BakedContourTrans.cpp rep/ContourCache.cpp rep/IdealContour.cpp rep/IdealSlice.cpp rep/Perturbation.cpp rep/svd.cpp rep/qef.cpp rep/WorldMaterial.cpp
GS_CLASSES += uni/WorldObjectMeta.cpp uni/WorldObjectLink.cpp uni/WorldObject.cpp uni/ContourObject.cpp uni/DualGridObject.cpp uni/FractalFieldObject.cpp uni/HermiteObject.cpp WorldManager.cpp
GS_CLASSES += store/WMBuffer.cpp store/WMBufferSet.cpp
GS_CLASSES += uni/WorldObjectPtr.cpp
GS_CLASSES += ResourceManager.cpp Spider.cpp WorldLoader.cpp

OBJS = 
PCH = game/game.h.gch

#PCH += $(LIB_PCH:%.h=$(LIB_DIR)%.h.gch)
OBJS += $(GS_CLASSES:%.cpp=$(OBJ_DIR)$(GS_DIR)%.o)

#UTILS = util/norm2height.cpp
#UTIL_BUILD = $(UTILS:%.cpp=%)

CC = g++
#CC = icc -parallel -static-intel 
OLDCC = g++ $(CC_SYS_FLAGS)
debug: OPTI = -march=native -mfpmath=sse,387 -mcmodel=large -O1 -ffast-math 
release: OPTI = -march=native -mfpmath=sse,387 -mcmodel=large -Ofast -O3 -ffast-math
debug: CCPLUS = $(OPTI) -g -DDEBUG
release: CCPLUS = $(OPTI) -flto -fwhole-program  -DOPTIMIZE

CFLAGS = -fpch-preprocess -fms-extensions -std=c++0x  $(CC_SYS_FLAGS) -pipe -c -I$(LIB_DIR) -I$(SHARED_DIR) -I$(EXT_DIR) -I$(CLIENT_DIR) -I./ $(CCPLUS) -MMD -MP `freetype-config --cflags`
BFLAGS = -fpch-preprocess -fms-extensions -std=c++0x  $(CC_SYS_FLAGS) -pipe -c $(CCPLUS)
CFLAGSNW = $(CFLAGS)
CFLAGS += -Wall -Wextra  -Wnon-virtual-dtor -Wcast-align -Wshadow -DPROGSTAT -Winvalid-pch
LFLAGS = -fms-extensions -std=c++0x  $(CC_SYS_FLAGS) -Wall -Wextra -MD -MP -pipe -I./ $(CCPLUS) -Wl,-rpath $(ARKE_PATH)
#release: LFLAGS += -Lext/static-linux

LIBS += -Wl,-Bstatic -lboost_system -lboost_filesystem -Wl,-Bdynamic -lGL -lGLEW -lGLU `sdl2-config --libs` -L./dyn/ -lassimp -lfreetype -lnoise -lpthread  -ltbb -lsnappy

CLIENT_CCF = `sdl2-config --cflags`

INCPATH = -I$(ARKE_PATH)lib/ -I$(ARKE_PATH)shared/ -I$(ARKE_PATH)client/ -I$(ARKE_PATH)  -I$(ARKE_PATH)/ext/ -I$(GS_DIR) -Iext/ann/include/
debug:   LFLAGS += 
release: LFLAGS +=  
	
CFLAGS += $(INCPATH)

debug : $(PCH) rpg2 $(UTIL_BUILD)
release : $(PCH) rpg2 $(UTIL_BUILD)
	#strip -s -R .comment -R .gnu.version --strip-unneeded ./rpg2

#dependent builds
.cpp.o: $(PCH)
	$(CC) $(CFLAGS) $< -o $@
	
#utils/% : $(OBJS) $(ARKE_PATH)libEngineClient.a
#	$(CC) $(CFLAGS) $(INCPATH) $(LFLAGS) -o $@ -L$(ARKE_PATH) $(OBJS) $(LIBS) $< -lEngineClient

$(GS_DIR)%.h.gch: $(GS_DIR)%.h
	$(CC) $(CFLAGS) -w -x c++-header $< 
	
$(OBJ_DIR)$(GS_DIR)%.o: $(GS_DIR)%.cpp $(GS_DIR)%.h $(PCH)
	$(CC) $(CFLAGS) $(GS_CCF) $< -o $@
	
#executable builds
#objects/clientMain.o : $(PCH) $(OBJS) $(CS_OBJS) client/main.cpp
#	$(CC) $(CFLAGS) client/main.cpp -o objects/clientMain.o
	
objects/GameManager.o : $(PCH) $(OBJS) GameManager.cpp
	$(CC) $(CFLAGS) -ftemplate-backtrace-limit=0 GameManager.cpp -o objects/GameManager.o
	
arke: FORCE
	#cd $(ARKE_PATH) && make -j 12 -f Makefile
	make -C $(ARKE_PATH) $(MAKECMDGOALS)
	
rpg2 : $(OBJS) objects/GameManager.o engine/libEngine.so engine/libEngineClient.a arke
	@rm prstat/*.txt||true
	@rm prstat/main/*.txt||true
	@rm prstat/worker/*.txt||true
	$(CC) $(INCPATH) $(LFLAGS) -o rpg2  -L./objects/ -L$(ARKE_PATH) $(OBJS) objects/GameManager.o -lEngine $(LIBS)
	
#tests : $(PCH) libEngineClient.a
#	$(CC) $(LFLAGS) $(G_OBJS) $(CSG_OBJS) -o tests  -L./objects/ -L./ shared/UnitTest.cpp
	
#secondary instructions
clean: 
	@find objects/ -name "*.o" -type f -delete
	@find objects/ -name "*.d" -type f -delete
	@rm rpg2 || true
	@rm tests || true
	@rm game/*.gch || true
	@rm .fuse_hidden* || true
	@rm vgcore.* || true
	
#linux packaging routine

backup : RPG2Backup.tar.gz

RPG2Backup.tar.gz : 
	cp -Rt backup/ game/ data/ *.h Makefile  && tar -c -f RPG2Backup.tar.gz backup/* && rm -r backup/*
	
scan : clean 
	scan-build make 2>&1
	
FORCE: