/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EditView.cpp
 * Author: Galen Swain
 * 
 * Created on May 26, 2016, 2:17 PM
 */

#include "EditView.h"
#include <client/Camera.h>
#include "uni/WorldObject.h"
#include "rep/IdealContour.h"
#include <algorithm>
#include <client/Shader.h>
#include <client/FontIndex.h>

FontIndex *EditView::fontIndex = nullptr;
TextureAtlas *EditView::texAtlas = nullptr;
Shader *EditView::shader = nullptr;
unsigned short EditView::pointFigureID = 0;

EditView::EditView() : object() {
}

EditView::~EditView() {
}

void EditView::setObject(const WorldObjectPtr& o) {
  object = o;
  this->retrieve();
}
    
void EditView::retrieve() {
  levels.clear();
  for(unsigned int i = 0; i < object->getContourCount(); i++) {
    levels.emplace_back(EditContour());
    levels.back().setContour(object->getContour(i));
  }
  
  path.setContour(&(object->path));
  this->build();
}
void EditView::commit() {
  
}
    
void EditView::build() {
  path.build();
  for(auto it = levels.begin(); it != levels.end(); ++it) {
    it->build();
  }
}
    
void EditView::render() {
  if(!object) return;
  //Render in view order, with path last
  std::list<EditContour*> rLevels;
  for(auto it = levels.begin(); it != levels.end(); ++it) rLevels.push_back(&(*it));
  
  auto cmpFunc = [=](EditContour* a, EditContour* b) {
    return glm::distance2(a->position(), activeCamera->transform.position) > glm::distance2(b->position(), activeCamera->transform.position);
  };
  rLevels.sort(cmpFunc);
  riEnsureState();
  //Bind transformation
  //shader->bindMat4(object->getGlobalMatrix(), 7);
  //and render
  riEnsureState();
  for(auto it = rLevels.begin(); it != rLevels.end(); ++it) (*it)->render();
  path.render();
}
    
void EditView::Initialize(FontIndex *fidx, TextureAtlas *atl) {//Load resources (point and font)
  fontIndex = fidx;
  texAtlas = atl;
  if(shader != nullptr) delete shader;
  shader = Shader::Load("editView.prgm");
  if(fontIndex != nullptr) pointFigureID = fontIndex->chr('O').selfID;
}

Mat4 EditView::getMatrixForContour(EditContour* ec) {
  return Mat4();
}