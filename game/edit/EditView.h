/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EditView.h
 * Author: Galen Swain
 *
 * Created on May 26, 2016, 2:17 PM
 */

#ifndef EDITVIEW_H
#define EDITVIEW_H
#include "game.h"
#include <deque>
#include "EditContour.h"
class EditContour;
class FontIndex;
class TextureAtlas;
class Shader;

class EditView {
public:
    friend class EditContour;
    
    EditView();
    EditView(const EditView& orig);
    virtual ~EditView();
    
    WorldObjectPtr object;
    
    EditContour path;
    std::deque<EditContour> levels;
    
    void setObject(const WorldObjectPtr& o);
    
    Mat4 getMatrixForContour(EditContour* ec);
    
    void retrieve();
    void commit();
    
    void build();
    
    void render();
    
    static void Initialize(FontIndex *fidx, TextureAtlas *atl);//Load resources (point and font)
    
    static unsigned short pointFigureID;
private:
    static FontIndex *fontIndex;
    static TextureAtlas *texAtlas;
    static Shader *shader;
};

#endif /* EDITVIEW_H */

