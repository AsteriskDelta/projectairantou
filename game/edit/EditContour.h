/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EditContour.h
 * Author: Galen Swain
 *
 * Created on May 26, 2016, 2:17 PM
 */

#ifndef EDITCONTOUR_H
#define EDITCONTOUR_H
#include "game.h"
#include <lib/Rect.h>
#include <lib/Color.h>
#include <list>
#include "comp/Workplane.h"

class ParticleSystem;
class IdealContour;
class IdealFeature;
class EditView;
class UIWindow;
class UIText;
class UILine;
class UIWindow;

class EditContour {
public:
    EditContour();
    virtual ~EditContour();
    
    IdealContour *contour;
    ParticleSystem *system;
    
    std::list<IdealFeature*> selection;
    Rect<float> selectionBox;
    Workplane workplane;
    float detailSize;
    
    bool isDirty;
    EditView *parent;
    
    UIWindow *window;
    
    void assignSystem();
    void freeSystem();
    
    void setContour(IdealContour *c);
    
    void build();
    void render();
    
    inline V3 position() const {
        return workplane.resolve(V3(0.f));
    }
private:
    void drawPoint(V3 pt, ColorRGBA col);
    void drawLine(V3 s, V3 e, ColorRGBA col = ColorRGBA(255,255,255,255));
    void drawGrid();
    void drawText(V3 pt, std::string s);
    void drawArrow(V3 s, V3 d);
    
    static std::list<ParticleSystem*> Systems;
};

#endif /* EDITCONTOUR_H */

