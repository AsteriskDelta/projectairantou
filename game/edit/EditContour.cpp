/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EditContour.cpp
 * Author: Galen Swain
 * 
 * Created on May 26, 2016, 2:17 PM
 */

#include "EditContour.h"
#include <client/ParticleSystem.h>
#include "rep/IdealContour.h"
#include <client/FontWriter.h>
#include <client/FontIndex.h>
#include "EditView.h"
#include <algorithm>
#include "ui/UIWindow.h"
#include "ui/UILine.h"
#include "ui/UIRect.h"
#include "ui/UIIcon.h"
#include "ui/UIText.h"

std::list<ParticleSystem*> EditContour::Systems = std::list<ParticleSystem*>();
static Mutex editContourSysMtx;

EditContour::EditContour() : contour(nullptr), system(nullptr), selection(), selectionBox(), isDirty(false), window(nullptr) {
  window = new UIWindow();
}

EditContour::~EditContour() {
  //this->freeSystem();
  if(window != nullptr) delete window;
}
/*
void EditContour::assignSystem() {
  if(system != nullptr) return;
  MutexGuard mtx(editContourSysMtx);
  
  if(Systems.size() == 0) {//Add new system
    system = new ParticleSystem(2048);
    system->setShader(EditView::shader);
    system->setAtlas(EditView::texAtlas);
  } else {//Take an existing one
    system = Systems.back();
    Systems.pop_back();
  }
}
void EditContour::freeSystem() {
  if(system == nullptr) return;
  system->clear();
  MutexGuard mtx(editContourSysMtx);
  Systems.push_back(system);
  system = nullptr;
}*/

void EditContour::setContour(IdealContour *c) {
  contour = c;
  //this->assignSystem();
}

void EditContour::build() { _prsguard();
  //system->clear();
  window->deleteChildren();
  detailSize = 0.3f;
  if(contour == nullptr) return;
  
  //Build the grid
  /*V3 gridStart = contour->bounds.root, gridSize = contour->bounds.del;
  gridSize.z *= 0.f;
  V3 gridPadding = glm::min(V3(1.f, 1.f, 0.f), gridSize/10.f);
  gridStart -= gridPadding;
  gridSize += 2.f*gridPadding;
  {
    ParticleVert *v = system->addVert();
    v->color = ColorRGBA(255,255,255,255);;
    v->pos = workplane.resolve(gridStart + gridSize*0.5f);
    v->normal = workplane.forward();;
    v->figureID = 510;
    v->size = V2(gridSize/2.f);
  }*/
  
  float epsilon = 0.1f;
  for(unsigned int i = 0; i < contour->features.size(); i++) {
    IdealFeature *const f = &(contour->features[i]);
    
    //Point
    bool isSelected = (std::find(selection.begin(), selection.end(), f) != selection.end());
    ColorRGBA col = isSelected? ColorRGBA(255,0,0,255) : ColorRGBA(0, 255, 255, 255);
    V3 ptPos = f->evaluate(0.f);
    this->drawPoint(ptPos, col);
    if(ptPos.z != 0.f) {//Draw line reaching from grid
      this->drawLine(V3(ptPos.x, ptPos.y, 0.f), ptPos);
    }
    std::stringstream ifss; ifss << "#"<<i<<" r"<<(round(f->smoothness*10.f)/10.f);
    this->drawText(ptPos, ifss.str());
    
    //Normal
    V3 cen = f->evaluate(0.5f);
    V3 normal = f->getHermite(0.5f);
    this->drawArrow(cen, normal);
  }
  
  //Outline
  /*for(float ft = 0.f; ft < 1.f; ) {
    const float nft = f->getNextT(ft, epsilon);
    this->drawLine(f->evaluate(ft), f->evaluate(nft));
    ft = nft;
  }*/
  BakedContour baked = contour->bake(epsilon);
  for(Uint16 i = 0; i < baked.size(); i++) {
    BakedContourVert *v = &(baked[i]), *n = &(baked[baked.getNextSiblingID(i)]);
    this->drawLine(v->pos, n->pos);
  }
  
  //Centroid
  this->drawPoint(contour->centroid, ColorRGBA(128, 0, 240, 255));
  std::stringstream contourSS; contourSS << "Area: " << contour->area;
  this->drawText(contour->centroid, contourSS.str());
  
  //system->upload();
  isDirty = true;
}
void EditContour::render() { _prsguard();
  if(isDirty) {
    isDirty = false;
  }
  /*
  system->bindShader();
  system->shader->bindMat4(parent->getMatrixForContour(this), 11);
  system->draw();
  system->unbindShader();
   * */
  //if(window != nullptr) window->draw();
}

void EditContour::drawPoint(V3 pt, ColorRGBA col) {
  auto *el = new UIIcon("ui/point.png");
  el->setPosition(workplane.resolve(pt));
  el->setNormal(V3(0.f));
  el->setSize(V2(detailSize));
  el->setTint(col);
  window->addChild(el);
  /*
  ParticleVert *v = system->addVert();
  v->color = col;
  v->pos = workplane.resolve(pt);
  v->normal = V3(0.f);
  v->figureID = 511;//EditView::pointFigureID;
  v->size = V2(detailSize);*/
}
void EditContour::drawLine(V3 s, V3 e, ColorRGBA col) {
  auto *el = new UILine("ui/line.png");
  el->setPosition(workplane.resolve(s), workplane.resolve(e));
  el->setTint(col);
  window->addChild(el);
  /*
  ParticleVert *v = system->addVert();
  v->color = ColorRGBA(255,255,255,255);//col;
  v->pos = workplane.resolve((s+e)/2.f);
  v->normal = workplane.forward();
  v->figureID = 511;//EditView::pointFigureID;
  v->size = V2(glm::distance(s,e)/2.f, detailSize);*/
}
void EditContour::drawGrid() {
  
}
void EditContour::drawText(V3 pt, std::string s) {
  auto *el = new UIText();
  el->setPosition(workplane.resolve(pt));
  el->setSize(V2(5.f, 2.f));
  el->setTextSize(0.3f);
  el->setText(s);
  //el->setTint(col);
  window->addChild(el);
  /*
  if(EditView::fontIndex == nullptr) return;
  FontWriter::Write(EditView::fontIndex, system, s, detailSize*2.f, pt, workplane.right(), workplane.forward());*/
}

void EditContour::drawArrow(V3 s, V3 dir) {
  this->drawLine(s, s+dir);
}