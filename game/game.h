/* 
 * File:   game.h
 * Author: Galen Swain
 *
 * Created on May 10, 2015, 6:18 PM
 */

#ifndef GAME_H
#define	GAME_H

#include <math.h>
#ifndef NULL
#define NULL 0
#endif
//#define VEC_DOUBLE
//typedef double wval;
typedef float wval;
#include <shared/Shared.h>

//Serialization handling
#ifdef USES_CEREAL
#include "cereal.hpp"
#else 
//I am literally going to programming hell for this.
//Oh well, 10x faster compile times are worth it.
#define CEREAL_NVP(x) char(0)
#define CEREAL_REGISTER_TYPE(T...) 
#define CEREAL_NVP_ID(idt, v) char(0)
/*
#define cereal::make_nvp(x, y) (y)*/
namespace cereal {
    template <typename T>
    inline constexpr char make_nvp(const std::string& s, const T& v) {
        _unused(s); _unused(v);
        return 0;
    }
}
#endif

#include <lib/V2.h>
#include <lib/V3.h>
#include <lib/V4.h>
#include <lib/Quat.h>
#include <lib/Color.h>
#include <lib/Flags.h>
#include <mutex>
#include <lib/ProgramStat.h>

#include "Transform.h"

typedef Int16 FieldInt;
const FieldInt FieldInt_Max = 32767, FieldInt_Min=-32768;

typedef unsigned long long WOID;

#define WM_EPSILON_UPDATE_THRESHOLD (0.08f)
#define WM_EPSILON_GEN_MIN  (0.05f)
#define WM_EPSILON_GEN_DIST (10.0f)

typedef Uint16 CompoundInt;
typedef std::recursive_mutex Mutex;
typedef std::lock_guard<Mutex> MutexGuard;

inline double wrapAngle(double x){
    x = fmod(x + 180,360);
    if (x < 0)
        x += 360;
    return x - 180;
}

#define WMBOOT_CHUNK_SUBRATIO 4
//Cannot be more than (2^13) 8192 to keep FP err less than 2^-11
#define WMBOOT_CHUNK_SUBINIT (4096*2)
struct WMBaseCoord;
struct WMBootCoord {
    inline WMBootCoord() : w(3) {};
    inline WMBootCoord(long long nx, long long ny, long long nz, Uint8 nw = 0) : x(nx), y(ny), z(nz), w(nw) {};
    
    long long x : 42;
    long long y : 42;
    long long z : 42;
    Uint8 w : 2;
    inline bool operator==(const WMBootCoord& o) const { return x == o.x && y == o.y && z == o.z && w == o.w; };
    inline operator bool() const { return w != 3; };
    
    inline WMBaseCoord getBase() const;
    inline WMBootCoord& operator=(const WMBaseCoord &o);
    //static constexpr WMBootCoord null = WMBootCoord(0,0,0,3);
};
struct WMBaseCoord {
    inline WMBaseCoord() : w(3) {};
    inline WMBaseCoord(long long nx, long long ny, long long nz, Uint8 nw = 0) : x(nx), y(ny), z(nz), w(nw) {};
    
    int x : 30;
    int y : 30;
    int z : 30;
    Uint8 w : 2;
    Uint8 pdd : 4;
    
    inline bool operator==(const WMBaseCoord& o) const { return x == o.x && y == o.y && z == o.z && w == o.w; };
    inline operator bool() const { return w != 3; };
    inline WMBootCoord getBoot() const;
    inline WMBaseCoord& operator=(const WMBootCoord &o) { return (*this = o.getBase()); };
};

inline WMBaseCoord WMBootCoord::getBase() const {
    WMBaseCoord ret;
    ret.x = x / WMBOOT_CHUNK_SUBINIT;
    ret.y = y / WMBOOT_CHUNK_SUBINIT;
    ret.z = z / WMBOOT_CHUNK_SUBINIT;
    return ret;
}

inline WMBootCoord WMBaseCoord::getBoot() const {
    WMBootCoord ret;
    ret.x = x*WMBOOT_CHUNK_SUBINIT;
    ret.y = y*WMBOOT_CHUNK_SUBINIT;
    ret.z = z*WMBOOT_CHUNK_SUBINIT;
    return ret;
}

inline WMBootCoord& WMBootCoord::operator=(const WMBaseCoord &o) { return (*this = o.getBoot()); };


#include "uni/WorldObjectPtr.h"

#endif	/* GAME_H */

