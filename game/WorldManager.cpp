/* 
 * File:   WorldManager.cpp
 * Author: Galen Swain
 * 
 * Created on June 15, 2015, 8:40 PM
 */

#include "WorldManager.h"
#include <list>
#include <thread>
#include <queue>
#include <lib/MessageQueue.h>
#include "uni/WorldObject.h"
#include "store/WMWorld.h"
#include "mat/MatManager.h"
#include <lib/Timer.h>
#include "field/WorldFields.h"
#include "store/WMBufferSet.h"
#include "Spider.h"
#include "ResourceManager.h"
#include "WorldLoader.h"
//#ifndef thread_local
//#define thread_local __thread
//#endif

namespace GameManager {
  Transform* GetCameraTransform();
};
namespace Spider {
  bool ProcessQueue();
  bool PrimaryQueue();
};

namespace WorldManager {
  WMBufferSet<WObjectVert> woBuffers = WMBufferSet<WObjectVert> ();
  WMBufferSet<WObjectVert> dbgBuffers = WMBufferSet<WObjectVert> (true);
  thread_local char *assembleBuff;
  thread_local unsigned int assembleLead, assembleMax;
  
  const bool singleThread = false;
  Mutex qMutex;
  Mutex ttx;
  
  struct WOQEntry {
    enum Action { None = 0, Update = 1, Delete = 2, Construct = 3, Transform = 4, Emplace = 5};
    unsigned short significance;
    unsigned char action;
    WorldObject *obj;
    
    WOQEntry() : significance(0), action(Action::Update), obj(NULL) {};
    WOQEntry(WorldObject *nobj) : significance(0),action(Action::Update), obj(nobj) {};
    WOQEntry(WorldObject *nobj, unsigned int nsig) : significance(nsig),action(Action::Update), obj(nobj) {};
    WOQEntry(unsigned char nact, WorldObject *nobj, unsigned int nsig = 0) : significance(nsig),action(nact), obj(nobj) {};
    bool operator<(const WOQEntry &o) const { return significance < o.significance; };
  };
  struct WMThreadCMD {
    unsigned short type;
    WorldObjectPtr obj;
    union {
      WOQEntry entry;
      struct {
        ImageRGBA* img_ptr;
        Uint16 img_id;
        bool force;
      };
      struct {
        Uint8 actID;
      };
    };
    std::string resName;
    
    WMThreadCMD() : type(0xFFFF) {};
    WMThreadCMD(const WOQEntry& e) : type(1), entry(e) {};
    WMThreadCMD(Uint16 nid,ImageRGBA* np, std::string rn = "") : type(2), img_ptr(np), img_id(nid), force(false), resName(rn) {};
    WMThreadCMD(ImageRGBA* np) : type(2), img_ptr(np), img_id(0xFFFF) {};
    
    static WMThreadCMD LoadObject(WorldObjectPtr o) {
      WMThreadCMD ret;
      ret.type = 3;
      ret.obj = o;
      ret.actID = 0;
      return ret;
    }
    static WMThreadCMD UnloadObject(WorldObjectPtr o) {
      WMThreadCMD ret;
      ret.type = 3;
      ret.obj = o;
      ret.actID = 1;
      return ret;
    }
  };
  
  std::priority_queue<WOQEntry> deltaWOQ;
  MessageQueue<WMThreadCMD> globalQueue(1<<16), primaryQueue(2048);
  
  const unsigned int maxWorlds = 3;
  WMWorld worlds[maxWorlds];
  inline WMWorld* getWorld(const Uint8 i = 0) { return &(worlds[i]); };
  float epsilon;

  void Initialize() {
    //Load all physical materials and the like
    MatManager::Initialize();
    
    //Initialize world roots
    for(Uint8 i = 0; i < maxWorlds; i++) {
      
    }
    
    if(!singleThread) Spider::SpawnWorkers();
  }
  
  void Cleanup() {
    if(!singleThread) {
      Console::Print("WorldManager::Cleaup() starting...");
      Spider::WaitAll();//Kill and wait on all worker threads
      Console::Print("All worker threads completed OK");
    }
  }
  
  float epsilonCurveFactor = 4.f;
  float epsilonScaleFactor = 3.f/5.f;
  float epsilonMinimum = 0.3f;
  float epsilonMinimumDistance = 30.f;
  float epsilonOffset = 0.f;
  
  float GetEpsilon(WorldObject *const obj) { _prsguard();
    float dist = glm::distance(GameManager::GetCameraTransform()->position, obj->getRenderPosition());
    std::cout << "cp: " << glm::to_string(GameManager::GetCameraTransform()->position)  <<", tp: " << glm::to_string(obj->getRenderPosition()) << " -> " << dist << "\n";
    float ret;
    if(dist < epsilonMinimumDistance) ret = epsilonMinimum;
    else ret = epsilonOffset+(log10((dist - epsilonMinimumDistance)/epsilonCurveFactor))*epsilonScaleFactor;///4.f);//The 8.f is used ONLY FOR TESTING
    
    std::cout << "dist: " << dist << " -> epsilon = " << ret << "\n";
    return std::max(epsilonMinimum, ret);
  }
  

  WorldObjectPtr Emplace(WorldObject * const obj) { //MutexGuard lck(qMutex);
    //std::cout << "adding " << obj << " to world\n";
    
    globalQueue.push(WOQEntry(WOQEntry::Emplace, obj));
    return WorldObjectPtr(obj);
  }
  void Remove(WorldObject * const obj) { //MutexGuard lck(qMutex);

    
    globalQueue.push(WOQEntry(WOQEntry::Action::Delete, obj));
  }
  void WasUpdated(WorldObject *const obj, WOF f) { 
    { //MutexGuard lck(qMutex);
    if(f.get(WorldObject::ChangedContour)) {
      //std::cout << "Got contour change...\n";
      globalQueue.push(WOQEntry(WOQEntry::Construct, obj));
    } }
    if(f.get(WorldObject::ChangedTransform) && obj->woBufferID != 65535) {
      //woBuffers.updateTransformData(obj->woBufferID, obj->getRegionTransform());
      //dbgBuffers.updateTransformData(obj->woBufferID, obj->getRegionTransform());
      
      globalQueue.push(WOQEntry(WOQEntry::Transform, obj));
    }
  }
  void DoUpdate(WorldObject *const obj) { 
    //MutexGuard lck(qMutex);
    globalQueue.push(WOQEntry(WOQEntry::Update, obj));
  }
  
  void TexCommit(Uint16 newID, void* img) {
    primaryQueue.push(WMThreadCMD(newID, (ImageRGBA*)img));
  }
  void ImgClose(void *img) {
    globalQueue.push((ImageRGBA*)img);
  }
  
  void QueueLoadObject(WorldObjectPtr obj) {
    globalQueue.push(WMThreadCMD::LoadObject(obj));
  }
  void QueueUnloadObject(WorldObjectPtr obj) {
    globalQueue.push(WMThreadCMD::UnloadObject(obj));
  }
  
  void ParticleTexCommit(Uint16 newID, std::string resName, void *img) {
    primaryQueue.push(WMThreadCMD(newID, (ImageRGBA*)img, resName));
  }
  
  void Update() { _prsguard();
    //Reassemble all required objects
    //{
    //MutexGuard lck(qMutex);
    //while(deltaWOQ.size() > 0) {
      //auto it = deltaWOQ.top(); deltaWOQ.pop();
      //globalQueue.push(WMThreadCMD(it));
      /*
      Timer tt; tt.start();
      meshAsmBuffer.clear(); dbgAsmBuffer.clear();
      std::cout << "Constructing with epsilon: " << GetEpsilon(it.obj) << "\n";
      const bool drawMeshDebug = it.obj->flags.get(WorldObject::DrawDbg);
      
      const float objEpsilon = GetEpsilon(it.obj);
      it.obj->construct(objEpsilon, &meshAsmBuffer, drawMeshDebug? &dbgAsmBuffer : NULL);
      if(drawMeshDebug) it.obj->constructBoundsMesh(objEpsilon, &dbgAsmBuffer);
      tt.stop();
      
      std::cout << "construction creating " << meshAsmBuffer.getVertCount() << "v and " << meshAsmBuffer.getIndCount() <<"i in " << tt.getElapsedMilli()<<"ms\n";
      //buffers[0].emplace(meshAsmBuffer.getDataPtr(), meshAsmBuffer.getVertCount()*sizeof(WObjectVert), meshAsmBuffer.getIndPtr(), meshAsmBuffer.getIndCount());
      //buffers[0].dirty = true;
      if(true || (meshAsmBuffer.getVertCount() > 0)) {
        it.obj->setWOBufferID(woBuffers.emplace(&meshAsmBuffer, it.obj->woBufferID));
      }
      if(dbgAsmBuffer.getVertCount() > 0 && dbgAsmBuffer.getIndCount() > 0) {
        std::cout << "upload asm with " << dbgAsmBuffer.getIndCount() << " lines to "<< it.obj->woBufferID<<"\n";
        dbgBuffers.emplace(&dbgAsmBuffer, it.obj->woBufferID);
      } else dbgBuffers.remove(it.obj->woBufferID);
      
      woBuffers.updateTransformData(it.obj->woBufferID, it.obj->getRegionTransform());
      dbgBuffers.updateTransformData(it.obj->woBufferID, it.obj->getRegionTransform());*/
      
    //}
    //}
    { _prsguard();
    if(singleThread) while(Spider::ProcessQueue());
    while(Spider::PrimaryQueue());
    }
    //WorldFields::active()->loadCompoundQueue();
    
    /*if(buffers[0].dirty) {
      buffers[0].upload();
    }*/
    //Uploads buffer changes
    //ttx.lock();
    //std::cout << "gqueue size() = " << globalQueue.count() << "\n";
    { _prsguard();
    woBuffers.update(0.05f);
    dbgBuffers.update(0.05f);
    }
    //Optimization is performed automatically now
    //if(_globalFrame % 63 == 0) woBuffers.optimize(0.05f);
    //if(_globalFrame % 89 == 0) dbgBuffers.optimize(0.05f);
    
    //Oh baby, you make my world go 'round...
    getWorld()->update();
    //ttx.unlock();
    //(oh yeah!)
  }
  void Render() { _prsguard();
    //Render all active buffers
    /*for(Uint16 i = 0; i < WMBUFFER_INST_MAX; i++) {
      WMBuffer<WObjectVert> *buffer = &buffers[i];
      if(!*buffer) continue;
      //std::cout << "rendering buffer #"<<i<<"\n";
      buffer->render();
    }*/
    woBuffers.render();
  }
  void RenderDebug() { _prsguard();
    dbgBuffers.render();
  }
};


#include <unistd.h>
namespace Spider {
  const unsigned int workerSleepUS = 200;
  const unsigned int workerSkipMS = 100;
  const unsigned int workSkipMax = (workerSkipMS*1000)/workerSleepUS;
  using namespace WorldManager;
  
  static thread_local MeshBuffer<WObjectVert> *meshAsmBuffer, *dbgAsmBuffer;
  bool ProcessQueue() { _prsguard();
    
    //if(!IsPrimary()) return false
    if(globalQueue.count() > 0) {
      //ttx.lock();
        WMThreadCMD cmd = globalQueue.pop();
        if(cmd.type == 1) {//WOQEntry {
        const WOQEntry &it = cmd.entry;
       // std::cout<<"got globalQueue wOQ obj->" << it.obj << "\n";
        
        if(it.action == WOQEntry::Construct) {
          Timer tt, et; tt.start();
          meshAsmBuffer->clear(); dbgAsmBuffer->clear();
          //std::cout << "got entry...\n";

          //std::cout << "Constructing with epsilon: " << GetEpsilon(it.obj) << "\n";
          const bool drawMeshDebug = it.obj->flags.get(WorldObject::DrawDbg);



          const float objEpsilon = GetEpsilon(it.obj);
          it.obj->construct(objEpsilon, meshAsmBuffer, drawMeshDebug? dbgAsmBuffer : NULL);

          if(drawMeshDebug) it.obj->constructBoundsMesh(objEpsilon, dbgAsmBuffer);
          
          //std::cout << "INDEX OUTPUT (" << meshAsmBuffer->indCount << "):\n";
          for(unsigned int i = 0; i < meshAsmBuffer->indCount; i++) {
            //std::cout << "pnd:\t\t" << meshAsmBuffer->bufferInd[i] << "\n";
          }
          //std::cout << "construction creating " << meshAsmBuffer->getVertCount() << "v and " << meshAsmBuffer->getIndCount() <<"i in " << tt.getElapsedMilli()<<"ms\n";
          //buffers[0].emplace(meshAsmBuffer->getDataPtr(), meshAsmBuffer->getVertCount()*sizeof(WObjectVert), meshAsmBuffer->getIndPtr(), meshAsmBuffer->getIndCount());
          //buffers[0].dirty = true;
          tt.stop();
          et.start();
          if(meshAsmBuffer->getIndCount() > 0 || dbgAsmBuffer->getIndCount() > 0) {
            if(true || (meshAsmBuffer->getVertCount() > 0)) {
              it.obj->setWOBufferID(woBuffers.emplace(meshAsmBuffer, it.obj->woBufferID));
            }
            if(dbgAsmBuffer->getVertCount() > 0 && dbgAsmBuffer->getIndCount() > 0) {
              //std::cout << "upload asm with " << dbgAsmBuffer->getIndCount() << " lines to "<< it.obj->woBufferID<<"\n";
              dbgBuffers.emplace(dbgAsmBuffer, it.obj->woBufferID);
            } else dbgBuffers.remove(it.obj->woBufferID);
    //return true;
            std::cout << "construct transform for " << it.obj->woBufferID << "\n";
            woBuffers.updateTransformData(it.obj->woBufferID, it.obj->getRegionTransform());
            dbgBuffers.updateTransformData(it.obj->woBufferID, it.obj->getRegionTransform());
            et.stop();
            
            
          std::cout << "construction creating " << meshAsmBuffer->getVertCount() << "v and " << meshAsmBuffer->getIndCount() <<"i in "<<tt.getElapsedMilli() << "ms and emplaced in " << et.getElapsedMilli()<<"ms on << " << it.obj << std::dec<< "\n";
          }
        } else if(it.action == WOQEntry::Update) {
          it.obj->update();
        } else if(it.action == WOQEntry::Transform) {
          //std::cout << "WTEntry.transform for " << it.obj->woBufferID << "\n";
          woBuffers.updateTransformData(it.obj->woBufferID, it.obj->getRegionTransform());
          dbgBuffers.updateTransformData(it.obj->woBufferID, it.obj->getRegionTransform());
        } else if(it.action == WOQEntry::Emplace) {
          getWorld()->add(it.obj);
          globalQueue.push(WOQEntry(WOQEntry::Update, it.obj));
          globalQueue.push(WOQEntry(WOQEntry::Construct, it.obj));
        } else if(it.action == WOQEntry::Delete) {
          if(it.obj->woBufferID != WO_BUFFER_NULL) {
            dbgBuffers.remove(it.obj->woBufferID);
            woBuffers.remove(it.obj->woBufferID);
          }
          getWorld()->remove(it.obj);
        } else {
          std::stringstream ss; ss << "Unhandled WOQ action, code=" << int(it.action);
          Console::Error(ss.str());
        }
        } else if(cmd.type == 2) {//te+image stuff
          if(cmd.img_id == 0xFFFF) {//Close image
            //ResourceManager::CloseImage(cmd.img_ptr);
          }
        } else if(cmd.type == 3) {
          if(cmd.actID == 0) WorldLoader::DoLoadObject(cmd.obj);
          else if(cmd.actID == 1) WorldLoader::DoUnloadObject(cmd.obj);
        }
        //ttx.unlock();
      } else return false;
    return true;
  }
  
  bool PrimaryQueue() { _prsguard();
    if(primaryQueue.count() > 0) {
      WMThreadCMD cmd = primaryQueue.pop();
      if(cmd.type == 2) {//te+image stuff
            if(cmd.img_id != 0xFFFF) {//Close image
              if(cmd.resName == "") ResourceManager::CommitTexture(cmd.img_id, cmd.img_ptr);
              else ResourceManager::CommitParticle(cmd.img_id, cmd.resName, cmd.img_ptr);
            }
          }
      return true;
      }
    return false;
  }
  
  void WorkerThread(std::mutex *mtx) {
    Uint16 checkSkipCnt = 0;
    meshAsmBuffer = new MeshBuffer<WObjectVert>();
    dbgAsmBuffer = new MeshBuffer<WObjectVert>();
    //std::cout << "Starting thread #" << Spider::GetID() << "\n";
    while((checkSkipCnt%workSkipMax) != 0 || !mtx->try_lock()) {
      ProcessQueue();
      if(!Spider::IsPrimary()) ProgramStat::SubmitFrame();
      checkSkipCnt++;
      usleep(200);
    }
    delete meshAsmBuffer;
    delete dbgAsmBuffer;
    WorkerCleanup(mtx);
  }
};