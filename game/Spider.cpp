/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Spider.cpp
 * Author: Galen Swain
 * 
 * Created on March 26, 2016, 10:14 AM
 */

#include "Spider.h"
#include <thread>
#include <vector>
#include <lib/System.h>
#include <iostream>

//#ifndef thread_local
//#define thread_local __thread
//#endif

namespace Spider {
  struct ThreadStore {
    std::thread *thread;
    std::mutex *runMtx;
    Uint16 id;
    Uint8 cls;
    bool done, killed;
  };
  std::vector<ThreadStore> threads;
  
  thread_local Uint16 localID = 0xFFFF;
  thread_local Uint8 localCls = 0;
  
  bool SpawnClass(Uint8 cls, Uint16 count) {
    if(count == 0) count = (std::thread::hardware_concurrency())-2;//*3/2 - 1;
    if(count == 0 || count == 0xFFFF) count = 6;
    
    std::stringstream ss; 
    if(cls == 1) ss << "Spawning " << count << " worker threads";
    else ss << "Spawning " << count << " threads of type #"<<cls;
    Console::Print(ss.str());
    
    ThreadStore newThread;
    for(Uint16 i = 0; i < count; i++) {
      newThread.id = threads.size();
      newThread.cls = cls;
      newThread.runMtx = new std::mutex();
      newThread.runMtx->lock();
      newThread.done = newThread.killed = false;
      newThread.thread = new std::thread(WorkerSetup, newThread.id, newThread.cls, newThread.runMtx);
      std::cout << "Made thread #" << newThread.id << "\n";
      threads.push_back(newThread);
    }
    
    
    return true;
  }
  bool KillClass(Uint8 cls) {
    for(auto it = threads.begin(); it != threads.end(); ) {
      auto next = std::next(it);
      if(it->cls == cls) Kill(it->id);
      
      it = next;
    }
    
    return true;
  }
  bool Kill(Uint16 id) {
    if(id > threads.size()) {
      Console::Error("Thread Spider attempted kill on ID greater than total thread count!");
      return false;
    }
    
    ThreadStore *ts = &threads[id];
    if(ts->killed) return true;
    
    //ts->thread->detach();
    ts->runMtx->unlock();
    ts->killed = true;
    //delete ts->thread;
    
    //threads.erase(threads.begin()+id);
    //Don't delete mutex here- the spider will self-delete on completion.
    
    return true;
  }
  
  void Wait(Uint16 id) {
    if(id > threads.size()) {
      Console::Error("Thread Spider attempted wait on ID greater than total thread count!");
      return;
    }
    
    ThreadStore *ts = &threads[id];
    if(ts->done) return;
    else if(!ts->killed) Kill(id);
    //Mutex was already deleted by cleanup routine
    ts->thread->join();
    delete ts->thread;
    
    ts->done = true;
  }
  
  void WaitAll() {
    for(unsigned int i = 0; i < threads.size(); i++) Wait(i);
  }
  
  bool KillAll() {
    for(unsigned int i = 0; i < threads.size(); i++) Kill(i);
    return true;
  }

  bool IsClass(Uint8 c) {
    return localCls == c;
  }

  Uint16 GetID() {
    return localID;
  }
  
  void WorkerSetup(Uint16 newID, Uint8 newCls, std::mutex *mtx) {
    localID = newID;
    localCls = newCls;
    ProgramStat::StartRecorderThread("prstat/worker");
    //Set up local variables
    //std::cout << "setstack: " << System::SetStackLimit(1024*1024*128) << "\n";
    WorkerThread(mtx);
  }
  
  void WorkerCleanup(std::mutex *mtx) {
    ProgramStat::StopRecorderThread();
    //std::cout << "Spider Thread #" << GetID() << " exiting...\n";
    delete mtx;
  }
};