#pragma once
#include "client.h"
#include <V3.h>
#include <Quat.h>
#include <Matrix.h>

#define TRANSFORM_DEPTH_LIM 255

class Transform {
public:
  inline Transform() : parent(NULL) {
    position = V3(0,0,0); rotation = Quat(); ratio = V3(1,1,1);
  };
  
  V3 position;
  Quat rotation;
  V3 ratio;
  Transform *parent;
  
  inline operator Mat4() {
    return glm::scale(ratio) * glm::mat4_cast(rotation) * glm::translate(-position);
  }
  
  void setRotation(const V3& forward, const V3& up);
  void lookAt(const V3 &target);
  inline Transform resolve();
  
  inline void translate(V3 offset);
  inline void rotate(Quat q);
  inline void rotate(V3 eAngles);
  inline void scale(V3 s);
  
  inline V3 forward() const;
  inline V3 back() const;
  inline V3 up() const;
  inline V3 lockedUp() const;
  inline V3 down() const;
  inline V3 right() const;
  inline V3 left() const;
protected:
  void doResolve(Transform &tr, int limit);
};

inline void Transform::translate(V3 offset) {
  position += offset;
}
inline void Transform::rotate(Quat q) {
  rotation = q * rotation;
}
inline void Transform::rotate(V3 eAngles) {
  rotation = rotation * Quat(eAngles);
    /*rotation *= glm::rotate(eAngles.x, V3_RIGHT);
    rotation *= glm::rotate(eAngles.y, V3_UP);
    rotation *= glm::rotate(eAngles.z, V3_FORWARD);*/
}
inline void Transform::scale(V3 s) {
  ratio *= s;
}

V3 Transform::forward() const {
  return V3_FORWARD * rotation;
}
V3 Transform::back() const {
  return (-V3_FORWARD) * rotation;
}
V3 Transform::up() const {
  return V3_UP * rotation;
}
V3 Transform::lockedUp() const {
  return V3_UP * rotation * V3(0.f, 1.f, 1.f);
}
V3 Transform::down() const {
  return (-V3_UP) * rotation;
}
V3 Transform::right() const {
  return V3_RIGHT * rotation;
}
V3 Transform::left() const {
  return (-V3_RIGHT) * rotation;
}

Transform Transform::resolve() {
  Transform ret;
  doResolve(ret, TRANSFORM_DEPTH_LIM);
  return ret;
}