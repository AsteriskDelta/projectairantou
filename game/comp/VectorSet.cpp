/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VectorSet.cpp
 * Author: Galen Swain
 * 
 * Created on May 12, 2016, 3:44 PM
 */

#include "VectorSet.h"
#include <list>
#include <algorithm>

template<typename T>
VectorSet<T>::VectorSet() : vectors() {
}
template<typename T>
VectorSet<T>::VectorSet(const VectorSet<T>& orig) : vectors(orig.vectors) {
}
template<typename T>
VectorSet<T>::~VectorSet() {
}

template<typename T>
void VectorSet<T>::add(const V3& vec, const T& val, float power) {
  std::list<VSVecEntry> svecs = this->appliedVectors(vec);
  T v = val;
  for(auto it = svecs.begin(); it != svecs.end() && v > 0.f; ++it) {
    if(it->w > 0.f) {
      float vFrac = clamp(v / it->unw, 0.f, 1.f);
      v = v - it->unw*val;
      reinterpret_cast<VectorSet<T>::Vector*>(it->v)->val += vFrac * it->unw * val;
    }
  }
  if(v > 0.02f) {
    vectors.push_back(Vector{vec, v, power});
  }
}

template<typename T>
void VectorSet<T>::add(VectorSet<T> *o, Transform& trans) {
  for(auto it = o->vectors.begin(); it != o->vectors.end(); ++it) {
    this->add(it->vec * trans.rotation, it->val, it->power);
  }
}

template<typename T>
void VectorSet<T>::clear() {
  vectors.clear();
}

template<typename T>
T VectorSet<T>::nearest(const V3& vec) const { _prsguard();
  float nd = -1.f; unsigned int ni = 0;
  for(unsigned int i = 0; i < vectors.size(); i++) {
    float d = glm::dot(vec, vectors[i].vec);
    if(d > nd) {
      nd = d;
      ni = i;
    }
  }
  
  return vectors[ni].val;
}
template<typename T>
T VectorSet<T>::lerp(const V3& vec) { _prsguard();
  /*
  auto cmpFunc = [=](const vecEntry& a, const vecEntry& b) {
    return a.w < b.w;
  };
  std::list<vecEntry> svecs;
  for(Vector& v : vectors) svecs.push_back({glm::dot(vec, v.vec) , &v});
  //std::sort(svecs.begin(), svecs.end(), cmpFunc);
  svecs.sort(cmpFunc);
  //Suppress vectors with similar dot products
  
  float tot = 0.f; T ret = T();
  for(auto it = svecs.begin(); it != svecs.end(); ++it) {
    for(auto ot = svecs.begin(); ot != svecs.end(); ++ot) {
      float c = glm::dot(it->v->vec, ot->v->vec);
      c = clamp(1.f - c, 0.f, 1.f);
      ot->w *= c;
    }
    
    if(it->w > 0.f) {
      tot += it->w;
      ret += it->v->val*it->w;
    }
  }
  ret /= tot;*/
  std::list<VSVecEntry> svecs = this->appliedVectors(vec);
  T ret = T();
  for(auto it = svecs.begin(); it != svecs.end(); ++it) {
    if(it->w > 0.f) {
      ret += reinterpret_cast<VectorSet<T>::Vector*>(it->v)->val*it->w;
    }
  }
  return ret;
}

template<typename T>
V3 VectorSet<T>::center() const { 
  V3 ret = V3(0.f); float div = 0.f;
  for(const Vector &v : vectors) {
    ret += (v.vec * v.val) * v.power;
    div += v.power;
  }
  return ret/div;
}

template<typename T>
std::list<VSVecEntry> VectorSet<T>::appliedVectors(const V3 &vec) { _prsguard();
  auto cmpFunc = [=](const VSVecEntry& a, const VSVecEntry& b) {
    return a.w < b.w;
  };
  std::list<VSVecEntry> svecs;
  for(Vector& vl : vectors) svecs.push_back({reinterpret_cast<void*>(&vl), glm::dot(vec, vl.vec), 0.f});
  svecs.sort(cmpFunc);
  //Suppress vectors with similar dot products
  
  float tot = 0.f;// T ret = T();
  for(auto it = svecs.begin(); it != svecs.end(); ++it) {
    if(it->w <= 0.01f) continue;
    for(auto ot = std::next(it); ot != svecs.end(); ++ot) {
      float c = glm::dot(reinterpret_cast<VectorSet<T>::Vector*>(it->v)->vec, reinterpret_cast<VectorSet<T>::Vector*>(ot->v)->vec);
      c = pow(clamp(1.f - c, 0.f, 1.f), reinterpret_cast<VectorSet<T>::Vector*>(it->v)->power);
      ot->w *= c;
    }
    
      tot += it->w;
      it->unw = it->w;
      //ret += it->v->val*it->w;
  }
  
  for(auto it = svecs.begin(); it != svecs.end(); ++it) it->w /= tot;
  return svecs;
}

template class VectorSet<float>;