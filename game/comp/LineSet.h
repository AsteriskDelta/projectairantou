/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LineSet.h
 * Author: Galen Swain
 *
 * Created on March 17, 2016, 7:24 PM
 */

#ifndef LINESET_H
#define LINESET_H
#include "game.h"
#include <V2.h>
#include <list>
#include <vector>
#include <iostream>
struct Line {
    V2 start, end;
    V2 normal;
    float score;
    unsigned short refL, refR, refV;
    std::list<unsigned int> refs;
    inline Line() : normal(V2(0.f)) , score(0.f) {};
    inline Line(V2 s, V2 e, V2 n = V2(0.f)) : start(s),end(e),normal(n) {};
    
    inline bool operator ==(const Line& o) const {
        return start == o.start && end == o.end && normal == o.normal;
    }
    
    inline Uint8 getLayer() const {
        return Uint8((this->refV) >> 15);
    }
    
    inline void expand() {
        V2 del = end - start;
        start += del * -0.0002f;
        end += del * 0.0002f;
    }
    
    inline Line expanded() const{
        V2 del = end - start;
        Line ret = *this;
        ret.start += del * -0.0004f;
        ret.end += del * 0.0004f;
        return ret;
    }
    
    inline V2 intersects(const Line& o, bool exact = false) const {
        if(!exact) return this->intersects(o.expanded(), true);
        float d, dx1, dx2, dx3, dy1, dy2, dy3, s, t;

        dx1 = end.x - start.x;     dy1 = end.y - start.y;
        dx2 = o.end.x - o.start.x; dy2 = o.end.y - o.start.y;
        dx3 = start.x - o.start.x; dy3 = start.y - o.start.y;

        d = dx1 * dy2 - dx2 * dy1;

        if(d != 0){
            s = dx1 * dy3 - dx3 * dy1;
            if((s <= 0 && d < 0 && s >= d) || (s >= 0 && d > 0 && s <= d)){
                t = dx2 * dy3 - dx3 * dy2;
                if((t <= 0 && d < 0 && t > d) || (t >= 0 && d > 0 && t < d)){
                    t = t / d;
                    return V2(start.x + t * dx1, start.y + t * dy1);
                }
            }
        }
        
        return V2NULL;
    }
    inline float scoreIntersection(const Line& o, V2& pt) const {
        pt = this->intersects(o, true);
        if(pt == V2NULL) return 0.f;
        
        float n = (glm::dot(normal, o.normal));
        //if(glm::distance2(normal,o.normal) < 0.03f) n = 1.f;
        //else if(glm::distance2(normal,-o.normal) < 0.02f) n = 0.f;
        float d = pow(std::max(0.001f, glm::distance(o.start, pt)), 0.2f);
        //std::cout << "dot " << n << " @ " << d << "\n";
        
        //score contradiction
        //V2 del = glm::normalize(pt-start);
        //float contraScore = glm::dot(normal, del);
        //if(contraScore < 0.02f && contraScore > -0.02f) contraScore = 1.f;
        
        return (std::max(0.f, n) / (d));// * sgn(contraScore);
        //return std::max(0., pow(glm::dot(normal, o.normal), 0.4f)) / d;
    }
    inline float scoreDistance(const Line& o, V2& pt) const {
        pt = this->intersects(o);
        if(pt == V2NULL) return 0.f;
        float d = std::max(0.0001f, glm::distance(o.start, pt));
        return 10.f/d;
    }
    
    inline bool intersects(const V2& pt) const {
        //V2 del = (pt - start);
        //if(glm::distance2(normal, glm::normalize(del)) > 0.012f) return false;
        //float lineDist = fabs((end.y - start.y)*pt.x - (end.x - start.x)*pt.y + end.x*start.y - end.y*start.x)
        /// sqrt(pow(end.y-start.y, 2.f) + pow(end.x-start.x, 2.f));
        float dot = glm::dot(glm::normalize(pt - start), normal);
        float dist = glm::distance2(start, pt) - glm::distance2(start, end);
        if(glm::distance2(end, pt) < 0.007f) return false;
        else if(glm::distance2(start, pt) < 0.007f) return true;
        else if(dist > 0.007f || dot < 0.996f) return false;
        else return true;
    }
};

struct LineResult {
    Line line;
    float score, dist;
    V2 pt;
    Line *supra;//Stores SHORT-TERM pointer to intersecting line
    
    inline bool operator<(const LineResult& o) const {
        return score < o.score;
    };
    inline bool operator>(const LineResult& o) const {
        return score > o.score;
    };
    inline operator bool() const {
        return score > 0.f;
    };
};

class LineSet {
public:
    LineSet();
    LineSet(const LineSet& orig);
    virtual ~LineSet();
    
    std::vector<Line> lines;
    void add(const Line& l);
    
    LineResult intersectBest(Line l) const;
    std::list<LineResult> intersectBestAll(Line l, unsigned short cnt = 0) const;
    std::list<LineResult> intersectBestCross(Line l, unsigned short cnt = 0);
private:

};

#endif /* LINESET_H */

