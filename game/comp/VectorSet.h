/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VectorSet.h
 * Author: Galen Swain
 *
 * Created on May 12, 2016, 3:44 PM
 */

#ifndef VECTORSET_H
#define VECTORSET_H
#include "game.h"
#include <Transform.h>
#include <vector>
#include <list>
//class VectorSet;
//struct VectorSet::Vector;
struct VSVecEntry {
    void* v;
    float w, unw; 
};
class Physical;
    
template<typename T>
class VectorSet {
public:
    friend class Physical;
    struct Vector {
        V3 vec;
        T val;
        float power;
#ifdef CEREAL_INCLUDED
        template<class Archive>
        void serialize(Archive &ar) { 
          ar(CEREAL_NVP(vec),
             CEREAL_NVP(val),
             CEREAL_NVP(power)
          );
        }
#endif
    };
    
    VectorSet();
    VectorSet(const VectorSet& orig);
    virtual ~VectorSet();
    
    void add(const V3& vec, const T& val, float power = 1.f);
    void add(VectorSet<T> *o, Transform& trans);
    
    void clear();
    
    T nearest(const V3& vec) const;
    T lerp(const V3& vec);
    
    V3 center() const;
private:
    std::vector<Vector> vectors;
    
    std::list<VSVecEntry> appliedVectors(const V3& v);
    
public:    
#ifdef CEREAL_INCLUDED
    template<class Archive>
    void serialize(Archive &ar) { 
      ar(CEREAL_NVP(vectors)
      );
    }
#endif
};

typedef VectorSet<float> VectorSetf;

#endif /* VECTORSET_H */

