/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LineSet.cpp
 * Author: Galen Swain
 * 
 * Created on March 17, 2016, 7:24 PM
 */

#include "LineSet.h"
#include <algorithm>
#include <iostream>
#include "lib/FixedPriorityQueue.h"

LineSet::LineSet() {
}

LineSet::LineSet(const LineSet& orig) {
  lines = orig.lines;
}

LineSet::~LineSet() {
}

void LineSet::add(const Line& l) {
  lines.push_back(l);
}

LineResult LineSet::intersectBest(Line l) const {
  std::list<LineResult> ret = this->intersectBestAll(l, 1);
  if(ret.begin() == ret.end()) return LineResult{Line(), 0.f, 0.f, V2NULL, NULL};
  else return *ret.begin();
}


std::list<LineResult> LineSet::intersectBestAll(Line l, unsigned short cnt) const { _prsguard();
  std::fixed_priority_queue<LineResult> lt((cnt == 0)? lines.size() : 8/*cnt*/);
  float maxDistance = 999999.f;
  //std::cout << "Intersecting...\n";
  for(auto it = lines.begin(); it != lines.end(); ++it) {
    LineResult inter;
    inter.score = it->scoreIntersection(l, inter.pt);
    if(inter.pt == V2NULL) {
      
      //std::cout << "\t"<<glm::to_string(l.start) << " -> " << glm::to_string(l.end) << " !// " <<glm::to_string(it->start) << " -> " << glm::to_string(it->end)<<"\n";
      continue;
    }
    
    //I'm officially on crack. We want to eliminate the ray when there's a negative dot between the HERMITES, not the deltas
    //Dammit, me.
    
    inter.dist = glm::distance(l.start, inter.pt);
    //V2 del = glm::normalize(inter.pt-l.start);
    float contraScore = glm::dot(l.normal, it->normal);//glm::dot(it->normal, del);
    if(glm::distance2(it->normal, l.normal) < 0.02f) contraScore = 1.f;//
    //std::cout << glm::to_string(it->normal) << " . " << glm::to_string(l.normal) << " = cs" << contraScore<<"\n";
    
    //if(inter.score != 0.f) std::cout << "found " << glm::to_string(inter.pt) <<", d="<<glm::distance(it->start, inter.pt) << ", c="<<contraScore<<"\n";
    if(inter.score > 0.f && inter.dist < maxDistance && contraScore >= 0.f) {
      inter.line = *it;
      lt.push(inter);
    } else if(contraScore< 0.f) {
      maxDistance = inter.dist;
    }
  }

  
  std::list<LineResult> ret;
  unsigned int retCnt = 0;
  while(!lt.empty()) {
    auto toAdd = lt.top();
    if(toAdd.dist < maxDistance) {
      ret.push_back(toAdd);
      retCnt++;
      if(retCnt == cnt) break;
    }
    //std::cout << "pushing score:\t\t" << toAdd.score << "\n";
    lt.pop();
  }
  return ret;
}

std::list<LineResult> LineSet::intersectBestCross(Line l, unsigned short cnt) { _prsguard();
  std::fixed_priority_queue<LineResult> lt((cnt == 0)? lines.size() : cnt);
  
  //std::cout << "Intersecting...\n";
  for(auto it = lines.begin(); it != lines.end(); ++it) {
    LineResult inter;
    inter.score = it->scoreDistance(l, inter.pt);
    //if(inter.score != 0.f) std::cout << "found " << glm::to_string(inter.pt) <<", d="<<glm::distance(it->start, inter.pt) << "\n";
    if(inter.score > 0.f) {
      inter.line = *it;
      //std::cout << "returning line with ref="<<it->refs.size() <<"\n";
      inter.score = it->score;
      inter.pt = it->start;
      inter.supra = &(*it);
      lt.push(inter);
    }
  }

  std::list<LineResult> ret;
  while(!lt.empty()) {
    auto toAdd = lt.top();
    ret.push_back(toAdd);
    //std::cout << "pushing score:\t\t" << toAdd.score << "\n";
    lt.pop();
  }
  return ret;
}