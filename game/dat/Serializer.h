/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Serializer.h
 * Author: Galen Swain
 *
 * Created on June 17, 2016, 5:12 PM
 */

#ifndef SERIALIZER_H
#define SERIALIZER_H
#include "game.h"

class WorldObject;
class Entity;

namespace Serializer {
    enum Type {
        Binary = 0,
        XML = 1,
        JSON = 2
    };
    bool FromWorldObject(WorldObject* wo, char* &buff, unsigned int &len, unsigned char type = 0);
    bool ToWorldObject(WorldObject *wo, char *buff, unsigned int len, unsigned char type = 0);
    
    bool FromEntity(Entity *e, char *&buff, unsigned int &len, unsigned char type = 0);
    bool ToEntity(Entity *e, char *buff, unsigned int len, unsigned char type = 0);
};

#endif /* SERIALIZER_H */

