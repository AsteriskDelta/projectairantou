/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Serializer.cpp
 * Author: Galen Swain
 * 
 * Created on June 17, 2016, 5:12 PM
 */

#define USES_CEREAL
#include "Serializer.h"
#include "uni/WorldObject.h"
#include "uni/ContourObject.h"
#include "entity/Entity.h"
#include <snappy.h>
#include <sstream>

#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#pragma GCC diagnostic ignored "-Wdelete-non-virtual-dtor"

namespace Serializer {
  thread_local char* compressBuffer = nullptr;
  const unsigned int compressBufferSize = 1024*256;
  inline void ensureCompressBuffer() {
    if(compressBuffer == nullptr) compressBuffer = new char[compressBufferSize];
  }
  
  template <typename T, typename A>
  bool FromObject(T* o, char *&buff, unsigned int &len, unsigned char type) { _prsguard();
    _unused(type);
    A *archive; std::stringstream stream; stream << "";
    //if(type == Type::Binary) archive = new cereal::BinaryOutputArchive(stream);
    //else if(type == Type::XML) archive = new cereal::XMLOutputArchive(stream);
    archive = new A(stream);
    
    (*archive)(*o);
    delete archive;
    //stream << "test";
    const unsigned int streamLen = stream.str().size();
    if(type == Type::Binary) {
      ensureCompressBuffer();
      size_t tmpLen;
      snappy::RawCompress(stream.str().c_str(), streamLen, compressBuffer, &tmpLen);
      len = tmpLen;
      buff = new char[len];
      memcpy(buff, stream.str().c_str(), len);
    } else {
      buff = new char[streamLen];
      memcpy(buff, stream.str().c_str(), streamLen);
      len = streamLen;
    }
    //std::cout << "FromObject returning buff of length " << len << "\n";
    return true;
  }
  
  template<typename T, typename A>
  bool ToObject(T *o, char*buff, unsigned int len, unsigned char type) { _prsguard();
    _unused(type);
    A *archive; std::stringstream stream;
    bool ret = true;
    
    if(type == Type::Binary) {
      ensureCompressBuffer();
      size_t tmpLen;
      ret &= snappy::GetUncompressedLength(buff, len, &tmpLen); if(!ret) return false;
      ret &= snappy::RawUncompress(buff, len, compressBuffer); if(!ret) return false;
      stream.write(compressBuffer, tmpLen);
    } else {
      stream.write(buff, len); 
    }
    stream.seekg(0, stream.beg);
    
    //if(type == Type::Binary) archive = new cereal::BinaryInputArchive(stream);
    //else if(type == Type::XML) archive = new cereal::XMLInputArchive(stream);
    archive = new A(stream);
    
    (*archive)(*o);
    delete archive;
    return true;
  }
  
  template<typename T>
  bool FromObjectWrapper(T* o, char*&buff, unsigned int &len, unsigned char type) {
    if(type == Type::Binary)  return FromObject<T, cereal::BinaryOutputArchive>(o, buff, len, type);
    else if(type == Type::JSON) return FromObject<T, cereal::JSONOutputArchive>(o, buff, len, type);
    else return FromObject<T, cereal::XMLOutputArchive>(o, buff, len, type);
  }
    template<typename T>
  bool ToObjectWrapper(T *o, char*buff, unsigned int len, unsigned char type) {
    if(type == Type::Binary) return ToObject<T, cereal::BinaryInputArchive>(o, buff, len, type);
    else if(type == Type::JSON) return ToObject<T, cereal::JSONInputArchive>(o, buff, len, type);
    else return ToObject<T, cereal::XMLInputArchive>(o, buff, len, type);
  }

  bool FromWorldObject(WorldObject* wo, char* &buff, unsigned int &len, unsigned char type) {
    return FromObjectWrapper<WorldObject>(wo, buff, len, type);
  }
  bool ToWorldObject(WorldObject *wo, char *buff, unsigned int len, unsigned char type) {
    return ToObjectWrapper<WorldObject>(wo, buff,len,type);
  }

  bool FromEntity(Entity *e, char *&buff, unsigned int &len, unsigned char type) {
    return FromObjectWrapper<Entity>(e, buff, len, type);
  }
  bool ToEntity(Entity *e, char *buff, unsigned int len, unsigned char type) {
     return ToObjectWrapper<Entity>(e, buff, len, type);
  }
};

#pragma GCC diagnostic pop