/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EntityHUD.h
 * Author: Galen Swain
 *
 * Created on June 7, 2016, 3:25 PM
 */

#ifndef ENTITYHUD_H
#define ENTITYHUD_H
#include "game.h"
#include "ui/UIObject.h"
#include "ui/UIWindow.h"

class Entity;
class UIBar;
class UIText;

class EntityHUD : public UIWindow {
public:
    EntityHUD();
    virtual ~EntityHUD();
    
    Entity *entity;
    UIBar *healthBar, *staminaBar, *manaBar, *divineBar;
    
    void setEntity(Entity *en);
    void prepare();
    virtual bool draw();
private:

};

#endif /* ENTITYHUD_H */

