/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DebugHUD.cpp
 * Author: Galen Swain
 * 
 * Created on June 7, 2016, 3:32 PM
 */

#include "DebugHUD.h"
#include "ui/UIText.h"
#include "Camera.h"
#include "lib/Application.h"
#include "lib/System.h"
#include "ui/UIWindow.h"

DebugHUD::DebugHUD() {
  this->setPassthrough(true);
  fpsText = new UIText();
  posText = new UIText();
  fpsText->setParent(this); posText->setParent(this);
  fpsText->setAlign(UIText::Align::Left,  UIText::Align::Bottom);
  posText->setAlign(UIText::Align::Right, UIText::Align::Bottom);
  
  sysText = new UIText(); sysText->setParent(this);
  sysText->setAlign(UIText::Align::Left, UIText::Align::Top);
  avgDeltaTime = 60.f;
  this->activate();
}

DebugHUD::~DebugHUD() {
}

bool DebugHUD::draw() {
  std::stringstream ss;
  avgDeltaTime = lerp(avgDeltaTime, _deltaTime, 0.5f);
  ss << "FPS: " << round(1.f/avgDeltaTime * 10.f)/10.f;
  fpsText->setText(ss.str());
  
  ss.str("");
  V3 cp = activeCamera == nullptr? V3(0.f) : activeCamera->transform.position;
  ss << "CV3( " << cp.x << ", " << cp.y << ", " << cp.z << ")";
  posText->setText(ss.str());
  
  ss.str("");
  ss << "MEM: " << int(round((System::GetMemoryUsage() / (1024))/1024.0)) << "MB";
  sysText->setText(ss.str());
  
  UIObject::draw();
  return true;
}