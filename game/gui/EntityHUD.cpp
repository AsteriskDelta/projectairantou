/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EntityHUD.cpp
 * Author: Galen Swain
 * 
 * Created on June 7, 2016, 3:25 PM
 */

#include "EntityHUD.h"
#include "ui/UIText.h"
#include "ui/UIBar.h"
#include "entity/Entity.h"
#include "entity/EntityResources.h"
#include "ui/UIWindow.h"

EntityHUD::EntityHUD() {
  this->setPassthrough(true);
  this->activate();
}

EntityHUD::~EntityHUD() {
}

void EntityHUD::setEntity(Entity *en) {
  entity = en;
}

void EntityHUD::prepare() {
  ColorRGBA healthColor   = ColorRGBA(200, 10, 20 , 200),
            staminaColor  = ColorRGBA(20, 200, 20 , 200),
            manaColor     = ColorRGBA(200, 10, 20 , 200),
            divinesColor  = ColorRGBA(128, 10, 200, 200);
  
  healthBar->setColor(healthColor);
  staminaBar->setColor(staminaColor);
  manaBar->setColor(manaColor);
  divineBar->setColor(divinesColor);
  
  healthBar->setLabelMode(UIBar::LabelMode::Full);
  staminaBar->setLabelMode(UIBar::LabelMode::Full);
  manaBar->setLabelMode(UIBar::LabelMode::Full);
  divineBar->setLabelMode(UIBar::LabelMode::Full);
}

float ersScale = 1.f/1000.f;
bool EntityHUD::draw() {
  if(entity == nullptr) return false;
  EntityResources *const ers = &entity->resources;
  
  //Set positions and size
  
  healthBar->setMaximum(ers->maximum.health*ersScale);
  healthBar->setValue(ers->current.health*ersScale);
  
  staminaBar->setMaximum(ers->maximum.stamina*ersScale);
  staminaBar->setValue(ers->current.stamina*ersScale);
  
  manaBar->setMaximum(ers->maximum.mana*ersScale);
  manaBar->setValue(ers->current.mana*ersScale);
  
  divineBar->setMaximum(ers->maximum.divines*ersScale);
  divineBar->setValue(ers->current.divines*ersScale);
  
  UIObject::draw();
  return true;
}