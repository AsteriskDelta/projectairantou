/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DebugHUD.h
 * Author: Galen Swain
 *
 * Created on June 7, 2016, 3:32 PM
 */

#ifndef DEBUGHUD_H
#define DEBUGHUD_H
#include "game.h"
#include "ui/UIObject.h"
#include "ui/UIWindow.h"

class UIText;

class DebugHUD : public UIWindow {
public:
    DebugHUD();
    virtual ~DebugHUD();
    
    virtual bool draw();
private:
    UIText *fpsText, *posText, *sysText;
    float avgDeltaTime;
};

#endif /* DEBUGHUD_H */

