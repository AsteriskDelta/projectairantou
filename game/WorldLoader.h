/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldLoader.h
 * Author: Galen Swain
 *
 * Created on April 9, 2016, 2:34 PM
 */

#ifndef WORLDLOADER_H
#define WORLDLOADER_H
#include "game.h"
#include "uni/WorldObjectPtr.h"
class WorldObject;

namespace WorldLoader {
    WorldObjectPtr GetObject(WOID id);
    void RemoveObject(WorldObjectPtr &o);
    
    WorldObjectPtr LoadObject(WOID id);
    void UnloadObject(WorldObjectPtr &o);
    
    WorldObjectPtr MakeDeferredObject(WOID id);
    void DoLoadObject(WorldObjectPtr &o);
    void DoUnloadObject(WorldObjectPtr &o);
};

#endif /* WORLDLOADER_H */

