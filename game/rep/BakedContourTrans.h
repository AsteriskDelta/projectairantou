/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BakedContourTrans.h
 * Author: Galen Swain
 *
 * Created on March 17, 2016, 2:10 PM
 */

#ifndef BAKEDCONTOURTRANS_H
#define BAKEDCONTOURTRANS_H

#include "game.h"
#include "BakedContour.h"
#include "uni/WorldObject.h"

#include "rep/BakedContour.h"
#include "rep/IdealContour.h"
#include "rep/ContourCache.h"
#include "comp/Workplane.h"
#include <list>

class ParticleSystem;

struct BCTPoint {
    V3 from, to;
    V3 hermiteFrom, hermiteTo;
    Uint8 smoothFrom, smoothTo;
    float lin, quad;
    BakedContourVert *lower, *upper;
    unsigned int id;
    V2 hermiteIntersect;
    unsigned short l, r, self;
    bool disabled, holy, preserve, matched;//The stupid flag here decides if the point can be altered/deleted
    Uint8 ccIdx;
    
    struct VT {
        float t;
        unsigned int vid;
    };
    std::list<VT> vts;
    
    inline V3 getPos(float t) {
        return from*(1.f-t)+to*t;
    }
    inline V3 getHermite(float t) {
        return hermiteFrom*(1.f-t)+hermiteTo*t;
    }
    inline Uint8 getSmoothness(float t) {
        return round(smoothFrom * (1.f-t) + smoothTo*t);
    }
    
    inline void addVertID(float t, unsigned int vid) {
        vts.push_back(VT{t, vid});
    }
    
    inline unsigned int getVertID(float t) {
        auto prev = vts.begin();
        for(auto it = vts.begin(); it != vts.end(); ++it) {
            if(it->t >= t) {
                //float dist = (it->t - prev->t);
                if(prev->t >= it->t) return it->vid;
                else if(t - prev->t < it->t - t) return prev->vid;
                else return it->vid;
            }
            prev = it;
        }
        return prev->vid;
    }
};

class BakedContourTrans {
public:
    BakedContourTrans();
    virtual ~BakedContourTrans();
    
    BakedContour* (contours)[2];
    std::vector <BCTPoint> points;
    bool constructBottom, constructTop;
    
    BCTPoint* getFirstPoint(V2 p, float t);
    unsigned int getFirstPointID(V2 p, float t);
    
    BCTPoint* getClosestPoint(V3 p, float t);
    
    void capWith(float epsilon, bool isUp);
    
    void calculate(BakedContour *a, BakedContour *b, Workplane planeA, Workplane planeB, float epsilon = 0.08f);
    void stitchWith(float epsilon, BakedContourTrans &o);
    void shareWith(float epsilon, BakedContourTrans &o);
    void construct(WorldObject *const wObj, float epsilon, WMMeshType* baseMesh, WMMeshType *const dbg, bool invertFaces = false, ParticleSystem *emitSys = NULL);
private:
    void makeEmits(unsigned int a, unsigned int b, unsigned int c, WMMeshType *mesh, ParticleSystem *sys);
};

#endif /* BAKEDCONTOURTRANS_H */

