/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IdealSlice.cpp
 * Author: Galen Swain
 * 
 * Created on May 11, 2016, 3:26 PM
 */

#include "IdealSlice.h"
#include "IdealContour.h"
#include "Perturbation.h"
#include "field/Subfield.h"
#include "field/WorldFields.h"
#include "phys/PhysicalProperties.h"
#include "ai/AICritic.h"
#include <algorithm>

thread_local IdealSlice* IdealSlice::Correlate::parent = nullptr;

IdealSlice::IdealSlice() : s(nullptr), e(nullptr), path(nullptr), subfield(nullptr), sZ(0.f), eZ(0.f), 
        contourRatio(0.f), dialationRatio(0.f), compounds(std::list<CompoundWeight>()),
        cRatio(false),dRatio(false), cList(false), isBottom(false), isTop(false),
        volume(0.f), bounds() {
}

IdealSlice::~IdealSlice() {
}

void IdealSlice::calculateContourRatio() {
  if(cRatio) return;
  contourRatio = 0.5f;
  cRatio = true;
}
void IdealSlice::calculateDialationRatio() {
  if(dRatio) return;
  
  dialationRatio = 1.f;
  dRatio = true;
}

void IdealSlice::calculateCompounds() {//Ratio of compounds over volume
  if(cList) return;
  compounds = subfield->getCompounds(this);
  cList = true;
}

std::vector<IdealSlice::Correlate> IdealSlice::correlateLayer(bool rev) {
  Correlate::setParent(this);
  std::vector<Correlate> ret;
  
  IdealContour *c = rev? e : s, *o = rev? s : e;
  unsigned int oto = rev? 0 : 2, cto = rev? 2 : 0;//Time index offsets

  //For each feature on the lower contour, add to correlate list and search for direct correlations
  ret.resize(c->size());
  std::cout << "building for "<<(rev?"E":"S")<<" " << c->size() << "\n";
  for (unsigned int i = 0; i < c->size(); i++) {
    std::cout << "ft " << i << ":::\n";
    const IdealFeature * const feature = c->getFeature(i);
    const IdealFeature * const other = o->getBestCorrelate(feature, c);

    Correlate * const correlate = &(ret[i]);
    correlate->tData[cto+0] = feature->startT();
    correlate->tData[cto+1] = feature->endT();

    if (other != nullptr) {//Found correlate,
      correlate->tData[oto+0] = other->startT();
      correlate->tData[oto+1] = other->endT();
      std::cout << "chose feature " << o->getFeatureID((correlate->tData[oto+0] + correlate->tData[oto+1]) / 2.f) << "\n";
    }
    
    //Calculate jump table for current layer
    correlate->tJump[cto+0] = c->getFeatureID(c->getPrevFeature(i));//ljump
    correlate->tJump[cto+1] = c->getFeatureID(c->getNextFeature(i));//rjump
  }
  
  /*
  if(rev) {
    //For each feature on the lower contour, add to correlate list and search for direct correlations
    ret.resize(e->size());
    std::cout << "building for E " << e->size() << "\n";
    for(unsigned int i = 0; i < e->size(); i++) {
      std::cout << "ft " << i << ":::\n";
      const IdealFeature *const feature = e->getFeature(i);
      const IdealFeature *const other = s->getBestCorrelate(feature, e);

      Correlate *const correlate = &(ret[i]);
      correlate->etMin = feature->startT();
      correlate->etMax = feature->endT();

      if(other != nullptr) {//Found correlate,
        correlate->stMin = other->startT();
        correlate->stMax = other->endT();
        std::cout << "chose feature " << s->getFeatureID((correlate->stMin + correlate->stMax)/2.f) << "\n";
      }
    }
  } else {
    //For each feature on the lower contour, add to correlate list and search for direct correlations
    ret.resize(s->size());
    std::cout << "building for S " << s->size() << "\n";
    for(unsigned int i = 0; i < s->size(); i++) {
      std::cout << "ft " << i << ":::\n";
      IdealFeature *const feature = s->getFeatureNC(i);
      IdealFeature *const other = e->getBestCorrelate(feature, s);

      Correlate *const correlate = &(ret[i]);
      correlate->stMin = feature->startT();
      correlate->stMax = feature->endT();

      if(other != nullptr) {//Found correlate,
        correlate->etMin = other->startT();
        correlate->etMax = other->endT();
        std::cout << "chose feature " << e->getFeatureID((correlate->etMin + correlate->etMax)/2.f) << "\n";
      }
    }
  }*/
  
  return ret;
}

void IdealSlice::expandCorrelates(std::vector<IdealSlice::Correlate> &correlateList, bool rev) {
  const unsigned int otf = rev? 0 : 2;//Offset to be mirrored
  const unsigned int ctf = rev? 2 : 0;
  
  //Assign unmatched timesteps
  for(IdealSlice::Correlate &c : correlateList) {
    if(c.isMapped(rev)) c.original = true;
    else c.original = false;
  }
  for(IdealSlice::Correlate &c : correlateList) {
    if(c.original) continue;
    float cTimeMid = c.getMidTime(rev);
    
    //Find the closest original element
    auto it = std::min_element(correlateList.begin(), correlateList.end(), [&](const IdealSlice::Correlate &a, const IdealSlice::Correlate &b) {
      const float aTime = a.getMidTime(rev), bTime = b.getMidTime(rev);
      return fabs(cTimeMid-aTime) < fabs(cTimeMid-bTime);
    });
    
    //Mirror its element mapping
    if(it == correlateList.end()) {
      std::cerr << "No correlate mapping found!!!\n";
      continue;
    }
    c.tData[otf+0] = it->tData[otf+0];
    c.tData[otf+1] = it->tData[otf+1];
  }
  
  //Equalize length of correlateList coding for same feature
  std::cout << "Searching for overlapping correlateList...\n";
  for(auto it = correlateList.begin(); it != correlateList.end(); ++it) {
    const float& cEndTime = it->tData[otf+1];
    float totalLength = 0.f; unsigned int remapCount = 0;
    
    for(auto ot = it; ot != correlateList.end() ; ++ot) {
      if(it->overlaps(!rev, *ot)) {
        totalLength += ot->getLength(rev);//Get length of original feature
        remapCount++;
      }
    }
    if(remapCount == 1) continue;//Only overlaps with itself, we can safely skip
    
    const float otherStart = it->tData[otf+0], otherEnd = it->tData[otf+1];
    float nextStart = otherStart;
    //With total length, remap all- store a copy of the matching correlate so it continues to match after updating
    std::cout << "remapping " << otherStart << " -> " << otherEnd << "\n";
    auto origCorrelate = *it;
    for(auto ot = it; ot != correlateList.end() ; ++ot) {
      if(origCorrelate.overlaps(!rev, *ot)) {
        const float targetStart = nextStart, targetEnd = nextStart + (ot->getLength(rev) / totalLength) * (otherEnd - otherStart);
        std::cout << "\tset to " << targetStart << " -> " << targetEnd << "\n";
        ot->tData[otf+0] = targetStart;
        ot->tData[otf+1] = targetEnd;
        nextStart = targetEnd;
      }
    }
  }
}

void IdealSlice::printCorrelates(const std::vector<IdealSlice::Correlate> &c, Uint8 revMode) const {
  std::cout << "Correlate list on S 0.0 -> " << s->length() << ", E 0.0 -> " << e->length() << "\n";
  float lastS = -1.f, lastE = -1.f;
  for(unsigned int i = 0; i < c.size(); i++) {
    const Correlate *const cor = &c[i];
    if(revMode != 1 && lastS != -1.f && lastS != cor->stMin) std::cout << "stMin MISMATCH with lastS=" << lastS << "\n";
    if(revMode != 0 && lastE != -1.f && lastE != cor->etMin) std::cout << "etMin MISMATCH with lastE=" << lastE << "\n";
    
    std::cout << cor->stMin << " : " << cor->etMin << "\t->\t";
    std::cout << cor->stMax << " : " << cor->etMax << "\t\t";
    
    std::cout << "S" << s->getFeatureID((cor->stMin+cor->stMax)/2.f) << " -> E" << e->getFeatureID((cor->etMin+cor->etMax)/2.f) << "\n";
    
    lastS = cor->stMax; lastE = cor->etMax;
  }
}

std::vector<IdealSlice::Correlate> IdealSlice::traverseCorrelates(const IdealSlice::Correlate &c, const std::vector<IdealSlice::Correlate> &others) {
  std::vector<Correlate> ret;
  //Find all correlates that span c
  float leTime = c.etMin, reTime = c.etMax;
  for(const Correlate &o : others) {
    if(o.etMin >= leTime && o.etMax <= reTime) ret.push_back(o);
  }
  if(ret.size() < 2) {
    ret.clear();
    ret.push_back(c);
  }
  return ret;
}

void IdealSlice::buildCorrelates(const std::vector<IdealSlice::Correlate> &sc, const std::vector<IdealSlice::Correlate> &ec) {
  correlates.clear();
  //Base upon lower layer
  for(const IdealSlice::Correlate &sCorrelate : sc) {
    const std::vector<IdealSlice::Correlate> traverses = this->traverseCorrelates(sCorrelate, ec);
    for(const IdealSlice::Correlate &correlate : traverses) correlates.push_back(correlate);
  }
  
  //For all in ec not mapped by sc:
  //TODO
  
  //Order and sort
  for(IdealSlice::Correlate &correlate : correlates) {
    correlate.tMin = correlate.stMin;
    correlate.tMax = correlate.stMax;
  }
  std::sort(correlates.begin(), correlates.end());
}

void IdealSlice::calculateCorrelates() {
  auto sCorrelates = this->correlateLayer(false);
  auto eCorrelates = this->correlateLayer(true);
  
  //Fill in unassigned correlates by sibling preference
  expandCorrelates(sCorrelates, false);
  expandCorrelates(eCorrelates, true);
  
  printCorrelates(sCorrelates, 0);
  printCorrelates(eCorrelates, 1);
  
  //Merge correlate list
  this->buildCorrelates(sCorrelates, eCorrelates);
  
  //Debug print correlate list
  printCorrelates(correlates);
}

float IdealSlice::calculateVolume() {
  if(volume != 0.f) return volume;
  this->calculateContourRatio();
  this->calculateDialationRatio();
  
  const float sContrib = path->getLength(sZ, eZ, s->centroid) * s->getIntegral() * contourRatio;
  const float eContrib = path->getLength(sZ, eZ, e->centroid) * e->getIntegral() * (1.f-contourRatio);
  
  volume = (sContrib+eContrib);
  
  //Calculate hull
  bounds.clear();
  bounds.encompassWorkplane(s->bounds, path->getWorkplane(sZ));
  bounds.encompassWorkplane(e->bounds, path->getWorkplane(eZ));
  
  return volume;
}

float IdealSlice::calculateProp(unsigned short propID) {
  this->calculateVolume();
  this->calculateCompounds();
  
  float ret = 0.f;
  for(CompoundWeight& w : compounds) {
    ret += w.str * w.compound->getProp(propID);
  }
  
  return ret*volume;
}

V3 IdealSlice::calculateCO(unsigned short propID) { _prsguard();
  V3 ret = subfield->getCO(this, propID);
  std::cout << "got CO=" << glm::to_string(ret) << "\n";
  //path->evaluate(ret);
  return ret;
}

Physical IdealSlice::calculatePhysical() { _prsguard();
  Physical ret;
  ret.s.volume = this->calculateVolume();
  ret.s.mass = this->calculateProp(Physp::Mass);
  ret.s.com = this->calculateCO(Physp::Mass)*float(ret.s.mass);
  
  //Surface calculate for slice goes here
  ret.surface = this->calculateSurface(ret.s.com/float(ret.s.mass));
  
  return ret;
}

Surface IdealSlice::calculateSurface(V3 co) { _prsguard();
  Surface ret;
  //SA calculation and roughness
  for(Uint8 cid = 0; cid <= 1; cid++) {
    IdealContour *c; float ratio, zCoord;
    if(cid == 0) {
      c = s; ratio = contourRatio;
      zCoord = sZ;
    } else {
      c = e; ratio = (1.f - contourRatio);
      zCoord = eZ;
    }
    
    for(auto it = c->features.begin(); it != c->features.end(); ++it) {
      //Resolve normal to object space
      V3 featCenter = it->getCentroid(), featHermite = it->getHermite();
      float featLen = it->distance();
      float featHeight = path->getLength(sZ, eZ, featCenter);
      float area = featLen*featHeight;
      ret.totalArea += area*ratio;
      
      //Resolve feature normal, within actual space
      V3 nStart = featCenter+V3(0.f, 0.f, zCoord); path->evaluate(nStart);
      V3 nEnd = featHermite+featCenter+V3(0.f, 0.f, zCoord); path->evaluate(nEnd);
      V3 normal = glm::normalize((nEnd - nStart));
      //V3 toFeature = glm::normalize(nStart - co);
      
      //Add normal and area to surface
      float areaStr = area*ratio * 1.f / 2.f;//glm::smoothstep(0.0001f, 0.0002f, clamp(glm::dot(normal, toFeature), 0.f, 1.f));
      float areaRes = it->getResistance();
      ret.area.add(normal, areaStr, 1.f);
      ret.resistance.add(normal, areaStr * areaRes, 1.f);
    }
  }
  
  V3 forward = path->getDelta(eZ), backward = -path->getDelta(sZ);
  float dArea = e->getIntegral() - s->getIntegral();
  ret.area.add((dArea >= 0.f)? forward : backward, fabs(dArea), 1.f);
  ret.resistance.add((dArea >= 0.f)? forward : backward, fabs(dArea) * path->getResistance((eZ + sZ)/2.f), 1.f);
  
  if(isBottom) {
    ret.area.add(backward, s->getIntegral(), 1.f);
    ret.resistance.add(backward, s->getIntegral(), 1.f);
    ret.totalArea += s->getIntegral();
  }
  if(isTop) {
    ret.area.add(forward, e->getIntegral(), 1.f);
    ret.resistance.add(forward, e->getIntegral(), 1.f);
    ret.totalArea += e->getIntegral();
  }
  
  //Cavitation calculation
  //ret.calculateCavitation();
  
  return ret;
}

V3 IdealSlice::evalCentroid(float t) {
  V3 ret = V3(0.f); V3 tmp = s->centroid+V3(0.f, 0.f, sZ);
  //std::cout << "start cen: " << glm::to_string(s->centroid) << "\n";
  path->evaluate(tmp );
  ret += tmp*t;
  tmp = e->centroid+V3(0.f, 0.f, eZ);
  path->evaluate(tmp);
  //std::cout << "end cen: " << glm::to_string(e->centroid) << "\n";;
  ret += tmp * (1.f-t);
  //std::cout << "ret cen: " << glm::to_string(ret) << "\n";;
  return ret;
}

WObjectVert IdealSlice::evaluateVert(float sTime, float eTime, float seLerp, float z) {
  WObjectVert sVert, eVert;
  
  sVert.pos.z = eVert.pos.z = z;
  return sVert.lerpTo(eVert, seLerp);
}

void IdealSlice::constructSub(float tStart, float tEnd, float epsilon, WMMeshType *const meshBuff, Flags flags, WMMeshType *const dbgBuff) {
  //Weights: Starting (s and e contours), Ending (s and e contours)
  float sliceWeights[4] = {(tStart - sZ) / (eZ - sZ), 0.f, 
                           (tEnd   - sZ) / (eZ - sZ), 0.f};
  sliceWeights[1] = 1.f - sliceWeights[0]; sliceWeights[3] = 1.f - sliceWeights[2];
  
  WObjectVert calcVerts[4];
  for(const Correlate &correlate : correlates) {
    //Each timestep within the correlate
    float fTime = 0.f;
    while(fTime <= 1.f) {
      float fTimeStart = fTime;
      float fTimeEnd = 1.f;
      
      float effTimes[4] = {
        correlate.stMin + correlate.getLength(false)*fTimeStart, correlate.stMin + correlate.getLength(false)*fTimeEnd,
        correlate.etMin + correlate.getLength(true )*fTimeStart, correlate.etMin + correlate.getLength(true )*fTimeEnd
      };//Lower start, end : Upper start, end
      float effLerp = sliceWeights[0] / (sliceWeights[0]+sliceWeights[2]);
      
      //We have all four t-values and lerp factor for each corner of a quad
      //In order: ll, lr, ur, ul
      calcVerts[0] = this->evaluateVert(effTimes[0], effTimes[2], effLerp,     tStart);
      calcVerts[1] = this->evaluateVert(effTimes[1], effTimes[2], effLerp,     tStart);
      calcVerts[2] = this->evaluateVert(effTimes[1], effTimes[3], 1.f-effLerp, tEnd);
      calcVerts[3] = this->evaluateVert(effTimes[0], effTimes[3], 1.f-effLerp, tEnd);
      
      //Flip quad if needed
      
      //Add verts and tris
      
      fTime = fTimeEnd;
      if(fTime == 1.f) break;
    }
  }
}

void IdealSlice::construct(float epsilon, WMMeshType *const meshBuff, Flags flags, WMMeshType *const dbgBuff) {
  //Break into subslices by epsilon
  float t = sZ, tInc = 1.f;
  while(t <= eZ) {
    float tStart = t;
    float tEnd = clamp(t + tInc, sZ, eZ);
    
    Flags activeFlags = 0x0;
    if(tStart == sZ && (flags & CapLower) != 0x0) activeFlags |= CapLower;
    if(tEnd == eZ && (flags & CapUpper) != 0x0) activeFlags |= CapUpper;
    
    this->constructSub(tStart, tEnd, epsilon, meshBuff, activeFlags, dbgBuff);
    
    t = tEnd;
    if(t == eZ) break;
  }
}