/* 
 * File:   IdealContour.h
 * Author: Galen Swain
 *
 * Created on May 15, 2015, 2:02 PM
 */

#ifndef IDEALCONTOUR_H
#define	IDEALCONTOUR_H
#include "game/game.h"
#include "BakedContour.h"
#include "comp/IntersectionHull.h"

class IdealContour;
class EditContour;

class IdealFeature {
public:
    enum {
        Line    = 0,
        Arc     = 1,
        Bezier  = 2,
        Rect    = 3,
        
        Dummy
    };
    union {
    struct {
        bool real  : 1;
        Uint8 type : 7;
        Uint8 level : 8;
        bool lConnect : 1;
        bool rConnect : 1;
        bool invHermite : 1;
        Uint8 padd : 5;
    };
    Uint32 tagRaw;
    };
    V3 start, mid;
    union {
        V3 end;
        V3 axis;
    };
    float radius, alpha, beta, phi;
    float smoothness;
    float theta, para;
    Uint16 jumpID;
    
    inline IdealFeature() : real(false), type(Dummy), level(0), jumpID(0xFFFF) {
      radius = alpha = beta = phi = smoothness = theta = para = 0.f;
      start = mid = end = V3(0.f);
      tagRaw = 0x0;
    };
    IdealFeature(Uint8 nt, bool r = false);
    IdealFeature(const IdealFeature& orig);
    ~IdealFeature();
    
    IdealFeature& operator=(const IdealFeature& o);
    
    inline V3 getAxis() const {
        return V3(0.f, 0.f, 1.f);
    }
    inline float getSmoothness() const {
        return smoothness;
    }
    
    //"To" functions denote spatial position changes, "between" changes in projecting vectors (normal)
    void closestPoints(const IdealFeature *const o, V3 *const spt, V3 *const opt) const;
    float distanceTo(const IdealFeature *const o) const;
    float angleTo(const IdealFeature *const o) const;
    float angleBetween(const IdealFeature *const o) const;
    V3 vectorTo(const IdealFeature *const o) const;
    
    bool couldIntersect(const IdealFeature *const o) const;
    
    V3 crossing(const V3& a, const V3& b) const;
    V3 crossing(const IdealFeature *const o) const;
    inline bool crosses(const IdealFeature *const o) const {
        return this->crossing(o) != V3_NULL;
    }
    inline bool crosses(const V3 &a, const V3 &b) const {
        return this->crossing(a,b) != V3_NULL;
    }
    
    //Returns given hermite (at ideal stage, normal) of the given face at param time t
    V3 getHermite(float t = 0.5f) const;
    inline V3 normal() const {
        return this->getHermite();
    }
    
    void setHermite(const V3& herm);
    //Interpolate feature to other given feature
    IdealFeature interpolateTo(const IdealFeature& o) const;
    //Gets bounds relative to workplane, or given matrix
    std::vector<V3> getExtremes(Mat4* mat = NULL) const;
    
    void resolve(BakedContour *const contour, float epsilon, const IdealFeature *const prev = NULL, const IdealFeature *const next = NULL) const;
    float distance() const;
    inline float length() const { return this->distance();};
    
    float getNextT(float t, float epsilon) const;
    
    inline float startT() const{
        return para - this->distance();
    }
    inline float endT() const{
        return para;
    }
    
    float crossSum(const IdealFeature *const p, const IdealFeature *const n) const;
    float curveSum(const IdealFeature *const p, const IdealFeature *const n) const;
    V3 crossCentroid(const IdealFeature *const p, const IdealFeature *const n, float cross, float curve) const;
    
    BakedContourVert evaluateVert(float t, const IdealFeature *const p, const IdealFeature *const n) const;
    
    float getDeltaStep(float t, float epsilon) const;
    
    V3 evaluate(const float t) const;
    void evaluate(V3 &v, const float t) const;
    
    float getResistance() const;
    
    inline V3 getCentroid() const {
        return this->evaluate(0.5f);
    }
    
    inline V3 delta() const {
        return this->evaluate(1.f) - this->evaluate(0.f);
    }
    
    float intersectTime(V3 vStart, V3 vEnd) const;
    V3 intersect(V3 vStart, V3 vEnd) const;
    int intersectSign(V3 vStart, V3 vEnd) const;
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(start),
                CEREAL_NVP(mid),
                CEREAL_NVP(end),
                CEREAL_NVP(radius),
                CEREAL_NVP(alpha),
                CEREAL_NVP(beta),
                CEREAL_NVP(phi),
                CEREAL_NVP(smoothness),
                CEREAL_NVP(theta),
                CEREAL_NVP(para),
                CEREAL_NVP(jumpID),\
                CEREAL_NVP(tagRaw)
                );
    }
protected:
    
};

//CEREAL_REGISTER_TYPE(IdealFeature);

class IdealSlice;
//Represents perfect contour on a given plane
class IdealContour {
public:
    friend class IdealSlice;
    friend class EditContour;
    
    IdealContour();
    IdealContour(const IdealContour& orig);
    virtual ~IdealContour();
    
    struct {
        bool prefab : 1;
        bool dirty : 1;
    };
    struct {
        float linear, quadratic;
    };
    IntersectionHull bounds;
    
    void addFeature(const IdealFeature &feature);
    
    //Preprocess distance to evaluate at t [z]]
    void parametricize(bool isPath = false);
    void evaluate(V3 &v) const;
    V3 evaluate(float t) const;
    V3 getDelta(float t) const;
    
    Workplane getWorkplane(float t);
    
    float deltaMag(const float t, const float diff) const;
    
    float getDeltaStep(float z, float epsilon) const;
    float getFeatureTime(unsigned int offset) const;
    inline float getMaxExtrusion() const {
        if(features.size() == 0) return 0.f;
        return (features.end()-1)->para;
    }
    inline float length() const {
        return this->getMaxExtrusion();
    }
    
    IdealFeature* getBestCorrelate(const IdealFeature *const o, const IdealContour *const otherContour = nullptr);
    
    const IdealFeature* getFeature(float t) const;
    const IdealFeature* getFeatureSibling(float t, int dir) const;
    inline const IdealFeature* getNextFeature(float t) const {
        return this->getFeatureSibling(t, 1);
    }
    inline const IdealFeature* getPrevFeature(float t) const {
        return this->getFeatureSibling(t, -1);
    }
    
    bool crosses(const V3& f, const V3& t) const;
    
    const IdealFeature* getFeature(unsigned int id) const;
    IdealFeature* getFeatureNC(unsigned int t);
    const IdealFeature* getFeatureSibling(unsigned int id, int dir) const;
    inline const IdealFeature* getNextFeature(unsigned int id) const {
        return this->getFeatureSibling(id, 1);
    }
    inline const IdealFeature* getPrevFeature(unsigned int id) const {
        return this->getFeatureSibling(id, -1);
    }
    
    BakedContourWeights getLevelWeights(float z) const;
    
    BakedContour bake(float epsilon) const;
    inline operator BakedContour() { return this->bake(0.1f); };
    
    float integrate();
    //Returns the length of the GEOMETRIC CENTROID travel
    float getLength(float startTime, float endTime, V3 pt) const;
    
    float area, variance, radius, weight;
    float getIntegral() const {
        return area;
    }
    
    float getResistance(float t) const;
    
    float getVariance();
    
    bool isOutwardConvex() const;
    bool contains(V3 pt) const;
    
    V3 centroid;
private:
    std::vector<IdealFeature> features;
public:
    unsigned short getFeatureID(float t) const;
    unsigned short getFeatureID(const IdealFeature *const fptr) const;
    unsigned int size() const {
        return features.size();
    }
    
    template<class Archive>
    void save(Archive & archive) const {
        archive(CEREAL_NVP(features)); 
    }

    template<class Archive>
    void load(Archive & archive) {
        archive(CEREAL_NVP(features)); 
    }
};


CEREAL_REGISTER_TYPE(IdealContour);

#endif	/* IDEALCONTOUR_H */

