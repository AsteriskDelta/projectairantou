/* 
 * File:   WorldMaterial.h
 * Author: Galen Swain
 *
 * Created on May 23, 2015, 1:23 PM
 */

#ifndef WORLDMATERIAL_H
#define	WORLDMATERIAL_H
#include "../game.h"

struct WMatID {
    Uint16 id;
    
};

struct WMatParam {
    Uint8 roughness : 4;
};

class WorldMaterial {
public:
    WorldMaterial();
    WorldMaterial(const WorldMaterial& orig);
    virtual ~WorldMaterial();
private:

};

#endif	/* WORLDMATERIAL_H */

