/* 
 * File:   BakedContour.cpp
 * Author: Galen Swain
 * 
 * Created on May 15, 2015, 2:03 PM
 */

#include "BakedContour.h"
#include <iostream>

BakedContourVert BakedContour::dummyVert = BakedContourVert();

BakedContour BakedContour::dummy = BakedContour();

BakedContour::BakedContour() {
  verts = std::vector<BakedContourVert>();
  prevID = CVNULL;
  
}

BakedContour::BakedContour(const BakedContour& orig) {
  verts = std::vector<BakedContourVert>(orig.verts);
  center = orig.center;
  planeNormal = orig.planeNormal;
  prevID = orig.prevID;
}

BakedContour::~BakedContour() {
}

BakedContourVert* BakedContour::addVert(const BakedContourVert &v) {
  verts.push_back(v);
  return &(*verts.rbegin());
}

BakedContourVert& BakedContour::getNearest(const V3 &pos) {
  float bestDist = 100000.f; Uint16 bestID = 0xFFFF;
  for(Uint16 i = 0; i < verts.size(); i++) {
    const float dist = glm::distance2(pos, verts[i].pos);
    if(dist < bestDist) {
      bestDist = dist; bestID = i;
    }
  }
  if(bestID == 0xFFFF) {
    return dummyVert;
  } else return verts.at(bestID);
}

const BakedContourVert& BakedContour::getNearestXY(const V3 &pos) const {
  float bestDist = 100000.f; Uint16 bestID = 0xFFFF;
  for(Uint16 i = 0; i < verts.size(); i++) {
    const float dist = glm::distance2(V2(pos), V2(verts[i].pos));
    if(dist < bestDist) {
      bestDist = dist; bestID = i;
    }
  }
  if(bestID == 0xFFFF) {
    return dummyVert;
  } else return verts.at(bestID);
}

BakedContourVert& BakedContour::getNearestXYNC(const V3 &pos) {
  float bestDist = 100000.f; Uint16 bestID = 0xFFFF;
  for(Uint16 i = 0; i < verts.size(); i++) {
    const float dist = glm::distance2(V2(pos), V2(verts[i].pos));
    if(dist < bestDist) {
      bestDist = dist; bestID = i;
    }
  }
  if(bestID == 0xFFFF) {
    return dummyVert;
  } else return verts.at(bestID);
}

BakedContourVert& BakedContour::getNearestXYNCNorm(const V3 &pos, const V3 &norm) {
  float bestDist = 100000.f; Uint16 bestID = 0xFFFF;
  for(Uint16 i = 0; i < verts.size(); i++) {
    const float dist = glm::distance2(V2(pos), V2(verts[i].pos));
    if(dist < bestDist && (pos == verts[i].pos || glm::dot(norm, verts[i].normal) > FACE_DOT_THRESH)) {
      bestDist = dist; bestID = i;
    }
  }
  if(bestID == 0xFFFF) {
    return dummyVert;
  } else return verts.at(bestID);
}

BakedContourVert& BakedContour::getNearestXYNCPNorm(const V3 &pos, const V3 &norm) {
  float bestDist = 100000.f; Uint16 bestID = 0xFFFF;
  for(Uint16 i = 0; i < verts.size(); i++) {
    const float dist = glm::distance2(V2(pos), V2(verts[i].pertPos));
    if(dist < bestDist && (pos == verts[i].pertPos || glm::dot(norm, verts[i].normal) > FACE_DOT_THRESH)) {
      bestDist = dist; bestID = i;
    }
  }
  if(bestID == 0xFFFF) {
    return dummyVert;
  } else return verts.at(bestID);
}

BakedContour BakedContour::lerpTo(BakedContour &o, float t, float epsilon) {
  BakedContour ret = *this;
  for (unsigned int v = 0; v < ret.verts.size(); v++) {
    BakedContourVert &vert = ret.verts.at(v);
    const BakedContourVert &oVert = o.getNearest(vert.pos);
    
    vert.lerpTo(oVert, t);
  }
  
  ret.simplify(epsilon);
  
  ret.center = glm::mix(center, o.center, t);
  ret.planeNormal = glm::normalize(glm::mix(planeNormal, o.planeNormal, t));
  
  return ret;
}

void BakedContour::clearFlags() {
  for(auto v = verts.begin(); v != verts.end(); ++v) {
    v->linked = false;
    v->pulled = false;
    v->proc = false;
    v->arc = false;
    v->lRend = v->rRend = false;
  }
}

void BakedContour::splitFilter(float epsilon) {
  _unused(epsilon);
  const unsigned int vMax = verts.size();
  for(unsigned int v = 0; v < vMax; v++) {
    auto vert = verts.begin()+v;
    for(Uint8 sib = 1; sib <= 1; sib++) {//Only evaluate for rightmost sibling
      if(vert->siblingID[sib] != CVNULL) {
        auto vSib = verts.begin() + vert->siblingID[sib];
        if(glm::dot(vert->normal, vSib->normal) < FACE_DOT_THRESH) {//Split the verts apart for proper node rendering
          verts.push_back(*vert);
          //Refresh iterators
          vert = verts.begin()+v;
          vSib = verts.begin() + vert->siblingID[sib];
          auto vNew = verts.rbegin();
          
          vert->siblingID[sib] = this->getID(&(*vNew));
          vNew->siblingID[(sib+1)%2] = this->getID(&(*vert));
          vNew->siblingID[sib] = this->getID(&(*vSib));
          vSib->siblingID[(sib+1)%2] = this->getID(&(*vNew));
          vNew->normal = vSib->normal;
          if(sib == 0) {
            vNew->rConnect = false;
            vSib->lConnect = false;
          } else {
            vNew->lConnect = false;
            vert->rConnect = false;
          }
        }
      }
    }
  }
}
/*
void BakedContour::simplify(float epsilon) {
  //Combine all siblings less than epsilon apart
  clearFlags();
  epsilon = pow(epsilon, 0.8f)*1.2f;// / 1.5f;
  std::vector<BakedContourVert> newVerts;
  newVerts.reserve(verts.size());
  Uint16 newID = 0;
  for (int v = 0; v < int(verts.size()) && verts.size() >= 3; v++) {
    BakedContourVert &vert = verts.at(v); if(vert.arc) continue;
    const Uint16 lOff = vert.siblingID[0], rOff = vert.siblingID[1];
    
    //We force the algorithm to avoid successive simplifiction, due to the introduction of artifacts
    float lvDist =  vert.distanceToXY(verts.at(lOff)), rvDist = vert.distanceToXY(verts.at(rOff));
    bool lSibling = vert.lConnect && lOff != CVNULL && lvDist < epsilon && lvDist > 0.001f,
         rSibling = vert.rConnect && rOff != CVNULL && rvDist < epsilon && rvDist > 0.001f;
    
    bool canSimplify = lOff != CVNULL && rOff != CVNULL;
    if(lOff != CVNULL) canSimplify &= glm::dot(vert.normal, verts.at(lOff).normal) > FACE_DOT_SIMPLIFY && verts.at(lOff).lConnect;
    if(rOff != CVNULL) canSimplify &= glm::dot(vert.normal, verts.at(rOff).normal) > FACE_DOT_SIMPLIFY && verts.at(rOff).rConnect;
    
    if(lSibling && rSibling && canSimplify && false) {
      verts.at(lOff).arc = verts.at(rOff).arc = true;//Mark to be skipped
      verts.at(lOff).id = verts.at(rOff).id = newID;
      vert.siblingID[0] = verts.at(lOff).siblingID[0];
      vert.siblingID[1] = verts.at(rOff).siblingID[1];
    } else if(lSibling && vert.lConnect && canSimplify&&false) {
      BakedContourVert &lVert = verts.at(lOff);
      lVert.arc = true; lVert.id = newID;
      vert.siblingID[0] = lVert.siblingID[0];
      vert.pos = lVert.pos; vert.normal = lVert.normal;
    } else if(rSibling && vert.rConnect && canSimplify&&false) {
      BakedContourVert &rVert = verts.at(rOff);
      rVert.arc = true; rVert.id = newID;
      vert.siblingID[1] = rVert.siblingID[1];
      vert.pos = rVert.pos; vert.normal = rVert.normal;
    }
    
    newVerts.push_back(vert);
    vert.id = newID;
    newID++;
  }
  
  for(unsigned int v = 0; v < newVerts.size(); v++) {
    BakedContourVert &vert = newVerts.at(v);
    if(vert.siblingID[0] != CVNULL) vert.siblingID[0] = verts.at(vert.siblingID[0]).id;
    if(vert.siblingID[1] != CVNULL) vert.siblingID[1] = verts.at(vert.siblingID[1]).id;
  }
  verts = newVerts;
  clearFlags();
}*/

void BakedContour::simplify(float epsilon) {
  //Combine all siblings less than epsilon apar
  clearFlags();
  epsilon = pow(epsilon, 0.8f)*1.3f;// / 1.5f;
  std::vector<BakedContourVert> newVerts;
  newVerts.reserve(verts.size());
  Uint16 newID = 0;
  for (int v = 0; v < int(verts.size()) && verts.size() >= 3; v++) {
    BakedContourVert &vert = verts.at(v); if(vert.arc) continue;
    const Uint16 lOff = vert.siblingID[0], rOff = vert.siblingID[1];
    
    //We force the algorithm to avoid successive simplifiction, due to the introduction of artifacts
    bool lSibling = vert.lConnect && lOff != CVNULL && vert.distanceToXY(verts.at(lOff)) < epsilon && !verts.at(lOff).arc,
         rSibling = vert.rConnect && rOff != CVNULL && vert.distanceToXY(verts.at(rOff)) < epsilon && !verts.at(rOff).arc;
   
    bool canSimplify = true;
    if(lOff != CVNULL) canSimplify = canSimplify && glm::dot(vert.normal, verts.at(lOff).normal) > FACE_DOT_SIMPLIFY;
    if(rOff != CVNULL) canSimplify = canSimplify && glm::dot(vert.normal, verts.at(rOff).normal) > FACE_DOT_SIMPLIFY;
    lSibling &= canSimplify;
    rSibling &= canSimplify;
    
    if(lSibling && rSibling) {
      verts.at(lOff).arc = verts.at(rOff).arc = true;//Mark to be skipped
      verts.at(lOff).id = verts.at(rOff).id = newID;
      vert.siblingID[0] = verts.at(lOff).siblingID[0];
      vert.siblingID[1] = verts.at(rOff).siblingID[1];
    } else if(lSibling) {
      BakedContourVert &lVert = verts.at(lOff);
      lVert.arc = true; lVert.id = newID;
      vert.siblingID[0] = lVert.siblingID[0];
      vert.pos = lVert.pos; vert.normal = lVert.normal;
    } else if(rSibling) {
      BakedContourVert &rVert = verts.at(rOff);
      ///std::cout << "simplifying "<< v << " & " << rOff<<" to " <<  rVert.siblingID[1]<<"\n";
      rVert.arc = true; rVert.id = newID;
      vert.siblingID[1] = rVert.siblingID[1];
      //vert.pos = rVert.pos; vert.normal = rVert.normal;
    }
    
    newVerts.push_back(vert);
    vert.id = newID;
    newID++;
  }
  
  for(unsigned int v = 0; v < newVerts.size(); v++) {
    BakedContourVert &vert = newVerts.at(v);
    if(vert.siblingID[0] != CVNULL) vert.siblingID[0] = verts.at(vert.siblingID[0]).id;
    if(vert.siblingID[1] != CVNULL) vert.siblingID[1] = verts.at(vert.siblingID[1]).id;
  }
  verts = newVerts;
}

inline V2 v2Intersect(V2 p1, V2 p2, V2 p3, V2 p4, float *t = NULL) {
  // Store the values for fast access and easy
  // equations-to-code conversion
  if(t != NULL) *t = 999.f;
  float x1 = p1.x, x2 = p2.x, x3 = p3.x, x4 = p4.x;
  float y1 = p1.y, y2 = p2.y, y3 = p3.y, y4 = p4.y;

  float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
  // If d is zero, there is no intersection
  if (d == 0) return V2NULL;

  // Get the x and y
  float pre = (x1*y2 - y1*x2), post = (x3*y4 - y3*x4);
  float x = ( pre * (x3 - x4) - (x1 - x2) * post ) / d;
  float y = ( pre * (y3 - y4) - (y1 - y2) * post ) / d;

  // Check if the x and y coordinates are within both lines
  if(x < min(x1, x2) || x > max(x1, x2) || x < min(x3, x4) || x > max(x3, x4) ) return V2NULL;
  if(y < min(y1, y2) || y > max(y1, y2) || y < min(y3, y4) || y > max(y3, y4) ) return V2NULL;
  const V2 inter = V2(x, y);
  
  if(t != NULL) *t = glm::distance(p1, inter) / glm::distance(p1, p2);
  // Return the point of intersection
  return inter;
}

void BakedContour::add(BakedContour &o, float epsilon) {
  //std::cout << "adding plane of " << o.verts.size() << " verts\n";
  //for(auto it = o.verts.begin(); it != o.verts.end(); ++it) {
  //  this->addVert(*it);
  //}
  this->csgOperand(o, epsilon, Operand::Additive);
}
void BakedContour::subtract(BakedContour &o, float epsilon) {
  this->csgOperand(o, epsilon, Operand::Subtractive);
}

void BakedContour::csgOperand(BakedContour &o, float epsilon, Uint8 op) {
  BakedContour *drawing = this, *secting = &o;
  if(drawing->verts.size() == 0) return;
  
  BakedContour ret;
  drawing->clearFlags(); secting->clearFlags();
  
  //unsigned int endDrawing = drawing->verts.size()-1, endSecting; 
  BakedContourVert drawFrom; bool doEscape = false;
  /* Operand Table:
   *     |  A - B | B - A | A & B | A + B | A ^ B |
   * a/T |    F   |   T   |   T   |   F   |       |
   * SPC |    T   |   T   |   T   |   T   |       |
   * AIN |    F   |   F   |   T   |   T   |       |
   * SUB |    T   |   T   |   T   |   F   |       |
   */
  const bool additiveTraverse = (op == Operand::AND);//(op == Operand::Add);
  const bool swapProcChain = true;//(op == Operand::Subtract);
  const bool additiveInvNormal = (op == Operand::Add || op == Operand::AND);
  const bool sectUnprocBlk  = (op != Operand::Add && op != Operand::AND);
  
  for(auto it = verts.begin(); it != verts.end() && !doEscape; ++it) {//We must check every vert of the drawing set, for non-connected contours
    if(it->proc) continue;//If we were the sibling of an earlier vertex, continue
    drawFrom = *it; unsigned int drawEscapeID = getID(&(*it)), sectEscapeID = CVNULL;
    //!std::cout << "dFID: " << drawEscapeID << "\n";
    unsigned int drawFromID = drawEscapeID, drawToID, drawInterFrom = 0xFFFFFFFF, drawLastID = 0;
    bool firstOuter = true, begun = false;
    Uint8 forwardSiblingID = 1;//1 for right/drawing, 0 for left/secting
    drawing = this; secting = &o;
    
    while(drawFrom.siblingID[forwardSiblingID] != CVNULL && !drawFrom.proc) {//While we have a right sibling and haven't completed without intersections
      if((drawFromID == drawEscapeID && !firstOuter)) {
        //!std::cout << "ESCAPE\n";
        doEscape = true;
        //Mark sibling chain 
        break;
      }
      BakedContourVert drawTo = drawing->verts.at(drawFrom.siblingID[forwardSiblingID]); float bestInterTime = 2.f;
      drawToID = drawFrom.siblingID[forwardSiblingID];
      
      bool wasIntersected = false; Uint8 bestForwardSiblingID = forwardSiblingID;
      //Check the intersecting set to see if we have an intersection... 
      for(unsigned int sectingID = 0; sectingID < secting->verts.size(); sectingID++) {
        //Determine possible line of intersecting
        auto sectFrom = secting->verts.begin() + (sectingID);
        const unsigned int sectToID = sectFrom->siblingID[1];
        if(!sectFrom->rConnect) continue;//No endpoint, skip
        auto sectTo = secting->verts.begin() + sectToID;
        
        float interTime = 3.f;
        V2 inter = v2Intersect(drawFrom.getV2(), drawTo.getV2(), sectFrom->getV2(), sectTo->getV2(), &interTime);
        if(glm::distance(inter, drawFrom.getV2( )) <= 0.003f) {
          //Shift the intersection by a small amount to avoid eternal collision while still using only valid contours
          V2 interDelta = drawFrom.getV2() - drawTo.getV2();
          float interDeltaLen = glm::length(interDelta);
          if(inter == drawFrom.getV2() || drawFromID == CVNULL) {
            inter = V2NULL;
            //!std::cout << "ignoring intersection at origin\n";
          } else if(interDeltaLen < 0.001f) {//Perturb by a random amount and bite the distortion, soz guyz
            std::cout << "MALFORMED CONTOUR DETECTED, ABORTING\n";
            doEscape = true;
            break;
          } else {//Just shift it over
            //!std::cout << "SHIFTING INTER:" << glm::to_string(inter) << " to ";
            inter = drawFrom.getV2() + interDelta * clamp(epsilon*epsilon, 0.004f, 0.08f);//*(invertNormal? -1.f : 1.f);
            interTime = glm::distance(drawFrom.getV2(), inter) / glm::distance(drawFrom.getV2(), drawTo.getV2());
            //!std::cout << glm::to_string(inter) << "@"<<interTime << "\n";
          }
        }
        //if(inter != V2NULL) std::cout << "inter " << interTime << " < " << bestInterTime << "\n";
        
        if(inter != V2NULL && interTime < bestInterTime) {//If we have an intersection closer to the start point
          if(!begun) {//We started drawing lines from here, so no need to revisit it
            drawEscapeID = drawToID;
          }
          
          //Set the intersections sibling flags, and denote the new direction of traversal
          V3 interNormal = glm::normalize(drawFrom.normal * (1.f-interTime) + drawTo.normal * interTime) * ((secting == this || !additiveInvNormal)? 1.f : -1.f);// * (invertNormal? -1.f : 1.f);
          V2 interNormalV2 = V2(interNormal);
          //V3 sectMinusVec = glm::normalize(V3(inter, sectFrom->pos.z) - sectFrom->pos); float sectMinusDist = glm::distance(interNormal, sectMinusVec);
          //V3 sectPlusVec = glm::normalize(V3(inter, sectTo->pos.z) - sectTo->pos); float sectPlusDist = glm::distance(interNormal, sectPlusVec);
          V2 sectDelta = glm::normalize(V2(sectFrom->pos) - V2(sectTo->pos));
          float dAngle = fabs(glm::angle(interNormalV2, sectDelta));
          if((dAngle > 90.f*DEG_TO_RAD) != additiveTraverse) {//Traverse along negative only if we are subtracting, xor with traversal state
            //!std::cout << "Left traverse (angle="<<(dAngle*RAD_TO_DEG)<<")\n";
            bestForwardSiblingID = 0;
          } else {//Traverse along positive for all additive and certain subtractive
            bestForwardSiblingID = 1;
            //!std::cout << "Right traverse (angle="<<(dAngle*RAD_TO_DEG)<<")\n";
          }
          //!std::cout << glm::to_string(drawFrom.getV2()) << "->"<<glm::to_string(drawTo.getV2()) << " and " << glm::to_string(sectFrom->getV2())<<"->"<<glm::to_string(sectTo->getV2())<<"\n";
          bestInterTime = interTime;
          drawTo.pos.x = inter.x; drawTo.pos.y = inter.y;
          
          drawTo.normal = interNormal;
          if(bestForwardSiblingID == 1) {
            sectEscapeID = sectingID;//Update to escape if we make it around sect, denote we're drawing to an intersection
            drawToID = sectingID;//Set to flag up to the previous intersection
            drawTo.siblingID[bestForwardSiblingID] = sectToID; 
            drawTo.siblingID[(bestForwardSiblingID+1)%2] = drawFromID;
          } else {
            sectEscapeID = sectToID;//Update to escape if we make it around sect, denote we're drawing to an intersection
            drawToID = sectToID;//Set to flag up to the previous intersection
            drawTo.siblingID[bestForwardSiblingID] = sectingID; 
            drawTo.siblingID[(bestForwardSiblingID+1)%2] = drawToID;
          }
          
          wasIntersected = true;
        }
      }
      
      if(begun) {//Add the proposed line, otherwise continue searching for an intersection (so we have know for sure the point is included in the final set)
        BakedContourVert *av;  //Add drawFrom
        av = ret.addVert(drawFrom);
        av->lConnect = av->rConnect = true;
        av->siblingID[0] = (ret.verts.size() == 1)? CVNULL : ret.verts.size()-2;
        av->siblingID[1] = ret.verts.size();
        if(((drawing != this) || drawFromID == CVNULL)  && !sectUnprocBlk) {//WARNING: may not be needed!!!!!!!!!!!
          av->normal *= -1.f;
          //!std::cout << "inverting normal\n";
        }
        
        //Set proc on the from vert, if required
        if(drawFromID!= CVNULL) drawing->verts[drawFromID].proc = true;
      }
      
      if(wasIntersected) {//If we were intersected, swap the active contours
        //!std::cout << "Intersection from " << glm::to_string(drawFrom.pos) << " at " << glm::to_string(drawTo.pos) << ", swapping..\n";
        if(!begun) {
          //!std::cout << "set BEGUN\n";
          begun = true;
        }
        forwardSiblingID = bestForwardSiblingID;
        
        if(drawInterFrom != 0xFFFFFFFF/* && drawing != this*/) {//Eliminate all points passed over
          unsigned int drawElimID = drawToID;
          const Uint8 sibOffset = swapProcChain? 1 : 0;
          while(drawElimID != CVNULL) {
            secting->verts[drawElimID].proc = true;
            //!std::cout << "proc'ing " << drawElimID << " to " << drawInterFrom << "\n";
            if(drawElimID == drawInterFrom) break;
                 
            drawElimID = secting->verts[drawElimID].siblingID[(forwardSiblingID+sibOffset)%2];
          }
        }
        drawInterFrom = drawLastID;
        drawToID = CVNULL;
        
        std::swap(drawEscapeID, sectEscapeID);
        std::swap(drawing, secting);
      }
      
      //!std::cout << "\tfrom " << drawFromID << " to " << drawToID << "\n";
      firstOuter = false;
      drawFrom = drawTo;
      drawFromID = drawToID;
      if(drawFromID != CVNULL) drawLastID = drawFromID;
      //!std::cout << "\t\tnextID:" << drawFrom.siblingID[forwardSiblingID] << ", escape:"<<  (drawFromID != drawEscapeID) << ", proc:"<< drawFrom.proc<<"\n";
    }
  }
  
  if(ret.verts.size() > 0) {
    ret.verts.rbegin()->siblingID[1] = 0;
    ret.verts.begin()->siblingID[0] = ret.verts.size()-1;
    ret.verts.rbegin()->rConnect = true;
    ret.verts.begin()->lConnect = true;
  }
  
  //For all lines that weren't intersected or connected, add them now- their siblings are also *promised* to not be added, so blindly loop and add
  for(auto it = verts.begin(); it != verts.end(); it++) {
    if(!it->proc) {
      std::cout << "adding unproc vert, sib " << it->siblingID[0] << " = " <<getID(&verts[it->siblingID[0]])<< "\n";
      BakedContourVert *const added = ret.addVert(*it);
      unsigned int addedID = ret.getID(added);
      //if(it->siblingID[0] != CVNULL) added->siblingID[0] = getID(&verts[it->siblingID[0]]);
      //if(it->siblingID[1] != CVNULL) added->siblingID[1] = getID(&verts[it->siblingID[1]]);
      it->proc = it->pulled = true;
      it->id = addedID;
    }
  }
  
  //For all untouched verts in o, determine if they should be added- if so, do so
  if(sectUnprocBlk) {
    for(auto it = o.verts.begin(); it != o.verts.end(); it++) {
      if(!it->proc) {
        bool pointInPoly = it->pulled;
        if(!pointInPoly) {//Run raycast test if we don't know for sure- proc would be true if we knew, and pulled is true only if it passed
          const V2 deltaPCheck = V2(5000.f, 5000.f);
          V2 pCheckStart = it->getV2() + deltaPCheck, pCheckEnd = it->getV2();// - deltaPCheck;
          unsigned int intersections = 0;

          for(auto iit = verts.begin(); iit != verts.end(); ++iit) {
            if(iit->siblingID[1] == CVNULL || !iit->rConnect) continue;
            V2 inter = v2Intersect(pCheckStart, pCheckEnd, iit->getV2(), verts[iit->siblingID[1]].getV2());
            if(inter != V2NULL) {
              intersections++;
              //std::cout << "PiP: " << glm::to_string(inter) << "\n";
            }
          }

          if((intersections % 2) == 1) pointInPoly = true;
        }

        bool procState = true, pulledState = false;
        if(pointInPoly) {//Internal set- invert normals and add
          //std::cout << "adding SECT unproc vert, sib " << it->siblingID[0] << " = " <<o.getID(&o.verts[it->siblingID[0]])<< "\n";
          BakedContourVert *const added = ret.addVert(*it);
          unsigned int addedID = ret.getID(added);
          it->proc = it->arc = true;
          it->id = addedID;
          it->normal *= -1.f;
          procState = false; pulledState = true;
        }

        /*Mark sibling group state*/ {
          unsigned int drawElimID = o.getID(&(*it));
          const unsigned  int drawElimFrom = it->siblingID[0];
          const Uint8 sibOffset = 1;
          if(drawElimFrom != CVNULL) {
            while (drawElimID != drawElimFrom && drawElimID != CVNULL) {
              //!std::cout << drawElimID << "::" << o.verts[drawElimID].proc << " set proc:" << procState << "\n";
              if(o.verts[drawElimID].proc) break;
              o.verts[drawElimID].proc = procState;
              o.verts[drawElimID].pulled = pulledState;
              //std::cout << "proc'ing " << drawElimID << " to " << drawInterFrom << "\n";
              drawElimID = o.verts[drawElimID].siblingID[sibOffset];
            }
          }
        }

        it->proc = true;
        it->pulled = pulledState;
      }
    }
  }
  
  //Resolve all passthrough verts to their true sibling identities
  for(auto it = verts.begin(); it != verts.end(); it++) {
    if(it->pulled) {
      //std::cout << "setting v paththrough\n";
      it->pulled = false; it->proc = true;
      if(it->siblingID[0] != CVNULL) ret.verts[it->id].siblingID[0] = verts[it->siblingID[0]].id;
      if(it->siblingID[1] != CVNULL) ret.verts[it->id].siblingID[1] = verts[it->siblingID[1]].id;
    }
  }
  if(sectUnprocBlk) {
    for(auto it = o.verts.begin(); it != o.verts.end(); ++it) {
      if(it->arc) {
        it->arc = false; it->proc = true;
        //std::cout << "setting sect paththrough\n";
        if(it->siblingID[0] != CVNULL) ret.verts[it->id].siblingID[0] = o.verts[it->siblingID[0]].id;
        if(it->siblingID[1] != CVNULL) ret.verts[it->id].siblingID[1] = o.verts[it->siblingID[1]].id;
      }
    }
  }
  
  *this = ret;
}

Uint16 BakedContour::getID(const BakedContourVert *const v) const {
  return (v - verts.data());
}

Uint16 BakedContour::getNextSiblingID(Uint16 id) const {
  const BakedContourVert *const v = &verts[id];
  //if(v->rConnect) return (id+1)%verts.size();
  //else return v-
  return v->siblingID[1];
}

//
float BakedContour::calculateDifference(BakedContour &o) const {
  float difference = 0.f, median = 0.f;
  const float eta = sqrt(1.f / verts.size());
  
  for(Uint16 i = 0; i < verts.size(); i++) {
    const BakedContourVert &vert = verts.at(i);
    const BakedContourVert &other = o.getNearest(vert.pos);
    
    const float diff = glm::distance2(vert.pos, other.pos);
    difference += diff;
    median += eta * (diff - median);
  }
  
  difference /= float(verts.size());
  difference = sqrt(difference);
  median = sqrt(median);
  const float ret = (difference + median + std::max(difference, median)) / 3.f;
  //std::cout << "diff: " << ret << "\n";
  return ret;
}