/* 
 * File:   ContourCache.cpp
 * Author: Galen Swain
 * 
 * Created on May 26, 2015, 11:28 AM
 */

#include "ContourCache.h"

ContourCache::ContourCache() {
  
}

ContourCache::ContourCache(const ContourCache& orig) {
  contours = orig.contours;
}

ContourCache::~ContourCache() {
  
}

BakedContour ContourCache::get(const V3& planePoint, const V3& planeNormal, float epsilon) {
  const float planeD = 10.f;
  BakedContour ret;
  //For each workplane extrusion that can possibly intersect our projected slice
  //std::cout << "intersecting with plane by " << glm::to_string(planePoint) << " facing " << glm::to_string(planeNormal) << " by " << contours.size()<<" contours\n";
  BakedContour *prev = &(contours.begin()->contour);
  auto it = contours.begin();
  ++it;
  unsigned int pVertID = 0xFFFFFFFF;
  for(; it != contours.end(); ++it) {
    BakedContour *current = &(it->contour);
    //Iterate through points, determine if they intersect
    for(auto cVert = current->verts.begin(); cVert != current->verts.end(); ++cVert) {
      V3 cPoint = cVert->pertPos;
      BakedContourVert *pVert = &(prev->verts.at(cVert->parent));
      V3 pPoint = pVert->pertPos;
      //pPoint.x = cPoint.x; pPoint.y = cPoint.y;
      //std::cout << "Testing "<<  glm::to_string(pPoint) << "->" << glm::to_string(cPoint) << "\n";
      /*
      const V3 distC = planeNormal * (cPoint - planePoint), distP = planeNormal * (pPoint - planePoint);
      if(glm::dot(distC,distP) > 0) continue;//No intersection
      const V3 xVec = (cPoint - pPoint) / glm::abs(cPoint - pPoint);
      float cosTheta = cos(glm::dot(planeNormal, xVec));
      if(cosTheta == 0.f) continue;//Infinite intersection points, skip FOR NOW
      
      const V3 intersection = cPoint - xVec * (distC / cosTheta);
      const float ptTime = glm::distance(pPoint, intersection) / glm::distance(pPoint, cPoint);
       */
      V3 u = cPoint - pPoint;
      V3 w = pPoint - planePoint;

      float     D = glm::dot(planeNormal, u);
      float     N = -glm::dot(planeNormal, w);

      if (fabs(D) < 0.01f) {           // segment is parallel to plane
          if (N == 0) continue;
          else continue;//No intersection
      }
      // they are not parallel
      // compute intersect param
      float sI = N / D;
      if (sI < 0 || sI > 1) continue;                        // no intersection

      float ptTime = sI;
      V3 intersection = pPoint + sI * u;                  // compute segment intersect point

      BakedContourVert tmp;
      tmp.pos = intersection - planePoint;
      tmp.normal = (1.f-ptTime) * pVert->normal + ptTime * cVert->normal;
      tmp.siblingID[0] = pVertID;
      tmp.lConnect = (pVertID != CVNULL);
      if(pVertID != 0xFFFFFFFF) {
        ret.verts[pVertID].siblingID[1] = pVertID+1;
        ret.verts[pVertID].rConnect = true;
      }
      
      ret.addVert(tmp); pVertID++;
    }
    
    prev = current;
  }
  if(ret.size() > 0) {
    const unsigned int lastVertID = ret.verts.size()-1;
    ret.verts[0].siblingID[0] = lastVertID;
    ret.verts[0].lConnect = true;
    ret.verts[lastVertID].siblingID[1] = 0;
    ret.verts[lastVertID].rConnect = true;
  }
  
  return ret;
}