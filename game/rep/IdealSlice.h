/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IdealSlice.h
 * Author: Galen Swain
 *
 * Created on May 11, 2016, 3:26 PM
 */

#ifndef IDEALSLICE_H
#define IDEALSLICE_H
#include "game.h"
#include "field/Subfield.h"
#include "phys/Physical.h"
#include "phys/Surface.h"
#include "comp/IntersectionHull.h"
#include <vector>

#include "uni/WorldObject.h"
class IdealContour;

class IdealSlice {
public:
    friend class Subfield;
    typedef unsigned int Flags;
    enum {
        CapLower        = BIT02,
        CapUpper        = BIT03
    };
    
    IdealSlice();
    virtual ~IdealSlice();
    
    IdealContour *s, *e, *path;
    Subfield *subfield;
    float sZ, eZ;
    float contourRatio, dialationRatio;
    std::list<CompoundWeight> compounds;
    bool cRatio, dRatio, cList;
    bool isBottom, isTop;
    
    float volume;
    IntersectionHull bounds;
    
    struct Correlate {
        union {
            struct {
                float stMin, stMax, etMin, etMax;
                float slJump, srJump, elJump, erJump;
            };
            struct {
                float tData[4];
                float tJump[4];
            };
        };
        float tMin, tMax;
        bool original;
        
        inline Correlate() : stMin(-1.f), stMax(-1.f), etMin(-1.f), etMax(-1.f), 
        slJump(-1.f), srJump(-1.f), elJump(-1.f), erJump(-1.f), tMin(-1.f), tMax(-1.f), original(false) {
            
        }
        inline bool operator<(const Correlate& o) const {
            return tMin < o.tMin;
        }
        
        inline bool isMapped(bool rev) const {
            const unsigned int off = rev? 0 : 2;
            return tData[off+0] >= 0.f && tData[off+1] >= 0.f;
        }
        inline float getMidTime(bool rev) const {
          return rev? (etMin+etMax)/2.f : (stMin+stMax)/2.f;  
        }
        inline float getLength(bool rev) const {
            const unsigned int off = rev? 2 : 0;
            return tData[off+1] - tData[off+0];
        }
        inline bool overlaps(bool rev, const Correlate& o) const {
            const unsigned int off = rev? 2 : 0;
            return tData[off+0] == o.tData[off+0] && tData[off+1] == o.tData[off+1];
            /*
            return (tData[off+0] <= o.tData[off+0] && tData[off+1] >= o.tData[off+0]) ||
                   (tData[off+0] < o.tData[off+1] && tData[off+1] > o.tData[off+1]);*/
        }
        
        static thread_local IdealSlice *parent;
        static inline void setParent(IdealSlice *const p) {
            parent = p;
        }
    };
    std::vector<Correlate> correlates;
    
    void calculateContourRatio();
    void calculateDialationRatio();
    void calculateCompounds();
    
    std::vector<Correlate> correlateLayer(bool rev);
    void expandCorrelates(std::vector<IdealSlice::Correlate> &correlates, bool rev);
    std::vector<Correlate> traverseCorrelates(const IdealSlice::Correlate &c, const std::vector<IdealSlice::Correlate> &others);
    void printCorrelates(const std::vector<Correlate> &c, Uint8 rm = 255) const;
    void buildCorrelates(const std::vector<Correlate> &sc, const std::vector<Correlate> &ec);
    void calculateCorrelates();
    
    float calculateVolume();
    float calculateProp(unsigned short propID);
    V3 calculateCO(unsigned short propID);
    Surface calculateSurface(V3 co);
    
    V3 evalCentroid(float t);
    
    Physical calculatePhysical();
    
    WObjectVert evaluateVert(float sTime, float eTime, float seLerp, float z);
    
    void constructSub(float tStart, float tEnd, float epsilon, WMMeshType *const meshBuff, Flags flags, WMMeshType *const dbgBuff = nullptr);
    void construct(float epsilon, WMMeshType *const meshBuff, Flags flags, WMMeshType *const dbgBuff = nullptr);
private:

};

#endif /* IDEALSLICE_H */

