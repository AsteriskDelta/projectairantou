/* 
 * File:   ContourCache.h
 * Author: Galen Swain
 *
 * Created on May 26, 2015, 11:28 AM
 */

#ifndef CONTOURCACHE_H
#define	CONTOURCACHE_H
#include <iostream>
#include <list>
#include "BakedContour.h"

struct ContourCacheEntry {
    float t;
    BakedContour contour;
};

class ContourCache {
public:
    ContourCache();
    ContourCache(const ContourCache& orig);
    virtual ~ContourCache();
    
    inline void add(float t, const BakedContour& c);
    
    inline BakedContour get(float t, float epsilon);
    BakedContour get(const V3& planePoint, const V3& planeNormal, float epsilon);
private:
    std::list<ContourCacheEntry> contours;
};

inline void ContourCache::add(float t, const BakedContour& c) {
    //std::cout << "caching contour at " << t << ", "<<c.size() << " verts\n";
  for(auto it = contours.begin(); it != contours.end(); ++it) {
    if(it->t > t) {
      /*auto inserted = */contours.insert(it, ContourCacheEntry{t, c});
      return;
    }
  }
  contours.push_back(ContourCacheEntry{t, c});
}

inline BakedContour ContourCache::get(float t, float epsilon) {
  for(auto it = contours.begin(); it != contours.end(); ++it) {
    if(it->t > t) {
      if(it == contours.begin()) return it->contour;
      
      auto prev = it; --prev;
      const float dist = it->t - prev->t;
      return prev->contour.lerpTo(it->contour, (t - prev->t) / dist, epsilon);
    }
  }
  return (contours.begin() == contours.end()) ? BakedContour::dummy : contours.end()->contour;
}

#endif	/* CONTOURCACHE_H */

