/* 
 * File:   IdealContour.cpp
 * Author: Galen Swain
 * 
 * Created on May 15, 2015, 2:02 PM
 */

#include "IdealContour.h"
#include <iostream>
#include <cstring>

IdealFeature::IdealFeature(Uint8 nt, bool r) {
  radius = alpha = beta = phi = smoothness = theta = para = 0.f;
  start = mid = end = V3(0.f);
  tagRaw = 0x0;
  type = nt;
  real = r;
  start = V3(0);
  lConnect = rConnect = true;
  invHermite = false;
  smoothness = 0.f;
}

IdealFeature::IdealFeature(const IdealFeature& o) {
  memcpy(this, &o, sizeof(IdealFeature));
}

IdealFeature::~IdealFeature() {
  return;
}

IdealFeature& IdealFeature::operator=(const IdealFeature& o) {
  *this = IdealFeature(o);
  return *this;
}


static inline bool aboutEqual(const V3& a, const V3& b) {
  return glm::distance2(a,b) < 0.01f;
}

static inline void exclosestPoints(const V3 p0, const V3 u, const V3 q0, const V3 v, V3 *const spt, V3 *const opt) {
  *spt = V3_NULL;
  *opt = V3_NULL;
  const V3 w0 = q0 - p0;
  
  const float 
          a = glm::dot(u, u ),
          b = glm::dot(u, v ),
          c = glm::dot(v, v ),
          d = glm::dot(u, w0),
          e = glm::dot(v, w0);
  float denom = (a*c - b*b);
  if(fabs(denom) < 0.01f) return;
  
  const float sc = (b*e - c*d) / denom;
  const float tc = (a*e - b*d) / denom;
  
  *spt = p0 + u*sc;
  *opt = q0 + v*tc;
}

void IdealFeature::closestPoints(const IdealFeature *const o, V3 *const spt, V3 *const opt) const {
  //Implemented as http://geomalgorithms.com/a07-_distance.html
  const V3 u = this->delta(), v = o->delta();
  const V3 p0 = this->evaluate(0.f), q0 = o->evaluate(0.f);
  
  exclosestPoints(p0, u, q0, v, spt, opt);
  /*
  //Implemented as 
  const V3 p1 = this->evaluate(0.f), p2 = this->evaluate(1.f);
  const V3 p3 = o->evaluate(0.f), p4 = o->evaluate(1.f);
  
  float mua, mub;
   * */
}
float IdealFeature::distanceTo(const IdealFeature *const o) const {
  return glm::length(this->vectorTo(o));
}
float IdealFeature::angleTo(const IdealFeature *const o) const {
  return glm::dot(this->vectorTo(o), this->normal());
}
float IdealFeature::angleBetween(const IdealFeature *const o) const {
  return glm::dot(this->normal(), o->normal());
}
V3 IdealFeature::vectorTo(const IdealFeature *const o) const {
  V3 a, b;
  this->closestPoints(o, &a, &b);
  
  if(a == V3_NULL || b == V3_NULL) return V3(0.f);
  else return (b-a);
}

bool IdealFeature::couldIntersect(const IdealFeature *const o) const {
  V3 a, b;
  this->closestPoints(o, &a, &b);
  
  if(a == V3_NULL || b == V3_NULL) return true;//Overlap
  
  //If both closest points are endpoints, line's normal extrusion cannot touch other line
  bool endpointA = aboutEqual(a, this->evaluate(0.f)) || aboutEqual(a, this->evaluate(1.f));
  bool endpointB = aboutEqual(b, o->evaluate(0.f))    || aboutEqual(b, o->evaluate(1.f));
  
  if(endpointA && endpointB && !aboutEqual(a, b)) return false;
  else return true;
}

V3 IdealFeature::crossing(const IdealFeature *const o) const {
  V3 a, b;
  this->closestPoints(o, &a, &b);
  
  if(a == V3_NULL || b == V3_NULL) return V3_NULL;
  return aboutEqual(a,b)? a : V3_NULL;
}

V3 IdealFeature::crossing(const V3& f, const V3& t) const {
  V3 a, b;
  const V3 u = this->delta(), v = t-f;
  const V3 p0 = this->evaluate(0.f), q0 = f;
  
  exclosestPoints(p0, u, q0, v, &a, &b);
  
  if(a == V3_NULL || b == V3_NULL) return V3_NULL;
  return aboutEqual(a,b)? a : V3_NULL;
}

//Returns given hermite (at ideal stage, normal) of the given face at param time t
V3 IdealFeature::getHermite(float t) const {
  V3 tmp;
  switch(type) {
    case Line:
      return glm::normalize(glm::cross(end-start, getAxis()) * (invHermite? -1.f : 1.f));
      break;
    case Arc:
      tmp =  glm::normalize(mid - this->evaluate(t)) * (invHermite? -1.f : 1.f);
      return tmp;
      break;
    case Dummy:
    default:
      return V3(0.f);
      break;
  }
}
void IdealFeature::setHermite(const V3& herm) {
  V3 hAxis; float score;
  switch(type) {
    case Line:
      hAxis = glm::cross(end-start, herm);
      score = glm::dot(hAxis, this->getAxis());
      if(score <= 0.f) invHermite = false;
      else invHermite = true;
      break;
    case Arc:
      hAxis = glm::cross(mid-start, herm);
      score = glm::dot(hAxis, this->getAxis());
      if(score >= 0.f) invHermite = false;
      else invHermite = true;
      break;
    case Dummy:
    default:
      invHermite = false;
      Console::Warning("IdealFeature::setHermite called on dummy object!");
      break;
  }
  return;
}
//Interpolate feature to other given feature
IdealFeature IdealFeature::interpolateTo(const IdealFeature& o) const {
  IdealFeature ret;
  
  //TODO: me
  return ret;
}
//Gets bounds relative to workplane, or given matrix
std::vector<V3> IdealFeature::getExtremes(Mat4* mat) const {
  Mat3 smat = (mat == NULL)?Mat3(1.f) : Mat3(*mat);
  std::vector<V3> ret; float rad, tmp;
  switch(type) {
    case Line:
      ret.push_back(start);
      ret.push_back(end);
      break;
    case Arc:
      rad = glm::distance(start, mid);
      //std::cout << "begin extremes of arc to " << theta << ":\n";
      for(tmp = 0.f; tmp <= theta && tmp != 360.f; tmp += 90.f) {
        //std::cout << "rotating " << glm::to_string(mid-start) << " by " << tmp << "\n";
        ret.push_back(mid + smat*glm::rotate(start-mid, tmp*DEG_TO_RAD, this->getAxis()));
        //std::cout << "extreme includes " << glm::to_string(*(ret.rbegin())) << " @ " << tmp << "\n";
      }
      break;
    case Dummy:
    default:
      break;
  }
  return ret;
}

void IdealFeature::resolve(BakedContour *const contour, float epsilon, const IdealFeature *const prev, const IdealFeature *const next) const {
  BakedContourVert tmp;
  float tStep; Uint16 curID;
  switch(type) {
    case Line:
      if(!prev) {
        tmp.pos = this->evaluate(0.f);
        tmp.pulled = true;
        tmp.rConnect = true; tmp.lConnect = false;
        if(prev == NULL) tmp.hermite = this->getHermite(0.f);
        else tmp.hermite = glm::normalize(this->getHermite(0.f) + prev->getHermite(1.f));
        contour->addVert(tmp);
      }
      tmp.pos = this->evaluate(1.f);
      tmp.pulled = true;
      tmp.rConnect = rConnect; tmp.lConnect = true;
      
      //std::cout << "using:" << glm::to_string(this->getHermite(1.f)) << "\n";
      if(next == NULL) tmp.hermite = this->getHermite(1.f);
      else tmp.hermite = glm::normalize(this->getHermite(1.f) + next->getHermite(0.f));
      contour->addVert(tmp);
      /*
      tmp.pos = V3(0.f);
      tmp.lConnect = lConnect; tmp.rConnect = rConnect;
      this->evaluate(tmp.pos, 0.f);
      tmp.normal = glm::normalize(glm::cross(V3_UP, end - start));
      //tmp.siblingID[0] = contour->prevID; tmp.siblingID[1] = CVNULL;
      curID = contour->getID(contour->addVert(tmp));
      if(contour->prevID != CVNULL) {
        contour->verts[contour->prevID].siblingID[1] = curID;
        contour->verts[contour->prevID].rConnect = true;
      }
      contour->prevID = curID;
      //std::cout << "F" << glm::to_string(tmp.pos) << "\n";;
      
      tmp.pos = V3(0.f);
      this->evaluate(tmp.pos, 1.f);
      //tmp.siblingID[0] = contour->prevID; tmp.siblingID[1] = CVNULL;
      tmp.rConnect = false;
      curID = contour->getID(contour->addVert(tmp));
      if(contour->prevID != CVNULL) contour->verts[contour->prevID].siblingID[1] = curID;
      contour->prevID = curID;
      //std::cout << "T" << glm::to_string(tmp.pos) << "\n";;*/
      break;
    case Arc:
      tStep = this->getDeltaStep(0.f, epsilon);//epsilon / ((2.f * float(M_PI) * radius) * (theta/360.f));
      for(float t = prev?tStep : 0.f; t <= 1.f; t = std::min(1.f, t + tStep)) {
        tmp.pos = this->evaluate(std::min(t, 1.f));
        if(t == 0.f) tmp.lConnect = false;
        else tmp.lConnect = true;
        if(t == 1.f && !next) tmp.rConnect = false;
        else tmp.rConnect = true;
        
        if(t == 0.f || t == 1.f) tmp.pulled = true;
        else tmp.pulled = false;
        
        if(t == 0.f && prev != NULL) tmp.hermite = glm::normalize(this->getHermite(0.f) + prev->getHermite(1.f));
        else if(t == 1.f && next != NULL) tmp.hermite = glm::normalize(this->getHermite(1.f) + next->getHermite(0.f));
        else tmp.hermite = this->getHermite(t);
        //tmp.normal = glm::normalize(tmp.pos - mid);
        contour->addVert(tmp);
       
        if(t >= 1.f) break;
      }
      /*
      tStep = this->getDeltaStep(0.f, epsilon);//epsilon / ((2.f * float(M_PI) * radius) * (theta/360.f));
      for(float t = 0.f; t <= 1.f; t = std::min(1.f, t + tStep)) {
        tmp.pos = V3(0.f);
        tmp.lConnect = true;
        tmp.rConnect = false;
        this->evaluate(tmp.pos, std::min(t, 1.f));
        
        tmp.normal = glm::normalize(tmp.pos - mid);
        tmp.siblingID[0] = contour->prevID; tmp.siblingID[1] = CVNULL;
        curID = contour->verts.size();
        contour->addVert(tmp);
        if(contour->prevID != CVNULL) {
          //std::cout << "Mapping " << contour->prevID << " -> " << curID << "\n";
          contour->verts[contour->prevID].siblingID[1] = curID;
          contour->verts[contour->prevID].rConnect = true;
        }
        contour->prevID = curID;
        if(t >= 1.f) break;
      }*/
      break;
    case Dummy:
    default:
      Console::Warning("Unknown feature type!\n");
      break;
  }
  
  //Stitch first and last, if connected
}

float IdealFeature::distance() const {
  switch(type) {
    case Line:
      return glm::distance(start, end);
      break;
    case Arc:
      return (float(M_PI) * radius) * (theta/360.f);
      break;
    case Dummy:
    default:
      return 0.f;
      break;
  }
}

float IdealFeature::getNextT(float t, float epsilon) const {
  switch(type) {
    case Line:
      if(getSmoothness() == 0.f) {
        if(t == std::nextafter(this->para, 0.f)) return this->para;
        else return std::nextafter(this->para, 0.f);//Next last value
      }
      //else return /*std::min(std::nextafter(this->para, 0.f), */float(t + fabs(pow(epsilon, this->getSmoothness()/M_PI))) ;//); 
      return lerp(std::max(t + epsilon, std::nextafter(this->para, 0.f)), t + epsilon, this->getSmoothness());
      break;
    case Arc:
      return t + sqrt(epsilon);
      break;
    case Dummy:
    default:
      return 0.f;
      break;
  }
}

inline V3 remapVector(V3 v, V3 normal) {
  return v * quatFromToRotation(normal, V3_FORWARD);
  //Quat rot = quatFromToRotation(normal, V3_FORWARD);
  //rot = Quat(v, normal);
  if(normal == V3_FORWARD) return v*V3(-1.f, 1.f, 1.f);
  else if(normal == -V3_FORWARD) return -v;
  
  V3 axis = glm::normalize(glm::cross(V3_FORWARD, normal));
  float angle = acos(glm::dot(glm::normalize(V3_FORWARD), glm::normalize(normal)));
  if(!std::isfinite(axis.x) || !std::isfinite(axis.y) || !std::isfinite(axis.z)) return v;
  //std::cout << "remapping on " << axis.x << ","<<axis.y<<","<<axis.z << " by " << angle << "\n";
  return glm::rotate(v,- angle, -axis);//v * rot;
}

void IdealFeature::evaluate(V3 &v, const float t) const {
  V3 diff, normal;
  switch(type) {
    case Line:
      diff  = end - start;
      normal = glm::normalize(diff);
      //std::cout << glm::to_string(normal) << " NORMAL\n";
      v = start + diff * t + remapVector(v, normal);
      break;
    case Arc:
      if(theta > FLT_EPSILON) diff = glm::rotate(start - mid, theta*t * DEG_TO_RAD, axis);
      else diff = start - mid;
      //normal = glm::normalize(glm::cross(glm::normalize(diff), axis));
      normal = glm::rotate(glm::normalize(glm::cross(start - mid, axis)), theta*t * DEG_TO_RAD, axis);
      v = mid + diff + ((theta*t) == 0? v : remapVector(v * V3(-1.f, 1.f, 1.f), normal));
      break;
    case Dummy:
    default:
      break;
  }
}

V3 IdealFeature::evaluate(const float t) const {
  V3 ret = V3(0.f);
  this->evaluate(ret, t);
  return ret;
}

BakedContourVert IdealFeature::evaluateVert(float t, const IdealFeature *const p, const IdealFeature *const n) const {
  BakedContourVert ret;
  const float prevSmooth = (p == NULL)? 0.f : p->getSmoothness(), nextSmooth= (n == NULL)? 0.f : n->getSmoothness();
  
  ret.arc = false;
  ret.pulled = false;
  
  if(this->getSmoothness() == 0.f || t == 0.5f  || (t < 0.5f && prevSmooth == 0.f) || (t > 0.5f && nextSmooth == 0.f)) {
    ret.pos = this->evaluate(t);
    ret.hermite = this->getHermite(t);
    if(t == 0.f || std::nextafter(t, 0.f) == 0.f) {
      ret.hermite = glm::normalize((ret.hermite + p->getHermite(1.f)));
      //std::cout << "blending hermites: " <<glm::to_string(p->getHermite(1.f)) << " ->" << glm::to_string(this->getHermite(0.f)) << " \n";
    } else if(t == 1.f || std::nextafter(t, 1.f) == 1.f) {
      ret.hermite = glm::normalize((ret.hermite + n->getHermite(0.f)));
    }
    ret.smoothness = 0;
     ret.pulled = ret.arc = true;
    //std::cout << "evaluating point at t = " << t << ", hermite = " << glm::to_string(ret.hermite) << "\n";
  } else {
    const IdealFeature *const isf = (t > 0.5f)? n : p;
    const V3 endPt = (t > 0.5f)?this->evaluate(1.f) : this->evaluate(0.f);
    float actSmoothness = this->getSmoothness() * isf->getSmoothness();
    const float actPower = 1.f/(actSmoothness);
    
    //The following equations are literally insane. See notebook subdivisions of triangles and recomp. of circles.
    // The sin wave-hVec comp works nicely, but I have no idea why the linear component must be multiplied by two...
    V3 basePt = this->evaluate(0.5f), targetPt = isf->evaluate(0.5f);
    V3 midPt = (basePt*0.5f + targetPt * 0.5f);
    V3 baseVec = targetPt - basePt, hVec = glm::distance(midPt, endPt) *glm::normalize(endPt - midPt) /2.f;//glm::normalize(endPt - midPt) * glm::distance(midPt, (basePt + endPt)/2.f) * (1.f/actPower);
    //float smoothT = (t > 0.5f)? 1.f-(t - 0.5f) : (t);
    float baseT = fabs(t - 0.5f);//(t > 0.5f)? (t - 0.5f) : (t + 0.5f);
    //float mulT = (t > 0.5f)? (1.f - t)*2.f : (t)*2.f;
    
    float curveY =pow(sin(clamp(float(baseT*0.9f) + 0.05f, 0.f, 1.f)*M_PI), actPower);//*actSmoothness;//pow(1.f - pow(smoothT*2.f - 1.f, actPower), 1.f/actPower);
    float linY = (1.f-actSmoothness) * baseT * 2.f;
    //std::cout << "evaluating curve at st=" << smoothT << " -> " << curveY << "\n";
    ret.pos = basePt + baseVec * baseT + curveY* hVec + linY * (endPt - midPt);
    
    float hmTime = cos(baseT*M_PI) / 2.f + 0.5f;
    //V3 baseHermite = glm::normalize(this->getHermite(0.5f) + isf->getHermite(0.f));
    ret.hermite = glm::normalize(this->getHermite(0.5f) * hmTime + isf->getHermite(0.5f) * (1.f-hmTime));
    ret.smoothness = round(clamp(actSmoothness*255.f, 0.f, 255.f));
  }
  
  ret.normal = ret.hermite;
  //if(this->getSmoothness() < 0.22f &&( t == 0.f  || t == 1.f)) {
   
  //}
  
  return ret;
}

float IdealFeature::crossSum(const IdealFeature *const p, const IdealFeature *const n) const {
  _unused(p);
  V3 //pPt = p->evaluate(0.f), 
     sPt = this->evaluate(0.f), 
     //ePt = this->evaluate(1.f), 
     nPt = n->evaluate(0.f);
  //V3 pVec = V3(pPt - sPt), 
  //   nVec = V3(nPt - ePt),
  //   sVec = V3(ePt - sPt);
  
  float ret = 0.f;
  //ret += glm::length(glm::cross(pVec, sVec));
  //ret += glm::length(glm::cross(sVec, nVec));
  ret += sPt.x * nPt.y - sPt.y * nPt.x;
  //std::cout << "crossSum=" << ret << "\n";
  return ret;
}

float IdealFeature::curveSum(const IdealFeature *const p, const IdealFeature *const n) const {
  _unused(p);
  const IdealFeature *const isf = n;
  V3 basePt = this->evaluate(0.5f), targetPt = isf->evaluate(0.5f);
  V3 midPt = (basePt*0.5f + targetPt * 0.5f), endPt = this->evaluate(1.f);
  float actSmoothness = this->getSmoothness() * isf->getSmoothness();
  //V3 baseVec = targetPt - basePt, hVec = glm::distance(midPt, endPt) *glm::normalize(endPt - midPt) /2.f;
  
  float xDist = glm::distance(basePt, targetPt);
  float height = glm::distance(midPt, endPt)/2.f;
  float tot = (1.f/2.f * pow(xDist, 2.f)) / 2.f;//Area of active triangle
  float area = height/2.f*(M_PI * xDist + cos(M_PI * xDist) - 1.f) / M_PI;//(height - height*xDist)/M_PI;//Area of curve
  float ret = -(tot - area) * actSmoothness;
  
  //std::cout << "curveSum: " << tot << " - " << area << " = " << ret << "\n";
  return ret;
}

V3 IdealFeature::crossCentroid(const IdealFeature *const p, const IdealFeature *const n, float cross, float curve) const {
  V3 sPt = this->evaluate(0.f), nPt = n->evaluate(0.f);
  
  return (sPt + nPt)*(cross + curve*sgn(cross));
}

float IdealFeature::getDeltaStep(float t, float epsilon) const {
  float res;
  switch(type) {
    case Line:
      return 1.f;
    case Arc://Ensure arc's symmetry
      res =  2.f * 2.f / floor(2.f / (epsilon / ((1.4f * float(M_PI) * sqrt(radius)) * (theta/360.f)) ));
      if(res == 0.f || res > 0.3334f) return 0.3334f;
      
      return res;
    case Dummy:
    default:
      Console::Warning("Unknown feature type!\n");
      return 1.f;
  }
}

float IdealFeature::getResistance() const {
  if(smoothness >= 1.f) return 1.f / smoothness;
  else return 4.f * (1.f - smoothness);
}


float IdealFeature::intersectTime(V3 vStart, V3 vEnd) const {
  float t = -1.f;
  V2IntersectLines(V2(vStart), V2(vEnd), V2(this->evaluate(0.f)), V2(this->evaluate(1.f)), &t);
  
  return t;
}
V3 IdealFeature::intersect(V3 vStart, V3 vEnd) const {
  float t = this->intersectTime(vStart, vEnd);
  if(t < 0.f) return V3NULL;
  return vStart*(1.f-t) + vEnd + t;
}
int IdealFeature::intersectSign(V3 vStart, V3 vEnd) const {
  float t = this->intersectTime(vStart, vEnd);
  if(t < 0.f || t > 0.999f) return 0;
  
  float dot = glm::dot(this->getHermite(t), glm::normalize(vEnd-vStart));
  
  return -sgn(dot);
}

IdealContour::IdealContour() {
  prefab = false;
  dirty = true;
  linear = 1.f;
  quadratic = 0.f;
  variance = 0.f;
}

IdealContour::IdealContour(const IdealContour& orig) {
  prefab = orig.prefab;
  dirty = true;
  linear = orig.linear;
  quadratic = orig.quadratic;
  features = orig.features;
  variance = orig.variance;
}

IdealContour::~IdealContour() {
}

void IdealContour::addFeature(const IdealFeature &feature) {
  features.push_back(feature);
  dirty = true;
}

bool IdealContour::crosses(const V3& f, const V3& t) const {
  for(unsigned int i = 0; i < features.size(); i++) {
    const IdealFeature *const feature = &features[i];
    if(feature->crosses(f,t)) return true;
  }
  return false;
}

struct ICBKTmp {
  unsigned short startVert, endVert;
};

IdealFeature* IdealContour::getBestCorrelate(const IdealFeature *const o, const IdealContour *const otherContour) {
  IdealFeature *ret = nullptr; float bestDist = FLT_MAX, bestDot = -1.f;
  float maxDistP = FLT_MAX, maxDistN = FLT_MAX;//Maximum distance in positive and negative directions, prevents clipping through backfaces
  
  for(Uint8 pass = 0; pass <= 1; pass++) {
    const bool prepass = (pass == 0);
    
    for(unsigned int i = 0; i < this->size(); i++) {
      IdealFeature *const f = &features[i];

      //Find features whose normals overlap at some extreme, and are oriented within 90d
      V3 epc, epo; o->closestPoints(f, &epc, &epo);
      const bool couldIntersect = o->couldIntersect(f);
      const float dist = o->distanceTo(f);
      //const float dotTo = o->angleTo(f);
      const float dotBetween = o->angleBetween(f);

      float *maxDist = (dotBetween >= 0.f)? &maxDistP : &maxDistN;

      if(prepass && couldIntersect) {//If prepass, assign constraints
        if(dotBetween <= 0.f && dist < *maxDist) *maxDist = dist;
      } else if(couldIntersect) {//Final run, find best canidate
        bool distTest = (dist < bestDist) || (fabs(bestDist - dist) < 0.01f && dotBetween > bestDot);
        std::cout << "feature " << i << " could intersect o, dist="<<dist << " and dot=" << dotBetween << " : maxDist=" << *maxDist << " bestDist=" << bestDist << " bestDot=" << bestDot<< "\n";
        if(dist <= *maxDist && distTest && dotBetween > 0.f && (otherContour == nullptr || !otherContour->crosses(epc, epo))) {
          ret = f;
          bestDist = dist;
          bestDot = dotBetween;
        }
      }
      
    }//End feature loop
  }
  
  return ret;
}

unsigned short IdealContour::getFeatureID(float t) const  {
  unsigned short lastIdx = features.size()-1;
  for(unsigned short i = 0; i <= lastIdx; i++) {
    if(i == lastIdx || t < features[i].para) return i;
  }
  return lastIdx;
  /*
  for(auto it = features.begin() ; it != features.end(); ++it) {
    if(z <= it->para || (it == last && z <= it->para)) {
      float featureDist = (z - prevPara) / (it->para - prevPara);
      float ret = it->getDeltaStep(featureDist, epsilon) * (it->para - prevPara);//std::max(0.f, nextafterf(it->para, it->para + 100.f) - z));
      
      if(z == it->para || z + ret > it->para) ret = nextafterf(it->para, it->para*2.f) - z;
      //std::cout << "PR: " << it->para << " PRV: " << prevPara << " d" << ret << "\n";
      
      if(it == last) {
        ret = std::min(ret, it->para - z);
      }
      return ret;
    } else prevPara = it->para;
  }
  return 1.f;*/
}

unsigned short IdealContour::getFeatureID(const IdealFeature *const fptr) const {
  unsigned short lastIdx = features.size()-1;
  for(unsigned short i = 0; i <= lastIdx; i++) {
    const IdealFeature *const optr = &(features[i]);
    if(fptr == optr) return i;
  }
  
  return lastIdx;
}

const IdealFeature* IdealContour::getFeature(unsigned int id) const {
  return &features[id];
}
IdealFeature* IdealContour::getFeatureNC(unsigned int id) {
  return &features[id];
}
const IdealFeature* IdealContour::getFeatureSibling(unsigned int id, int dir) const {
  const IdealFeature *c = getFeature(id);
  while(dir < 0) {
    int newID;
    if(c->lConnect) {
      newID = id - 1;
      if(newID < 0) newID += features.size();
    } else {
      newID = c->jumpID;
      if(newID == 0xFFFF) return NULL;
    }
    id = newID;
    c = &features[id];
    dir++;
  }
  while(dir > 0) {
    int newID;
    if(c->rConnect) {
      newID = (id + 1) % features.size();
    } else {
      newID = c->jumpID;
      if(newID == 0xFFFF) return NULL;
    }
    id = newID;
    c = &features[id];
    dir--;
  }

  return c;
}

const IdealFeature* IdealContour::getFeature(float t) const {
  return &features[getFeatureID(t)];
}
const IdealFeature* IdealContour::getFeatureSibling(float t, int dir) const {
  int id = int(this->getFeatureID(t));
  const IdealFeature *c = &features[id];
  while(dir < 0) {
    int newID;
    if(c->lConnect) {
      newID = id - 1;
      if(newID < 0) newID += features.size();
    } else {
      newID = c->jumpID;
      if(newID == 0xFFFF) return NULL;
    }
    id = newID;
    c = &features[id];
    dir++;
  }
  while(dir > 0) {
    int newID;
    if(c->rConnect) {
      newID = (id + 1) % features.size();
    } else {
      newID = c->jumpID;
      if(newID == 0xFFFF) return NULL;
    }
    id = newID;
    c = &features[id];
    dir--;
  }

  return c;
}
#define VERTIDX_NULL (0xFFFF)
BakedContour IdealContour::bake(float epsilon) const {
  BakedContour ret;
  //std::cout << "baking contour\n";
  //const unsigned short postProcCode = 48000, postProcOffset = 50000;
  std::vector<ICBKTmp> tmpInfo; tmpInfo.resize(features.size());
  //unsigned int fi = 0;
  
  //Wind over parametric t, instead of features - sets leftmost sibling correctly
  float t = 0.f, tNext, tMax = this->getMaxExtrusion();
  if(epsilon > (1.f/4.f) * tMax) epsilon /= 2.f;
  Uint16 firstVertIdx = VERTIDX_NULL, lastVertIdx = VERTIDX_NULL; bool lastConnect = false;
  while(t < tMax) {
    //std::cout << "Evaluating at " << t << " with e=" << epsilon << "\n";
    const IdealFeature *p = this->getPrevFeature(t), *c = this->getFeature(t), *n = this->getNextFeature(t);
    float localT = std::max(0.f, t - c->startT()) / c->distance();
    tNext = c->getNextT(t, epsilon);//Ensure we always move forward, and skip any redundent verts
    if(tNext <= t) tNext = std::nextafter(t, tMax);
    else if(tNext == std::nextafter(c->para, 0.f) && n != NULL) tNext = c->para;
    
    BakedContourVert newVert = c->evaluateVert(localT, p, n);
    bool isLastInFeature = (tNext >= c->para);
    if(lastVertIdx != VERTIDX_NULL && lastConnect) newVert.siblingID[0] = lastVertIdx;
 
    lastVertIdx = ret.size();
    if(!lastConnect) firstVertIdx = lastVertIdx;
    
    if((isLastInFeature && !c->rConnect)) {
      lastConnect = false;
      //Close loop with firstVerts left sibling
      ret.verts[firstVertIdx].siblingID[0] = lastVertIdx;
    } else lastConnect = true;
    
    ret.addVert(newVert);
    
    if(tNext <= t) tNext = std::nextafter(t, tMax);
    t = tNext;
  }
  
  ret.verts[firstVertIdx].siblingID[0] = lastVertIdx;
  
  //Close winding by writing right siblings
  for(int i = 0; i < ret.size(); i++) {
    BakedContourVert *v = &ret.verts[i];
    if(v->siblingID[0] == 0xFFFF) continue;
    
    BakedContourVert *leftSib = &ret.verts[v->siblingID[0]];
    leftSib->siblingID[1] = i;
  }
  
  std::cout << "Contour vert siblings:\n";
  for(int i = 0; i < ret.size(); i++) {
    BakedContourVert *v = &ret.verts[i];
    BakedContourVert *o = (v->siblingID[0] == 0xFFFF) ? v : &ret.verts[v->siblingID[0]];
    
    std::cout << "\t" << v->siblingID[0] << " -> " << i << " -> " << v->siblingID[1] << "\t\t" << glm::to_string(v->pos) << " h=" << glm::to_string(v->hermite) << "\t\tdP = " << glm::to_string(glm::normalize(o->pos - v->pos))<<"\n";
  }
  
  /*
  for(auto it = features.begin() ; it != features.end(); ++it) {
    const IdealFeature *prev = (it == features.begin())? &(*features.rbegin()) : &(*std::prev(it)), 
                 *next = (std::next(it) == features.end())? &(*features.begin()) : &(*std::next(it));
    if(!it->lConnect && !it->rConnect && it->jumpID != 0xFFFF) {
      prev = next = &(*it);
    } else if(!it->lConnect) {
      if(it->jumpID != 0xFFFF) prev = &features[it->jumpID];
      else prev = NULL;
    } else if(!it->rConnect) {
      if(it->jumpID != 0xFFFF) next = &features[it->jumpID];
      else next = NULL;
    }
    
    unsigned int startVerts = ret.verts.size();
    it->resolve(&ret, epsilon, prev, next);
    unsigned int endVert = ret.verts.size()-1;
    
    //Connect and calculate actual hermites
    for(unsigned int i = startVerts; i < ret.verts.size(); i++) {
      unsigned int ni = (i+1 == ret.verts.size())? postProcCode : i+1, pi = (i == startVerts)? postProcCode : i-1;
      if(!it->lConnect && it->jumpID != 0xFFFF && i == startVerts) pi = postProcOffset+it->jumpID;
      else if(!it->rConnect && it->jumpID != 0xFFFF && i == ret.verts.size()-1) ni = postProcOffset+it->jumpID;
      //std::cout << pi << "->\t " << i << "->" << ni << "\n";
      BakedContourVert *vert = &(ret.verts[i]);//, *next = &(ret.verts[ni]);
      vert->normal = vert->hermite;
      //std::cout << "norm:" << glm::to_string(vert->normal) << "\n";
      vert->id = i;
      vert->siblingID[0] = pi; vert->siblingID[1] = ni; 
      vert->lConnect = (i == startVerts && (it->lConnect || it->jumpID != 0xFFFF)) || (i > startVerts);
      vert->rConnect = (i == endVert && (it->rConnect || it->jumpID != 0xFFFF)) || (i < endVert);
    }
    
    tmpInfo[fi].startVert = startVerts;
    tmpInfo[fi].endVert = endVert;
    fi++;
  }*/
  /*
  for(unsigned int i = 0; i < ret.verts.size(); i++) {
    BakedContourVert *vert = &(ret.verts[i]);
    if(vert->siblingID[0] == postProcCode && vert->lConnect) vert->siblingID[0] = ret.verts.size()-1;
    if(vert->siblingID[1] == postProcCode && vert->rConnect) vert->siblingID[1] = (i+1)%ret.verts.size();
    
    if(vert->siblingID[0] >= postProcOffset && vert->lConnect) {
      vert->siblingID[0] = tmpInfo[vert->siblingID[0]-postProcOffset].endVert;
    }
    if(vert->siblingID[1] >= postProcOffset && vert->rConnect) {
      vert->siblingID[1] = tmpInfo[vert->siblingID[1]-postProcOffset].startVert;
    }
  }*/
 
  //Connect first and last
  /*if(ret.size() >= 2 && ret.verts[0].lConnect) {
    ret.verts[0].siblingID[0] = ret.verts.size()-1;
    ret.verts[ret.verts.size()-1].siblingID[1] = 0;
    ret.verts
  }*/
  
  //std::cout << "Built contour with " << ret.size() << " verts\n";
  return ret;
}

void IdealContour::parametricize(bool path) {
  if(features.size() < 1) {
    
    return;
  }
  
  float distance = 0.f; const float connEpsilon = 0.002f;
  auto lastIter = std::prev(features.end()); V3 last = path? V3(0.f) : lastIter->evaluate( 1.f);
  bounds.clear();
  bounds.setType2D(IntersectionHull::Flat);
  unsigned short lastConnectedFeature = 0, featureID = 0;
  //std::cout << "\nBEGIN PARAMET\n";
  for(auto it = features.begin() ; it != features.end(); ++it) {
    if(it->start == V3(0)) it->start = last;
    //std::cout << glm::to_string(it->start) << " svec\n";
    if(it->type == IdealFeature::Arc) it->radius = glm::distance(it->start, it->mid);
    
    it->para = (distance += it->distance());
    //std::cout << "testing " << glm::to_string(it->evaluate( 0.f)) << "->" << glm::to_string(last) << ", d="<<glm::distance2(it->evaluate( 0.f), last)<<"\n";
    if(glm::distance2(it->evaluate( 0.f), last) < connEpsilon) {
      it->lConnect = true; lastIter->rConnect = true;
      lastIter->rConnect = true;
      //std::cout << featureID << "connected to " << ((featureID == 0)? features.size()-1 : featureID-1) << "\n";
      it->jumpID = 0xFFFF;
    } else {
      it->lConnect = it->rConnect = false; lastIter->rConnect = false;
      lastIter->jumpID = lastConnectedFeature;
      features[lastConnectedFeature].jumpID = ((featureID == 0)? features.size()-1 : featureID-1);
      features[lastConnectedFeature].lConnect = false;
      lastConnectedFeature = featureID;
    }
    
    //std::cout << "feature dist: " << it->para << "\n";
    last = it->evaluate( 1.f);
    lastIter = it;
    
    auto ftBounds = it->getExtremes();
    for(auto pt : ftBounds) {
      //std::cout << "encompass " << glm::to_string(pt) << "\n";
      bounds.encompass(pt);
    }
    //std::cout << "param bounds has a=" << bounds.area() << "\n";
    
    featureID = (featureID+1)%features.size();
  }
  //std::cout << "Output bounds: " << glm::to_string(bounds.root) << ", " << glm::to_string(bounds.root+bounds.del) << "\n";
  features[features.size()-1].rConnect = false;
  features[0].lConnect = false;
  features[features.size()-1].jumpID = lastConnectedFeature;
  features[lastConnectedFeature].jumpID = features.size()-1;
  
  //Debug printout for ensuring proper connection flags
  
  //std::cout << "Print features:\n";
  for(featureID = 0; featureID < features.size(); featureID++) {
    IdealFeature *const f = &(features[featureID]);
    //std::cout << featureID << "\tleft:" << f->lConnect  << ", right:" << f->rConnect<<", jump: " << f->jumpID << "\t\t" << glm::to_string(f->evaluate(0.f)) << " -> " << glm::to_string(f->evaluate(1.f)) << "\n";
  }
  
  this->integrate();
}

float IdealContour::integrate() {
  float crossSum = 0.f, curveSum = 0.f;
  centroid = V3(0.f);
  
  for(unsigned int i = 0; i < features.size(); i++) {
    const IdealFeature *p = this->getPrevFeature(i), *c = this->getFeature(i), *n = this->getNextFeature(i);
    if(p == NULL) p = c;
    if(n == NULL) n = c;
    
    float crossVal = c->crossSum(p, n);
    crossSum += crossVal;
    float curveVal = c->curveSum(p, n);
    curveSum += curveVal;
    
    centroid += c->crossCentroid(p, n, crossVal, curveVal);
  }
  area = fabs(crossSum/2.f) + curveSum;
  centroid /= 6.f*area*sgn(crossSum);
  //std::cout << "Got area= " << area << "\n";
  return area;
}

float IdealContour::getLength(float startTime, float endTime, V3 pt) const {//TODO: calculate length via the centroid, so this works for curves too!!
  const float rawDist = endTime - startTime;//Oh hey, it's already length-parametricized! That was easy ^_^
  
  int approxSteps = int(ceil(fabs(rawDist)* 30.f));
  V3 prevPt; float dist = 0.f;
  for(int i = 0; i <= approxSteps; i++) {
    float t = startTime + (endTime-startTime)*(float(i)/float(approxSteps));
    V3 evalPt = pt + V3(0.f, 0.f, t);
    this->evaluate(evalPt);
    
    if(i > 0) {
      dist += glm::distance(prevPt, evalPt);
    }
    prevPt = evalPt;
  }
  
  return dist;
}

void IdealContour::evaluate(V3 &v) const {
  float prevPara = 0.f;  auto last = std::prev(features.end());;
  //bool transformed = false;
  //std::cout << "evaluation for " << glm::to_string(v) << "\n";
  for(auto it = features.begin() ; it != features.end(); ++it) {
    if(v.z <= it->para || it == last) {
      float featureDist = (v.z - prevPara) / (it->para - prevPara);
      v.z = 0.f;
      it->evaluate(v, featureDist);
      //transformed = true;
      break;
    } else prevPara = it->para;
  }
  
  //if(!transformed) {
  //  //std::cout << "ERR: no evaluation for " << glm::to_string(v) << "\n";
  //}
}

V3 IdealContour::evaluate(float t) const {
  float prevPara = 0.f;  auto last = std::prev(features.end());;
  V3 v = V3(0.f, 0.f, t);
  //bool transformed = false;
  //std::cout << "evaluation for " << glm::to_string(v) << "\n";
  for(auto it = features.begin() ; it != features.end(); ++it) {
    if(v.z <= it->para || it == last) {
      float featureDist = (v.z - prevPara) / (it->para - prevPara);
      v.z = 0.f;
      it->evaluate(v, featureDist);
      return v;
    } else prevPara = it->para;
  }
  
  //if(!transformed) {
  //  //std::cout << "ERR: no evaluation for " << glm::to_string(v) << "\n";
  //}
  return V3(0.f);
}

V3 IdealContour::getDelta(float t) const {
  float prevPara = 0.f;  auto last = std::prev(features.end());
  const float featEpsilon = 0.05f;
  for(auto it = features.begin() ; it != features.end(); ++it) {
    if(t <= it->para || it == last) {
      float featureDist = (t - prevPara) / (it->para - prevPara);
      t = 0.f;
      return glm::normalize(it->evaluate(featureDist+featEpsilon) - it->evaluate(featureDist));
      //transformed = true;
      break;
    } else prevPara = it->para;
  }
  return V3_UP;
}

Workplane IdealContour::getWorkplane(float t)  {
  V3 pseudoPt = V3(0.f, 0.f, t); this->evaluate(pseudoPt);
  //std::cout << "getWorkplane c=" << glm::to_string(pseudoPt) << ", del=" << glm::to_string(this->getDelta(t)) << "\n";
  return Workplane(pseudoPt, this->getDelta(t));
}

float IdealContour::deltaMag(const float t, const float diff) const {
  float dist = 0.f, zeroDist;
  const float dmOffsetX[4] = {1.f, -1.f, 1.f, -1.f}, dmOffsetY[4] = {1.f, 1.f, -1.f, -1.f};
  V3 a = V3(0.f, 0.f, t), b = V3(0.f, 0.f, t + diff);
  zeroDist = glm::distance(a, b);
  
  for(Uint8 i = 0; i < 4; i++) {
    a = V3(dmOffsetX[i], dmOffsetY[i], t); b = V3(dmOffsetX[i], dmOffsetY[i], t + diff);
    this->evaluate(a); this->evaluate(b);
    dist += abs(glm::distance(a, b) - zeroDist);
  }
  
  return dist/4.f;
}

float IdealContour::getDeltaStep(float z, float epsilon) const {
  float prevPara = 0.f; auto last = std::prev(features.end());;
  for(auto it = features.begin() ; it != features.end(); ++it) {
    if(z <= it->para || (it == last && z <= it->para)) {
      float featureDist = (z - prevPara) / (it->para - prevPara);
      float ret = it->getDeltaStep(featureDist, epsilon) * (it->para - prevPara);//std::max(0.f, nextafterf(it->para, it->para + 100.f) - z));
      
      if(z == it->para || z + ret > it->para) ret = nextafterf(it->para, it->para*2.f) - z;
      //std::cout << "PR: " << it->para << " PRV: " << prevPara << " d" << ret << "\n";
      
      if(it == last) {
        ret = std::min(ret, it->para - z);
      }
      return ret;
    } else prevPara = it->para;
  }
  return 1.f;
}

float IdealContour::getFeatureTime(unsigned int offset) const {
  float ret = 0.f;
  for(auto it = features.begin() ; it != features.end(); ++it) {
    if(offset == 0) break;
    //std::cout << " adding " << it->para << "\n";
    ret = it->para;
    offset--;
  }
  return ret;
}

BakedContourWeights IdealContour::getLevelWeights(float z) const {
  float prevPara = 0.f;  auto last = std::prev(features.end());
  BakedContourWeights ret;
  
  for(auto it = features.begin() ; it != features.end(); ++it) {
    if(z <= it->para) {
      float featureDist = (z - prevPara) / (it->para - prevPara);
      if(it == features.begin()) {
        ret.fromID = Uint8(it - features.begin());
        ret.toID = 0xFF;
        ret.weight = 1.f;
        ret.length = it->para - prevPara;
        ret.thru = 1.f;
        ret.isLast = false;
      } else {
        ret.fromID = Uint8(it - features.begin() - 1);
        ret.toID = Uint8(it - features.begin());
        ret.weight = 1.f-featureDist;
        ret.length = it->para - prevPara;
        ret.thru = featureDist;
        ret.isLast = true;
      }
      break;
    } else if(it == last) {
       ret.fromID = Uint8(it - features.begin());
       ret.toID = 0xFF;
       ret.weight = 1.f;
       ret.length = it->para - prevPara;
       ret.thru = 1.f;
       ret.isLast = true;
    } else prevPara = it->para;
  }
  return ret;
}

float IdealContour::getResistance(float t) const {
  const IdealFeature *const feat = this->getFeature(t);
  if(feat == NULL) return 1.f;
  else return feat->getResistance();
}

bool IdealContour::isOutwardConvex() const {
  return this->contains(centroid);
}
bool IdealContour::contains(V3 pt) const {
  int crossCnt = 0;
  V3 outerPt = bounds.root+glm::max(V3(1.f, 1.f, 0.f), bounds.del*2.f);
  for(const IdealFeature& feat : features) {
    int localCrossCnt = feat.intersectSign(outerPt, pt);
    crossCnt += localCrossCnt;
  }
  
  return (crossCnt != 0);
}

float IdealContour::getVariance() {
  if(variance != 0.f) return variance;
  
  float crossSum = 0.f, curveSum = 0.f, dWeight = 0.f; variance = 0.f; radius = 0.f;
   for(unsigned int i = 0; i < features.size(); i++) {
    const IdealFeature *p = this->getPrevFeature(i), *c = this->getFeature(i), *n = this->getNextFeature(i);
    if(p == NULL) p = c;
    if(n == NULL) n = c;
    
    float crossVal = c->crossSum(p, n);
    crossSum += crossVal;
    float curveVal = c->curveSum(p, n);
    curveSum += curveVal;
    
    V3 lCen = c->crossCentroid(p, n, crossVal, curveVal);
    const float dw = glm::length(lCen);
    const float fw = max(max(glm::distance(centroid, (p->evaluate(0.f))), glm::distance(centroid, (p->evaluate(1.f)))), glm::distance(centroid, (p->evaluate(0.f)+p->evaluate(1.f))/2.f));
    dWeight += dw;
    variance += dw * fw;//glm::distance(centroid, (p->evaluate(0.f)+p->evaluate(1.f))/2.f);
    radius = max(radius, float(fw));
  }
  //area = fabs(crossSum/2.f) + curveSum;
  //centroid /= 6.f*area*sgn(crossSum);
  variance /= 6.f*area*sgn(crossSum)*dWeight;
  
  return variance;
}