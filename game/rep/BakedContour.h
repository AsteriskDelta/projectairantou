/* 
 * File:   BakedContour.h
 * Author: Galen Swain
 *
 * Created on May 15, 2015, 2:03 PM
 */

#ifndef BAKEDCONTOUR_H
#define	BAKEDCONTOUR_H
#include "game/game.h"
#include <lib/V2.h>
#include <lib/V3.h>
#include <vector>

#define CVNULL 0xFFFF
#define FACE_DOT_THRESH 0.1f
#define FACE_DOT_SIMPLIFY 0.1f

struct BakedContourVert {
    V3 pos;//Relative pos
    V3 normal;//Relative normal
    V3 hermite;
    V3 pertPos;
    struct {
        bool linked : 1;
        bool pulled : 1;
        bool lConnect : 1;
        bool rConnect : 1;
        bool proc : 1;
        bool arc : 1;
        bool lRend : 1;
        bool rRend : 1;
    };
    Uint8 childCounter; Uint8 paddA, paddB;
    Uint16 siblingID[2]; Uint16 parent, id;
    float parentDist; Uint8 smoothness;
    inline operator V2() const { return V2(pos.x, pos.y); };
    inline V2 getV2() const { return V2(pos.x, pos.y); };
    
    //I hate myself right now...
    inline BakedContourVert() : pos(V3(0.f)), normal(V3(0.f)), hermite(V3(0.f)), pertPos(V3(0.f)), 
    linked(false), pulled(false),lConnect(false),rConnect(false),proc(false),arc(false),lRend(false),rRend(false),
    childCounter(0), paddA(0), paddB(0), siblingID{CVNULL, CVNULL}, parent(CVNULL), id(CVNULL), parentDist(9999.f), smoothness(0) {
        
    };
    
    inline bool operator==(const BakedContourVert &o) const {
        return pos == o.pos && normal == o.normal && hermite == o.hermite;
    }
    inline bool operator!=(const BakedContourVert& o) const {
        return !(*this == o);
    }
    
    inline void lerpTo(const BakedContourVert &o, float t) {
        const float u = 1.f - t;
        pos = pos*t + o.pos*u;
        normal = normal*t + o.normal*u;
        hermite = hermite*t + o.hermite*u;
        //lengthwise = lengthwise*t + o.lengthwise*u;

    }
    
    inline float distanceTo(const BakedContourVert &o) const {
        return glm::distance(pos, o.pos);
    }
    inline float distanceToXY(const BakedContourVert &o) const {
        return glm::distance(getV2(), o.getV2());
    }
    inline float distanceToSq(const BakedContourVert &o) const {
        return glm::distance2(pos, o.pos);
    }
};

struct BakedContourWeights {
    Uint8 fromID, toID;
    bool isLast;
    float weight, length, thru;
};

class BakedContour {
public:
    BakedContour();
    BakedContour(const BakedContour& orig);
    virtual ~BakedContour();
    
    std::vector<BakedContourVert> verts;
    V3 center, planeNormal;
    Uint16 prevID;
    
    inline Uint16 size() const { return verts.size(); };
    
    inline operator bool() { return verts.size() > 0; };
    inline BakedContour& operator=(const BakedContour &orig) {
       verts = orig.verts;
  center = orig.center;
  planeNormal = orig.planeNormal;
        return *this;
    }
    inline BakedContourVert& operator[](const unsigned int i) {
        return verts.at(i);
    }
    
    void clearFlags();
    
    void splitFilter(float epsilon);
    void simplify(float epsilon);
    
    enum Operand {
        Additive, Add = Additive,
        Subtractive, Subtract = Subtractive,
        OR = Additive,
        XOR, Exclusive = XOR,
        AND, Mutual = AND
    };
    void csgOperand(BakedContour &o, float epsilon, Uint8 op);
    void add(BakedContour &o, float epsilon);
    void subtract(BakedContour &o, float epsilon);
    
    Uint16 getID(const BakedContourVert *const v) const;
    Uint16 getNextSiblingID(Uint16 id) const;
    
    BakedContourVert* addVert(const BakedContourVert &v);
    BakedContourVert& getNearest(const V3 &pos);
    const BakedContourVert& getNearestXY(const V3 &pos) const;
    BakedContourVert& getNearestXYNC(const V3 &pos);
    //Match within close position or near normal
    BakedContourVert& getNearestXYNCNorm(const V3 &pos, const V3& norm);
    BakedContourVert& getNearestXYNCPNorm(const V3 &pos, const V3& norm);
    
    BakedContour lerpTo(BakedContour &o, float t, float epsilon);
    
    float calculateDifference(BakedContour &o) const;
    
    static BakedContour dummy;
    static BakedContourVert dummyVert;
    
    inline BakedContourVert& last() { 
        if(verts.size() == 0) return dummyVert;
        else return *(verts.rbegin());
    };
private:
};

#endif	/* BAKEDCONTOUR_H */

