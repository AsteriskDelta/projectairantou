/* 
 * File:   ContourPlane.h
 * Author: Galen Swain
 *
 * Created on June 4, 2015, 1:56 PM
 */

#ifndef CONTOURPLANE_H
#define	CONTOURPLANE_H

#include "lib/V3.h"

class ContourPlane {
public:
    ContourPlane();
    ContourPlane(const ContourPlane& orig);
    virtual ~ContourPlane();
    
    V3 origin, normal;
private:

};

#endif	/* CONTOURPLANE_H */

