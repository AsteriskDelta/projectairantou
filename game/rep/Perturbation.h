/* 
 * File:   Perturbation.h
 * Author: Galen Swain
 *
 * Created on May 17, 2015, 8:00 PM
 */

#ifndef PERTURBATION_H
#define	PERTURBATION_H
#include "game.h"

class Perturbation {
public:
    Perturbation();
    Perturbation(const Perturbation& orig);
    virtual ~Perturbation();
    
    void evaluate(V3 &vec) const;
    
    template<class Archive>
    void serialize(Archive & archive) {
        _unused(archive);/*
        archive(CEREAL_NVP(low), CEREAL_NVP(mid), CEREAL_NVP(high),
                CEREAL_NVP(lowData.weight), CEREAL_NVP(lowData.sq), 
                CEREAL_NVP(highData.weight), CEREAL_NVP(highData.sq)
                );*/
    }
private:

};

#endif	/* PERTURBATION_H */

