/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BakedContourTrans.cpp
 * Author: Galen Swain
 * 
 * Created on March 17, 2016, 2:10 PM
 */

#include "BakedContourTrans.h"
#include "comp/LineSet.h"
#include <algorithm>
#define BCT_SKIPPED_MAX (32)
#include <glm/gtx/perpendicular.hpp>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#include "engine/ext/polypartition.h"
#pragma GCC diagnostic pop
#include <algorithm>
#include <ParticleSystem.h>
#include "field/WorldFields.h"
#include "field/Compound.h"

BakedContourTrans::BakedContourTrans() : constructBottom(false), constructTop(false) {
}


BakedContourTrans::~BakedContourTrans() {
}

inline V2 gpt(const V3& v) {
  return V2(v.x, v.y);
}


void BakedContourTrans::calculate(BakedContour *a, BakedContour *b, Workplane planeA, Workplane planeB, float epsilon) { _prsguard();
  //std::cout << "PROC contour\n";
  const float epsilonFrac = 2.f;
  const float epsilon2 = (epsilon*epsilon) / (epsilonFrac*epsilonFrac);
  contours[0] = a; contours[1] = b;
  points.clear();
  LineSet sets[4];//a, b, hermites
  //Construct sets
  unsigned int bctID = 0;
  unsigned int secondBCOffset = a->verts.size();
  for(Uint8 cci = 0; cci < 2; cci++) {
    /*for(auto it = contours[cci]->verts.begin(); it != contours[cci]->verts.end(); ++it) {
      auto nt = (std::next(it) == contours[cci]->verts.end())? contours[cci]->verts.begin() : std::next(it);
      
      sets[cci].add(Line(gpt(it->pos), gpt(nt->pos), glm::normalize(gpt((it->hermite+nt->hermite)/2.f))));
    }*/
    unsigned short vertMax = contours[cci]->verts.size();
    //std::cout << "begin conn print:\n";
    for(unsigned short i = 0; i < vertMax; i++) {
      BakedContourVert *vert = &(contours[cci]->verts[i]);
      unsigned short ni = vert->siblingID[1];
      //std::cout << i << "->" << ni << "\n";
      if(ni == 0xFFFF) {
        std::stringstream ss; ss << "Next sibling pointer is null for baked vert " << i;
        Console::Warning(ss.str());
        continue;
      }
      BakedContourVert *next = &(contours[cci]->verts[ni]);
      
      Line toAdd = Line(gpt(vert->pos), gpt(next->pos), glm::normalize(gpt((vert->hermite+next->hermite)/2.f)));
      toAdd.refL = vert->id + cci*secondBCOffset;//cci << 15 | vert->id;
      toAdd.refR = next->id + cci*secondBCOffset;//cci << 15 | next->id;
      toAdd.refV = cci << 15 | vert->id;
      vert->parent = bctID++;
      sets[cci].add(toAdd);
    }
  }
  
  //Intersect points and hermites to generate contour
  unsigned short newPointID = 0;
  for(Uint8 cci = 0; cci < 2; cci++) {
    Workplane &wp = (cci == 0)? planeA : planeB, &owp = (cci == 1)? planeA : planeB;//planeB;
    int dbgVertCount = 0;
    for(auto it = contours[cci]->verts.begin(); it != contours[cci]->verts.end(); ++it) {
      BCTPoint add; Uint8 hSetID = 2;//cci+2;
      if(cci == 0) {
        add.l = it->siblingID[0];
        add.r = it->siblingID[1];
      }
      //std::cout << "Begin line test:\n";
      //Determine highest scoring points of intersection with other contour, first foreward, then backward, selecting for distance
      LineResult inter; {//Make sure we cast a little back on the first, otherwise we'll miss a close/identical contour!!!
        LineResult otherInter = sets[(cci+1)%2].intersectBest(Line(gpt(it->pos)-gpt(it->hermite)*0.01f, gpt(it->pos)+gpt(it->hermite) * 100.f, gpt(it->hermite)));
        inter = sets[(cci+1)%2].intersectBest(Line(gpt(it->pos), gpt(it->pos)+gpt(it->hermite) *-100.f, gpt(it->hermite)));
        if(otherInter.score > inter.score && inter.score == 0.f) inter = otherInter;
      };
      if(inter.line.start == inter.line.end || inter.score == 0.f) {
        inter.line = Line(gpt(it->pos), gpt(it->pos)+gpt(it->hermite) *-100.f);
        
        //std::cout << "Line along has NO intersection(s="<<inter.score<<")!!! " << glm::to_string(it->pos) << " along " << glm::to_string(it->hermite) << "\n";
        inter.pt = inter.line.end;
      }
      
      //Check intersection against existing hermites- replace if we found a better one, or replace others if we are
      Line testLine;
      testLine.start = gpt(it->pos);
      testLine.end = inter.pt;
      testLine.normal = glm::normalize(testLine.end - testLine.start);
      testLine.score = inter.score;
      
      auto intersectedHermites = sets[hSetID].intersectBestCross(testLine, 32);
      //std::cout << "on point: " << glm::to_string(it->pos) << " facing " << glm::to_string(it->hermite) << "\n";
      //std::cout << "casting with " << glm::to_string(testLine.start) << " -> " << glm::to_string(testLine.end) << ", #" << int(cci)<<"."<<dbgVertCount<<"\n";
      bool hermiteOverridden = false;
      add.self = newPointID++;
      if(inter.pt != V2NULL||true) {
        add.l = it->siblingID[0];
        add.r = it->siblingID[1];
      }
      /**/
      for(auto &iline : intersectedHermites) {
        //std::cout << "testing " << glm::to_string(gpt(it->pos)) << " and " << glm::to_string(inter.pt) << " with " << glm::to_string(iline.line.end) << "\n"; 
        float deltaDist = glm::distance(gpt(it->pos), inter.pt) - glm::distance(gpt(it->pos), iline.line.end);
        //std::cout << "found intersecting hermite:\t" << iline.score <<" > " << inter.score << " @ : "<<deltaDist<<"?\n";
        if((iline.score >= inter.score&& deltaDist > 0.f) || iline.line.getLayer() != cci) {
          //inter.line = iline.line;
          if(iline.line.getLayer() == cci) inter.pt = iline.line.end;
          else inter.pt = iline.line.start;
          inter.score = iline.score;
          
          //Add to sequence of points at intersection
          //std::cout << "atempting to reference " << inter.line.ref << "\n";
          /*
          if(iline.line.refs.size() == 0) {
            Console::Error("BakedContourTrans::calculate hermite line reference list is empty!");
          } else if( *(iline.line.refs.begin()) >= points.size()) {
            Console::Error("BakedContourTrans::calculate hermite line reference greater than BCTPoint array!");
          } else {
            unsigned int refID = *(iline.line.refs.begin());
            BCTPoint *target = &points[refID];
            //add.r = target->r;
            //target->r = newPointID;
            //add.l = refID;
            //hermiteOverridden = true;
          }*/
          iline.line.refs.push_back(add.self);
        } else if(iline.score < inter.score) {//Take over the hermite line
          iline.line.end = iline.pt = inter.pt;
          iline.score = inter.score;
          //unsigned int prevLID = inter.line.refL;//points[*iline.refs.begin()]->l;
          for(auto refIter = iline.line.refs.begin(); refIter != iline.line.refs.end(); ++refIter) {
            BCTPoint *alterPt = &points[*refIter];
            alterPt->hermiteIntersect = gpt(V3(inter.pt- iline.line.start, 0.f) );
            if(alterPt->hermiteTo == V3(0.f)) {//bottom
              //alterPt->from = wp.resolve(it->pos);
              alterPt->to = owp.resolve(V3(inter.pt, it->pos.z));
              alterPt->hermiteTo = glm::normalize(V3(inter.pt- iline.line.start, 0.f));
            } else {//top
              alterPt->from = owp.resolve(V3(inter.pt, it->pos.z));
              alterPt->hermiteFrom = glm::normalize(V3(inter.pt- iline.line.start, 0.f));
              //alterPt->to = wp.resolve(it->pos);
            }
            
            //Update siblings
            //unsigned int newRID = (std::next(refIter) == iline.line.refs.end())? add.self : *std::next(refIter);
            //unsigned int newLID = prevLID;
            unsigned int curID = *refIter;
            //points[prevLID].r = curID;
            //points[curID].l = prevLID;
            //points[curID].r = newRID;
            //points[newRID].l = curID;
            
            //prevLID = curID;
            inter.line.refs.push_back(curID);
          }
          //add.r = inter.line.refR;//points[*iline.line.refs.rbegin()].r;
          //hermiteOverridden = true;
          
          //Remove the old hermite line
          sets[hSetID].lines.erase(std::remove(sets[hSetID].lines.begin(), sets[hSetID].lines.end(), 
                  *iline.supra), sets[hSetID].lines.end());
        }
      }
      //std::cout << "\n\n";
      //std::cout << "inter " << glm::to_string(gpt(it->pos)) << "\tto " << glm::to_string(gpt(it->pos)+gpt(it->hermite)*100.f) << "\t scored " << inter.score << " at " << glm::to_string(inter.pt) << "\n";
      if(!hermiteOverridden && inter.score > 0.f) {
        //inter.supra->refR = add.self;
      }
      
      if(cci == 0) {//bottom
        //std::cout << "preres: " << glm::to_string(it->pos) << "\n";
        add.from = wp.resolve(it->pos);
        
        //std::cout << "postres: " << glm::to_string(add.from) << "\n";
        add.to = owp.resolve(V3(inter.pt, it->pos.z));
        add.hermiteFrom = it->hermite;
        add.hermiteTo = V3(0.f);
        add.smoothFrom = it->smoothness; add.smoothTo = 0;
        if(inter.pt != V2NULL&&!hermiteOverridden) {
          add.l = it->siblingID[0];
          add.r = it->siblingID[1];
          //README:I have *no* idea what I was thinking when I interpolated the normals here. If there are issues down the road, LOOK HERE!!!
          //add.hermiteTo = it->hermite;//glm::normalize(V3((inter.line.normal+V2(it->hermite.x, it->hermite.y)), it->hermite.z));
          V3 otherHermite = V3(inter.line.normal, 0.f);//V3(-inter.line.normal.y, inter.line.normal.x, 0.f);
          float blendStr = clamp(std::min(glm::distance2(inter.line.start, inter.pt), glm::distance2(inter.line.end, inter.pt))*100, 0.f, 1.f);
          otherHermite *= sgn(glm::dot(otherHermite, it->hermite));
          add.hermiteTo = glm::normalize(it->hermite*(1.f - blendStr) + otherHermite * blendStr);
          //add.smoothTo = 
          //std::cout << "blending " << glm::to_string(otherHermite) << " li with " << glm::to_string(it->hermite) << " = " << glm::to_string(add.hermiteTo) << "\n";
        }
        /*if(inter.pt != V2NULL && !hermiteOverridden) {
          BCTPoint *nl = &points[inter.line.refL], *nr = &points[inter.line.refR];
          nl->r = add.self; nr->l = add.self;
          add.l = nl->self; add.r = nr->self;
        }*/
      } else {//top
        add.from = owp.resolve(V3(inter.pt, it->pos.z));
        add.to = wp.resolve(it->pos);
        add.hermiteTo = it->hermite;
        add.hermiteFrom = V3(0.f);
        add.smoothTo = it->smoothness; add.smoothFrom = 0;
        //Intersect and update sibling ordering
        if(inter.pt != V2NULL && !hermiteOverridden) {
          //BCTPoint *nl = &points[inter.line.refL], *nr = &points[inter.line.refR];
          //nl->r = add.self; nr->l = add.self;
          //add.l = nl->self; add.r = nr->self;
          //add.hermiteFrom = it->hermite;//glm::normalize(V3((inter.line.normal+V2(it->hermite.x, it->hermite.y)), it->hermite.z));
          V3 otherHermite = V3(inter.line.normal, 0.f);//V3(-inter.line.normal.y, inter.line.normal.x, 0.f);
          float blendStr = clamp(std::min(glm::distance2(inter.line.start, inter.pt), glm::distance2(inter.line.end, inter.pt))*100, 0.f, 1.f);
          otherHermite *= sgn(glm::dot(otherHermite, it->hermite));
          add.hermiteFrom = glm::normalize(it->hermite*(1.f - blendStr) + otherHermite * blendStr);
          //std::cout << "blenditp " << glm::to_string(otherHermite) << " li with " << glm::to_string(it->hermite) << " = " << glm::to_string(add.hermiteFrom) << "\n";
        }
      }
      //add.from = wp.resolve(it->pos); add.to = wp.resolve(it->pos);
      
      if(inter.score > 0.f) add.hermiteIntersect = gpt(V3(inter.pt, 0.f) - it->pos);
      else  add.hermiteIntersect = gpt(it->hermite);
      add.lin = 1.f; add.quad = 0.f;
      add.holy = it->pulled;
      add.preserve = it->arc;
      add.disabled = false;
      add.ccIdx = cci;
      add.matched = false;
      points.push_back(add);
      
      //Add to hermite set (we always want to do this, so a bad intersection can be fixed later!)
      //if(inter.score > 0.f) {
        inter.line.start = gpt(it->pos);
        inter.line.end = inter.pt;
        inter.line.refV = cci << 15 | add.self;
        inter.line.normal = glm::normalize(inter.line.end - inter.line.start);
        //inter.line.normal = V2(inter.line.normal.y, -inter.line.normal.x);
        inter.line.score = inter.score;
        inter.line.refs.push_back(add.self);//point to our newly created object.
        sets[hSetID].add(inter.line);
      //}
      dbgVertCount++;
    }
  }
  
  for(int i = 0; i < int(points.size()); i++) {
    BCTPoint* c = &(points[i]);
    if(c->hermiteFrom == V3(0.f) || c->hermiteTo == V3(0.f) || (V2(c->hermiteTo) == c->hermiteIntersect || V2(c->hermiteFrom) == c->hermiteIntersect)) {
      //std::cout << "FOUND NIN\n";
      c->disabled = true;
    }
  }
  
  
  //With generated contour, calculate winding order for rendering
  //For baseline, we use a comparison based on the from contour, ordering by givens
  float simplifyThresh = epsilon2*2.f;//pow((epsilon), 2.f);
  for(Uint8 cci = 0; cci < 1; cci++) { _prsguard();
    unsigned short vertMax = contours[cci]->verts.size();
    const Uint8 oci = (cci+1)%2;
    //unsigned short prevLeft = points[0].l;
    std::vector<BakedContourVert> &ccVerts = contours[cci]->verts;
    
    for(unsigned short i = 0; i < vertMax; i++) {
      BakedContourVert *const iBCVert = &ccVerts[i%vertMax];
      BCTPoint *pt = &points[iBCVert->parent];
      if(pt->hermiteTo == V3(0.f) || pt->hermiteFrom == V3(0.f) || (V2(pt->hermiteTo) == pt->hermiteIntersect && V2(pt->hermiteFrom) == pt->hermiteIntersect)) {
        pt->disabled = true; continue;
      }
      
      BCTPoint *next = &points[ccVerts[iBCVert->siblingID[1]].parent];//&points[contours[cci]->verts[((i+1)%vertMax)].parent];//pt->r];
      Line connection = Line(gpt(pt->from), gpt(next->from), gpt(glm::normalize(next->from - pt->from))),
           connectionTo = Line(gpt(pt->to), gpt(next->to), gpt(glm::normalize(next->to - pt->to)));
      //connection.expand(); connectionTo.expand();
      //std::cout << "on cci " << i << " -> " << iBCVert->siblingID[1] << "\n";
      for(unsigned short o = 0; o < contours[oci]->verts.size(); o++)  {
        BCTPoint *other = &points[contours[oci]->verts[o].parent];
        if(other->hermiteTo == V3(0.f) || other->hermiteFrom == V3(0.f) || (V2(other->hermiteTo) == other->hermiteIntersect && V2(other->hermiteFrom) == other->hermiteIntersect)) {
          other->disabled = true;
        }
        
        if(other->disabled) continue;
        
        if((/*connectionTo.intersects(gpt(other->to)) && */(connectionTo.intersects(gpt(other->to)) || connection.intersects(gpt(other->from))) || (glm::distance2(gpt(other->from), gpt(pt->from)) < 0.0005f && glm::distance2(gpt(other->to), gpt(pt->to)) < 0.0005f)) && 
                glm::dot(pt->hermiteFrom, other->hermiteFrom) >= 0.f && glm::dot(pt->hermiteTo, other->hermiteTo) >= 0.f) {
          //std::cout << glm::to_string(other->from) << " crosses " << glm::to_string(connection.start) << "->" << glm::to_string(connection.end) << "\n";
          float dist2 = std::max(glm::distance2(pt->from, other->from), glm::distance2(pt->to, other->to));
          std::cout << "simplify " << i<<", " << glm::to_string(pt->from) << " & " << glm::to_string(other->from) << "? " << dist2 << " > " << simplifyThresh << "\n";
          if(dist2 < simplifyThresh) {
            std::cout << "SIMPLIFYING\n";
            if(other->holy && pt->holy ){
              other->disabled = true;
              pt->from = (pt->from+other->from)/2.f;
              pt->to = (pt->to + other->to)/2.f;
              pt->hermiteFrom = glm::normalize(pt->hermiteFrom + other->hermiteFrom);
              pt->hermiteTo = glm::normalize(pt->hermiteTo + other->hermiteTo);
              //std::cout << "double holy\n";
            } else if(other->holy && !pt->holy) {
              //std::cout << "other is holy\n";
              //prev->r = other->self;
              //other->l = prev->self;
              other->from = (pt->from+other->from)/2.f;
              other->to = (pt->to + other->to)/2.f;
              other->hermiteFrom = glm::normalize(pt->hermiteFrom + other->hermiteFrom);
              other->hermiteTo = glm::normalize(pt->hermiteTo + other->hermiteTo);
              pt->disabled = true;
              //pt = other;
            } else if(pt->holy && !other->holy) {
              
              //std::cout << "disabling other\n";
              pt->from = (pt->from+other->from)/2.f;
              pt->to = (pt->to + other->to)/2.f;
              pt->hermiteFrom = glm::normalize(pt->hermiteFrom + other->hermiteFrom);
              pt->hermiteTo = glm::normalize(pt->hermiteTo + other->hermiteTo);
              other->disabled = true;
            } else {
              pt->from = (pt->from+other->from)/2.f;
              pt->to = (pt->to + other->to)/2.f;
              pt->hermiteFrom = glm::normalize(pt->hermiteFrom + other->hermiteFrom);
              pt->hermiteTo = glm::normalize(pt->hermiteTo + other->hermiteTo);
              other->disabled = true;
            } 
          } else {
            
          }
          std::cout << "postS: pt=" << pt->disabled << ", other=" <<other->disabled << "\n";
              pt->smoothFrom = round((pt->smoothFrom + other->smoothFrom)/2.f);
              pt->smoothTo = round((pt->smoothTo+ other->smoothTo)/2.f);
          pt->r = other->self;
          next->l = other->self;
          other->r = next->self;
          other->l = pt->self;
          
          //std::cout << "linking " << other->l<< " to " << pt->r << "\n";
          
          pt = other;
        } 
      }
      pt->r = next->self;
      next->l = pt->self;
      
      //std::cout << "end linking " << next->l << " to " << pt->r << "\n";
      //prevLeft = i;
    }
  }
  //Final pass to simplify close endpoints
  for(int i = 0; i < int(points.size()); i++) {
    BCTPoint* c = &(points[i]), *l, *r, *rr;//Current, left, right, and right-of-right
    if(c->disabled) {
      std::cout << i << " DIS\n";
      continue;
    } else if(c->hermiteTo == V3(0.f) || c->hermiteFrom == V3(0.f) || (V2(c->hermiteTo) == c->hermiteIntersect && V2(c->hermiteFrom) == c->hermiteIntersect)) {
      //std::cout << "found NULL hermite on " << i << "\n";
      c->disabled = true;
      continue;
    }
    l = &points[c->l]; r = &points[c->r];
    Uint8 chkCounter = 0;
    while(l->disabled && (chkCounter++) < BCT_SKIPPED_MAX) l = &points[l->l];
    chkCounter = 0;
    while(r->disabled && (chkCounter++) < BCT_SKIPPED_MAX) r = &points[r->r];
    rr = &points[r->r];
    chkCounter = 0;
    while(rr->disabled && (chkCounter++) < BCT_SKIPPED_MAX) rr = &points[rr->r];
    chkCounter = 0;
    
    l->r = c->self;
    c->l = l->self;
    r->l = c->self;
    c->r = r->self;
    
    if(rr != NULL) {
      Line hLineCurrent = Line(gpt(l->from), gpt(c->from)), hLineNext = Line(gpt(r->from), gpt(rr->from));
      Line tLineCurrent = Line(gpt(l->to), gpt(c->to)), tLineNext = Line(gpt(r->to), gpt(rr->to));
      hLineCurrent.expand(); tLineCurrent.expand();
      if(hLineCurrent.intersects(hLineNext) != V2NULL || tLineCurrent.intersects(tLineNext) != V2NULL  || tLineCurrent.intersects(hLineNext) != V2NULL || hLineCurrent.intersects(tLineNext) != V2NULL) {//Prevent siblings from forming a loop
        //std::cout << "found crossing siblings on i="<<i<<"! rPres="<<r->preserve<<" and cPres=" << c->preserve << "\n";
        //std::swap(c->from, r->from);
        //std::swap(c->hermiteFrom, r->hermiteFrom);
        //std::swap(c->to, r->to);
        //std::swap(c->hermiteTo, r->hermiteTo);
        //c->hermiteFrom *= 100.f; c->hermiteTo *= 100.f;
        //c->from = r->from;
        //c->to = r->to;
        //c->hermiteFrom = r->hermiteFrom;
        //c->hermiteTo = r->hermiteTo;
        if(!r->preserve) {//Our siblings just changed, so make sure we re-iterate over everything that's changed
          r->disabled = true;
          i--;
        } else {
          c->disabled = true;
          i = std::min(int(r->self), std::min(int(l->self) - 1, i-1));
        }
        //c->r = rr->self;
        //rr->l = c->self;
      }
    }
    
    //if(glm::dot(c->from - l->from, c->from - r->from) > 0.9f || glm::dot(c->to - l->to, c->to - r->to) > .9f) {
    //  std::cout << "!!!!Found doubleback\n";
    //  //c->disabled = true; r->disabled = true;
    //  c->hermiteFrom *= 100.f; c->hermiteTo *= 100.f;
    //}
    
    //std::cout << "cv: " << glm::to_string(c->from - l->from) << " == " << glm::to_string((r->from - c->from)) << " || " <<
    //        glm::to_string((c->to - l->to)) << " == " << glm::to_string(r->to - c->to) << "\n";
    //if((c->from - l->from) == (r->from - c->from) || (c->to - l->to) == (r->to - c->to)) {
    //  std::cout << "Found doubling back!\n";
    //  c->disabled = true;
    //}
    
    //std::cout << "fsimp: " << glm::to_string(c->from) << "\n";
    /*
    float avgWeight = 0.5;
    
    while(
            (glm::distance2(c->from, r->from) < simplifyThresh && glm::dot(c->hermiteFrom, r->hermiteFrom) > 0.25) &&
            (glm::distance2(c->to, r->to) < simplifyThresh && glm::dot(c->hermiteTo, r->hermiteTo) > 0.25) && 
            (chkCounter++) < BCT_SKIPPED_MAX) {
      BCTPoint *const other = r, *const pt = c;
      //std::cout << "presim " << glm::to_string(pt->from) << " -> ";
      pt->from = (pt->from*avgWeight + other->from)/(avgWeight+1.f);
      
      //std::cout << "post " << glm::to_string(pt->from) << "\n";
      pt->to = (pt->to*avgWeight  + other->to)/(avgWeight+1.f);
      pt->hermiteFrom = glm::normalize(pt->hermiteFrom*avgWeight  + other->hermiteFrom * (1.f-avgWeight));
      pt->hermiteTo = glm::normalize(pt->hermiteTo*avgWeight  + other->hermiteTo * (1.f-avgWeight));
      pt->smoothFrom = round((pt->smoothFrom*avgWeight + other->smoothFrom* (1.f-avgWeight)));
      pt->smoothTo = round((pt->smoothTo*avgWeight+ other->smoothTo* (1.f-avgWeight)));
      avgWeight += 1.f;
      other->disabled = true;
      r = other;
      //while(r->disabled && (chkCounter++) < BCT_SKIPPED_MAX) r = &points[r->r];
      
    }*/
    //chkCounter = 0;
    //while(r->disabled && (chkCounter++) < BCT_SKIPPED_MAX) r = &points[r->r];
    
    //Final simplify pass for hermites
    
    //c->l = l->self;
    //c->r = r->self;
  }
  
  for(unsigned short i = 0; i < points.size(); i++) {
    BCTPoint* c = &(points[i]), *l, *r;//, *rr;//Current, left, right, and right-of-right
    
    //std::cout << "\t" << c->l << " -> " << c->self << " ->" << c->r << " " << (c->disabled? " DISABLED" : "") << "\n";
    if(c->disabled) continue;
    l = &points[c->l]; r = &points[c->r];
    Uint8 chkCounter = 0;
    while(l->disabled && (chkCounter++) < BCT_SKIPPED_MAX) l = &points[l->l];
    chkCounter = 0;
    while(r->disabled && (chkCounter++) < BCT_SKIPPED_MAX) r = &points[r->r];
    
    l->r = c->self;
    c->l = l->self;
    r->l = c->self;
    c->r = r->self;
    
    //Simplify pass
    float dfl = glm::distance2(l->from, c->from), dfr = glm::distance2(c->from, r->from);
    float dtl = glm::distance2(l->to, c->to), dtr = glm::distance2(c->to, r->to);
    _unused(dfl); _unused(dtl);
    if(dfr < epsilon2 && (r->holy || !c->holy)) {
      c->from = r->from;
    }
    if(dtr < epsilon2 && (r->holy || !c->holy)) {
      c->to = r->to;
    }
    
    if(l->r != c->self || r->l != c->self || c->l == c->r || r->r == c->r) {
      //std::cout << "SIBLINGS DONT MATCH!!!\n";
      c->disabled = true;
    }
  }
}

void BakedContourTrans::capWith(float epsilon, bool isUp) { _prsguard();
  _unused(epsilon);
  for(unsigned int i = 0; i < points.size(); i++) {
    if(points[i].disabled) continue;
    BCTPoint *pt = &points[i];
    
    if(isUp) {
      pt->hermiteTo = glm::normalize(pt->hermiteTo + V3_FORWARD);//hermite;
    } else {
      pt->hermiteFrom = glm::normalize(pt->hermiteFrom - V3_FORWARD);;//hermite;
    }
  }
}

void BakedContourTrans::stitchWith(float epsilon, BakedContourTrans &o) { _prsguard();
  _unused(epsilon);//Link all uppers from other to lowers in this
  if(o.points.size() < 2 || points.size() < 2) return;
  return;
  for(unsigned int i = 0; i < o.points.size(); i++) {
    BCTPoint  *opt = &o.points[i];
    opt->matched = false;
  }
  
  for(unsigned int i = 0; i < points.size(); i++) {
    if(points[i].disabled) continue;
    BCTPoint *pt = &points[i], *opt = o.getClosestPoint(pt->getPos(0.f), 1.f);
    if(opt == NULL) continue;
    
    pt->from = opt->to;
    pt->hermiteFrom = opt->hermiteTo;
    pt->matched = opt->matched = true;
    
    //BCTPoint *l = &points[pt->l], *r = &points[pt->r];
    //Uint8 chkCounter = 0;
    //while(l->disabled && (chkCounter++) < BCT_SKIPPED_MAX) l = &points[l->l];
    //while(r->disabled && (chkCounter++) < BCT_SKIPPED_MAX) r = &points[r->r];
    
    //V3 across = glm::normalize(r->from - l->from);
    V3 up = V3(0.f, 0.f, 1.f);
    V3 dfDz = glm::normalize(pt->to - opt->from);
    V3 right = glm::normalize(glm::cross(pt->hermiteFrom, up));
    V3 hermite = glm::cross(right, dfDz);
    hermite *= sgn(glm::dot(hermite, pt->hermiteFrom));
    pt->hermiteFrom = opt->hermiteTo = hermite;
  }
  
  for(unsigned int i = 0; i < o.points.size(); i++) {
    BCTPoint  *opt = &o.points[i];
    if(!opt->disabled && !opt->matched) {
      BCTPoint *pt = this->getClosestPoint(opt->getPos(1.f), 0.f);
      if(pt == NULL) continue;
      
      opt->to = pt->from;
      opt->hermiteTo = pt->hermiteFrom;
      opt->matched = true;
      /*
      V3 up = V3(0.f, 0.f, 1.f);
      V3 dfDz = glm::normalize(pt->to - opt->from);
      V3 right = glm::normalize(glm::cross(pt->hermiteFrom, up));
      V3 hermite = glm::cross(right, dfDz);
      hermite *= sgn(glm::dot(hermite, pt->hermiteFrom));
      pt->hermiteFrom = opt->hermiteTo = hermite;
       */
    }
  }
}

void BakedContourTrans::shareWith(float epsilon, BakedContourTrans &o) { _prsguard();
  _unused(epsilon);
}

void BakedContourTrans::construct(WorldObject *const wObj, float epsilon, WMMeshType* baseMesh, WMMeshType *const dbg, bool invertFaces, ParticleSystem *emitSys) { _prsguard();
  if(baseMesh == NULL) {
    Console::Error("BakedContourTrans::construct() called with NULL baseMesh!");
    return;
  }
  //std::cout << "dbg verts:\n";
  //std::cout << "constructing with " << points.size() << "\n";
  for(unsigned short i = 0; i < points.size(); i++) {
    BCTPoint* c = &(points[i]), *l, *r;//Current, left, right
    l = &points[c->l]; r = &points[c->r];
    if(c->disabled) {
      //std::cout << i << " is disabled\n";
      continue;
    }
    float featLength = glm::distance(c->from, c->to);
    //Add and register unique verts
    for(float t = 0.f; t <= 1.f; ) {
      WObjectVert vert; unsigned int vid;
      
      vert.pos = c->getPos(t);
      vert.normal = vert.sourceNormal = c->getHermite(t);
      vert.smoothness = c->getSmoothness(t);//(c->holy)? 0 : 255;
      
      
      //float deltaT = 0.1f;
      //deltaT is relative to the compound and the given coordinate- assign texture data here, as well
      WorldObject::VertCompoundInfo vertCompoundInfo = wObj->assignTextureCoords(&vert, epsilon);
      float deltaT = vertCompoundInfo.subdivLength/featLength;
      //std::cout << "dT = " << deltaT << "\n";
      
      //std::cout << glm::to_string(c->from) << " vert to " << glm::to_string(c->to) << "\n";
      vid = baseMesh->addVert(vert);
      //std::cout << "v_"<<vid<<" = " << glm::to_string(vert.pos) << "\n";
      if(dbg) {
        //vert.coordA = V2(0.f, 1.f); vert.coordB = V2(0.f, 0.f);
        vert.sourcePos = V3(0.f, 1.f, 0.f);
        dbg->addVert(vert);
        //std::cout << "uploading vert: " << glm::to_string(vert.pos) << " -> " << glm::to_string(vert.pos + vert.normal) << "\n";
        vert.pos += vert.normal;//V3(c->hermiteIntersect.x, c->hermiteIntersect.y, 0.f);//c->hermite;
        //vert.coordA = V2(1.f, 0.0f); vert.coordB = V2(1.f, 0.f);1.5
        vert.sourcePos = V3(1.f, 0.f, 1.f);
        dbg->addLineInd(vid, dbg->addVert(vert));
        baseMesh->addVert(vert);
      }
      c->addVertID(t, vid);
      
      if(t == 1.f) break;
      t = std::min((t+deltaT), 1.f);
    }
  }
  
  //Build connected faces
  
  for(unsigned short i = 0; i < points.size(); i++) {
    BCTPoint* c = &(points[i]), *l, *r;//Current, left, right
    if(c->disabled) continue;
    //std::cout << "Building via " << c->l << "->"<<c->self<<"->"<<c->r<<"\n";
    if(c->r >= points.size()) {
      //std::cout << "skipping " << i << "\n";
      continue;
    }
    l = &points[c->l]; r = &points[c->r];
    //std::cout << "starting with l=" << l->self << ", r=" << r->self << "\n";
    Uint8 chkCounter = 0;
    while(l->disabled && (chkCounter++) < BCT_SKIPPED_MAX) l = &points[l->l];
    while(r->disabled && (chkCounter++) < BCT_SKIPPED_MAX) r = &points[r->r];
    //std::cout << "ending with l=" << l->self << ", r=" << r->self << ", chekCounter = " << int(chkCounter) << " < 10?\n";
    //std::cout << "stitched " << l->self << "\t"<<c->self<<"\t"<<r->self<<"\n";
    
    unsigned int prevVertID = 0xFFFFFFFF; float prevT = 0.f;
    
    for(auto it = c->vts.begin(); it != c->vts.end(); ++it) {
      unsigned int rVertID = r->getVertID(it->t), lVertID = l->getVertID(it->t);
      unsigned int sVertID = it->vid;
      
      if(dbg) dbg->addLineInd(sVertID, rVertID);
      
      if(prevVertID != 0xFFFFFFFF) {
        if(dbg) dbg->addLineInd(sVertID, prevVertID);
        
        unsigned int rPrevVertID = r->getVertID(prevT);
        V3 dPoint = glm::normalize(r->getPos(it->t) - c->getPos(it->t));//glm::normalize(baseMesh->bufferVert[it->vid].pos - baseMesh->bufferVert[rVertID].pos);
        V3 dUp = glm::normalize(c->to - c->from);
        //float dLength = glm::dot(glm::normalize(c->to - c->from), glm::cross(dPoint, c->getHermite(it->t)));
        float dLength = glm::dot(glm::cross(dPoint, dUp), glm::normalize((c->getHermite(it->t)+r->getHermite(it->t))/2.f));
        V3 avgHermite = (baseMesh->bufferVert[lVertID].normal +
                baseMesh->bufferVert[rVertID].normal +
                baseMesh->bufferVert[sVertID].normal + 
                baseMesh->bufferVert[rPrevVertID].normal)/4.f;
        V3 avgPos = (baseMesh->bufferVert[lVertID].pos +
                baseMesh->bufferVert[rVertID].pos +
                baseMesh->bufferVert[sVertID].pos + 
                baseMesh->bufferVert[rPrevVertID].pos)/4.f;
        V3 midA = glm::normalize((baseMesh->bufferVert[rPrevVertID].pos + baseMesh->bufferVert[sVertID].pos)/2.f-avgPos);
        V3 midB = glm::normalize((baseMesh->bufferVert[prevVertID].pos + baseMesh->bufferVert[rVertID].pos)/2.f-avgPos);
        bool rev = glm::dot(avgHermite, midA) > glm::dot(avgHermite, midB);
        
        if((dLength < 0.f)^invertFaces) {//Inner facing
          if(!rev) {
            baseMesh->addTriInd(prevVertID, sVertID, rVertID);
            baseMesh->addTriInd(prevVertID, rVertID, rPrevVertID);
            
            this->makeEmits(prevVertID, sVertID, rVertID, baseMesh, emitSys);
            this->makeEmits(prevVertID, rVertID, rPrevVertID, baseMesh, emitSys);
          } else {
            baseMesh->addTriInd(sVertID, rVertID, rPrevVertID);
            baseMesh->addTriInd(prevVertID, sVertID, rPrevVertID);
            
            this->makeEmits(sVertID, rVertID, rPrevVertID, baseMesh, emitSys);
            this->makeEmits(prevVertID, sVertID, rPrevVertID, baseMesh, emitSys);
          }
        } else {//Outer
          if(!rev) {
            baseMesh->addTriInd(prevVertID, rVertID,sVertID);
            baseMesh->addTriInd(prevVertID,rPrevVertID, rVertID);
            
            this->makeEmits(prevVertID, rVertID, sVertID, baseMesh, emitSys);
            this->makeEmits(prevVertID, rPrevVertID, rVertID, baseMesh, emitSys);
          } else {
            baseMesh->addTriInd(sVertID, rPrevVertID, rVertID);
            baseMesh->addTriInd(prevVertID, rPrevVertID, sVertID);
            
            this->makeEmits(sVertID, rPrevVertID, rVertID, baseMesh, emitSys);
            this->makeEmits(prevVertID, rPrevVertID, sVertID, baseMesh, emitSys);
          }
        }
        
      }
      
      prevVertID = sVertID;
      prevT = it->t;
    } 
  }
  
  for(Uint8 endCase = 0; endCase <= 1; endCase++) { _prsguard();
    bool constructSwitch; float getT;
    if(endCase == 0) {
      getT = 0;
      constructSwitch = constructBottom;
    } else {
      getT = 1.0;
      constructSwitch = constructTop;
    }
    
    if(constructSwitch && points.size() >= 3) {
      bool builtFace = false; Uint8 faceOrder = 0;
      while(!builtFace && faceOrder <= 1) {
        
      //std::cout << "constructing end for case #" << int(endCase) << "\n";
      //Compose poly structure
      TPPLPoly inPoly; std::list<TPPLPoly> polys;
      V3 prevPos = V3_NULL;
      std::vector<bool> ptsProc(points.size());
      std::list<TPPLPoint> ptsFin;
      std::fill(ptsProc.begin(), ptsProc.begin()+points.size(), false);
      Uint8 chkCounter = 0;
      Uint16 leadingPt = 0;//Wind through all existing points, forming all ones that make a complete polygon
      while(leadingPt < points.size()) {
        BCTPoint *c = &points[leadingPt], *n = NULL;
        
        if(faceOrder == 0) {
          if(c->l < points.size()) n = &points[c->l];
          while(n->disabled && (chkCounter++) < BCT_SKIPPED_MAX) n = &points[n->l];
        } else {
          if(c->r < points.size()) n = &points[c->r];
          while(n->disabled && (chkCounter++) < BCT_SKIPPED_MAX) n = &points[n->r];
        }
        if(!c->disabled) {
          while(n != NULL && (!ptsProc[c->self])) {
            V3 newPos = c->getPos(getT);
            if(glm::distance2(prevPos, newPos) > 0.001f && !c->disabled) {
              ptsFin.push_back(TPPLPoint{newPos.x, newPos.y});
        //std::cout << "adding " << c->self << " to triang, f= " << glm::to_string(c->from) << "\n";
            }
            prevPos = newPos;

            //if(ptsProc[c->self]) break;
            
            ptsProc[c->self] = true;
            c = n;
            if(c->l > points.size()) n = NULL;
            else {
              chkCounter = 0;
              if(faceOrder == 0) {
                if(c->l < points.size()) n = &points[c->l];
                while(n->disabled && (chkCounter++) < BCT_SKIPPED_MAX) n = &points[n->l];
              } else {
                if(c->r < points.size()) n = &points[c->r];
                while(n->disabled && (chkCounter++) < BCT_SKIPPED_MAX) n = &points[n->r];
              }
              //n = &points[c->r];
              //while(n->disabled && (chkCounter++) < BCT_SKIPPED_MAX) n = &points[n->r];
            }
          }
        }
        ptsProc[leadingPt] = true;
        while(ptsProc[leadingPt]) leadingPt++;
      }

      inPoly.Init(ptsFin.size()); unsigned short polyI = 0;
      for(auto it = ptsFin.begin(); it != ptsFin.end(); ++it) {
        inPoly[polyI] = *it;
        //std::cout << "\tadd pt: (" << inPoly[polyI].x <<", "<< inPoly[polyI].y<<")\n";
        polyI++;
      }


      //Pass, monotonize, and triangulate
      TPPLPartition pp; std::list<TPPLPoly> tris; int res = 1;
      //res = pp.ConvexPartition_HM(&inPoly, &polys);
      //polys.push_back(inPoly);
      std::list<TPPLPoly> tmpPoly; tmpPoly.push_back(inPoly);
      //res = pp.RemoveHoles(&tmpPoly, &polys);
      res = pp. ConvexPartition_HM(&inPoly, &polys);
      //std::cout << " Convex partition: " << res << " returning " << polys.size() << " of " << polys.begin()->GetNumPoints()<<"\n";
      if(polys.size() > 0 && polys.begin()->GetNumPoints() >= 3) {
        for(auto it = polys.begin(); it != polys.end(); ++it) {
          res = pp.Triangulate_OPT(&(*it), &tris);
          //std::cout << " Triangulate :" << res << " returning up to" << tris.size() << "\n";
        }
        _unused(res);

        //Recompose to vertex structure - points are unique due to preemptive mesh simplification, so associate and reconstruct
        for(auto it = tris.begin(); it != tris.end(); ++it) {
          if(it->GetNumPoints() != 3) {
            Console::Warning("Triangulation returned non-triangle mesh!");
            continue;
          }

          BCTPoint *a = this->getFirstPoint(V2(it->GetPoint(0).x, it->GetPoint(0).y), getT),
                   *b = this->getFirstPoint(V2(it->GetPoint(1).x, it->GetPoint(1).y), getT),
                   *c = this->getFirstPoint(V2(it->GetPoint(2).x, it->GetPoint(2).y), getT);

          if(getT == 0.f) baseMesh->addTriInd(b->getVertID(getT), a->getVertID(getT), c->getVertID(getT));
          else baseMesh->addTriInd(a->getVertID(getT), b->getVertID(getT), c->getVertID(getT));
        }
        builtFace = true;
      }
      
      faceOrder++;
    }
    }
  }
  
  return;/*
  for(unsigned short i = 0; i < points.size(); i++) {
    BCTPoint* c = &(points[i]), *l, *r;//Current, left, right
    if(i == 0) l = &points[points.size()-1];
    else l = &points[i-1];
    r = &points[(i+1)%points.size()];
    
    WObjectVert vert;
    vert.pos = c->from;
    vert.normal = c->hermiteFrom;
    vert.smoothness = 0;
    
    if(baseMesh) c->id = baseMesh->addVert(vert);
    if(dbg) {
      vert.coordA = V2(0.f, 1.f); vert.coordB = V2(0.f, 0.f);
      dbg->addVert(vert);
    }
    
    vert.pos = c->to;//c->from-V3(0.f,0.f,1.f);
    vert.normal = c->hermiteTo;
    vert.smoothness = 0;
    
    if(baseMesh) baseMesh->addVert(vert);
    if(dbg) {
      vert.coordA = V2(0.f, 1.f); vert.coordB = V2(0.f, 0.f);
      dbg->addVert(vert);
    }
    
    if(dbg) dbg->addLineInd(c->id, c->id+1);
  }
  
  for(unsigned short i = 0; i < points.size(); i++) {
    BCTPoint* c = &(points[i]), *l, *r;//Current, left, right
    //if(i == 0) l = &points[points.size()-1];
    //else l = &points[i-1];
    //r = &points[(i+1)%points.size()];
    l = &points[c->l]; r = &points[c->r];
    
    if(c->r != 0xFFFF) dbg->addLineInd(c->id, points[c->r].id);
    //dbg->addLineInd(c->id, r->id);
    //dbg->addLineInd(c->id+1, r->id+1);
    
    //Debug hermite
    WObjectVert vert;
    vert.pos = c->from + c->hermiteFrom;//V3(c->hermiteIntersect.x, c->hermiteIntersect.y, 0.f);//c->hermite;
    vert.coordA = V2(1.f, 0.0f); vert.coordB = V2(1.f, 0.f);
    vert.normal = c->hermiteFrom;
    vert.smoothness = 0;
    if(dbg) {
      dbg->addLineInd(c->id, dbg->addVert(vert));
    }
    vert.pos = c->to + c->hermiteTo;//V3(c->hermiteIntersect.x, c->hermiteIntersect.y, 0.f);//c->hermite;
    vert.coordA = V2(1.f, 0.0f); vert.coordB = V2(1.f, 0.f);
    vert.normal = c->hermiteTo;
    vert.smoothness = 0;
    if(dbg) {
      dbg->addLineInd(c->id+1, dbg->addVert(vert));
    }
  }*/
}
unsigned int BakedContourTrans::getFirstPointID(V2 p, float t) { _prsguard();
  for(unsigned int i = 0; i < points.size(); i++) {
    if(points[i].disabled) continue;
    V3 lPt = points[i].getPos(t);
    if(glm::distance2(gpt(lPt), p) < 0.001f) {
      //std::cout << "matching " << glm::to_string(p) << " to " << glm::to_string(lPt) << " gives i=" << i << "\n";
      return i;
    }
    //if(lPt.x == p.x && lPt.y == p.y) return i;
  }
  //for(unsigned int i = 0; i < points.size(); i++) {
  //  if(points[i].disabled) continue;
  //  V3 lPt = points[i].getPos(t);
  //  if(glm::distance2(gpt(lPt), p) < 0.1f) return i;
    //if(lPt.x == p.x && lPt.y == p.y) return i;
  //}
  std::stringstream ss;
  ss << "BakedContourTrans::getFirstPointID(" << glm::to_string(p) << ", " << t << ") couldn't find a suitable point!";
  Console::Warning(ss.str());
  return 0;
}

BCTPoint* BakedContourTrans::getFirstPoint(V2 p, float t) { _prsguard();
  return &points[getFirstPointID(p, t)];
}

BCTPoint* BakedContourTrans::getClosestPoint(V3 p, float t) { _prsguard();
  float bestDist = 99999.f;
  BCTPoint *bestPoint = NULL;
  for(unsigned int i = 0; i < points.size(); i++) {
    if(points[i].disabled) continue;
    
    V3 point = points[i].getPos(t);
    float dist = glm::distance2(p, point);
    if(dist < bestDist) {
      bestPoint = &points[i];
      bestDist = dist;
    }
  }
  return bestPoint;
}

void BakedContourTrans::makeEmits(unsigned int ai, unsigned int bi, unsigned int ci, WMMeshType *mesh, ParticleSystem *sys) { _prsguard();
  if(sys == NULL || mesh == NULL) return;
  
  WObjectVert *va = &mesh->vert(ai), *vb = &mesh->vert(bi), *vc = &mesh->vert(ci);
  V3 off = va->pos;
  V3 axisA = vb->pos - va->pos;
  V3 axisB = vc->pos - va->pos;
  V3 out = glm::cross(axisA, axisB);
  float area = glm::length(out);
  
  //Extract all compounds to use and their respective weights
  struct CompoundLK {
    float w;
    Compound *c;
  };
  std::list<CompoundLK> compounds = std::list<CompoundLK>();
  for(Uint8 i = 0; i < 3; i++) { _prsguard();//Holy god this is terrible code... each() operator FTW
    WObjectVert *v;
    if(i == 0) v = va;
    else if(i == 2) v = vb;
    else v = vc;
    
    Compound *ca = WorldFields::active()->getCompound(v->matA), *cb = WorldFields::active()->getCompound(v->matB);
    for(Compound *c : std::list<Compound*>({ca, cb})) {
      float w = (c == ca)? float(v->weightA)/255.f : float(v->weightB)/255.f;
      if(w == 0.f) continue;
      
      bool doIns = true;
      for(CompoundLK& lk : compounds) {
        if(c != lk.c) continue;
        doIns = false;
        lk.w = std::max(w, lk.w);
      }
      
      if(doIns) compounds.push_back(CompoundLK{w, c});
    }
  }
  
  //For each compound, emit as required
  for(CompoundLK& lk : compounds) { _prsguard(); 
    for(CompoundRenderInfo::TexEmit& par : lk.c->renderInfo.emits) {
      const float density = lk.w * par.density;
      const int count = ceil(density * area);
      for(int i = 0; i < count; i++) {
        //Randomly sample {count} points within the passed tris, and emit a particle along {out}
        float av = (rand()%10000)/10000.f, bv = (rand()%10000)/10000.f;
        if(bv > 1.f-av) {
          bv = 1.f - bv; av = 1.f - av;
        }
        
        ParticleVert *pv = sys->addVert();
        if(pv == NULL) continue;
        
        pv->size = par.getSize();
        pv->figureID = par.particleID;
        pv->pos = off + axisA * av + axisB * bv + out*(pv->size.y*0.4f);
        
        //Get random point on the surface of an elipsoid for normals
        float outFrac = (rand()%100)/100.f*0.4f + 0.2f;
        float avFrac = (rand()%100)/50.f - 1.f, bvFrac = (rand()%100)/50.f - 1.f;
        if(avFrac == 0.f && bvFrac == 0.f) avFrac = 0.1f;
        const float vLen = (avFrac*avFrac + bvFrac*bvFrac);
        avFrac /= vLen; bvFrac /= vLen;
        pv->normal = glm::normalize(outFrac*out + avFrac*axisA + bvFrac*axisB);
        
        pv->detA = 0;
        pv->detB = 0;
      }
    }
  }
}