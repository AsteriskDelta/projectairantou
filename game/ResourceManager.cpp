/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ResourceManager.cpp
 * Author: Galen Swain
 * 
 * Created on March 5, 2016, 11:08 AM
 */

#include <lib/AppData.h>
#include <lib/Image.h>
#include <client/Texture.h>
#include "ResourceManager.h"
#include <vector>
#include <unordered_map>
#include "Spider.h"
#include "WorldManager.h"
#include "field/WorldFields.h"
#include <client/UniformBlock.h>
#include <client/TextureAtlas.h>
#include <unistd.h>

namespace ResourceManager {
  std::unordered_map<std::string, unsigned short> textureLookup;
  std::unordered_map<std::string, unsigned short> atlasLookup;
  std::unordered_map<unsigned short, unsigned short> atlasIDMap;
  std::unordered_map<std::string, ImageRGBA*> imageLookup;
  Texture<true>* diffTex;
  UniformBlock *diffUB;
  std::vector<CompoundRenderInfo::GPUStruct> diffUBVector;
  bool ubDirty = true;
  
  TextureAtlas *particleAtlas;
  
  const unsigned short maxTex = 16;
  unsigned short cntTex = 1;
  unsigned short cntAtlas = 1;
  
  Mutex imgMutex;
  
  bool Initialize() {
    diffTex = new Texture<true>(TextureFormat::Unsigned, 8, 4, maxTex);
    diffTex->setMipmap(true);
    diffTex->setMipmapCount(0);
    diffTex->setSamples(1);
    diffTex->setWrap(TextureWrap::Repeat);
    diffTex->setFilter(TextureFilter::Linear);
    diffTex->setSize(1024,1024);
    diffTex->commit();
    diffTex->allocate();
    
    particleAtlas = new TextureAtlas(2048, 2048, 2);
    particleAtlas->setName("particleAtlas");
    particleAtlas->useCache();
    particleAtlas->upload();
    
    diffUB = new UniformBlock();
    diffUB->allocate();
    diffUBVector.resize(256);
    
    ImageRGBA white(1024,1024);
    white.fill(ColorRGBA(255,255,255,255));
    diffTex->upload(&white,1,1,0);
    return true;
  }
  
  bool Release() {
    //for(auto ptr : textureLookup) delete ptr.second;
    for(auto ptr : imageLookup) delete ptr.second;
    if(diffTex != NULL) delete diffTex;
    return true;
  }
  
  void Cleanup() {
    delete diffUB;
    delete particleAtlas;
    delete diffTex;
  }
  
  bool Commit() {
    //diffTex->mipmap();
    
    return true;
  }
  
  ImageRGBA* LoadImage(std::string rPath, bool retain) { _prsguard();//MutexGuard mxg(imgMutex);
    ImageRGBA* ret = new ImageRGBA(rPath);
    if(retain) imageLookup[rPath] = ret;
    return ret;
  }
  
  void CloseImage(void* img) { _prsguard(); //MutexGuard mxg(imgMutex);
    for(auto it = imageLookup.begin(); it != imageLookup.end(); ++it) {
      if(it->second == (ImageRGBA*)img) {
        imageLookup.erase(it);
        delete (ImageRGBA*)img;
        return;
      }
    }
  }
  
  
  void CommitTexture(Uint16 cid, void *img) { _prsguard();
    diffTex->upload((ImageRGBA*)img,0,0,cid);
    
    diffTex->setFilter(TextureFilter::Linear);
    diffTex->commit();
    diffTex->mipmap();
    
    
    CloseImage(img);
    std::cout << "uploaded  to arr[" << cid<< "]\n";
    ubDirty = true;
  }
  void lct(unsigned short c, void* img) {
    CommitTexture(c, (ImageRGBA*)img);
  }
  
  void CommitParticle(Uint16 cid, std::string resName, void *img) {
    std::cout << "CommitParticle() CALLED\n";
    Uint16 aid = particleAtlas->emplace(resName, (ImageRGBA*)img);
    CloseImage(img);
    atlasIDMap[cid] = aid;
    particleAtlas->upload();
  }
  
  void CommitDiffUB() { _prsguard();
    if(!ubDirty) return;
    for(unsigned short i = 0; i < cntTex; i++) {
      Compound *compound = WorldFields::active()->getCompoundByTexture(i);
      if(compound == NULL) {
        if(i > 0) {
          std::cout << "Compound " << i << " was returned NULL, couldn't upload GPUStruct!\n";
        }
      } else {
        compound->renderInfo.makeUB(&diffUBVector, i);
      }
    }
        diffUB->upload(reinterpret_cast<char*>(&diffUBVector[0]),sizeof(CompoundRenderInfo::GPUStruct)*diffUBVector.size());
        ubDirty = false;
  }
  
  bool LoadTexture(std::string rPath, bool retain) { _prsguard();
    if(textureLookup.find(rPath) != textureLookup.end()) return true;
    ImageRGBA* img = LoadImage(rPath, retain);
    if(img == NULL) return false;
    
    if(textureLookup.find(rPath) != textureLookup.end()) return true;
    const Uint16 newID = cntTex++;
    textureLookup[rPath] = newID;
    
    if(!Spider::IsPrimary()) {
      WorldManager::TexCommit(newID, img);
      return true;
    }
    CommitTexture(newID, img);
    return true;
  }
  
  unsigned short GetTexture(std::string rPath, bool retain) { _prsguard();
    auto lc = textureLookup.find(rPath);
    if(lc == textureLookup.end()) {
      if(!LoadTexture(rPath, retain)) return 0;
      lc = textureLookup.find(rPath);
    }
    
    return lc->second;
  }
  ImageRGBA* GetImage(std::string rPath, bool retain) { _prsguard();
    auto lc = imageLookup.find(rPath);
    if(lc == imageLookup.end()) {
      if(!LoadImage(rPath, retain)) return NULL;
      lc = imageLookup.find(rPath);
    }
    
    return lc->second;
  }
  
  bool AtlasTexture(std::string rPath, bool retain) {
    if(atlasLookup.find(rPath) != atlasLookup.end()) return true;
    else if(particleAtlas->hasResource(rPath) && particleAtlas->find(rPath) != NULL) {
        atlasLookup[rPath] = cntAtlas++;
        atlasIDMap[cntAtlas-1] = particleAtlas->find(rPath)->selfID;
        return true;
      }
    //std::cout << "loading atlast " << rPath << "\n";
    ImageRGBA* img = LoadImage(rPath, retain);
    img->generateSDF(16);
    if(img == NULL) return false;
    
    if(atlasLookup.find(rPath) != atlasLookup.end()) return true;
    const Uint16 newID = cntAtlas++;
    atlasLookup[rPath] = newID;
    
    if(!Spider::IsPrimary()) {
      WorldManager::ParticleTexCommit(newID, rPath, img);
      return true;
    }
    CommitParticle(newID, rPath, img);
    return true;
  }
  
  unsigned short GetParticle(std::string rPath, bool retain) { _prsguard();
  //std::cout << "GetParticle " << rPath << "\n";
    auto lc = atlasLookup.find(rPath);
    if(lc == atlasLookup.end()) {
      if(!AtlasTexture(rPath, retain)) return 0;
      lc = atlasLookup.find(rPath);
    }
    
    return lc->second;
  }
  
  unsigned short ParticleAtlasID(unsigned short in, bool waitFor) {
    auto it = atlasIDMap.find(in);
    if(it == atlasIDMap.end()) {
      if(waitFor) {
        while((it = atlasIDMap.find(in)) == atlasIDMap.end()) usleep(100);
      } else return 0;
    }
    return it->second;
  }
  
  Texture<true>* GetDiffuse() {
    return diffTex;
  }
  
  UniformBlock *GetDiffuseUB() {
    return diffUB;
  }
  
  TextureAtlas* GetParticles() {
    return particleAtlas;
  }
};

