/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Physical.cpp
 * Author: Galen Swain
 * 
 * Created on May 11, 2016, 2:37 PM
 */

#include "Physical.h"
#include <Mesh.h>
#include "uni/WorldObject.h"

Physical::Physical() : velocity(V3(0.f)),angular(Quat()), surface(), axials(), s() {
  
}

Physical::Physical(const Physical& orig) : surface(orig.surface), axials(orig.axials), s(orig.s) {
}

Physical::~Physical() {
}

double Physical::mass() const {
  return s.mass;
}
double Physical::volume() const {
  return s.volume;
}
float Physical::density() const {
  return s.mass/s.volume;
}

V3 Physical::COM() const {
  return s.com/float(s.mass);
}

void Physical::incorporate(Physical *o, Transform &trans) {
  const float oMass = o->mass();
  s.mass += oMass;
  s.volume += o->volume();
  surface.incorporate(&(o->surface), trans);
  s.com += trans*o->COM() * oMass;
}

Physical::operator bool() const {
  return (s.mass > 0.f) && (s.volume > 0.f);
}

int Physical::apply(WorldObject *const obj, float dt) { _prsguard();
  const float cdt = clamp(dt, 0.f, 1.f);
  V3 dPosition = velocity*dt;
  Quat dRotation = angular*dt;
  
  V3 direction = glm::normalize(dPosition);
  float resistance = surface.resistance.lerp(direction) + 1.f;
  dPosition /= resistance;//sqrt(resistance);
  velocity = lerp(velocity, velocity/resistance, cdt);
  //std::cout << "vel=" << glm::to_string(velocity)<<", res=" << resistance << "\n";
  
  //Account for resistance by changing angular velocity
  if(glm::length2(direction) != 0.f) {
    V3 dCross1 = (direction == V3_UP)? V3_RIGHT : glm::normalize(glm::cross(V3_UP, direction));
    V3 dCross2 = glm::normalize(glm::cross(dCross1, direction));
    
    const float dfc = 0.9f;
    V3 dOther1 = direction * dfc + dCross1 * (1.f-dfc);
    V3 dOther2 = direction * dfc + dCross2 * (1.f-dfc);

    //float eps = 1.f/90.f/M_PI;
    //Quat q1 = glm::angleAxis(float(M_PI/4.f), dCross1), q2 = glm::angleAxis(float(M_PI/4.f), dCross2);
    V2 angDelta = V2(surface.resistance.lerp(dOther1), surface.resistance.lerp(direction*(dOther2)));
    angDelta = glm::normalize(V2(resistance - 1.f) - angDelta);
    V3 angTarget = glm::normalize(direction + angDelta.x*dOther1 + angDelta.y*dOther2);
    //Quat deltaAngular = ((resistance*angDelta.x*q1) * (resistance*angDelta.y*q2));
    Quat deltaAngular = quatFromToRotation(direction, angTarget)*dt;
    
    std::cout << "angDelta=" << glm::to_string(angDelta)<< ", angTarget="<< glm::to_string(angTarget) <<", deltaAng=" << glm::to_string(deltaAngular) << "\n";
    angular *= deltaAngular;
    dRotation = angular*dt;
    //dRotation *= angular;
    //dRotation += deltaAngular*dt;
    //dRotation = glm::slerp(dRotation, Quat(), cdt);
  }
  obj->transform.position += dPosition;
  obj->transform.rotation *= dRotation;
  
  if(glm::length2(dPosition) != 0.f || dRotation != Quat()) return 1;
  
  return 0;
}

void Physical::applyAccel(V3 dir, float str) {
  velocity += dir*str;
}

Physical::Prop::Prop() : mass(0.f), volume(0.f), com(V3(0.f)) {
  
}

void Physical::clear() {
  s.mass = s.volume = 0.f; s.com = V3(0.f);
  surface.clear();
}

void Physical::zero() {
  velocity = V3(0.f);
  angular = Quat();
}

//#include <iostream>
void Physical::constructMesh(float epsilon, void* m) { _prsguard();
  _unused(epsilon);
  WMMeshType *const mesh = reinterpret_cast<WMMeshType*>(m);
  WObjectVert addVert;
  std::vector<unsigned int> vertIDs;
  
  addVert.pos = this->COM();
  addVert.sourcePos = V3(1.f);
  vertIDs.push_back(mesh->addVert(addVert));
  
  addVert.pos = this->COM() + V3_UP;
  addVert.sourcePos = V3_UP;
  vertIDs.push_back(mesh->addVert(addVert));
  addVert.pos = this->COM() + V3_RIGHT;
  addVert.sourcePos = V3_RIGHT;
  vertIDs.push_back(mesh->addVert(addVert));
  addVert.pos = this->COM() + V3_FORWARD;
  addVert.sourcePos = V3_FORWARD;
  vertIDs.push_back(mesh->addVert(addVert));
  
  mesh->addLineInd(vertIDs[0], vertIDs[1]);
  mesh->addLineInd(vertIDs[0], vertIDs[2]);//Vertical
  mesh->addLineInd(vertIDs[0], vertIDs[3]);
  
  //Draw surface
  std::cout << "DEBUG PHYS\n";
  const float resistanceScale = 1.f/10.f;
  for(auto vec = surface.resistance.vectors.begin(); vec != surface.resistance.vectors.end(); ++vec) {
    addVert.pos = this->COM();
    addVert.sourcePos = V3(1.f);
    unsigned int v0 = mesh->addVert(addVert);

    addVert.pos = this->COM() + vec->vec * float(vec->val) * resistanceScale;
    addVert.sourcePos = V3(0.8f, 0.8f, 0.f);
    unsigned int v1 = mesh->addVert(addVert);
    std::cout << "\t"<<glm::to_string(vec->vec) << " = " << vec->val << std::endl;
    mesh->addLineInd(v0, v1);
  }
}