/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Physical.h
 * Author: Galen Swain
 *
 * Created on May 11, 2016, 2:37 PM
 */

#ifndef PHYSICAL_H
#define PHYSICAL_H
#include "Surface.h"
#include "game.h"

class WorldObject;
class ContourObject;
class IdealSlice;

class Physical {
public:
    struct Axial {
        V3 coOffset;
        Quat angular;
        
        template<class Archive>
        void serialize(Archive& archive) {
            archive(CEREAL_NVP(coOffset), CEREAL_NVP(angular));
        }
    };
    
    friend class ContourObject;
    friend class WorldObject;
    friend class IdealSlice;
    
    Physical();
    Physical(const Physical& orig);
    virtual ~Physical();
    
    operator bool() const;
    
    int apply(WorldObject *const obj, float dt);
    void applyAccel(V3 dir, float str);
    
    void incorporate(Physical *o, Transform& trans);
    
    V3 velocity;
    Quat angular;
    Surface surface;
    std::list<Axial> axials;
    
    double mass() const;
    double volume() const;
    float density() const;
    
    V3 COM() const;
    
    void clear();
    void zero();
    
    void constructMesh(float epsilon, void* mesh);
protected:
    struct Prop {
        double mass, volume;
        V3 com;//center of mass
        Prop();
    } s;
    
public:
    template<class Archive>
    void serialize(Archive& archive) {
        archive(CEREAL_NVP(velocity),
                CEREAL_NVP(angular),
                CEREAL_NVP(surface),
                CEREAL_NVP(axials),
                
                CEREAL_NVP(s.mass),
                CEREAL_NVP(s.volume),
                CEREAL_NVP(s.com)
                );
    }

};
//CEREAL_REGISTER_TYPE(Physical);
#endif /* PHYSICAL_H */

