/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PhysicalProperties.cpp
 * Author: Galen Swain
 * 
 * Created on May 12, 2016, 9:04 AM
 */

#include "PhysicalProperties.h"
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <lib/XML.h>

PhysicalProperties::PhysicalProperties() {
  this->clear();
}

PhysicalProperties::PhysicalProperties(const PhysicalProperties& orig) {
  memcpy(this, &orig, sizeof(PhysicalProperties));
}

PhysicalProperties::~PhysicalProperties() {
}

float PhysicalProperties::getProp(Uint8 id, Uint8 subID) {
  switch(id) {
    case Physp::Density:
      return density;
      break;
    case Physp::Roughness:
      return roughness;
      break;
    default:
      if(id < Physp::Delta || id > (Physp::Delta + ELEMENT_DELTA_MAX)) {
        std::cerr << "PhysicalProperties::getProp("<<id<<", "<<subID<<") has no handler for ID"<<std::endl;
        return 0.f;
      }
      id = id - Physp::Delta;
      struct Delta *del = &delta[id];
      switch(subID) {
        case 0:
          return del->capacity;
          break;
        case 1:
          return del->conductance;
          break;
        default:
          std::cerr << "PhysicalProperties::getProp("<<id<<", "<<subID<<") has no handler for subID"<<std::endl;
          return 0.f;
          break;
      }
      break;
  }
}
float PhysicalProperties::getDelta(Uint8 id, Uint8 subID) {
  return this->getProp(id + Physp::Delta, subID);
}

void PhysicalProperties::clear() {
  density = roughness = 0.f;
  for(Uint8 i = 0; i < ELEMENT_DELTA_MAX; i++) {
      struct Delta *del = &delta[i];
      
      del->capacity = 0.f;
      del->conductance = 0.f;
  }
}

void PhysicalProperties::loadPhysicalXMLTag(void* xmlTag) {
  XMLNode *const n = reinterpret_cast<XMLNode*>(xmlTag);
  if(n != NULL) {
    density = n->get<float>("mass.density", 0.f)/1000.f;
    roughness = n->get<float>("mass.roughness", 0.f);
  }
}