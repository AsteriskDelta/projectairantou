/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PhysicalProperties.h
 * Author: Galen Swain
 *
 * Created on May 12, 2016, 9:04 AM
 */

#ifndef PHYSICALPROPERTIES_H
#define PHYSICALPROPERTIES_H
#include "game.h"
#include "field/Element.h"
#include "field/ElementClass.h"

enum Physp : Uint8 {
    Density = 0, Mass = 0,
    Roughness = 1,
    
    
    Delta = 32
};

class PhysicalProperties {
public:
    PhysicalProperties();
    PhysicalProperties(const PhysicalProperties& orig);
    virtual ~PhysicalProperties();
    
    float density;
    float roughness;
    
    struct Delta {
        float capacity;
        float conductance;
    };
    struct Delta delta[ELEMENT_DELTA_MAX];
    
    float getProp(Uint8 id, Uint8 subID = 0);
    float getDelta(Uint8 id, Uint8 subID = 0);
    
    void clear();
    
    void loadPhysicalXMLTag(void *tag);
private:

};

#endif /* PHYSICALPROPERTIES_H */

