/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Surface.h
 * Author: Galen Swain
 *
 * Created on May 11, 2016, 2:39 PM
 */

#ifndef SURFACE_H
#define SURFACE_H
#include "game.h"
#include "Transform.h"
#include "comp/VectorSet.h"

class Physical;
class Surface {
public:
    friend class Physical;
    Surface();
    Surface(const Surface& orig);
    virtual ~Surface();
    
    VectorSetf area, resistance, cavitation;
    float totalArea;
    
    void clear();
    
    void incorporate(Surface *o, Transform& trans);
    void calculateCavitation();
    
    template<class Archive>
    void serialize(Archive &ar) { 
      ar(CEREAL_NVP(area),
         CEREAL_NVP(resistance),
         CEREAL_NVP(cavitation),
         CEREAL_NVP(totalArea)
      );
    }
private:

};

#endif /* SURFACE_H */

