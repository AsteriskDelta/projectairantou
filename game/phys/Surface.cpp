/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Surface.cpp
 * Author: Galen Swain
 * 
 * Created on May 11, 2016, 2:39 PM
 */

#include "Surface.h"

Surface::Surface() {
}

Surface::Surface(const Surface& orig) : area(orig.area), resistance(orig.resistance), cavitation(orig.cavitation) {
  
}

Surface::~Surface() {
}

void Surface::incorporate(Surface *o, Transform& trans) {
  area.add(&(o->area), trans);
  resistance.add(&(o->resistance), trans);
  cavitation.add(&(o->cavitation), trans);
}

void Surface::calculateCavitation() {
  cavitation.clear();
}

void Surface::clear() {
  area.clear();
  resistance.clear();
  cavitation.clear();
}