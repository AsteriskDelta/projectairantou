/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Spider.h
 * Author: Galen Swain
 *
 * Created on March 26, 2016, 10:14 AM
 */

#ifndef SPIDER_H
#define SPIDER_H
#include "game.h"
#include <mutex>

namespace Spider {
    enum Class : Uint8 {
        Primary = 0,
        Worker = 1
    };
    //Zero denotes "use all availible threads"
    bool SpawnClass(Uint8 cls, Uint16 count = 1);
    inline bool SpawnWorkers(Uint16 count = 0) {
        return SpawnClass(Worker, count);
    }
    bool KillClass(Uint8 cls);
    bool Kill(Uint16 id);
    inline bool KillWorkers() {
        return KillClass(Worker);
    }
    bool KillAll();
    
    void Wait(Uint16 id);
    void WaitAll();
    
    bool IsClass(Uint8 c);
    inline bool IsPrimary() {
        return IsClass(Primary);
    }
    inline bool IsWorker() {
        return IsClass(Worker);
    }
    
    Uint16 GetID();
    
    void WorkerSetup(Uint16 newID, Uint8 newCls, std::mutex *mtx);
    void WorkerThread(std::mutex *mtx);
    void WorkerCleanup(std::mutex *mtx);
};

#endif /* SPIDER_H */

