/* 
 * File:   Entity.h
 * Author: Galen Swain
 *
 * Created on June 29, 2015, 1:48 PM
 */

#ifndef ENTITY_H
#define	ENTITY_H
#include "game.h"
#include "EntityResources.h"
#include "EntityStats.h"
#include "EntityInventory.h"
class AIEntity;
class EntityKnowledge;

class Entity {
public:
    Entity();
    virtual ~Entity();
    
    void calculateResources();
    void calculateAll();
    
    EntityResources resources;
    EntityStats stats;
    EntityInventory inventory;
    AIEntity *ai;
    EntityKnowledge *knowledge;
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(resources),
                CEREAL_NVP(stats),
                CEREAL_NVP(inventory)
                );
        
        //archive(CEREAL_NVP(frees));
    }
private:

};

CEREAL_REGISTER_TYPE(Entity);

#endif	/* ENTITY_H */

