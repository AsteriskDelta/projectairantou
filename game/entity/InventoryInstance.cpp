/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InventoryInstance.cpp
 * Author: Galen Swain
 * 
 * Created on June 6, 2016, 11:23 AM
 */
#include <algorithm>
#include "InventoryInstance.h"
#include "uni/WorldObject.h"

InventoryInstance::InventoryInstance() : compounds(), objects() {
}

InventoryInstance::InventoryInstance(const InventoryInstance& orig) : compounds(orig.compounds), objects(orig.objects) {
}

InventoryInstance::~InventoryInstance() {
}

unsigned int InventoryInstance::canAddObject(WorldObjectPtr ptr, unsigned int cnt) {
  unsigned int massCnt = floor(remainingMass() / ptr->physical.mass());
  unsigned int volCnt = floor(remainingVolume() < ptr->physical.volume());
  unsigned int useCnt = ::min(cnt, ::min(massCnt, volCnt));
  if(useCnt <= 0) return 0;
  
  return useCnt;
}
unsigned int InventoryInstance::addObject(WorldObjectPtr ptr, unsigned int cnt) {
  unsigned int useCnt;
  if((useCnt = canAddObject(ptr, cnt)) <= 0) return 0;
  auto it = std::find(objects.begin(), objects.end(), WorldObjectStore(ptr));
  if(it == objects.end()) {
    objects.push_back(WorldObjectStore(ptr, useCnt));
  } else {
    it->stackCount += useCnt;
  }
  return cnt - useCnt;
}
unsigned int InventoryInstance::removeObject(WorldObjectPtr ptr, unsigned int cnt) {
  auto it = std::find(objects.begin(), objects.end(), ptr);
  if(it != objects.end()) {
    unsigned int useCnt = ::min(cnt, it->stackCount);
    unsigned int remCnt = it->stackCount - useCnt;
    if(remCnt == 0) {
      objects.erase(it);
    } else {
      it->stackCount = remCnt;
    }
    return useCnt;
  } else return 0;
}

double InventoryInstance::canAddCompound(CompoundIdx idx, double vol) {
  return compounds.canAdd(idx, vol, remainingVolume(), remainingMass());
}
double InventoryInstance::addCompound(CompoundIdx idx, double vol) {
  double toAdd = this->canAddCompound(idx, vol);
  if(toAdd <= 0.0) return 0.0;
  
  return compounds.add(idx, vol, remainingVolume(), remainingMass());
}
double InventoryInstance::removeCompound(CompoundIdx idx, double vol) {
  return compounds.remove(idx, vol);
}

void InventoryInstance::clearMaximum() {
  max.mass = max.volume = 0.0;
}

double InventoryInstance::mass() {
  double ret = 0.0;
  ret += compounds.mass();
  for(WorldObjectStore &store : objects) ret += store.ptr->physical.mass() * store.stackCount;
  return ret;
}
double InventoryInstance::volume() {
  double ret = 0.0;
  ret += compounds.volume();
  for(WorldObjectStore &store : objects) ret += store.ptr->physical.volume() * store.stackCount;
  return ret;
}

double InventoryInstance::remainingMass() {
  return ::max(0.0, max.mass - mass());
}
double InventoryInstance::remainingVolume() {
  return ::max(0.0, max.volume - volume());
}