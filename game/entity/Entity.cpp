/* 
 * File:   Entity.cpp
 * Author: Galen Swain
 * 
 * Created on June 29, 2015, 1:48 PM
 */

#include "Entity.h"

Entity::Entity() {
}

Entity::~Entity() {
}

void Entity::calculateResources() {
  resources.clearMaximum();
  stats.addMaximums(&resources);
}
void Entity::calculateAll() {
  this->calculateResources();
}