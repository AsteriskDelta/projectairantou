/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EntityInventory.cpp
 * Author: Galen Swain
 * 
 * Created on June 6, 2016, 9:11 AM
 */

#include "EntityInventory.h"
#include "uni/WorldObject.h"

EntityInventory::EntityInventory() : rawElements() {
}

EntityInventory::EntityInventory(const EntityInventory& orig) : InventoryInstance(orig), rawElements(orig.rawElements) {
}

EntityInventory::~EntityInventory() {
}

const double elementMassMultiplier = 1.0/1000.0, elementVolumeMultiplier = 1.0/1000.0;
double EntityInventory::canAddElement(ElementIdx idx, double units) {
  _unused(idx);
  return ::min(units, ::min(remainingVolume()/elementVolumeMultiplier, remainingMass()/elementMassMultiplier));
}
double EntityInventory::addElement(ElementIdx idx, double units) {
  double toAdd = this->canAddElement(idx, units);
  if(toAdd <= 0.0) return 0.0;
  rawElements.set(idx, rawElements.get(idx)+toAdd);
  
  return ::max(0.0, units - toAdd);
}
double EntityInventory::removeElement(ElementIdx idx, double units) {
  double toRemove = ::min(units, rawElements.get(idx));
  if(toRemove <= 0.0) return 0.0;
  rawElements.set(idx, rawElements.get(idx) - toRemove);
  
  return toRemove;
}

double EntityInventory::mass() {
  double ret = InventoryInstance::mass();
  for(Uint8 i = 0; i < rawElements.size(); i++) ret += rawElements[i] * elementMassMultiplier;
  return ret;
}
double EntityInventory::volume() {
  double ret = InventoryInstance::volume();
  for(Uint8 i = 0; i < rawElements.size(); i++) ret += rawElements[i] * elementVolumeMultiplier;
  return ret;
}

double EntityInventory::remainingMass() {
  return ::max(0.0, max.mass - mass());
}
double EntityInventory::remainingVolume() {
  return ::max(0.0, max.volume - volume());
}