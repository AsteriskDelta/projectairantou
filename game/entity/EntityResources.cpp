/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EntityResources.cpp
 * Author: Galen Swain
 * 
 * Created on June 6, 2016, 9:11 AM
 */

#include "EntityResources.h"
#include <cmath>

LRInt EntityResources::overflowDiv = 1000;

EntityResources::V::V() : health(0), stamina(0), mana(0), divines(0) {
}
EntityResources::V::V(const V& o) : health(o.health), stamina(o.stamina), mana(o.mana), divines(o.divines) {
}
EntityResources::V EntityResources::V::operator+(const EntityResources::V& o) const {
  EntityResources::V ret = *this;
  ret.health += o.health;
  ret.stamina += o.stamina;
  ret.mana += o.mana;
  ret.divines += o.divines;
  return ret;
}
EntityResources::V EntityResources::V::operator-() const {
  EntityResources::V ret;
  ret.health = -health;
  ret.stamina = -stamina;
  ret.mana = -mana;
  ret.divines = -divines;
  return ret;
}
EntityResources::V EntityResources::V::operator-(const EntityResources::V& o) const {
  EntityResources::V ret = *this;
  ret.health -= o.health;
  ret.stamina -= o.stamina;
  ret.mana -= o.mana;
  ret.divines -= o.divines;
  return ret;
}
EntityResources::V EntityResources::V::operator*(float f) const {
  EntityResources::V ret;
  ret.health = round(f * health);
  ret.stamina = round(f * stamina);
  ret.mana = round(f * mana);
  ret.divines = round(f * divines);
  return ret;
}
EntityResources::V EntityResources::V::operator*(const EntityResources::V& o) const {
  EntityResources::V ret = *this;
  ret.health *= o.health;
  ret.stamina *= o.stamina;
  ret.mana *= o.mana;
  ret.divines *= o.divines;
  return ret;
}
EntityResources::V EntityResources::V::operator/(float f) const {
  EntityResources::V ret;
  ret.health = floor(health/f);
  ret.stamina = floor(stamina/f);
  ret.mana = floor(mana/f);
  ret.divines = floor(divines/f);
  return ret;
}
EntityResources::V EntityResources::V::operator/(const EntityResources::V& o) const {
  EntityResources::V ret = *this;
  ret.health /= o.health;
  ret.stamina /= o.stamina;
  ret.mana /= o.mana;
  ret.divines /= o.divines;
  return ret;
}

EntityResources::V::operator bool() const {
  return health >= 0 && stamina >= 0 && mana >= 0 && divines >= 0;
}
bool EntityResources::V::operator<(const EntityResources::V& o) const {
  return !bool(*this - o);
}
bool EntityResources::V::operator>(const EntityResources::V& o) const {
  return bool(*this - o);
}
bool EntityResources::V::isZero() const {
  return health == 0 && stamina == 0 && mana == 0 && divines == 0;
}

EntityResources::V EntityResources::V::infinite() {
  EntityResources::V ret;
  ret.health = std::numeric_limits<LRInt>::max();
  ret.stamina = std::numeric_limits<LRInt>::max();
  ret.mana = std::numeric_limits<LRInt>::max();
  ret.divines = std::numeric_limits<LRInt>::max();
  return ret;
}

EntityResources::V EntityResources::V::clamp(const EntityResources::V& min, const EntityResources::V& max) {
  EntityResources::V ret;
  ret.health  = ::clamp(health, min.health, max.health);
  ret.stamina = ::clamp(stamina, min.stamina, max.stamina);
  ret.mana    = ::clamp(mana, min.mana, max.mana);
  ret.divines = ::clamp(divines, min.divines, max.divines);
  return ret;
}

EntityResources::EntityResources() : current(), maximum(), delta(), overflow() {
}

EntityResources::EntityResources(const EntityResources& o) : current(o.current), maximum(o.maximum), delta(o.delta), overflow(o.overflow) {
}

EntityResources::~EntityResources() {
}

bool EntityResources::couldApply(const struct EntityResources::V &v) const {
  return v < current;
}
bool EntityResources::apply(const struct EntityResources::V &v) {
  bool ret = this->couldApply(v);
  current = (current + v).clamp(V(), maximum);
  return ret;
}

void EntityResources::update(float deltaTime) {
  EntityResources::V del = delta*deltaTime;
  EntityResources::V overflowDel = overflow/float(overflowDiv);
  current = (del + overflowDel).clamp(EntityResources::V(), maximum.isZero()? V::infinite() : maximum);
  overflow = overflow + (delta * float(overflowDiv) * deltaTime - del * float(overflowDiv)) - (overflowDel * float(overflowDiv));
  
}

void EntityResources::clearMaximum() {
  maximum = V();
}