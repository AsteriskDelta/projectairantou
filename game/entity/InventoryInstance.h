/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InventoryInstance.h
 * Author: Galen Swain
 *
 * Created on June 6, 2016, 11:23 AM
 */

#ifndef INVENTORYINSTANCE_H
#define INVENTORYINSTANCE_H
#include "game.h"
#include "field/CompoundList.h"

class InventoryInstance {
public:
    InventoryInstance();
    InventoryInstance(const InventoryInstance& orig);
    virtual ~InventoryInstance();
    
    struct V {
        double mass, volume;
        template<class Archive>
        void serialize(Archive & archive) {
            archive(CEREAL_NVP(mass) , CEREAL_NVP(volume));
        }
    };
    V current, max, objectCurrent;
    
    CompoundList compounds;
    struct WorldObjectStore {
        WorldObjectPtr ptr;
        unsigned int stackCount;
        inline WorldObjectStore() : ptr(), stackCount(0) {};
        inline WorldObjectStore(WorldObjectPtr p, unsigned short sc = 1) : ptr(p), stackCount(sc) {};
        inline bool operator==(const WorldObjectStore &o) const { return ptr == o.ptr; };
        
        template<class Archive>
        void serialize(Archive & archive) {
            archive(CEREAL_NVP_ID("woID", ptr.woID()),
                    CEREAL_NVP(stackCount)
                    );
        }
    };
    std::vector<WorldObjectStore> objects;
    
    unsigned int canAddObject(WorldObjectPtr ptr, unsigned int cnt);
    unsigned int addObject(WorldObjectPtr ptr, unsigned int cnt);
    unsigned int removeObject(WorldObjectPtr ptr, unsigned int cnt);
    
    double canAddCompound(CompoundIdx idx, double vol);//How much can be added
    double addCompound(CompoundIdx, double vol);//volume remaining
    double removeCompound(CompoundIdx, double vol);//volume actually removed
    
    void clearMaximum();
    
    virtual double mass();
    virtual double volume();
    virtual double remainingMass();
    virtual double remainingVolume();
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(current),
                CEREAL_NVP(max),
                CEREAL_NVP(objectCurrent),
                CEREAL_NVP(compounds),
                CEREAL_NVP(objects)
                );
    }
private:

};

#endif /* INVENTORYINSTANCE_H */

