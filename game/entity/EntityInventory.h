/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EntityInventory.h
 * Author: Galen Swain
 *
 * Created on June 6, 2016, 9:11 AM
 */

#ifndef ENTITYINVENTORY_H
#define ENTITYINVENTORY_H
#include "game.h"
#include "field/Element.h"
#include "field/FieldVector.h"
#include "InventoryInstance.h"

class EntityInventory : public InventoryInstance {
public:
    EntityInventory();
    EntityInventory(const EntityInventory& orig);
    virtual ~EntityInventory();
    
    //Stored in units of 1pt/cubic
    FieldVector<ElementIdx, double> rawElements;
    
    double canAddElement(ElementIdx idx, double units);
    double addElement(ElementIdx idx, double units);//Returns remaining units (those not added))
    double removeElement(ElementIdx idx, double units);//Returns units removed
    
    virtual double mass() override;
    virtual double volume() override;
    virtual double remainingMass() override;
    virtual double remainingVolume() override;
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(rawElements));
    }
private:

};

CEREAL_REGISTER_TYPE(FieldVector<ElementIdx, double>);
CEREAL_REGISTER_TYPE(EntityInventory);

#endif /* ENTITYINVENTORY_H */

