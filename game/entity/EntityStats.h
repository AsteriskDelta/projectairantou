/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EntityStats.h
 * Author: Galen Swain
 *
 * Created on May 17, 2016, 3:33 PM
 */

#ifndef ENTITYSTATS_H
#define ENTITYSTATS_H
#include "game.h"

class EntityResources;
typedef unsigned short LSInt;


class EntityStats {
public:
    EntityStats();
    virtual ~EntityStats();
    
    unsigned int level;
    Uint64 expTotal;

    //Physical stats
    union {
        struct {
            LSInt strength;
            LSInt agility;
            LSInt dexterity;
            LSInt vitality;
            LSInt endurance;
        };
        LSInt physicals[5];
    };

    //Mental
    union {
        struct {
            LSInt intelligence;
            LSInt willpower;
            LSInt wisdom;
            LSInt charisma;
            LSInt perception;
        };
        LSInt mentals[5];
    };

    //Divine
    union {
        struct {
            LSInt fortune;
            LSInt empathy;
            LSInt influence;
            LSInt intervention;
            LSInt guidance;
        };
        LSInt divines[5];
    };
    
    union {
    struct {
        unsigned int physical;
        unsigned int mental;
        unsigned int divine;
        unsigned int any;
    } free;
    unsigned int frees[4];
    };
    
    unsigned int physicalTotal();
    unsigned int mentalTotal();
    unsigned int divineTotal();
    unsigned int pointTotal();
    unsigned int freeTotal();
    unsigned int overTotal();
    
    Uint64 getDeltaExp();
    Uint64 getDeltaMaxExp();
    Uint64 getBaseLevelExp();
    Uint64 getNextLevelExp();
    
    void addXP(Uint64 xp);
    void addLevel();
    void setLevel(unsigned int l);
    
    void addMaximums(EntityResources* er);
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(level), CEREAL_NVP(expTotal));
        
        archive(CEREAL_NVP(strength),
                CEREAL_NVP(agility),
                CEREAL_NVP(dexterity),
                CEREAL_NVP(vitality),
                CEREAL_NVP(endurance)
                );
        archive(CEREAL_NVP(intelligence),
                CEREAL_NVP(wisdom),
                CEREAL_NVP(willpower),
                CEREAL_NVP(charisma),
                CEREAL_NVP(perception)
                );
        archive(CEREAL_NVP(fortune),
                CEREAL_NVP(empathy),
                CEREAL_NVP(influence),
                CEREAL_NVP(intervention),
                CEREAL_NVP(guidance)
                );
        
        archive(CEREAL_NVP(frees));
    }
private:
    static Uint64 getExpForLevel(unsigned int level);
    static Uint64 getExpToLevel(unsigned int level);
};

CEREAL_REGISTER_TYPE(EntityStats);

#endif /* ENTITYSTATS_H */

