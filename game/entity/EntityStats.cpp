/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EntityStats.cpp
 * Author: Galen Swain
 * 
 * Created on May 17, 2016, 3:33 PM
 */

#include "EntityStats.h"
#include "EntityResources.h"

EntityStats::EntityStats() {
  for(Uint8 i = 0; i < 5; i++) physicals[i] = mentals[i] = divines[i] = 0;
  for(Uint8 i = 0; i < 4; i++) frees[i] = 0;
  level = 0;
  expTotal = 0;
}

EntityStats::~EntityStats() {
}

unsigned int EntityStats::physicalTotal() {
  unsigned int ret = 0;
  for(Uint8 i = 0; i < 5; i++) ret += physicals[i];
  return ret;
}
  
unsigned int EntityStats::mentalTotal() {
  unsigned int ret = 0;
  for(Uint8 i = 0; i < 5; i++) ret += mentals[i];
  return ret;
}
unsigned int EntityStats::divineTotal() {
  unsigned int ret = 0;
  for(Uint8 i = 0; i < 5; i++) ret += divines[i];
  return ret;
}
unsigned int EntityStats::pointTotal() {
  return physicalTotal()+mentalTotal()+divineTotal();
}
unsigned int EntityStats::freeTotal() {
  unsigned int ret = 0;
  for(Uint8 i = 0; i < 4; i++) ret += frees[i];
  return ret;
}
unsigned int EntityStats::overTotal() {
  return pointTotal()+freeTotal();
}

Uint64 EntityStats::getDeltaExp() {
  return expTotal - getBaseLevelExp();
}
Uint64 EntityStats::getDeltaMaxExp() {
  return getNextLevelExp() - getBaseLevelExp();
}
Uint64 EntityStats::getBaseLevelExp() {
  return getExpToLevel(level);
}
Uint64 EntityStats::getNextLevelExp() {
  return getExpToLevel(level+1);
}

const float levelCurve = 1.5f, levelBase = 100.f;
const Uint32 levelPP = 1, levelMP = 1, levelDP = 1, levelAny = 2;
const LSInt statFieldMax = 100;

Uint64 EntityStats::getExpForLevel(unsigned int level) {
  return ceil(levelBase * pow(levelCurve, float(level)));
}
Uint64 EntityStats::getExpToLevel(unsigned int level) {//Integral of getExpForLevel
  //if(level == 0) return getExpForLevel(level);
  //else return ceil(levelBase * (levelBase *(pow(levelBase,double(level)) - 1.0))/(levelBase - 1.0) + 1.0); 
  Uint64 ret = 0;
  for(unsigned int l = 0; l < level; l++) ret += getExpForLevel(l);
  return ret;
}

void EntityStats::addXP(Uint64 xp) {
  expTotal += xp;
  Uint8 levelUpEscape = 0;
  while(expTotal >= getNextLevelExp() && (levelUpEscape++) < 255) {
    //std::cout << expTotal << " >= " << getNextLevelExp() << " for level " << level << "\n";
    this->addLevel();
  }
  
  //std::cout << "ENDED AT " << expTotal << " >= " << getNextLevelExp() << " for level " << level << "\n";
}
void EntityStats::addLevel() {
  level++;
  free.physical += levelPP;
  free.mental += levelMP;
  free.divine += levelDP;
  free.any += levelAny;
}

void EntityStats::addMaximums(EntityResources* er) {
  if(er == nullptr) return;
  
  LRInt dh, ds, dm, dd;
  LRInt ptScale = 1000;
  double healthProp = double(vitality + willpower + level) / double(statFieldMax);
  dh = floor(320*ptScale*healthProp);
  double staminaProp = double(endurance + level) / double(statFieldMax);
  ds = floor(500*ptScale*staminaProp);
  double manaProp = double(wisdom + level) / double(statFieldMax);
  dm = floor(600*ptScale*manaProp);
  double divineProp = double(influence + level) / double(statFieldMax);
  dd = floor(600*ptScale*divineProp);
  
  er->maximum.health    += 1*ptScale + dh;
  er->maximum.stamina   += 0*ptScale + ds;
  er->maximum.mana      += 0*ptScale + dm;
  er->maximum.divines   += 5*ptScale + dd;
}

void EntityStats::setLevel(unsigned int l) {
  //l -= level;
  //for(unsigned int i = 0; i < l; i++) this->addLevel();
  Uint64 newXP = getExpToLevel(l);
  if(newXP < expTotal) {
    level = l;
  } else {
    newXP -= expTotal;
    this->addXP(newXP);
  }
}