/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   EntityResources.h
 * Author: Galen Swain
 *
 * Created on June 6, 2016, 9:11 AM
 */

#ifndef ENTITYRESOURCES_H
#define ENTITYRESOURCES_H
#include "game.h"

typedef int LRInt;
class EntityResources {
public:
    EntityResources();
    EntityResources(const EntityResources& orig);
    virtual ~EntityResources();
    
    struct V {
        LRInt health;
        LRInt stamina;
        LRInt mana;
        LRInt divines;
        
        V();
        V(const V& o);
        V operator+(const V& o) const;
        V operator-() const;
        V operator-(const V& o) const;
        V operator*(float f) const;
        V operator*(const V& o) const;
        V operator/(float f) const;
        V operator/(const V& o) const;
        
        operator bool() const;
        bool operator <(const V& o) const;
        bool operator >(const V& o) const;
        
        bool isZero() const;
        
        V clamp(const V& min, const V& max);
        
        static V infinite();
        
        template<class Archive>
        void serialize(Archive & archive) {
            archive(CEREAL_NVP(health),
                    CEREAL_NVP(stamina),
                    CEREAL_NVP(mana),
                    CEREAL_NVP(divines)
                    );
        }
    };
    
    struct V current;
    struct V maximum;
    struct V delta;
    struct V overflow;
    static LRInt overflowDiv;
    
    bool couldApply(const struct V &v) const;
    bool apply(const struct V &v);
    
    void update(float deltaTime);
    
    void clearMaximum();
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(current),
                CEREAL_NVP(maximum),
                CEREAL_NVP(delta),
                CEREAL_NVP(overflow)
                );
        
        archive(CEREAL_NVP(overflowDiv));
    }
private:

};
CEREAL_REGISTER_TYPE(EntityResources);

#endif /* ENTITYRESOURCES_H */

