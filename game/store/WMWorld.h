/* 
 * File:   WMWorld.h
 * Author: Galen Swain
 *
 * Created on June 23, 2015, 8:33 AM
 */

#ifndef WMWORLD_H
#define	WMWORLD_H

#include "WMBootstrap.h"
#include "uni/WorldObject.h"

class WMWorld : public WorldObject {
public:
    WMWorld();
    virtual ~WMWorld();
    
    inline void add(WorldObject *const o) {
        //std::cout << "adding " << o << "\n";
        WorldObjectPtr oPtr = o;
        this->emplace(oPtr);
    }
    void remove(WorldObject *const o) {
        std::cout << "asked to delete " << o << "\n";
    }
    
    //WMBootstrap boot;
private:

};

#endif	/* WMWORLD_H */

