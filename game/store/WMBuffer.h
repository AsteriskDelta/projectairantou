/* 
 * File:   WMBuffer.h
 * Author: Galen Swain
 *
 * Created on June 19, 2015, 3:40 PM
 */

#ifndef WMBUFFER_H
#define	WMBUFFER_H
#include <list>
#include "uni/WorldObject.h"
#include "MeshBuffer.h"
#include <bitset>
#include <UniformBlock.h>

#define WMBUFFER_INST_MAX 78
#define WMBUFFER_MAX 65536
#define WMBUFFER_PREF 65536
#define WMBUFFER_MIN 1
#define WMBUFFER_IND_RATIO 20/3

#define WMBUFFER_OPT_REALTIME 0.05f
#define WMBUFFER_OPT_PARALLEL 0.5f
#define WMBUFFER_OPT_COMPLETE 100.f

struct WMBufferRegion {
    unsigned int start, end;//First valid and first invalid BYTES, respectivly.
    WMIndType *indData; unsigned int indCount;
    unsigned short id, buffID;//Used by master set
    
    inline WMBufferRegion() : start(0), end(0), indData(NULL), indCount(0), id(65535), buffID(65535) {};
    inline WMBufferRegion(unsigned int ns, unsigned int ne) : start(ns), end(ne), indData(NULL), indCount(0), id(65535), buffID(65535) {};
    inline WMBufferRegion(const WMBufferRegion& o) : start(o.start), end(o.end), indData(o.indData), indCount(o.indCount), id(o.id), buffID(o.buffID) {};
    
    inline bool operator==(const WMBufferRegion &o) { return start == o.start && end == o.end; };
    inline bool operator<(const WMBufferRegion &o) const { return start < o.start; };
    inline operator bool() const { return start < end; };
    inline unsigned int getSize() const { return end-start; };
    
    inline bool isValid() { return buffID != 65535; };
    
    static const WMBufferRegion null;
};

template<typename T, typename IT> class Mesh;
template<typename T, typename IT> class MeshBuffer;

template <typename T>
class WMBuffer {//All length parameters are in BYTES
public:
    const static unsigned short MaxTransforms = 256;
    
    WMBuffer();
    WMBuffer(const WMBuffer& orig);
    virtual ~WMBuffer();
    
    void initialize();
    
    WMBuffer<T>& operator=(const WMBuffer& o);
    
    inline operator bool() const {
        return totalSpace > 0 && mesh != NULL;
    }
    
    void allocate(unsigned int newSpace = WMBUFFER_MAX);//Defaults to WMBUFFER_MAX
    void deallocate();
    
    WMBufferRegion getRegion(unsigned int length, bool commit = false);
    
    bool canEmplace(MeshBuffer<T> *buff);
    int couldEmplace(MeshBuffer<T> *buff);
    WMBufferRegion emplace(MeshBuffer<T> *buff);
    
    bool canEmplace(unsigned int length);
    int couldEmplace(unsigned int length);
    WMBufferRegion emplace(char *nd, unsigned int length, const char *indD, unsigned int indCount);
    
    int  optimizeScore();//Larger = more savings
    bool optimize(float timeQuota = WMBUFFER_OPT_COMPLETE);
    bool update(WMBufferRegion& region, char *nd, unsigned int length, const char *indD, unsigned int indCount);
    bool update(WMBufferRegion& region, MeshBuffer<T> *buff);
    bool remove(WMBufferRegion region);
    
    bool upload();
    void uploadTransforms();
    bool render();
    
    void updateTransformData(unsigned short id, const WMBufferRegionTransform& d);
    
    bool dirty;
    bool isDbg;
    
    Transform transform;
private:
    void regionWasInserted(std::list<WMBufferRegion>::iterator it);//Only called on unallocated list
    
    std::list<WMBufferRegion> delta;//Altered areas between current and previous
    std::list<WMBufferRegion> allocated, unallocated;
    unsigned int freeSpace, totalSpace;
    unsigned int objectSize;
    
    unsigned short regionIDCounter;
    unsigned short getNewRegionID();
    
    mutable UniformBlock *transUB;
    bool dirtyUB;
    WMBufferRegionTransform transformData[MaxTransforms];
    std::bitset<MaxTransforms> idTable;
    
    mutable Mesh<T>* mesh;
    mutable Mutex mtx;
};

//#include "MeshBuffer.cpp"
#endif	/* WMBUFFER_H */

