/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WMBufferSet.cpp
 * Author: Galen Swain
 * 
 * Created on March 11, 2016, 8:19 AM
 */

#include "WMBufferSet.h"
#include "uni/WorldObject.h"
#include <list>

thread_local int dbgLckCnt =0;

template<typename T>
WMBufferSet<T>::WMBufferSet(bool wf) { MutexGuard mg(this->mtx);
  //buffers.reserve(64);
  isDbg = wf;
  this->pushBuffer();
}
template<typename T>
WMBufferSet<T>::WMBufferSet(const WMBufferSet &o) { MutexGuard mg(this->mtx);
  isDbg = o.isDbg;
  buffers = o.buffers;
  regionMap = o.regionMap;
}

template<typename T>
WMBufferSet<T>::~WMBufferSet() {
  
}

template<typename T>
unsigned int WMBufferSet<T>::emplace(MeshBuffer<T> *buff) {
  unsigned int idx = getNewID();
  return this->emplace(buff, idx);
}

template<typename T>
unsigned int WMBufferSet<T>::emplace(MeshBuffer<T> *buff, unsigned int idx) { _prsguard(); MutexGuard mg(this->mtx);
  //lck();
  if(idx == WO_BUFFER_NULL) {
    idx = getNewID();
    //std::cout << "getting new id: " << idx << "\n";
  } else this->remove(idx);
  
  int bestBuffer = -1, curBuffer = 0, bestBufferScore = INT_MAX; bool doOptimize = false;
  for(auto it = buffers.begin(); it != buffers.end(); ++it) {
    int bufferScore;
    
    if(it->canEmplace(buff)) {
      bestBuffer = curBuffer;
      bestBufferScore = 0;
      break;
    } else if((bufferScore = it->couldEmplace(buff)) < bestBufferScore) {
      bestBuffer = curBuffer; bestBufferScore = bufferScore;
      doOptimize = true;
      break;
    }
    curBuffer++;
  }
  
  //TODO: Add tradeoff calculation of new buffer vs optimizing existing
  WMBuffer<T>* target;
  //std::cout << "Finding for buffer " << bestBuffer << "\n";
  if(bestBuffer == -1) {//Add new buffer?
    bestBuffer = this->pushBuffer();
    target = &buffers[bestBuffer];
  } else {
    target = &buffers[bestBuffer];
    if(doOptimize) target->optimize(WMBUFFER_OPT_REALTIME);
  }
  std::cout << "Got bufferr " << bestBuffer << "\n";
  
  WMBufferRegion region = target->emplace(buff);
  region.buffID = bestBuffer;
  if(region.id > 255) {
    Console::Error("Unitialized region id!!!");
  }
  //unsigned short regionID = region.id;
  //IDX idx(bestBuffer, regionID);
  regionMap[idx] = region;
  
  //ulck();
  return idx;
}
template<typename T>
void WMBufferSet<T>::alter(unsigned int id, MeshBuffer<T> *buff) { _prsguard(); MutexGuard mg(this->mtx);
  //mtx.lock(); 
  //lck();
  WMBufferRegion& region = regionMap[id];
  WMBuffer<T> *buffer = &buffers[region.buffID];
  if(!buffer->update(region, buff)) {
    buffer->remove(region);
    this->emplace(buff, id);
  }
  //ulck();
  //mtx.unlock();
}
template<typename T>
void WMBufferSet<T>::remove(unsigned int id) { _prsguard(); MutexGuard mg(this->mtx);
  //lck();
  if(regionMap.find(id) == regionMap.end()) {
    if(!isDbg) Console::Warning("WMBuffer::remove given unmapped ID");
    ulck();
    return;
  }
  WMBufferRegion& region = regionMap[id];
  if(!region.isValid()) return;
  //std::cout << "Removing from buffer #" << region.buffID << " for id="<<id<<"\n";
  WMBuffer<T> *buffer = &buffers[region.buffID];
  buffer->remove(region);
  
  regionMap.erase(id);
  //ulck();
}

template<typename T>
void WMBufferSet<T>::update(float timeQuota) { _prsguard(); //MutexGuard mg(this->mtx);
  //mtx.lock();
  for(unsigned int bufferID = 0; bufferID < buffers.size(); bufferID++) {
    WMBuffer<T> *buffer = &buffers[bufferID];
    buffer->upload();
  }
  //mtx.unlock();
}
template<typename T>
void WMBufferSet<T>::optimize(float timeQuota) { _prsguard(); MutexGuard mg(this->mtx);
  //lck();
  //mtx.lock();
  for(unsigned int bufferID = 0; bufferID < buffers.size(); bufferID++) {
    WMBuffer<T> *buffer = &buffers[bufferID];
    if(buffer->optimizeScore() > 1024) buffer->optimize(WMBUFFER_OPT_PARALLEL);
  }
  //mtx.unlock();
  //ulck();
}
template<typename T>
void WMBufferSet<T>::render() { _prsguard(); //MutexGuard mg(this->mtx);
  for(unsigned int bufferID = 0; bufferID < buffers.size(); bufferID++) {
    WMBuffer<T> *buffer = &buffers[bufferID];
    buffer->render();
  }
  //buffers[0].render();
  //if(buffers.size() > 1) buffers[1].render();
}
template<typename T>
void WMBufferSet<T>::clear() { _prsguard();
  lck();
  std::list<unsigned int> idList;
  for(auto it = regionMap.begin(); it != regionMap.end(); ++it) idList.push_back(it->first);
  
  for(unsigned int idx : idList) this->remove(idx);
  regionMap.clear();
  ulck();
}

template<typename T>
unsigned int WMBufferSet<T>::getNewID() { _prsguard(); MutexGuard mg(this->mtx);
  unsigned int ret = rand();
  while(regionMap.find(ret) != regionMap.end()) ret = rand();
  return ret;
}

template<typename T>
unsigned short WMBufferSet<T>::pushBuffer() { _prsguard(); MutexGuard mg(this->mtx);
std::cout << "pushBuffer() called\n";
  //lck();
  unsigned short ret = buffers.size();
  //buffers.push_back();
  buffers.resize(buffers.size()+1);
  buffers[ret].initialize();
  buffers[ret].isDbg = isDbg;
  buffers[ret].allocate(65535);
  //ulck();
  return ret;
}

template<typename T>
void WMBufferSet<T>::updateTransformData(unsigned int id, const WMBufferRegionTransform& d) { _prsguard(); MutexGuard mg(this->mtx);
  //std::cout << "trans request to " << regionMap[id].buffID << "["<<regionMap[id].id<<"]\n";
  //lck();
  if(regionMap[id].buffID > buffers.size()) {
    if(isDbg) return;
    std::stringstream ss; ss<< "Update transform request for #"<<id<<" has invalid buffer target " << regionMap[id].buffID ;
    Console::Error(ss.str());
    ulck();
    return;
  }
  buffers[regionMap[id].buffID].updateTransformData(regionMap[id].id, d);
  //ulck();
}

template class WMBufferSet<WObjectVert>;