/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WMBufferSet.h
 * Author: Galen Swain
 *
 * Created on March 11, 2016, 8:19 AM
 */

#ifndef WMBUFFERSET_H
#define WMBUFFERSET_H
#include "game.h"
#include "WMBuffer.h"
#include <UniformBlock.h>
#include <unordered_map>
#include <vector>
#include <deque>
#include <mutex>


extern thread_local int dbgLckCnt;

template<typename T>
class WMBufferSet {
public:
    /*struct IDX {
        unsigned short bufferID;
        unsigned short objectID;
        
        inline IDX() : bufferID(0xFFFF), objectID(0xFFFF) {};
        inline IDX(unsigned int dat) { *this = *reinterpret_cast<IDX*>(&dat); };
        inline IDX(Uint16 b, Uint16 o) : bufferID(b), objectID(o) {};
        inline operator unsigned int() const { return *const_cast<unsigned int*>(this); };
    } __attribute__((packed));*/
    typedef unsigned int IDX;
    
    WMBufferSet(bool wireframe = false);
    WMBufferSet(const WMBufferSet &o);
    virtual ~WMBufferSet();
    
    unsigned int emplace(MeshBuffer<T> *buff);
    unsigned int emplace(MeshBuffer<T> *buff, unsigned int targetID);
    void alter(unsigned int id, MeshBuffer<T> *buff);
    void remove(unsigned int id);
    
    void update(float timeQuota);
    void optimize(float timeQuota);
    void render();
    void clear();
    
    void updateTransformData(unsigned int id, const WMBufferRegionTransform& d);
    
    
    bool isDbg;
private:
    unsigned int getNewID();
    unsigned short pushBuffer();
    
    std::recursive_mutex mtx;
    inline void lck() { 
        //dbgLckCnt++;
        //std::cout <<"LCKCNT: " <<dbgLckCnt << "\n";
        //mtx.lock(); 
    };
    inline void ulck() { 
        //dbgLckCnt--; mtx.unlock(); 
    };
    
    std::deque< WMBuffer<T> > buffers;
    std::unordered_map<unsigned int, WMBufferRegion > regionMap;
};

#endif /* WMBUFFERSET_H */

