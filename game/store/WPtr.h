/* 
 * File:   WPtr.h
 * Author: Galen Swain
 *
 * Created on July 9, 2015, 2:35 PM
 */

#ifndef WPTR_H
#define	WPTR_H
#include <cstdint>

#pragma pack(push, 1)
template<typename T>
class WPtr {
public:
    inline WPtr() : rawData(0x0) {};
    inline WPtr(T *const data) : loaded(true), ptr(reinterpret_cast<uintptr_t>(data) >> 1) {};
    inline WPtr(const uint64_t &id) : loaded(false), idx(id) {};
    inline WPtr(const WPtr& o) : rawData(o.rawData) {};
    inline ~WPtr() {};
    
    union {
        struct {
            bool loaded : 1;
            uint64_t idx : 63;
        };
        struct {
            bool ldAlias : 1;
            uintptr_t ptr : 63;
        };
        uint64_t rawData : 64;
    };
    
    inline T* getPtr() const {
        if(__builtin_expect(loaded, 1)) return reinterpret_cast<T*>(ptr << 1); 
        else {
            
            return 0x0;
        }
    };
    
    inline T& operator->() const { return (T&)*getPtr(); };
    inline T& operator*() const { return (T&)*getPtr(); };
    
    inline operator T*() const { return getPtr(); };
    
    inline WPtr& operator=(const WPtr& o) { rawData = o.rawData; return *this; };
private:

};
#pragma pack(pop)

#endif	/* WPTR_H */

