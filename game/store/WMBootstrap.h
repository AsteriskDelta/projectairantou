/* 
 * File:   WMBootstrap.h
 * Author: Galen Swain
 *
 * Created on June 22, 2015, 7:06 PM
 */

#ifndef WMBOOTSTRAP_H
#define	WMBOOTSTRAP_H
#include <unordered_map>
#include <forward_list>
#include "game.h"
#include <cstring>

//Such that a draw attempt
#define WMBOOT_CHUNK_ITMS ((WMBOOT_CHUNK_SUBRATIO*WMBOOT_CHUNK_SUBRATIO*WMBOOT_CHUNK_SUBRATIO)/2)

#define WMBOOT_CHUNK_SIZE 16
class WorldObject;

namespace std {
    template <> struct hash<WMBootCoord> {
        std::size_t operator()(const WMBootCoord& k) const {//Assumes 64 bit OS
            static_assert(sizeof (std::size_t) >= 8, "size_t must be at least 64 bits wide!");
            using std::size_t; using std::hash;//Hash so that sign bits are ALWAYS significant
            return ( ROL64((long long)k.x, 36) << 20) ^ ( ((long long) k.y) ) ^ ( ROL64((long long)k.z, 12) << 16) ^ (((long long)k.w) << 62);
        }
    };
};
struct WMBootDeferred {//Deferred WOs to load
    Uint64 id;
};
struct WMBootChunkMeta {//sizeof() MUST == 64
    Uint16 sharedCount;
    Uint16 exclusiveCount;
    Uint16 pA, pB;
};
static_assert(sizeof(WMBootChunkMeta) == sizeof(char*), "WMBootChunkMeta must have a size equal to that of a pointer!");

class WMBootChunk {
public:
    inline WMBootChunk() : isLeaf(true), objCount(0), activeChildren(0), parent(NULL) { memset(children, 0x0, sizeof(childMeta)); };
    inline WMBootChunk(const WMBootCoord& nc) : coord(nc), isLeaf(true), objCount(0), activeChildren(0), parent(NULL) {};
    virtual ~WMBootChunk();//Remove sibling's references
    
    WMBootCoord coord; 
    
    bool isLeaf; 
    Uint32 objCount;
    Uint64 activeChildren;
    
    WMBootChunk *parent;
    WMBootChunk *siblings[6];
    union {//When the active flag isn't set, each child is a count of objects that could fit in the subsection
        WMBootChunk (*children)[WMBOOT_CHUNK_SUBRATIO][WMBOOT_CHUNK_SUBRATIO][WMBOOT_CHUNK_SUBRATIO];
        WMBootChunkMeta childMeta[WMBOOT_CHUNK_SUBRATIO][WMBOOT_CHUNK_SUBRATIO][WMBOOT_CHUNK_SUBRATIO];
    };
    
    std::forward_list<WorldObject*> objs;//Top-level physically bound object groups
    std::forward_list<WMBootDeferred> deferred;
    
    void recurse();
    
    void add(WorldObject *const o);
    void remove(WorldObject *const o);
protected:
    void overlayObj(WorldObject *const o);
};

//Simple static chunking storage, allowing for streaming and stored physical entities
class WMBootstrap {
public:
    WMBootstrap();
    WMBootstrap(const WMBootstrap& orig);
    virtual ~WMBootstrap();
    
    inline WMBootChunk* get(WMBootCoord c) { 
        c = c.getBase();
        auto it = world.find(c);
        if(it == world.end()) {//unlikely
            auto ins = world.insert(std::make_pair(c, WMBootChunk(c)) );
            return &(ins.first->second);
        } else  return &(it->second);
    };
    inline WMBootChunk* operator() (WMBootCoord c) { 
        return this->get(c);
    };
    
    void add(WorldObject *const o);
    void remove(WorldObject *const o);
protected:
    std::unordered_map<WMBootCoord, WMBootChunk> world;
};

#endif	/* WMBOOTSTRAP_H */