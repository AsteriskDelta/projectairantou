/* 
 * File:   WMBuffer.cpp
 * Author: Galen Swain
 * 
 * Created on June 19, 2015, 3:40 PM
 */
#ifndef WMBUFFER_CPP
#define WMBUFFER_CPP
#include "WMBuffer.h"
#include <client/Mesh.h>
#include <algorithm>
#include <cstring>

#include <fstream>
#include <sstream>

const WMBufferRegion WMBufferRegion::null = WMBufferRegion();

template<typename T>
WMBuffer<T>::WMBuffer() : allocated(), mtx() { MutexGuard mg(this->mtx);
  freeSpace = totalSpace = 0;
  mesh = NULL; transUB = NULL;
  dirty = dirtyUB = true;
  regionIDCounter = 0;
  isDbg = false;
  
}

template<typename T>
WMBuffer<T>::WMBuffer(const WMBuffer& o) : mtx() {
  o.mtx.lock();
  freeSpace = o.freeSpace;
  totalSpace = o.totalSpace;
  dirty = o.dirty;
  isDbg = o.isDbg;
  transform = o.transform;
  delta = o.delta;
  allocated = o.allocated;
  unallocated = o.unallocated;
  objectSize = o.objectSize;
  regionIDCounter = o.regionIDCounter;
  transUB = o.transUB; dirtyUB = o.dirtyUB;
  memcpy(transformData, o.transformData, sizeof(transformData));
  idTable = o.idTable;
  mesh = o.mesh;
  o.mtx.unlock();
}

template<typename T>
WMBuffer<T>::~WMBuffer() {
  deallocate();
}

template<typename T>
WMBuffer<T>& WMBuffer<T>::operator=(const WMBuffer& o) {
  std::lock(mtx, o.mtx);
  freeSpace = o.freeSpace;
  totalSpace = o.totalSpace;
  dirty = o.dirty;
  isDbg = o.isDbg;
  transform = o.transform;
  delta = o.delta;
  allocated = o.allocated;
  unallocated = o.unallocated;
  objectSize = o.objectSize;
  regionIDCounter = o.regionIDCounter;
  transUB = o.transUB; dirtyUB = o.dirtyUB;
  memcpy(transformData, o.transformData, sizeof(transformData));
  idTable = o.idTable;
  mesh = o.mesh;
  o.mesh = NULL; o.transUB = NULL;
  mtx.unlock();
  o.mtx.unlock();
  return *this;
}

template<typename T>
void WMBuffer<T>::initialize() {
  freeSpace = totalSpace = 0;
  mesh = NULL; transUB = NULL;
  dirty = dirtyUB = true;
  regionIDCounter = 0;
  isDbg = false;
  allocated = delta = std::list<WMBufferRegion>();
  
}

template<typename T>
void WMBuffer<T>::allocate(unsigned int newSpace) { _prsguard(); MutexGuard mg(this->mtx);//Defaults to WMBUFFER_MAX
  
  const unsigned int vertSize = sizeof(T);
  if(newSpace*vertSize < totalSpace) {
    return;//We can't shrink
  }
  
  MeshType mtype = isDbg? MeshType::Lines : MeshType::Tris;
  
  if(mesh == NULL) mesh = new Mesh<T>(mtype, newSpace, newSpace * WMBUFFER_IND_RATIO, true);
  else mesh->allocate(newSpace, newSpace * WMBUFFER_IND_RATIO, true);
  
  if(!isDbg) mesh->setTessellated(3);
  
  if(totalSpace < newSpace*vertSize) {
    unallocated.push_back(WMBufferRegion{totalSpace, newSpace*vertSize});
    this->regionWasInserted( std::prev(unallocated.end()) );
    freeSpace += (newSpace*vertSize) - totalSpace;
    totalSpace = newSpace*vertSize;
  }
  
  for(unsigned short i = 0; i < MaxTransforms; i++) {
    transformData[i].tint = V4(1.f, 1.f, 1.f, 1.f);
    //transformData[i].tmat = Mat4(1.f);
    transformData[i].dual = DualQuat();
    transformData[i].meta = V4(0.f);
  }
}
template<typename T>
void WMBuffer<T>::deallocate() { MutexGuard mg(this->mtx);
  if(mesh != NULL) delete mesh;
  if(transUB != NULL && _graphicsContextExists) delete transUB;
}

template<typename T>
WMBufferRegion WMBuffer<T>::getRegion(unsigned int length, bool commit) { _prsguard(); MutexGuard mg(this->mtx);
  //mtx.lock();
  if(freeSpace < length) {
    //mtx.unlock();
    return WMBufferRegion::null;
  }
  for(auto it = unallocated.begin(); it != unallocated.end(); ++it) {
    if(it->getSize() > length) {
      WMBufferRegion ret = WMBufferRegion(it->start, it->start + length);
      if(commit) {
        //std::cout << "adding region " << it->start << "->"<<(it->start+length) << "\n";
        bool inserted = false;
        ret.id = this->getNewRegionID();
        for(auto at = allocated.begin(); at != allocated.end(); ++at) {
          if(ret.end < at->end) {
            allocated.insert(at, ret);
            inserted = true;
            break;
          }
        }
        //for(auto it = allocated.begin(); it != allocated.end(); ++it) {
          //std::cout << "alloc region from " << it->start << "->" <<it->end << "\n";
        //}
        if(!inserted) allocated.push_back(ret);
        it->start = it->start + length;
        regionWasInserted(it);
      }
      
      
      //mtx.unlock();
      return ret;
    }
  }
  
  //mtx.unlock();
  return WMBufferRegion::null;
}

template<typename T>
bool WMBuffer<T>::canEmplace(MeshBuffer<T> *buff) {
  return this->canEmplace(buff->getVertCount()*sizeof(T));
}
template<typename T>
int WMBuffer<T>::couldEmplace(MeshBuffer<T> *buff) {
  return this->couldEmplace(buff->getVertCount()*sizeof(T));
}
template<typename T>
WMBufferRegion WMBuffer<T>::emplace(MeshBuffer<T> *buff) { _prsguard(); MutexGuard mg(this->mtx);
  return this->emplace(buff->getDataPtr(), buff->getVertCount()*sizeof(T), buff->getIndPtr(), buff->getIndCount());
}

template<typename T>
bool WMBuffer<T>::canEmplace(unsigned int length) { _prsguard(); MutexGuard mg(this->mtx);
  return  bool(this->getRegion(length, false)) && regionIDCounter < MaxTransforms;
}
template<typename T>
int WMBuffer<T>::couldEmplace(unsigned int length) { _prsguard(); MutexGuard mg(this->mtx);
  if(regionIDCounter >= MaxTransforms) return INT_MAX;
  unsigned int optScore = this->optimizeScore();
  
  if(optScore < length) return INT_MAX;
  else return int(optScore);
}

template<typename T>
WMBufferRegion WMBuffer<T>::emplace(char *nd, unsigned int length, const char *indD, unsigned int indCount) { _prsguard(); MutexGuard mg(this->mtx);
  unsigned int reqLength = 0;
  if(reqLength < length) reqLength = length;
  //std::cout << "fgetR " << reqLength << "\n";
  WMBufferRegion region = this->getRegion(reqLength, true);
  if(!region && length == 0) return region;
  /*if(!region) {
    region = this->getRegion(length, true);
    reqLength = length;
  }*/
  if(!region) {
    this->optimize();
    this->getRegion(reqLength, true);
  }
  if(!region) {
    Console::Error("Invalid region given by WMBuffer::getRegion!");
    return region;
  }
  
  thread_local unsigned char transIDTable[256];
  //Fill in table
  transIDTable[0] = region.id;
  /*for() {
    
  }*/
  
  T* darr = reinterpret_cast<T*>(nd);
  for(unsigned int i = 0; i < length/sizeof(T); i++) {
    darr[i].transformIDs.x = transIDTable[darr[i].transformIDs.x];
    darr[i].transformIDs.y = transIDTable[darr[i].transformIDs.y];
    darr[i].transformIDs.z = transIDTable[darr[i].transformIDs.z];
    darr[i].transformIDs.w = transIDTable[darr[i].transformIDs.w];
    darr[i].transformWeights.x = 255;
  }
  
  freeSpace -= region.getSize();
  bool ret = this->update(region, nd, length, indD, indCount);
  //std::cout << "emplaced with transid: " << region.id << "\n";
  return ret? region : WMBufferRegion::null;
}

template<typename T>
int WMBuffer<T>::optimizeScore() { _prsguard(); MutexGuard mg(this->mtx);
  
  unsigned int lastEnd = allocated.size() > 0? allocated.rbegin()->end : 0;
  if(lastEnd == 0 || freeSpace == 0) return 0;
  int ret = freeSpace - (totalSpace-lastEnd);
  std::cout << "opt score: " << ret << ", le: " << allocated.rbegin()->end/sizeof(T)  << "\n"; 
  return ret;
}
template<typename T>
bool WMBuffer<T>::optimize(float timeQuota) { _prsguard(); MutexGuard mg(this->mtx);
  unsigned int offset = 0;
  //mtx.lock();
  //std::cout << "attempting to optimize wmbuffer...\n";
  //return false;
  unsigned int lastEnd = allocated.size() > 0? allocated.rbegin()->end : 0;
  if(lastEnd == 0 || freeSpace == 0 || lastEnd - freeSpace <= 0) {
    //mtx.unlock();
    return true;
  }
  
  char* data = reinterpret_cast<char*>(mesh->getDataPtr());
  for(auto it = allocated.begin(); it != allocated.end(); ++it) {
    if(it->start > offset) {
      memmove(&data[offset], &data[it->start], it->getSize());
      it->end = offset + it->getSize(); it->start = offset;
    }
    offset = std::max(offset, it->end);
  }
  
  unallocated.clear();
  unallocated.push_front(WMBufferRegion(offset, totalSpace));
  
  dirty = true;
  //this->upload();
  //mtx.unlock();
  return true;
}
template<typename T>
bool WMBuffer<T>::update(WMBufferRegion& rr, char *nd, unsigned int length, const char *indD, unsigned int indCount) { _prsguard(); MutexGuard mg(this->mtx);
  unsigned int reqLength = 0;
  reqLength = std::max(length, reqLength);
  if(!rr) return false;
  
  //mtx.lock();
  if(rr.getSize() < reqLength && this->canEmplace(reqLength)) {
    //std::cout << "returning from update, " << rr.getSize() << " < " << reqLength << "\n";
    this->remove(rr);
    //mtx.unlock();
    return bool(rr = this->emplace(nd, length, indD, indCount));
  }
  
  WMBufferRegion *region = NULL;
  for(auto it = allocated.begin(); it != allocated.end(); ++it) {
    if(rr == *it) {
      region = &(*it);
    }
  }
  if(region == NULL) {
    //std::cout << "Region not emplaced in WMBuffer!\n";
  }
  //std::cout << "copying to ["<<mesh<<"], " << nd << "\n";
  //std::cout << "exact " << mesh->getDataPtr() << " from " << nd << "\n";
  //if(!isDbg) std::cout << "UPDATE CALL("<<((int*)indD)<< " up to " << indCount << "):\n";
  for(unsigned int i = 0; i < length/sizeof(T); i+= 2) {
    T* vert = &(((T*)nd)[i]);
    //if(!isDbg)std::cout << "v_"<<i<<" = " << glm::to_string(vert->pos) << "\n";
  }
  for(unsigned int i = 0; i < indCount; i++) {
    //if(!isDbg) std::cout << "gnd: " << (((WMIndType*)indD)[i])<< "\n";
  }
  
  memcpy(&(mesh->getDataPtr()[region->start]), nd, length);
  if(region->indData != NULL) delete[] region->indData;
  region->indData = new WMIndType[indCount];
  memcpy(region->indData, indD, indCount*sizeof(WMIndType));
  region->indCount = indCount;
  //std::cout << "adding region from " << region->start << " to " << region->end << " DAT:"<<region->indData<<"\n";
  rr = *region;
  if(rr.id >= 256) {
    std::cout << "INVALID RID: " <<rr.id << "\n\n";
  }
  delta.push_back(*region);
  dirty = true;
  //mtx.unlock();
  return true;
}
template<typename T>
bool WMBuffer<T>::update(WMBufferRegion& rr, MeshBuffer<T> *buff) { _prsguard(); MutexGuard mg(this->mtx);
  return this->update(rr, buff->getDataPtr(), buff->getVertCount()*sizeof(T), buff->getIndPtr(),buff->getIndCount());
}

template<typename T>
bool WMBuffer<T>::remove(WMBufferRegion region) { _prsguard();
  if(!region.isValid()) return true;
  mtx.lock();
  for(auto it = allocated.begin(); it != allocated.end(); ++it) {
    if(region == *it) {
      freeSpace += it->getSize();
      dirty = true;
      try {idTable.set(it->id, 0); regionIDCounter--; } 
      catch(...) { Console::Error("WMBuffer saved region id is invalid!"); };
      
      //Remove from allocated, insert to unalloc
      if(it->indData != NULL) delete[] it->indData;
      allocated.erase(it);
      auto insertedIter = unallocated.end();
      region.indData = NULL; region.indCount = 0; 
      for (auto at = unallocated.begin(); at != unallocated.end(); ++at) {
        if(at->start >= region.end) {
          insertedIter = unallocated.insert(at, region);
          break;
        }
      }
      if(insertedIter == unallocated.end()) unallocated.push_back(region);
      
      //Optimize unalloc, looping from the iter before to the iter after insertion
      auto endIter = std::next(insertedIter);
      for (auto at = (insertedIter == unallocated.begin())? unallocated.begin() : std::prev(insertedIter); at != unallocated.end() && at != endIter; ) {
        auto nt = std::next(at); if(nt == unallocated.end()) break;
        if(at->end == nt->start) {//combine
          at->end = nt->end;
          bool doBreak = nt == endIter;
          at = unallocated.erase(nt);
          if(at == unallocated.end() || doBreak) break;
        } else ++at;
      }
      
      //std::cout << "removed " << region.start << "->" << region.end << "\n";
      //for (auto at = unallocated.begin(); at != unallocated.end(); ++at) {
        //std::cout << "unalloc\t\t"<<at->start <<"\t->\t" << at->end << "\n";
      //}
      
      //Turns out we don't care, since indices get changed anyway
      //delta.push_back(region);
      mtx.unlock();
      return true;
    }
  }
  std::stringstream ss; ss << "WMBuffer::Remove failed on region " << region.start << " -> " << region.end << "\n";
  Console::Error(ss.str());
  mtx.unlock();
  return false;
}

template<typename T>
bool WMBuffer<T>::upload() { _prsguard(); MutexGuard mg(this->mtx);
  //std::cout << "Uploading:\n";
  if(mesh == NULL || !dirty) return false;
  else if(allocated.begin() == allocated.end()) return true;
  //mtx.lock();
  delta.sort();
  //std::cout << "allocated OK\n";
  WMIndType *indBuffer = (WMIndType*)mesh->getIndPtr(); unsigned int indOffset = 0;
  //char *const dataPtr = mesh->getDataPtr();
  const unsigned int newVCount = allocated.rbegin()->end/sizeof(T);
  //Determine how verts need to be uploaded, do so; MUST be done prior to indice updates!!!
  //mesh->setVertCount(newVCount);
  while(!delta.empty()) {
    WMBufferRegion region = delta.front(); delta.pop_front();
    //Expand update region as required
    auto next = delta.begin(); const unsigned int regionJoinThresh = 2048;
    while(next != delta.end() && (next->start - region.end) < regionJoinThresh) {//Relies on sorted delta order
      region.end = next->end;
      delta.pop_front();
      next = delta.begin();
    }
    //std::cout << "updating region " << region.start << "->" << region.end << "\n";
    //Update denoted region via subbuffer call
    //if(region.getSize() > 0) mesh->subVerts(region.start/sizeof(T), region.getSize()/sizeof(T));
    if(!isDbg) {
      for(unsigned int i = 0; i < region.getSize()/sizeof(T); i+= 2) {
        T* vert = &(((T*)mesh->getDataPtr())[region.start/sizeof(T)+i]);
        //if(!isDbg)std::cout << "v_"<<i<<" = " << glm::to_string(vert->pos) << "\n";
      }
    }
    //mesh->subVerts(region.start/sizeof(T), (region.end - region.start)/sizeof(T));
  }
  //if(!isDbg) std::cout << "build with " << allocated.size() << " regions";
  //Always rebuild indicies
  //if(allocated.size() > 1) std::swap(*allocated.begin(), *std::next(allocated.begin()));
  for(auto it = allocated.begin(); it != allocated.end(); ++it) {
    const WMBufferRegion &region = *it;
    const unsigned int baseInd = region.start/sizeof(T);
    //Copy to mesh's ind buffer
    memcpy(&indBuffer[indOffset], region.indData, region.indCount*sizeof(WMIndType));
    if(!isDbg) {
      //std::cout << "BUFFER INDS: " << region.start << " -> " << region.end << "\n";;
    }
    for(unsigned int i = indOffset; i < indOffset + region.indCount; i++) {
      //if(!isDbg) std::cout << "frm: " << region.indData[i] << "\n";
      //if(!isDbg) std::cout << "ind: " << indBuffer[i] << "\t\t" << baseInd << "\n";
      indBuffer[i] += baseInd;
    }
    //std::cout << "setting ind " << indOffset << " += " << region.indCount << " (" << it->start/sizeof(T) << "->"<<it->end/sizeof(T)<<") DAT:"<< region.indData<<"\n";
    indOffset += region.indCount;
  }
  
  this->uploadTransforms();
  riEnsureState();
  
  //Get data pertaining to bounds of allocated area, update mesh for rendering
  auto lastRegion = allocated.rbegin();
  mesh->setIndCount(indOffset);
  mesh->upload();
  
  if(!isDbg) mesh->setTessellated(3);
  //if(indOffset > 0) mesh->subInd(0, indOffset);
  //std::cout << "uploading mesh with " << newVCount << " verts, " << indOffset << " inds\n";
  //upload all indices
  //mesh->setIndCount(indOffset);
  //mesh->uploadInds(indOffset);
  riEnsureState();
  //mesh->upload();
  /*std::stringstream ss; ss << "dbg/ib_"<<_globalFrame;
  if(this->isDbg) ss << "dbg_";
  else ss << "tri_";
  ss <<".raw";
  std::ofstream out(ss.str());
  out.write(mesh->getIndPtr(), mesh->getIndCount()*sizeof(WMIndType));
  out.close();
  
  ss.str("");
  ss << "dbg/vb_"<<_globalFrame;
  if(this->isDbg) ss << "dbg_";
  else ss << "tri_";
  ss <<".raw";
  out.open(ss.str());
  out.write(mesh->getDataPtr(), mesh->getVertCount()*sizeof(T));
  out.close();*/
  
  
  riEnsureState();
  
  dirty = false;
  //mtx.unlock();
  return true;
}

template<typename T>
void WMBuffer<T>::uploadTransforms() { _prsguard();
  mtx.lock();
  if(transUB == NULL) {
    transUB = new UniformBlock();
    transUB->allocate();
  }
  //memset(transformData, 0xFF, sizeof(transformData));
  transUB->upload((char*)transformData, sizeof(transformData), true);
  dirtyUB = false;
  mtx.unlock();
  //std::cout << "uploading transform data "<<sizeof(transformData)<<"!\n";
}

template<typename T>
bool WMBuffer<T>::render() { _prsguard(); MutexGuard mg(this->mtx);
  if(!mesh) return false;
  //transformData[0].tmat = glm::translate(V3(sin(_globalTimef)*5, cos(_globalTimef)*5, 0.f));
  if(dirtyUB) uploadTransforms();
  if(dirty) upload();
  
  transUB->bindTo(14);
  mesh->draw();
  return true;
}

template<typename T>
void WMBuffer<T>::updateTransformData(unsigned short id, const WMBufferRegionTransform& d) { _prsguard(); MutexGuard mg(this->mtx);
  mtx.lock();
  //std::cout << "updating transform for #" << id << ", " << &(transformData[id]) << "<- " << &d << "\n";
  WMBufferRegionTransform *target = &(transformData[id]);
  //if(!(*target != d)) return;
  memcpy(target, &d, sizeof(WMBufferRegionTransform));
  dirtyUB = true;
  mtx.unlock();
}

//Called only on unallocated list
template<typename T>
void WMBuffer<T>::regionWasInserted(std::list<WMBufferRegion>::iterator it) { _prsguard();
  /*if(it != unallocated.begin()) {//Check recombination with previous
    auto prev = std::prev(it);
    if(prev->end == it->start) {
      it->start = prev->start;
      unallocated.erase(prev);
    }
  }
  auto next = ++it;
  if(next != unallocated.end()) {
    if(next->start == it->end) {
      it->end = next->end;
      unallocated.erase(next);
    }
  }*/
}

template<typename T>
unsigned short WMBuffer<T>::getNewRegionID() { _prsguard();
  mtx.lock();
  for(unsigned short i = 0; i < MaxTransforms; i++) {
    if(!idTable[i]) {
      idTable[i] = 1;
      regionIDCounter++;
      mtx.unlock();
      return i;
    }
  }
  mtx.unlock();
  Console::Error("WMBuffer::Region transform ID overrun!");
  return 0;
}

template class WMBuffer<WObjectVert>;
#endif