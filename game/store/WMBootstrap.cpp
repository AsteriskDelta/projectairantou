/* 
 * File:   WMBootstrap.cpp
 * Author: Galen Swain
 * 
 * Created on June 22, 2015, 7:06 PM
 */

#include "WMBootstrap.h"
#include "uni/WorldObject.h"

void WMBootChunk::recurse() {
  Uint8 i; Uint64 tmpActiveChildren = activeChildren;
  while((i = FSB64(tmpActiveChildren)) != sizeof(tmpActiveChildren)) {
    //Process active child
    
    
    tmpActiveChildren ^= (Uint64(0x1)) << i;//clear bit at idx
  }
}

void WMBootChunk::add(WorldObject *const o) {
  objs.push_front(o);
}
void WMBootChunk::remove(WorldObject *const o) {
  objs.remove(o);
}
WMBootChunk::~WMBootChunk() {
  
}

WMBootstrap::WMBootstrap() {
}

WMBootstrap::WMBootstrap(const WMBootstrap& orig) {
}

WMBootstrap::~WMBootstrap() {
}

void WMBootstrap::add(WorldObject *const o) {
  //WMBootCoord c;
  //for(Uint16 wpOffset = 0; bool(c = o->getWidePos(wpOffset)); wpOffset++) {
  //  get(c)->add(o);
  //}
}
void WMBootstrap::remove(WorldObject *const o) {
  //WMBootCoord c;
  //for(Uint16 wpOffset = 0; bool(c = o->getWidePos(wpOffset)); wpOffset++) {
  //  get(c)->remove(o);
  //}
}