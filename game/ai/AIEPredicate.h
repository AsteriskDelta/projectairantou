/* 
 * File:   AIEPredicate.h
 * Author: Galen Swain
 *
 * Created on June 30, 2015, 1:17 PM
 */

#ifndef AIEPREDICATE_H
#define	AIEPREDICATE_H
#include "AIPersonality.h"
#include "store/WPtr.h"

class AIEntity;
class AIEPredicate {
public:
    AIEPredicate();
    AIEPredicate(const AIEPredicate& orig);
    virtual ~AIEPredicate();
    
    AIPType liking;
    AIPType loving;
    AIPType attraction;
    AIPType loyalty;
    
    AIPType rivalry;
    AIPType respect;
    AIPType dominance;
    AIPType solidarity;
    
    AIPType familiarity;
    AIPType similarity;
    
    AIPType significance;
    
    WPtr<AIEntity> to;
    
    inline bool operator<(const AIEPredicate &o) const { return significance < o.significance; };
private:

};

#endif	/* AIEPREDICATE_H */

