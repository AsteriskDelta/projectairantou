/* 
 * File:   Naming.h
 * Author: Galen Swain
 *
 * Created on June 26, 2015, 9:27 AM
 */

#ifndef NAMING_H
#define	NAMING_H
#include <string>
#include <sstream>
#include "game.h"

namespace Naming { struct Specs; };
struct Name {
  std::string first, fSuffix, fPrefix;
  std::string middle;
  std::string family;
  std::string tru;
  
  inline operator std::string() const {
      return first + " " + middle + " " + family + " [" + tru + "]";
  }
  
  Naming::Specs classify();
};
inline std::ostream& operator<< (std::ostream& os, const Name& o) {
       os << std::string(o);
       return os;
}       

namespace Naming {
  struct Specs {
    Int16 gender;
    Int16 harsh;
    Int16 intel;
    Int16 high;
    Int16 myst;
    Uint8 actFlags;
    
    inline Specs() : actFlags(0xFF) {};
    
    
    inline float D(Uint8 d) const { 
        return float(*(&gender + d)) * float((actFlags >> d) & 0x1);
    }
    
    inline operator std::string() const {
        std::stringstream ret;
        ret << "Naming::Specs(gender:"<<int(gender)<<", ";
        ret << "harsh:"<<int(harsh)<<", ";
        ret << "intel:"<<int(intel)<<", ";
        ret << "high:"<<int(high)<<", ";
        ret << "myst:"<<int(myst)<<")";
        return ret.str();
    }
  };
  
  void Init();//Load and construct statistical models
  
  Name GetName();
  Name GetName(const Specs& specs);
};

inline std::ostream& operator<<(std::ostream& os, const Naming::Specs& o) {
    os << std::string(o);
    return os;
}  

#endif	/* NAMING_H */

