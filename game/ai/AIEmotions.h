/* 
 * File:   AIEmotions.h
 * Author: Galen Swain
 *
 * Created on June 30, 2015, 11:44 AM
 */

#ifndef AIEMOTIONS_H
#define	AIEMOTIONS_H

class AIEmotions {
public:
    AIEmotions();
    AIEmotions(const AIEmotions& orig);
    virtual ~AIEmotions();
private:

};

#endif	/* AIEMOTIONS_H */

