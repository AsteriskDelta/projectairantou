/* 
 * File:   AIGoal.h
 * Author: Galen Swain
 *
 * Created on June 28, 2015, 2:12 PM
 */

#ifndef AIGOAL_H
#define	AIGOAL_H

class AIGoal {
public:
    AIGoal();
    AIGoal(const AIGoal& orig);
    virtual ~AIGoal();
    
    inline bool operator<(const AIGoal o) const { return true; };
private:

};

#endif	/* AIGOAL_H */

