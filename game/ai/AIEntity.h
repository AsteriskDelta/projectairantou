/* 
 * File:   AIEntity.h
 * Author: Galen Swain
 *
 * Created on June 28, 2015, 1:42 PM
 */

#ifndef AIENTITY_H
#define	AIENTITY_H
#include <list>
#include <queue>
#include <map>
#include "AIGoal.h"
#include "AIEPredicate.h"
#include "store/WPtr.h"

class AIDrive; class AICritic;
class Entity;

struct AIDriveStruct {
    AIDrive* drive;
    inline AIDrive& operator->() { return *drive; };
};

class AIEntity {
public:
    AIEntity();
    AIEntity(const AIEntity& orig);
    virtual ~AIEntity();
    
    Entity *entity;
    std::list<AIDriveStruct> drives;
    std::map<WPtr<AIEntity>, AIEPredicate> relations;
    
    std::priority_queue<AIGoal> goals;
private:

};

#endif	/* AIENTITY_H */