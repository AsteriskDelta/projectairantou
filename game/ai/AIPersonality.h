/* 
 * File:   AIPersonality.h
 * Author: Galen Swain
 *
 * Created on July 6, 2015, 4:24 PM
 */

#ifndef AIPERSONALITY_H
#define	AIPERSONALITY_H

typedef char AIPType;

class AIPersonality {
public:
    AIPersonality();
    AIPersonality(const AIPersonality& orig);
    virtual ~AIPersonality();

    AIPType extroversion; //Positive is extroverted, negative is introverted
    AIPType intelligence;
    AIPType curiosity;
    AIPType idealism;

    AIPType stability; //Negative has strong emotional variance, postive a more stable personality
    AIPType outlook; //Negative tends to a negative outlook, positive- positive
    AIPType empathy;

    AIPType independence;
    AIPType dominance;
    AIPType bravery;
    AIPType liveliness;
    AIPType drive;

    AIPType vigilance;
    AIPType privateness;
    AIPType purity;
private:

};

#endif	/* AIPERSONALITY_H */

