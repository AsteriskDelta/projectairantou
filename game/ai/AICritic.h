/* 
 * File:   AICritic.h
 * Author: Galen Swain
 *
 * Created on June 28, 2015, 2:12 PM
 */

#ifndef AICRITIC_H
#define	AICRITIC_H

class AICritic {
public:
    AICritic();
    AICritic(const AICritic& orig);
    virtual ~AICritic();
private:

};

#endif	/* AICRITIC_H */

