/* 
 * File:   Naming.cpp
 * Author: Galen Swain
 * 
 * Created on June 26, 2015, 9:27 AM
 */

#include "Naming.h"
#include <lib/AppData.h>
#include <lib/ProbTable.h>
#include <lib/MDTable.h>

#include <iostream>
#include <unordered_map>
#include <cstring>
#include <cctype>
#include <set>

namespace Naming {
  std::unordered_map<std::string, ProbTable<std::string> > dlGroup;
  MDTable<5, Specs, Name> nameCache;
  const Uint8 ctxlMax = 2;
  
  std::set<std::string> filteredNames;
  
  void addPatt(char* start, unsigned int len) {
    if(len == 0 || start[0] == '#' || start[0] == 0x0 || start[0] == '\n') return;
    *start = toupper(*start);
    for(unsigned int i = 1; i < len; i++) start[i] = tolower(start[i]);
    
    char subbuff[8]; 
    for(unsigned int i = 0; i < len-1; i++) {
      std::string key = (i==0)? std::string()+start[i] : (std::string()+start[i-1]+start[i]);
      for(unsigned int j = 1; j < 6 && i+j < len; j++) {//For each acceptable range
        memset(&subbuff, 0x00, sizeof(subbuff));
        memcpy(&subbuff, &start[i+1], j); subbuff[j+1] = 0;
        int weight = pow(j, 3.6f);
        //if(weight > 1) weight /= 2;
        dlGroup[key].vote(std::string(subbuff), weight);
      }
    }
  }
  
  
  bool initAddPatterns(std::string name, std::string fullPath) {
    if(name.find(".nms.txt") == std::string::npos) return false;
    
    std::cout << "parsing " << fullPath << "\n";
    unsigned int fLen = 0;
    char* file = AppData::system.readFileText(fullPath, fLen);
    unsigned int fCur = 0;
    while(fCur < fLen) {
      unsigned int fLead = fCur;
      while(fLead < fLen && file[fLead] != '\n' &&file[fLead] != 0x00) {
        fLead++;
      }
      //file[fLead] = 0x00;//Already set by readFileText
      addPatt(&file[fCur], fLead-fCur);
      fCur = fLead+1;
    }
    
    delete[] file;
    return true;
  }
  
  void Init() {
    Directory dir = AppData::root;
    dir.cd("statsrc/names");
    if(dir) {
      dir.listCallback(&initAddPatterns);
    } else Console::Error("Naming statsrc dir doesn't exist!");
    
    for(auto it = dlGroup.begin(); it != dlGroup.end(); ++it) it->second.construct();
    
    //Build the censor list
    unsigned int fLen = 0, fCur = 0;
    char* file = AppData::root.readFileText("statsrc/wordFilter.txt", fLen);
    while(fCur < fLen) {
      unsigned int fLead = fCur;
      while(fLead < fLen && file[fLead] != '\n' &&file[fLead] != 0x00) {
        fLead++;
      }
      //file[fLead] = 0x00;
      const unsigned int insLength = fLead-fCur;
      if(insLength >= 2) {
        std::string ins = std::string(&file[fCur]);
        filteredNames.insert(ins);
      }
      fCur = fLead+1;
    }
    
    delete[] file;
    
    //Build the name cache table
    for(unsigned int i = 0; i < 1024; i++) {
      Name name = GetName();
      Specs specs = name.classify();
      
      nameCache.add(specs, name);
    }
  }
  
  const char vowels[] = {'a','e','i','o','u', 'y'};
  const char silents[] = {'h'};
  inline bool isConsonant(char c) {
    c = tolower(c);
    if(std::find(vowels, vowels+sizeof(vowels), c) != (vowels+sizeof(vowels)) ) return false;
    else return true;
  }
  inline bool isVowel(char c) {
    c = tolower(c);
    return std::find(vowels, vowels+sizeof(vowels), c) != (vowels+sizeof(vowels));
  }
  inline bool isSilent(char c) {
    c = tolower(c);
    return std::find(silents, silents+sizeof(silents), c) != (silents+sizeof(silents));
  }
  inline char swapcase(const char& c) {
    if(c <= 90) return tolower(c);
    else return toupper(c);
  }
  
  std::string getSubName( Uint8 len) {
    std::string ret = "";
    while(true) {
      char fChar = 65 + rand()%26;
      std::string dlKey = std::string()+fChar;
      ret += fChar; bool failed = false;
      
      while(ret.size() < len) {
        unsigned int i = ret.size()-1;
        std::string app = dlGroup[dlKey].get();
       // if(app == std::string()) { app = dlGroup[tolower(fChar)].get(); };
        if(app == std::string()) { failed = true; break; };
        ret.append(app);
        //std::cout << "appending " << app << "\n";
        fChar = ret[ret.size()-1];
        dlKey = ret.substr(ret.size()-2, 2);
      }
      if(failed) continue;
      else break;
    }
    
    //Minor corrections
    for(unsigned int i = 0; i < ret.size(); i++) {
      if(ret[i] == '.') ret[i] = '-';
      else if(ret[i] == ' ') ret[i] = '-';
    }
    for(unsigned int i = 0; i < ret.size()-1; i++) {
      if(isSilent(ret[i]) && isSilent(ret[i+1])) ret.erase(i, 1);
      else if(ret[i] == 'd' && isConsonant(ret[i+1])) ret.erase(i, 1);
    }
    if(ret.size() > 3 && ret[ret.size()-2] == '-') ret.push_back('i');
    while(ret.size() > 0 && (ret[ret.size()-1] == '-' || ret[ret.size()-1] == '\'') ) ret.pop_back();
    for(unsigned int i = 0; i < ret.size()-2; i++) {
      if(isConsonant(ret[i]) && isConsonant(ret[i+1]) && isConsonant(ret[i+2])) {
        ret[i+1] = vowels[rand()%sizeof(vowels)];
      } else if(isVowel(ret[i]) && isVowel(ret[i+1]) && isVowel(ret[i+2])) {
        ret.erase(i, 1);
      } 
    }
    if(ret[ret.size()-1] == 'c') ret.push_back('e');
    
    //Test for filters, redo if fails- WARNING: recursive call, should I ever block this?
    std::string sFilterKey = ret;
    std::transform(sFilterKey.begin(), sFilterKey.end(), sFilterKey.begin(), ::tolower);
    if(filteredNames.find(sFilterKey) != filteredNames.end()) return getSubName(len);
    
    return ret;
  }
  
  Name GetName() {
    Name ret;
    ret.first = getSubName(rand() % 4 + 2);
    ret.middle = getSubName(rand() % 3 + 3);
    ret.family = getSubName(rand() % 5 + 3);
    ret.tru = getSubName(rand() % 7 + 5);
    return ret;
  }
  
  Name GetName(const Specs& specs) {
    Name ret;
    Name* newName = nameCache.getNearest(specs);
    ret = *newName;
    
    nameCache.remove(newName);
    {
      Name name = GetName();
      Specs lspecs = name.classify();
      
      nameCache.add(lspecs, name);
    }
    
    return ret;
  }
};

Naming::Specs Name::classify() {
  Naming::Specs ret; std::string str;
  ret.gender = ret.harsh = ret.intel = ret.high = ret.myst = 0;
  
  for(int sw = 0; sw < 1; sw++) {
    if(sw == 0) str = first;
    else if(sw == 1) str = middle;
    else str = family;
    if(str.size() < 3) continue;
    
    //Feminine names generally end with a vowel, or a silent consonant
    const bool isECons = str[str.size()-1] == 'e'  && Naming::isConsonant(str[str.size()-2]) && !Naming::isConsonant(str[str.size()-3]);
    if(!isECons && (Naming::isVowel(str[str.size()-1]) || (Naming::isSilent(str[str.size()-1]) && Naming::isVowel(str[str.size()-2])) )) ret.gender -= 30;
    else ret.gender += 30;

    ret.high = Int16(str.size()-5) * 4;

    for(unsigned int i = 0; i < str.size(); i++) {
      char one = str[i];
      std::string two = (str.size()-i > 2)? str.substr(i, 2) : "  ";
      std::string three = (str.size()-i > 3)? str.substr(i, 3) : "   ";
      const bool twoVowels = Naming::isVowel(two[0]) && Naming::isVowel(two[1]) && two != "  ";
      const bool allVowels = Naming::isVowel(three[0]) && Naming::isVowel(three[1]) && Naming::isVowel(three[2]) && three != "   ";

      if(two == "ae" || two == "ie") ret.myst += 20;
      if(allVowels) {
        ret.gender -= 20;
        ret.myst += 10;
        ret.harsh -= 40;
        ret.intel += 20;
      }
      
      if(Naming::isVowel(one) && two[1] == '-') ret.gender -= 10;
      else if(two[1] == '-') ret.gender += 4;

      if(two == "ck" || two == "kk" || two == "cc" || one == 'x') ret.harsh += 10;
      else if(twoVowels) ret.harsh -= 6;

      if(one == 'x' || one == 'q') { ret.intel += 10; ret.myst += 10; };
    }
  }
  
  ret.myst = ret.high = ret.intel = ret.harsh = 0;
  return ret;
}