/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ResourceManager.h
 * Author: Galen Swain
 *
 * Created on March 5, 2016, 11:08 AM
 */

#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H
#include "game.h"

#ifndef TEXTURE_INC
template<bool Arr = false>
class Texture;
#endif
class UniformBlock;
class TextureAtlas;

namespace ResourceManager {
    bool Initialize();
    bool Release();
    void Cleanup();
    
    bool Commit();
    
    //void lct(unsigned short c, void* img);
    void CommitTexture(Uint16 cid, void* img);
    bool LoadTexture(std::string rPath, bool retain = false);
    unsigned short GetTexture(std::string rPath, bool retain = false);
    bool AtlasTexture(std::string rPath, bool retain = false);
    
    ImageRGBA* LoadImage(std::string rPath, bool retain = false);
    ImageRGBA* GetImage(std::string rPath, bool retain = false);
    void CloseImage(void* img);
    
    unsigned short GetParticle(std::string rPath, bool retain = false);
    unsigned short ParticleAtlasID(unsigned short in, bool waitFor = false);
    void CommitParticle(Uint16 cid, std::string resName, void *img);
    
    Texture<true>* GetDiffuse();
    UniformBlock* GetDiffuseUB();
    TextureAtlas* GetParticles();
    
    void CommitDiffUB();
};

#endif /* RESOURCEMANAGER_H */

