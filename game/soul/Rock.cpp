/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Rock.cpp
 * Author: Galen Swain
 * 
 * Created on April 7, 2016, 2:17 PM
 */

#include "Rock.h"
#include "gen/ScaledBase3D.h"
#include <noise/noise.h>
#include "uni/ContourObject.h"

namespace Soul {
  static noise::module::Perlin* continentBase;
  static noise::module::Curve * continentCurve;
  static noise::module::ScaledBase3D* continentScale;
  static noise::module::RidgedMulti* continentCarver_raw;
  static noise::module::ScaleBias* continentCarver;
  static noise::module::Add* continentCarved;
  static noise::module::Clamp* continentComplete;
  
  static noise::module::Add* continentPostNoise;
  static noise::module::Perlin* continentNoiseRaw;
  static noise::module::ScaleBias* continentNoiseScaled;
  
Rock::Rock() {
}

Rock::Rock(const Rock& orig) : Soul(orig) {
}

Rock::Rock(WorldObject *const newObj) : Soul(newObj) {
  
}

Rock::~Rock() {
}

bool Rock::genesis() {
  if(!Soul::genesis()) return false;
  ContourObject *const c = (ContourObject*)object;
  
  IdealContour *testContour, *testPath;
  testPath = new IdealContour(); Uint16 sliceTot = 0;
  IdealFeature tmpFeature;
    /*
    for(int i = 0; i <= sliceTot; i++) {
      float ci = float(i)/float(sliceTot)*10.f + center.z;
      testContour = new IdealContour;
      tmpFeature.type = IdealFeature::Line;
      tmpFeature.start = V3(  0.f,  0.f, 0.f); tmpFeature.end = V3( 10.f,  0.f, 0.f); tmpFeature.setHermite(V3( 0.f,-1.f, 0.f)); testContour->addFeature(tmpFeature);
      tmpFeature.start = V3( 10.f,  0.f, 0.f); tmpFeature.end = V3( 10.f, 10.f, 0.f); tmpFeature.setHermite(V3( 1.f, 0.f, 0.f));
      //tmpFeature.end = tmpFeature.end = V3( 10.f, 10.f+std::sin(M_PI*float(ci+center.x)/float(sliceTot)*.f), 0.f);
      testContour->addFeature(tmpFeature);
      //tmpFeature.start = V3( 10.f, 10.f, 0.f); tmpFeature.end = V3(  0.f, 10.f, 0.f); tmpFeature.setHermite(V3( 0.f, 1.f, 0.f)); testContour->addFeature(tmpFeature);
      V3 le = tmpFeature.end ;
      for(int j = 0; j < sliceTot; j++) {
        float cj = float(j)/float(sliceTot)*10.f + center.x;
        float s = float(j)/float(sliceTot), e = float(j+1)/float(sliceTot);
        float v = std::sin(M_PI*float(cj+ci)/float(sliceTot)*4.f);
        tmpFeature.start = le; tmpFeature.end = V3(  10.f-e*10.f, 10.f+v, 0.f);
        le = tmpFeature.end;
        tmpFeature.setHermite(V3( 0.f, 1.f, 0.f)); testContour->addFeature(tmpFeature);
      }
      tmpFeature.start = le; tmpFeature.end = V3(  0.f,  0.f, 0.f); tmpFeature.setHermite(V3(-1.f, 0.f, 0.f)); testContour->addFeature(tmpFeature);
      //tmpFeature.type = IdealFeature::Line; tmpFeature.start = V3(-1.f, 0.f, 0.f); tmpFeature.end = V3(1.f, 0.f, 0.f); testContour->addFeature(tmpFeature);
      testContour->prefab = true;
      testContour->quadratic = 1.f;
      testContour->parametricize();
      c->addContour(testContour);
    }
*/
  float seed = 547892;//rand();
  //continentBase->SetSeed(399430);
  continentBase->SetSeed(seed);
  const float hStep = 0.3f, tStep = 25.5f, rStep = 0.05f;
  for(float h = -10.f; h <= 10.f; h += hStep ) {
    //if(continentComplete->GetValue(0.f, h, 0.f) <= 0.08f) continue;
    //std::cout << "height " << h<<"\n";
    testContour = new IdealContour;
    tmpFeature.type = IdealFeature::Line;
    V3 le;
    
    for(float theta = -tStep; theta < 360.f; theta += tStep) {
      float r = 0.f; float val, x, y; unsigned int escapeCount = 0;
      do {
        r += rStep;
        x = r*cos(wrapAngle(theta)*DEG_TO_RAD);
        y = r*sin(wrapAngle(theta)*DEG_TO_RAD);
        val = continentComplete->GetValue(x, h, y);
        escapeCount++;
      } while(val > 0.02f && escapeCount < 255);
      
      //std::cout << "\tf(theta:" << wrapAngle(theta) << ", radius:" <<r<<")\t= "<<val<<"\t" << ((escapeCount == 255)?"ESCAPED":"")<<"\n";
      
      tmpFeature.start = le; tmpFeature.end = V3(x, y, 0.f);
      le = tmpFeature.end;
      tmpFeature.smoothness = (float(rand()%100)/100.f);
      tmpFeature.setHermite(V3( cos(wrapAngle(theta)*DEG_TO_RAD), sin(wrapAngle(theta)*DEG_TO_RAD), 0.f));
      if(theta >= 0.f && r > rStep) testContour->addFeature(tmpFeature);
    }
    
    float s = h, e = h+hStep;
    tmpFeature.type = IdealFeature::Line;
    tmpFeature.start = V3(0.f, s, 0.f); tmpFeature.end = V3(0.f, e, 0.f); 
    tmpFeature.level = sliceTot++; c->path.addFeature(tmpFeature);
    
    testContour->prefab = true;
    testContour->quadratic = 1.f;
    if(testContour->size() > 2) {
      testContour->parametricize();
      c->addContour(testContour);
    }
  }
  
  c->extrusionLength = ceil(float(sliceTot)/hStep);
  c->path.parametricize(true);
  c->subfield.setDefault("rock.roughBlack");
  
  return true;
}
bool Rock::update(float dt) {
  if(!Soul::update(dt)) return false;
  //for(WorldObject* &wo : object->children) {
    
  //}
  return true;
}

void Rock::Initialize() {
  double continentFrequency = 0.2;
  
  //Continent Base
  continentBase = new noise::module::Perlin();
  continentBase->SetOctaveCount (8);
  continentBase->SetFrequency (0.4);
  continentBase->SetPersistence (0.342);
  continentBase->SetLacunarity(1.772);
  continentBase->SetSeed(rand());
  
  continentScale = new noise::module::ScaledBase3D();
  continentScale->SetSourceModule(0, *continentBase);
  continentScale->SetCenter(0.f, 0.f, 0.f);
  continentScale->SetDistances(4.f, 2.f, 3.f);
  continentScale->SetBases(-1.0, 0.6);
  continentScale->SetMods(0.0, 1.0);
  continentScale->SetRange(0.0, 1.0);
  
  //Noise
  continentNoiseRaw = new noise::module::Perlin();
  continentNoiseRaw->SetOctaveCount (6);
  continentNoiseRaw->SetFrequency (1.7);
  continentNoiseRaw->SetPersistence (0.512);
  continentNoiseRaw->SetLacunarity(1.272);
  continentNoiseRaw->SetSeed(rand());
  
  continentNoiseScaled = new noise::module::ScaleBias();
  continentNoiseScaled->SetSourceModule (0, *continentNoiseRaw);
  continentNoiseScaled->SetScale (0.08);
  continentNoiseScaled->SetBias (0.015);
  
  //Carver
  continentCarver_raw = new noise::module::RidgedMulti();
  continentCarver_raw->SetOctaveCount (7);
  continentCarver_raw->SetFrequency(continentFrequency * 2.3);
  //continentCarver_raw->SetPersistence(0.6);
  continentCarver_raw->SetSeed(rand());
  
  continentCarver = new noise::module::ScaleBias();
  continentCarver->SetSourceModule (0, *continentCarver_raw);
  continentCarver->SetScale (-0.2);
  continentCarver->SetBias (0.1);
  
  continentCarved = new noise::module::Add();
  continentCarved->SetSourceModule (0, *continentScale);
  continentCarved->SetSourceModule (1, *continentCarver);
  
  //Apply noise
  continentPostNoise = new noise::module::Add();
  continentPostNoise->SetSourceModule (0, *continentCarved);
  continentPostNoise->SetSourceModule (1, *continentNoiseScaled);
  
  /*continentCurve = new noise::module::Curve();
  continentCurve->SetSourceModule(0, *continentPostNoise);
  continentCurve->AddControlPoint (0.0, 0.0);
  continentCurve->AddControlPoint (0.2, 0.2);
  continentCurve->AddControlPoint (0.4, 0.38);
  continentCurve->AddControlPoint (0.6, 0.52);
  continentCurve->AddControlPoint (0.85, 0.72);
  continentCurve->AddControlPoint (1.0, 1.0);*/
  
  continentComplete = new noise::module::Clamp();
  
  continentComplete->SetSourceModule (0, *continentScale);
  continentComplete->SetBounds (-1.0, 1.0);
}

};