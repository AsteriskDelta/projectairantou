/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Rock.h
 * Author: Galen Swain
 *
 * Created on April 7, 2016, 2:17 PM
 */

#ifndef ROCK_H
#define ROCK_H
#include "Soul.h"
namespace Soul {
class Rock : public Soul {
public:
    Rock();
    Rock(WorldObject *const newObj);
    Rock(const Rock& orig);
    virtual ~Rock();
    
    virtual bool genesis();
    virtual bool update(float dt);
    
    static void Initialize();
private:

};
};

#endif /* ROCK_H */

