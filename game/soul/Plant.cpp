/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Plant.cpp
 * Author: Galen Swain
 * 
 * Created on March 25, 2016, 12:32 PM
 */

#include "Plant.h"
namespace Soul {

PlantSoul::PlantSoul() : Soul(){
}
PlantSoul::PlantSoul(WorldObject *const newObj) : Soul(newObj) {
}

PlantSoul::PlantSoul(const PlantSoul& orig) : Soul(orig) {
}

PlantSoul::~PlantSoul() {
}

bool PlantSoul::genesis() {
  if(!Soul::genesis()) return false;
  
  return true;
}
bool PlantSoul::update(float dt) {
  if(!Soul::update(dt)) return false;
  for(WorldObjectPtr &wo : object->children) {
    
  }
  return true;
}

};
