/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Plant.h
 * Author: Galen Swain
 *
 * Created on March 25, 2016, 12:32 PM
 */

#ifndef PLANT_H
#define PLANT_H
#include "Soul.h"

namespace Soul {

class PlantSoul : public Soul {
public:
    PlantSoul();
    PlantSoul(WorldObject *const newObj);
    PlantSoul(const PlantSoul& orig);
    virtual ~PlantSoul();
    
    virtual bool genesis();
    virtual bool update(float dt);
protected:

};

};

#endif /* PLANT_H */

