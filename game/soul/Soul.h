/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Soul.h
 * Author: Galen Swain
 *
 * Created on March 23, 2016, 2:20 PM
 */

#ifndef SOUL_H
#define SOUL_H
#include "game.h"
#include "uni/WorldObject.h"
#include "rep/IdealContour.h"
namespace Soul {
class Soul {
public:
    Soul();
    Soul(WorldObject *const newObj);
    Soul(const Soul& orig);
    virtual ~Soul();
    
    struct ID {
        unsigned int type : 24;
        unsigned int seed : 24;
        unsigned int stage : 16;
        
        template<class Archive>
        void serialize(Archive & archive) {
            archive(CEREAL_NVP(type), CEREAL_NVP(seed), CEREAL_NVP(stage));
        }
    } id;
    
    WorldObject *object;
    
    void setObject(WorldObject *const newObj);
    
    virtual bool genesis();
    virtual bool update(float dt);
    
    Uint64 getID() const;
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(
                CEREAL_NVP(id)
                );
    }
protected:

};
};

#endif /* SOUL_H */

