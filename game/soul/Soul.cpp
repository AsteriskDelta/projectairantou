/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Soul.cpp
 * Author: Galen Swain
 * 
 * Created on March 23, 2016, 2:20 PM
 */

#include "Soul.h"
namespace Soul {
Soul::Soul() {
  object = NULL;
}

Soul::Soul(WorldObject *const newObj) {
  this->setObject(newObj);
}

Soul::Soul(const Soul& orig) {
}

Soul::~Soul() {
}

void Soul::setObject(WorldObject *const newObj) {
  object = newObj;
}

bool Soul::genesis() {
  if(object == NULL) return false;
  
  return true;
}
bool Soul::update(float dt) {
  if(object == NULL) {
    Console::Error("Soul::update() called with NULL object!");
    return false;
  }
  
  return true;
}

Uint64 Soul::getID() const {
  return 0;
}

}
