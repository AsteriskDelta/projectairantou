/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InstrRule.h
 * Author: Galen Swain
 *
 * Created on May 20, 2016, 4:17 PM
 */

#ifndef INSTRRULE_H
#define INSTRRULE_H
#include "game.h"
#include <vector>

class InstrChar;
class IdealContour;
class InstrSystem;

class InstrRule {
public:
    struct Instance {
      unsigned short type;
      Uint8 detA, detB;
      
      inline float str() const {
        return float(detA)/255.f;
      }
      inline void str(float v) {
        detA = (Uint8)round(clamp(v*255.f, 0.f, 255.f));
      }
    };
    
    InstrRule();
    virtual ~InstrRule();
    
    Uint16 id;
    InstrSystem *parent;
    std::vector<Uint16> applyToChars;
    
    struct SubdivData {
        float min, avg, max;
        float chance;
        float splitPropMin, splitPropMax;
        Uint16 toChar;
        Uint8 splitCount;
        bool isEmit;
    };
    std::vector<SubdivData> subdivs;
    
    void evaluate(Instance *const inst, float dt);
private:

};

#endif /* INSTRRULE_H */

