/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InstrSystem.cpp
 * Author: Galen Swain
 * 
 * Created on May 20, 2016, 4:13 PM
 */

#include "InstrSystem.h"
#include "InstrSubsystem.h"

InstrSystem::InstrSystem() {
}

InstrSystem::~InstrSystem() {
}

void InstrSystem::evaluate(InstrSystem::Instance *inst, float dt = 0.f) {
  for(unsigned int i = 0; i < inst->featureData.size(); i++) {
    const unsigned int featType = inst->featureData[i].type;
    InstrSubsystem::Instance *featInst = &(inst->featureData[i].inst);
    
    if(featType >= subsystems.size()) {
      std::stringstream ss; ss << "InstrSystem::evaluate instance called with " << featType << " >= " << subsystems.size() << " subsystem count";
      Console::Warning(ss.str());
    } else {
      subsystems[featType].evaluate(featInst, dt);
    }
  }
}