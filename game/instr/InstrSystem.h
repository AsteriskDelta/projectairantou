/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InstrSystem.h
 * Author: Galen Swain
 *
 * Created on May 20, 2016, 4:13 PM
 */

#ifndef INSTRSYSTEM_H
#define INSTRSYSTEM_H
#include "game.h"
#include "InstrChar.h"
#include "InstrRule.h"
#include "InstrSubsystem.h"
#include <vector>

class InstrSystem {
public:
    struct Instance {
        unsigned int seed;
        float deferredTime;
        WorldObjectPtr *object;//Assumed to be contour object
        
        struct FeatData {
            unsigned short type;
            InstrSubsystem::Instance inst;
        };
        std::vector<FeatData> featureData;
    };
    
    InstrSystem();
    virtual ~InstrSystem();
    
    std::vector<InstrChar> chars;
    std::vector<InstrRule> rules;
    std::vector<InstrSubsystem> subsystems;
    unsigned int seed;
    
    void evaluate(Instance *inst, float dt);
private:

};

#endif /* INSTRSYSTEM_H */

