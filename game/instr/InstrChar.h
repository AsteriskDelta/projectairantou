/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InstrChar.h
 * Author: Galen Swain
 *
 * Created on May 20, 2016, 4:16 PM
 */

#ifndef INSTRCHAR_H
#define INSTRCHAR_H
#include "game.h"
#include <vector>

class IdealContour;
class IdealFeature;
class InstrRule;

//Relates to a given particle, invoking rules as applicable
class InstrChar {
public:
    struct Instance {
        IdealFeature *feature;
    };
    
    InstrChar();
    InstrChar(const InstrChar& orig);
    virtual ~InstrChar();
    
    Uint16 id;
    std::vector<Uint16> appliedRuleIDs;
private:

};

#endif /* INSTRCHAR_H */

