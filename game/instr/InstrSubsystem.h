/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   InstrSubsystem.h
 * Author: Galen Swain
 *
 * Created on May 31, 2016, 3:30 PM
 */

#ifndef INSTRSUBSYSTEM_H
#define INSTRSUBSYSTEM_H
#include "InstrRule.h"
#include <vector>
class InstrSystem;

class InstrSubsystem {
public:
    struct Instance {
        unsigned int localID;
        float deferredTime;
        IdealContour *contour;
        std::vector<InstrRule::Instance> featureData;
    };
    
    InstrSubsystem();
    virtual ~InstrSubsystem();
    InstrSubsystem(InstrSubsystem&&) noexcept {}
    //InstrSubsystem& operator=(InstrSubsystem&&) noexcept { return *this; }
    
    InstrSystem *parent;
    std::vector<InstrRule> rules;
    
    void evaluate(Instance *inst, float dt);
private:

};

#endif /* INSTRSUBSYSTEM_H */

