/* 
 * File:   WorldManager.h
 * Author: Galen Swain
 *
 * Created on June 15, 2015, 8:40 PM
 */

#ifndef WORLDMANAGER_H
#define	WORLDMANAGER_H
#include "game.h"
#include "store/WMBuffer.h"
#include "uni/WorldObject.h"

//class ImageRGBA;

namespace WorldManager {
    void Initialize();
    void Cleanup();
    
    float GetEpsilon(WorldObject *const obj);
    
    //Should be called after physical links made
    WorldObjectPtr Emplace(WorldObject *const obj);
    
    void Remove(WorldObject *const obj);
    void WasUpdated(WorldObject *const obj, WOF f = WOF(WorldObject::ChangedAll));
    void DoUpdate(WorldObject *const obj);
    void TexCommit(Uint16 newID, void* img);
    void ImgClose(void *img);
    
    void ParticleTexCommit(Uint16 newID, std::string resName, void *img);
    
    void QueueLoadObject(WorldObjectPtr obj);
    void QueueUnloadObject(WorldObjectPtr obj);
    
    void Update();
    void Render();
    void RenderDebug();
};

#endif	/* WORLDMANAGER_H */

