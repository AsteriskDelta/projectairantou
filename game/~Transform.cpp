#include "Transform.h"
#include <cmath>

void Transform::doResolve(Transform &tr, int limit) {
  if(limit < 0) return;
  else if(parent == NULL) {
    tr = *this; return;
  }
  
  parent->doResolve(tr, --limit);
  tr.ratio *= ratio;
  tr.rotation *= rotation;
  tr.position += (position * tr.ratio) * tr.rotation;
  
  
}

void Transform::lookAt(const V3 &lookVector) {
  if(lookVector == position) return;

  glm::vec3 direction = glm::normalize(lookVector-position);
  float dot = glm::dot(V3_FORWARD, direction);
  if (fabs(dot - (-1.0f)) < 0.000001f) {
      rotation = glm::angleAxis(float(RAD_TO_DEG * (M_PI)), V3_UP);
    return;
  } else if (fabs(dot - (1.0f)) < 0.000001f) {
      rotation = glm::quat();
      return;
  }

  float angle = -RAD_TO_DEG * (acosf(dot));

  glm::vec3 cross = glm::normalize(glm::cross(V3_FORWARD, direction));
  rotation = glm::normalize(glm::angleAxis(angle, cross));
}

void Transform::setRotation(const V3 &direction, const V3& up) {
  float dot = glm::dot(V3_FORWARD, direction);
  if (fabs(dot - (-1.0f)) < 0.000001f) {
      rotation = glm::angleAxis(float(RAD_TO_DEG * (M_PI)), V3_UP);
    return;
  } else if (fabs(dot - (1.0f)) < 0.000001f) {
      rotation = glm::quat();
      return;
  }

  float angle = -RAD_TO_DEG * (acosf(dot));

  glm::vec3 cross = glm::normalize(glm::cross(V3_FORWARD, direction));
  rotation = glm::normalize(glm::angleAxis(angle, cross));
}