/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   River.h
 * Author: Galen Swain
 *
 * Created on May 19, 2016, 9:35 AM
 */

#ifndef RIVER_H
#define RIVER_H
#include "game.h"
#include "Landform.h"
#include <list>
#include <lib/V2.h>

namespace Landform {
class River : public Landform {
public:
    River();
    virtual ~River();
    
    struct Instance {
        V2 pos, dir;
        float weight;
        struct Node {
            V2 pos, dir;
            float weight;
            bool filled;
        };
        std::list<Node> nodes;
    };
    std::list<Instance> instances;
    unsigned int instanceCount;
    
    void postLand(Continent::Overmap *map);
    void postWind1(Continent::Overmap *map);
    void postWind2(Continent::Overmap *map);
    void postBiome(Continent::Overmap *map);
    
    void debugDraw(Continent::Overmap *map, ImageRGBA *img);
private:

};
};
#endif /* RIVER_H */

