/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Landform.cpp
 * Author: Galen Swain
 * 
 * Created on May 18, 2016, 1:24 PM
 */

#include "Landform.h"
#include <list>
#include "River.h"

namespace Landform {
  Landform::Landform() {
    
  }

  Landform::~Landform() {
  }

  void Landform::postLand(Continent::Overmap *map) {
    _unused(map);

  }

  void Landform::postWind1(Continent::Overmap *map) {
    _unused(map);

  }

  void Landform::postWind2(Continent::Overmap *map) {
    _unused(map);

  }

  void Landform::postBiome(Continent::Overmap *map) {
    _unused(map);

  }
  
  Uint64 Landform::GetXOR() { return 0x0; };
  
  void Landform::debugDraw(Continent::Overmap *map, ImageRGBA *img) {
    _unused(img); _unused(map);
  }

  std::list<Landform*> landforms;
  
  void LoadAll() {
    Register(new River());
  }
  
  void Cleanup() {
    for (Landform*& land : landforms) delete land;
  }

  void Register(Landform *landform) {
    landforms.push_back(landform);
  }

  void Unregister(Landform *landform) {
    landforms.remove(landform);
  }

  void PostLand(Continent::Overmap *map) {
    std::cout << "POSTLAND\n";
    for (Landform*& land : landforms) land->postLand(map);
  }

  void PostWind1(Continent::Overmap *map) {
    for (Landform*& land : landforms) land->postWind1(map);
  }

  void PostWind2(Continent::Overmap *map) {
    for (Landform*& land : landforms) land->postWind2(map);
  }

  void PostBiome(Continent::Overmap *map) {
    for (Landform*& land : landforms) land->postBiome(map);

  }
  
  
  void DebugDraw(Continent::Overmap *map, ImageRGBA *img) {
    for (Landform*& land : landforms) land->debugDraw(map, img);
  }
};