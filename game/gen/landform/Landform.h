/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Landform.h
 * Author: Galen Swain
 *
 * Created on May 18, 2016, 1:24 PM
 */

#ifndef LANDFORM_H
#define LANDFORM_H
#include "game.h"
#include <lib/V2.h>
#include <lib/Image.h>

namespace Continent {
class Overmap;
class Generator;
};

namespace Landform {
    class Landform {
    public:
        Landform();
        virtual ~Landform();
        
        virtual void postLand(Continent::Overmap *map);
        virtual void postWind1(Continent::Overmap *map);
        virtual void postWind2(Continent::Overmap *map);
        virtual void postBiome(Continent::Overmap *map);
        
        virtual void debugDraw(Continent::Overmap *map, ImageRGBA *img);
        
        Uint64 GetXOR();
    private:

    };
    
    void LoadAll();
    void Cleanup();

    void Register(Landform *landform);
    void Unregister(Landform *landform);

    void PostLand(Continent::Overmap *map);
    void PostWind1(Continent::Overmap *map);
    void PostWind2(Continent::Overmap *map);
    void PostBiome(Continent::Overmap *map);
    void DebugDraw(Continent::Overmap *map, ImageRGBA *img);
};

#endif /* LANDFORM_H */

