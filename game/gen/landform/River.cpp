/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   River.cpp
 * Author: Galen Swain
 * 
 * Created on May 19, 2016, 9:35 AM
 */

#include "River.h"
#include "gen/continent/Overmap.h"

namespace Landform {
  River::River() {
  }

  River::~River() {
  }
  
  static Continent::Overmap *om;
  static V2T<Uint16> lakeFF(V2T<Uint16> pos, Uint16 maxElevation, bool write, int depth) {
    if(pos.x >= om->dataSize.x || pos.y >= om->dataSize.y || depth <= 0) return V2T<Uint16>(65535, 65535);
    Continent::Overmap::Pixel *pix = om->get(pos);
    bool wasProc = pix->isProc(); pix->setProc();
    if(!wasProc && pix->elevation <= maxElevation && !pix->isWaterBody() && pix->elevation > om->seaLevel) {
      if(write) {
        pix->moisture = pix->flipVal = 256*255;
        pix->flags |= Continent::Overmap::Pixel::Lake;
      }
      
      V2T<Uint16> res;
      res = lakeFF(pos+V2T<Uint16>( 1, 0), maxElevation, write, depth-1); if(res.x != 0 || res.y != 0) return res;
      res = lakeFF(pos+V2T<Uint16>(-1, 0), maxElevation, write, depth-1); if(res.x != 0 || res.y != 0) return res;
      res = lakeFF(pos+V2T<Uint16>( 0, 1), maxElevation, write, depth-1); if(res.x != 0 || res.y != 0) return res;
      res = lakeFF(pos+V2T<Uint16>( 0,-1), maxElevation, write, depth-1); if(res.x != 0 || res.y != 0) return res;
    }// else if(!wasProc && pix->elevation == maxElevation && !pix->isWaterBody()) return pos;
    
    return V2T<Uint16>(0,0);
  }

  void River::postLand(Continent::Overmap *map) {
    return;
    std::cout << "RIVERPOST\n";
    instances.clear();
    instanceCount = 10;// + (map->rand()%50);
    std::cout << "RIVER gen " << instanceCount << " instances\n";
    for(unsigned int i = 0; i < instanceCount; i++) {
      Instance inst;
      bool isValid = false; unsigned int tryCount = 0;
      while(!isValid && tryCount++ < 1024) {
        inst.pos = V2(map->randf(0.f, map->size.x), map->randf(0.f, map->size.y));
        inst.dir = glm::normalize(-map->getNearest(inst.pos)->fdSmooth());
        inst.weight = map->randf(30.f, 100.f);
        if(inst.dir != V2(0.f) && map->getNearest(inst.pos)->elevation > map->seaLevel) isValid = true;
      }
      
      if(!isValid) continue;
      instances.push_back(inst);
    }
    
    for(auto it = instances.begin(); it != instances.end(); ++it) {
      const float dDist = glm::length(map->size/V2(map->dataSize)) * 1.f;
      V2 pos = it->pos; V2 dir = it->dir; float weight = it->weight*8.f;
      std::cout << "RIVER FROM " << glm::to_string(pos) << "\n";
      V2T<Uint16> lastDownPos = map->pixCoord(pos); Uint16 lastDownElevation = map->get(lastDownPos)->elevation;
      float effElevation = map->getNearest(pos)->elevation;
      while(pos.x > 0.f && pos.y > 0.f&&pos.x < map->size.x && pos.y < map->size.y && weight > 0.f) {
        pos  += dir*dDist;
        Continent::Overmap::Pixel *pix = map->getNearest(pos);
        int maxDepth = 800;
        
        if(float(pix->elevation) >= effElevation - 2.f) {//Attempt a fill
          V2T<Uint16> overflowPos = V2T<Uint16>(65535,65535); Uint16 lakeMaxElev = 0;
          for(Uint16 off = 1; off < (65535-lastDownElevation)/4; off++) {
            map->clearProc(); om = map; 
            V2T<Uint16> newOverflowPos = lakeFF(lastDownPos, lastDownElevation + off*4, false, maxDepth);
            if(newOverflowPos == V2T<Uint16>(65535, 65535)) break;
            else {
              overflowPos = newOverflowPos;
              lakeMaxElev = lastDownElevation + off*4;
            }
          }
          
          if(overflowPos != V2T<Uint16>(65535,65535)) {//Fill lake and continue river
            map->clearProc(); om = map;
            
          std::cout << "LAKE AT " << glm::to_string(lastDownPos) << "\n";
            lakeFF(lastDownPos, lakeMaxElev, true, maxDepth);
            if(overflowPos != V2T<Uint16>(0,0)) {
              pos = map->mapCoord(overflowPos);
              dir = glm::normalize(-map->get(overflowPos)->fdSmooth());
              //weight = 200.f;
              lastDownElevation = map->get(overflowPos)->elevation; 
              lastDownPos = overflowPos;
            } else break;
            break;
          }
        } else if(pix->elevation < lastDownElevation) {//Record minima
          lastDownElevation = pix->elevation;
          lastDownPos = map->pixCoord(pos);
        }
        
        //if(weight == 0.f) break;
        float newDirWeight = glm::length(-pix->fdSmooth())*2.5f;
        V2 newDir = glm::normalize(-pix->fdSmooth());
        if(fabs(newDir.length()) > 0.1f) dir = newDir;//glm::normalize(lerp(dir, newDir, newDirWeight*clamp(1.f - glm::dot(dir, newDir), 0.f, 1.f)));
        it->nodes.push_back(Instance::Node{pos, dir, weight, false});
        weight -= dDist;
        
        effElevation = effElevation*0.9f*(1.f/dDist) + float(pix->elevation)*0.1f*dDist;
        if(pix->elevation < map->seaLevel || weight <= 0.f) break;
      }
      
      std::cout << "RIVER TO " << glm::to_string(pos) << "\n";
    }
  }

  void River::postWind1(Continent::Overmap *map) {
    _unused(map);

  }

  void River::postWind2(Continent::Overmap *map) {
    _unused(map);
    

  }

  void River::postBiome(Continent::Overmap *map) {
    _unused(map);
    

  }
  
  void River::debugDraw(Continent::Overmap *map, ImageRGBA *img) {
    for(auto it = instances.begin(); it != instances.end(); ++it) {
      V2 lastPos = it->pos;
      for(auto n = it->nodes.begin(); n != it->nodes.end(); ++n) {
        ColorRGBA col = ColorRGBA(255, 192, 80, 255);
        img->drawLine(map->pixCoord(lastPos), map->pixCoord(n->pos), col);
        lastPos = n->pos;
      }
    }
  }

};