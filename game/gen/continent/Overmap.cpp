/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Overmap.cpp
 * Author: Galen Swain
 * 
 * Created on May 16, 2016, 4:02 PM
 */

#include "Overmap.h"
#include <lib/Image.h>
#include "Generator.h"
#include "gen/landform/Landform.h"
#include "gen/biome/Biome.h"

namespace Continent {

  ColorRGBA Overmap::Pixel::toPixelB() const {
    ColorRGBA ret = ColorRGBA(0, 0, 0, 255);
    for (Uint8 i = 0; i < 4; i++) {
      ::Biome::Biome *biomePtr = ::Biome::Get(biomeData[i].id);
      if(biomeData[i].id == 0 || biomePtr == nullptr) continue;
      ret += biomePtr->mapColor * (biomeData[i].weight / 255.f);
    }
    return ret;
  }
  
Overmap::Overmap() : size(0.f, 0.f), dataSize(0,0), scale(0.f, 0.f), name(""), data(NULL), gen(nullptr), seed(0) {
  
}

Overmap::Overmap(V2T<Uint16> ds, V2 newSize, Uint64 nseed) : size(0.f, 0.f), dataSize(0,0), scale(0.f, 0.f), name(""), data(NULL), gen(nullptr), seed(nseed)  {
  this->setSize(newSize);
  this->allocate(ds);
}
Overmap::Overmap(std::string newName) : size(0.f, 0.f), dataSize(0,0), scale(0.f, 0.f), name(""), data(NULL), gen(nullptr), seed(0)  {
  this->setName(newName);
  this->Load();
}

Overmap::~Overmap() {
  this->deallocate();
}

Uint16 Overmap::getBaseTemp(V2 pt) {
  return (Uint16)round(sin(M_PI* (0.5*(pt.y / size.y) + 0.25))*65535.f);//parent->getBaseTemp(parentOffset + pt);
}

void Overmap::allocate(V2T<Uint16> ds) {
  if(data != NULL) deallocate();
  dataSize = ds;
  data = new Pixel[dataSize.x*dataSize.y];
  genAllocate();
}
void Overmap::deallocate() {
  if(data != NULL) delete data;
  dataSize = V2T<Uint16>(0,0);
  genDeallocate();
}

void Overmap::setSize(V2 newSize) {
  size = newSize;
  scale = V2(dataSize.x, dataSize.y) / size;
}
void Overmap::setName(std::string newName) {
  name = newName;
}

bool Overmap::Save() {
  return false;
}
bool Overmap::Load() {
  return false;
}

bool Overmap::WriteImage(std::string path) {
  ImageRGBA *img = new ImageRGBA(dataSize.x, dataSize.y);
  unsigned int allocSize = ((unsigned int)dataSize.x) * dataSize.y;
  for(unsigned int i = 0; i < allocSize; i++) img->data[i] = data[i].toPixel();
  Landform::DebugDraw(this, img);
  bool ret = img->Save(path+"_A.png");
  
  for(unsigned int i = 0; i < allocSize; i++) img->data[i] = data[i].toPixelB();
  ret &= img->Save(path+"_B.png");
  
  for(unsigned int i = 0; i < allocSize; i++) img->data[i] = data[i].toPixelC();
  ret &= img->Save(path+"_C.png");
  
  return ret;
}

void Overmap::generate() {
  Biome::Initialize();
  gen->generateLand();
  gen->derivativeMap();
  Landform::PostLand(this);
  for(int i = 0; i < 4; i++) gen->blurMoisture();
  for(int i = 0; i < 16; i++) {
    gen->carryMoisture();
    for(int l = 0; l < 2; l++) gen->blurMoisture();
  }
  for(int i = 0; i < 3; i++) gen->blurMoisture();
  Landform::PostWind1(this);
  
  
  //Biomes
  gen->assignBiomes();
  
  Landform::PostBiome(this);
}

void Overmap::genAllocate() {
  if(gen != nullptr) this->genDeallocate();
  
  randGen.seed(seed);
  gen = new Generator(this);
  gen->allocate();
  
}
void Overmap::genDeallocate() {
  if(gen == nullptr) return;
  
  delete gen;
}

Uint64 Overmap::rand() {
  return randGen();
}

float Overmap::randf(float min, float max) {
  Uint64 v = this->rand();
  const float d = max - min;
  v = (v % Uint64(d*1024.f));
  
  return min + (float(v)/1024.f);
}

void Overmap::clearProc() {
  const unsigned int allocSize = ((unsigned int)dataSize.x) * dataSize.y;
  for(unsigned int i = 0; i < allocSize; i++) data[i].clearProc();
}

};