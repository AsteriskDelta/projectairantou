/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Generator.cpp
 * Author: Galen Swain
 * 
 * Created on May 16, 2016, 4:36 PM
 */

#include "Generator.h"
#include "Overmap.h"
#include <noise/noise.h>
#include <tbb/tbb.h>
#include "gen/ScaledBase3D.h"
#include "gen/biome/Biome.h"
#define XOR_CONTINENT 	0x3fea29d6
#define XOR_MAGIC	0xa15e936c
#define XOR_VOLCANIC	0x329daf21

namespace Continent {
  Generator::Generator(Overmap *p) {
    parent = p;
    this->allocate();
  }
  
  Generator::~Generator() {
    this->deallocate();
  }

  void Generator::allocate() {
    this->allocateLand();
  }
  
  void Generator::deallocate() {
    
  }
  
  inline void Generator::assignTemp(const V2& pt, Overmap::Pixel *const pix) {
    pix->temp = parent->getBaseTemp(pt) * (1.0 - pow(float(pix->elevation)/65535.f, 2.f)*0.4f);
  }
  
  void Generator::generateLand() {
    Uint64 avgHeight = 0; Uint16 omSeaLevel = 72*256;
    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      Uint64 rowHeightSum = 0;
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        Overmap::Pixel *const p = parent->get(x,y);
        const V2 coord = V2(x,y)/V2(parent->dataSize) * parent->size;

        p->elevation = (Uint16)floor(65535.f * continentComplete->GetValue(coord.x, coord.y, 0.f));
        //owner->SetBaseTempWind(V2T<Uint16>(x, y));//getBaseTemp(x, y, elevation);
        //p->moisture = (p->elevation < omSeaLevel) ? 255*256 : 0;
        p->aura = 0.f;//floor(255.0 * magicCurve->GetValue(xCoord, yCoord, 0.0));
        this->assignTemp(coord, p);
        //p->flipVal = p->moisture;
        
        rowHeightSum += p->elevation;
      }
      avgHeight += rowHeightSum/parent->dataSize.y;
    }
    //);
    avgHeight /= parent->dataSize.x;
    parent->seaLevel = float(avgHeight)/2.f * 0.8f + omSeaLevel/2;
    
    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        Overmap::Pixel *const p = parent->get(x,y);
        
        p->flipVal = p->moisture = (p->elevation < parent->seaLevel) ? 256*255 : 0;
        if(p->moisture != 0) p->flags |= Overmap::Pixel::Ocean;
      }
    }
    //);
  }
  
  
  void Generator::derivativeMap() {
    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        V2T<Uint16> pt(x,y);
        Overmap::Pixel *const p = parent->get(pt);
        float dx = parent->unitsPerPX().x * float(parent->getSafe(pt+V2T<Uint16>(1,0))->elevation - parent->getSafe(pt-V2T<Uint16>(1,0))->elevation) / 2.f;
        float dy = parent->unitsPerPX().y * float(parent->getSafe(pt+V2T<Uint16>(0,1))->elevation - parent->getSafe(pt-V2T<Uint16>(0,1))->elevation) / 2.f;
        
        dx /= 16.f; dy /= 16.f;
        p->dElevation = V2T<Int8>(clamp((int)round(dx), -128, 127), clamp((int)round(dy), -128, 127));
        //Wind is the deravitive of temp
        dx = float(parent->getSafe(pt+V2T<Uint16>(1,0))->temp - parent->getSafe(pt-V2T<Uint16>(1,0))->temp) / 2.f;
        dy = float(parent->getSafe(pt+V2T<Uint16>(0,1))->temp - parent->getSafe(pt-V2T<Uint16>(0,1))->temp) / 2.f;
        
        dx /= -3.f/(parent->dataSize.x/1024.0); dy /= -3.f/(parent->dataSize.y/1024.0);
        p->flipVec = p->wind = V2T<Int8>(clamp((int)round(dx), -128, 127), clamp((int)round(dy), -128, 127));
      }
    }
    //);
    
    //Smoothed
    const int W = 5;
    double kernel[W][W];
    {
    double sigma = 1;
    double mean = W/2.0;
    double sum = 0.0; // For accumulating the kernel values
    for (int x = 0; x < W; ++x)  {
        for (int y = 0; y < W; ++y) {
            kernel[x][y] = exp( -0.5 * (pow((x-mean)/sigma, 2.0) + pow((y-mean)/sigma,2.0)) )
                             / (2 * M_PI * sigma * sigma);

            // Accumulate the kernel values
            sum += kernel[x][y];
        }
    }

// Normalize the kernel
    for (int x = 0; x < W; ++x) for (int y = 0; y < W; ++y) kernel[x][y] /= sum;
    }

    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        V2T<Uint16> pt(x,y);
        Overmap::Pixel *const p = parent->get(pt);
        V2 sum = V2(0.f); float tot = 0.f;
        const int kSize = 2;
        for(int kx = int(x)-kSize; kx < int(x)+kSize; kx++) {
          for(int ky = int(y)-kSize; ky < int(y)+kSize; ky++) {
            Overmap::Pixel *const kp = parent->getSafe(kx, ky);
            const float weight = kernel[kx - (int(x)-kSize)][ky - (int(y)-kSize)];
            sum += weight * V2(kp->dElevation);
            tot += weight;
          }
        }
        p->dSmooth = sum/tot;
      }
    }
    //);
  }
  
  void Generator::blurMoisture() {
    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        V2T<Uint16> pt(x,y);
        Overmap::Pixel *const p = parent->get(pt);
        p->flipVal = p->moisture;
      }
    }
    //);
    //Smoothed
    const int W = 15;
    double kernel[W][W];
    {
      double sigma = W*3.0/3.0;
      double mean = W/2.0;
      double sum = 0.0; // For accumulating the kernel values
      for (int x = 0; x < W; ++x)  {
          for (int y = 0; y < W; ++y) {
              kernel[x][y] = exp( -0.5 * (pow((x-mean)/sigma, 2.0) + pow((y-mean)/sigma,2.0)) )
                               / (2 * M_PI * sigma * sigma);

              // Accumulate the kernel values
              sum += kernel[x][y];
          }
      }

      // Normalize the kernel
      for (int x = 0; x < W; ++x) for (int y = 0; y < W; ++y) kernel[x][y] /= sum;
    }

    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        V2T<Uint16> pt(x,y);
        Overmap::Pixel *const p = parent->get(pt);
        if(p->moisture >= 65000) continue;
        float sum = 0.f; float tot = 0.f;
        const int kSize = 7, kStride = 1;
        for(int kx = int(x)-kSize*kStride; kx < int(x)+kSize*kStride; kx += kStride) {
          for(int ky = int(y)-kSize*kStride; ky < int(y)+kSize*kStride; ky += kStride) {
            Overmap::Pixel *const kp = parent->getSafe(kx, ky);
            const float weight = kernel[(kx - (int(x)-kSize))/kStride][(ky - (int(y)-kSize))/kStride];
            sum += weight * kp->flipVal;
            tot += weight;
          }
        }
        p->moisture = sum/tot;
      }
    }
    //);
    /*tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    //for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        V2T<Uint16> pt(x,y);
        Overmap::Pixel *const p = parent->get(pt);
        p->flipVal = p->moisture;
      }
    });*/
  }
  
  void Generator::carryMoisture() {
    //Prepare flip val
    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        V2T<Uint16> pt(x,y);
        Overmap::Pixel *const p = parent->get(pt);
        p->flipVal = p->moisture;
      }
    }
    //);
    const float wStride = 2.5f * ((parent->dataSize.x + parent->dataSize.y) / 2.f)/1024.f;
        
    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        V2T<Uint16> pt(x,y);
        Overmap::Pixel *const p = parent->get(pt);
        float sum = 0.f; bool isSrc = p->moisture >= 65000;
        if(isSrc) continue;
        //p->wind = V2T<Int8>(64, 64);//-p->dSmooth;
        unsigned int steps = sqrt(float(p->wind.x*p->wind.x + p->wind.y*p->wind.y));
        for(unsigned int i = 0; i < steps; i++) {
          float frac = float(i)/float(steps-1);
          V2T<Uint16> opt(x - frac*p->wind.x*wStride, y - frac*p->wind.y*wStride);
          Overmap::Pixel *const o = parent->getSafe(opt);
          
          const float mContrib = 0.82f*(1.f/steps) * o->flipVal * (1.f - frac);
          sum += mContrib;
        }
        p->moisture = clamp((int)round(sum + float(p->flipVal)*0.5f), 0, 65535);
      }
    }
    //);
  }
  
  void Generator::assignBiomes() {
    //tbb::parallel_for((Uint16)0, parent->dataSize.x, [&](Uint16 x) {
    for(Uint16 x = 0; x < parent->dataSize.x; x++) {
      for(Uint16 y = 0; y < parent->dataSize.y; y++) {
        V2T<Uint16> pt(x,y);
        Overmap::Pixel *const p = parent->get(pt);
        std::list<Biome::Weights> weights = Biome::Query(p);
        Uint8 bid = 0;
        for(auto it = weights.begin(); it != weights.end() && bid < 4; it++, bid++) {
          p->biomeData[bid].id = weights.begin()->biome->getID();
          p->biomeData[bid].weight = (Uint8)round(weights.begin()->weight*255.f);
        }
        /*if(weights.begin() != weights.end()) {
          p->biomeA = weights.begin()->biome->getID();
          p->biomeAWeight = (Uint8)round(weights.begin()->weight*15.f);
        }
        if(weights.size() > 1) {
          p->biomeB = weights.rbegin()->biome->getID();
          p->biomeBWeight = (Uint8)round(weights.rbegin()->weight*15.f);
        }*/
      }
    }
    //);
  }
  
  void Generator::allocateLand() {
    double continentFrequency = 0.0025f;
    int omOctaves = 26;
  
    //Continent Base
    continentBase = new noise::module::Perlin();
    continentBase->SetOctaveCount (omOctaves);
    continentBase->SetFrequency (continentFrequency);
    continentBase->SetPersistence (0.69);
    continentBase->SetLacunarity(1.652);
    continentBase->SetSeed(parent->getSeed(XOR_CONTINENT));

    continentScale = new noise::module::ScaledBase3D();
    continentScale->SetSourceModule(0, *continentBase);
    continentScale->SetCenter(parent->size.x / 2.f, parent->size.y / 2.f, 0.f);
    continentScale->SetDistances(parent->size.x/2.f, parent->size.y/2.f, 10.f);
    continentScale->SetBases(0.8, 0.1);
    continentScale->SetExponent(1.0);

    //Noise
    continentNoiseRaw = new noise::module::Perlin();
    continentNoiseRaw->SetOctaveCount (omOctaves);
    continentNoiseRaw->SetFrequency (1.5*3.f*continentFrequency);
    continentNoiseRaw->SetPersistence (0.612);
    continentNoiseRaw->SetLacunarity(1.272);
    continentNoiseRaw->SetSeed(parent->getSeed(XOR_CONTINENT));

    continentNoiseScaled = new noise::module::ScaleBias();
    continentNoiseScaled->SetSourceModule (0, *continentNoiseRaw);
    continentNoiseScaled->SetScale (0.08);
    continentNoiseScaled->SetBias (0.97);

    //Carver
    continentCarver_raw = new noise::module::RidgedMulti();
    continentCarver_raw->SetOctaveCount (omOctaves);
    continentCarver_raw->SetFrequency(continentFrequency * 1.5);
    continentNoiseRaw->SetLacunarity(1.672);
    //continentCarver_raw->SetPersistence(0.6);
    continentCarver_raw->SetSeed(parent->getSeed(XOR_CONTINENT + 0xFF));

    continentCarver = new noise::module::ScaleBias();
    continentCarver->SetSourceModule (0, *continentCarver_raw);
    continentCarver->SetScale (-0.4);
    continentCarver->SetBias (1.0);

    continentCarved = new noise::module::Multiply();
    continentCarved->SetSourceModule (0, *continentScale);
    continentCarved->SetSourceModule (1, *continentCarver);

    //Apply noise
    continentPostNoise = new noise::module::Multiply();
    continentPostNoise->SetSourceModule (0, *continentCarved);
    continentPostNoise->SetSourceModule (1, *continentNoiseScaled);
    
    continentMountainDetail = new noise::module::RidgedMulti();
    continentMountainDetail->SetOctaveCount (omOctaves);
    continentMountainDetail->SetFrequency(continentFrequency * 5.5);
    continentMountainDetail->SetLacunarity(1.272);
    //continentCarver_raw->SetPersistence(0.6);
    continentMountainDetail->SetSeed(parent->getSeed(XOR_CONTINENT + 0xCF));
    
    continentMountainDetailSC = new noise::module::ScaleBias();
    continentMountainDetailSC->SetSourceModule (0, *continentMountainDetail);
    continentMountainDetailSC->SetScale (0.15);
    continentMountainDetailSC->SetBias (0.88);
    
    continentMountainDetailed= new noise::module::Min();
    continentMountainDetailed->SetSourceModule (0, *continentPostNoise);
    continentMountainDetailed->SetSourceModule (1, *continentMountainDetailSC);
    
    //Final noise pass
    auto continentNoiseRaw2 = new noise::module::Perlin();
    continentNoiseRaw2->SetOctaveCount (omOctaves);
    continentNoiseRaw2->SetFrequency (1.5*4.f*continentFrequency);
    continentNoiseRaw2->SetPersistence (0.752);
    continentNoiseRaw2->SetLacunarity(1.302);
    continentNoiseRaw2->SetSeed(parent->getSeed(XOR_CONTINENT));
    
    auto nrmDet = new noise::module::RidgedMulti();
    nrmDet->SetOctaveCount (omOctaves);
    nrmDet->SetFrequency(continentFrequency * 6.5);
    nrmDet->SetLacunarity(1.272);
    //continentCarver_raw->SetPersistence(0.6);
    nrmDet->SetSeed(parent->getSeed(XOR_CONTINENT + 0xAF));
    
    auto nrmTot = new noise::module::Add();
    nrmTot->SetSourceModule (0, *nrmDet);
    nrmTot->SetSourceModule (1, *continentNoiseRaw2);
    
    auto continentNoise2 = new noise::module::ScaleBias();
    continentNoise2->SetSourceModule (0, *continentNoiseRaw2);
    continentNoise2->SetScale (0.15);
    continentNoise2->SetBias (0.85);
    
    auto continentPostNoise2 = new noise::module::Multiply();
    continentPostNoise2->SetSourceModule (0, *continentMountainDetailed);
    continentPostNoise2->SetSourceModule (1, *continentNoise2);

    continentCurve = new noise::module::Curve();
    continentCurve->SetSourceModule(0, *continentPostNoise2);
    continentCurve->AddControlPoint (-1.0, 0.0);
    continentCurve->AddControlPoint (0.3, 0.3);
    //continentCurve->AddControlPoint (0.8, 0.75);
    //continentCurve->AddControlPoint (0.95, 0.88);
    //continentCurve->AddControlPoint (1.0, 1.0);
    //continentCurve->AddControlPoint (0.2, 0.2);
    //continentCurve->AddControlPoint (0.4, 0.38);
    //continentCurve->AddControlPoint (0.6, 0.52);
    continentCurve->AddControlPoint (0.45, 0.48);
    
    continentCurve->AddControlPoint (0.85, 0.75);
    //continentCurve->AddControlPoint (0.98, 0.88);
    continentCurve->AddControlPoint (1.4, 0.999);

    continentComplete = new noise::module::Clamp();

    continentComplete->SetSourceModule (0, *continentCurve);
    continentComplete->SetBounds (0.0, 1.0);
  }
};