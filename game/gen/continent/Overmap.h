/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Overmap.h
 * Author: Galen Swain
 *
 * Created on May 16, 2016, 4:02 PM
 */

#ifndef CONTINENT_OVERMAP_H
#define CONTINENT_OVERMAP_H
#include "game.h"
#include <lib/V2.h>
#include <lib/Color.h>
#include <random>
#include <cstring>

namespace Planet {
    class Overmap;
};

namespace Continent {
class Generator;
class Overmap {
public:
    friend class Generator;
    struct Pixel {
        enum {
            Proc = BIT00,
            WaterBody = BIT01 | BIT02,
            Ocean = BIT01,
            Lake = BIT02,
            Land = 0x0,
        };
        
        Uint16 elevation;
        Uint16 moisture;
        Uint16 temp;
        struct {
            Uint8 aura : 4;
            Uint8 meta : 4;
            Uint8 flags;
        };
        V2T<Int8> wind;
        V2T<Int8> dElevation;
        
        /*struct {
            Uint16 biomeA;
            Uint16 biomeB;
            Uint8 biomeAWeight : 4;
            Uint8 biomeBWeight : 4;
        };*/
        struct BiomeData {
            Uint16 id;
            Uint8 weight;
        };
        struct BiomeData biomeData[4];
        
        V2T<Int8> flipVec;
        V2T<Int8> dSmooth;
        Uint16 flipVal;
        
        Uint16 magic, divine;
        
        inline Pixel() : elevation(0), moisture(0), temp(0),aura(0), meta(0), flags(0x0), wind(0,0), dElevation(0,0), magic(0), divine(0) {
            memset(biomeData, 0x0, sizeof(biomeData));
        };
        
        inline ColorRGBA toPixel() const {
            return ColorRGBA(temp/256, elevation/256, moisture/256);
        }
        ColorRGBA toPixelB() const;
        inline ColorRGBA toPixelC() const {
            return ColorRGBA(dSmooth.x+128, dSmooth.y+128, magic/256);
        }
        
        inline V2 fdSmooth() {
            V2 ret = V2(dSmooth.x/127.f, dSmooth.y / 127.f);
            ret = glm::min(ret, V2(1.f, 1.f));
            return ret;
        }
        
        inline bool isWaterBody() {return (flags & WaterBody) != 0x0; };
        inline bool isLake() {return (flags & Lake) != 0x0; };
        inline bool isOcean() {return (flags & Ocean) != 0x0; };
        inline bool isLand() { return (flags & WaterBody) == 0x0; };
        
        inline bool isProc() { return (flags & Proc) != 0x0; };
        inline void setProc() { flags |= Proc; };
        inline void clearProc() { flags &= ~Proc; };
        
        inline Uint16 slopeMagnitude() const {
            int v = sqrt(dSmooth.x*dSmooth.x + dSmooth.y*dSmooth.y);
            return (Uint16)clamp(v, 0, 65535);
        } 
        inline Uint16 windMagnitude() const {
            int v = sqrt(wind.x*wind.x + wind.y*wind.y);
            return (Uint16)clamp(v, 0, 65535);
        } 
    };
    
    Overmap();
    Overmap(V2T<Uint16> ds, V2 newSize,Uint64 nseed = 0);
    Overmap(std::string newName);
    virtual ~Overmap();
    
    V2 size;
    V2T<Uint16> dataSize;
    V2 scale;
    std::string name;
    Pixel *data;
    
    Generator *gen;
    Uint64 seed;
    
    Planet::Overmap *parent;
    V2 parentOffset;
    std::mt19937 randGen;
    Uint16 seaLevel;
    
    inline Uint64 getSeed(Uint64 k) { return (seed*k)^k; };
    
    inline V2T<Uint16> pixCoord(V2 pt) {
        return glm::min(V2T<Uint16>(pt/size * V2(dataSize)), dataSize-V2T<Uint16>(1));
    }
    inline V2 mapCoord(V2T<Uint16> pt) {
        return glm::min(V2(pt)/V2(dataSize)*size, size);
    }
    
    inline V2 unitsPerPX() {
        return V2(dataSize)/V2(size);
    }
    
    inline Pixel* get(V2T<Uint16> pt) { return &(data[pt.x + pt.y*dataSize.x]); };
    inline Pixel* get(Uint16 x, Uint16 y) { return &(data[x + y*dataSize.x]); };
    inline Pixel* getSafe(V2T<Uint16> pt) { pt = glm::min(pt, dataSize-V2T<Uint16>(1)); return this->get(pt); };
    inline Pixel* getSafe(Uint16 x, Uint16 y) { x = min(x, (Uint16)(dataSize.x-1)); y = min(y, (Uint16)(dataSize.y-1)); return &(data[x + y*dataSize.x]); };
    
    inline Pixel* getNearest(V2 pt) {
        return this->get(this->pixCoord(pt)); 
    }
    
    Uint16 getBaseTemp(V2 pt);
    
    void allocate(V2T<Uint16> ds);
    void setSize(V2 newSize);
    void setName(std::string name);
    void deallocate();
    
    void generate();
    void genAllocate();
    void genDeallocate();
    
    bool Save();
    bool Load();
    bool WriteImage(std::string path);
    
    Uint64 rand();
    float randf(float min, float max);
    
    void clearProc();
private:

};
};

#endif /* OVERMAP_H */

