/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Generator.h
 * Author: Galen Swain
 *
 * Created on May 16, 2016, 4:36 PM
 */

#ifndef CONTINENT_GENERATOR_H
#define CONTINENT_GENERATOR_H
#include "Overmap.h"

namespace noise {
    namespace module {
        class Perlin;
        class Curve;
        class ScaledBase3D;
        class RidgedMulti;
        class ScaleBias;
        class Min;
        class Clamp;
        class Add;
        class Billow;
        class Multiply;
        class Terrace;
    }
}

namespace Continent {
class Generator {
public:
    friend class Overmap;
    Generator(Overmap *p);
    virtual ~Generator();
    
    Overmap *parent;
    
    void generateLand();
    void derivativeMap();
    
    void blurMoisture();
    void carryMoisture();
    
    void assignBiomes();
    
    inline void assignTemp(const V2& pt, Overmap::Pixel *const pix);
private:
    void allocate();
    void deallocate();
    
    void allocateLand();
    
    noise::module::Perlin* continentBase;
    noise::module::Curve* continentCurve;
    noise::module::ScaledBase3D* continentScale;
    noise::module::RidgedMulti* continentCarver_raw;
    noise::module::ScaleBias* continentCarver;
    noise::module::Multiply* continentCarved;
    
    noise::module::RidgedMulti* continentMountainDetail;
    noise::module::Min* continentMountainDetailed;
    noise::module::ScaleBias* continentMountainDetailSC;
    
    noise::module::Clamp* continentComplete;

    noise::module::Multiply* continentPostNoise;
    noise::module::Perlin* continentNoiseRaw;
    noise::module::ScaleBias* continentNoiseScaled;

    noise::module::Billow* magicBase;
    noise::module::Curve* magicCurve;

    noise::module::RidgedMulti* volcanicBase;
    noise::module::Curve* volcanicCurve;
};
};

#endif /* GENERATOR_H */

