#pragma once

#include <noise/module/modulebase.h>

namespace noise {
  namespace module {
    class ScaledBase3D : public Module {
    public:
      ScaledBase3D();
      
      virtual int GetSourceModuleCount () const { return 1; };
      
      virtual double GetValue (double x, double y, double z) const;
      
      void SetCenter(double cX, double cY, double cZ);
      void SetDistances(double dX, double dY, double dZ);
      
      void SetBases(double lBase, double uBase);
      void SetMods(double lMod, double uMod);
      void SetRange(double min, double max);
      
      void SetExponent(double n);
    protected:
      double centerX, centerY, centerZ;
      
      double lowerBase, upperBase;
      double lowerMod, upperMod;
      
      //double maxDist;
      double distX, distY, distZ;
      
      int sources;
      
      double outMin, outMax;
      double range, offset;
      double exponent;
    };
  };
};