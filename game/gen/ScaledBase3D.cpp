#include "ScaledBase3D.h"
#include <lib/V3.h>
#include <iostream>
namespace noise {
  namespace module {

    double ScaledBase3D::GetValue (double x, double y, double z) const {
      V3 trigArgs = V3((x - centerX)/distX, (y - centerY)/distY, (z - centerZ)/distZ);
      trigArgs = glm::clamp(trigArgs, V3(-1.f), V3(1.f));
      V3 sins = V3(cos(trigArgs.x*M_PI/2.0), 
                   cos(trigArgs.y*M_PI/2.0), 
                   cos(trigArgs.z*M_PI/2.0));
      sins = glm::max(V3(0.f), sins);
      sins = glm::pow(sins, V3(exponent));
      float distV = (sins.x * sins.y * sins.z);
      //std::cout << "got " << x << ", cx=" << centerX << ", dx=" <<distX << "\n";
      return distV * (m_pSourceModule[0]->GetValue(x, y, z)) + lowerBase*(distV) + upperBase*(1.0-distV);
    }

    void ScaledBase3D::SetCenter(double cX, double cY, double cZ) {
      centerX = cX; centerY = cY; centerZ = cZ;
    }
    
    void ScaledBase3D::SetDistances(double cX, double cY, double cZ) {
      distX = cX; distY = cY; distZ = cZ;
    }

    void ScaledBase3D::SetBases(double lBase, double uBase) {
      lowerBase = lBase; upperBase = uBase;
    }

    void ScaledBase3D::SetMods(double lMod, double uMod) {
      lowerMod = lMod; upperMod = uMod;
    }

    void ScaledBase3D::SetRange(double min, double max) {
      range = (max - min) / 2.0;
      offset = min - -1.0;
    }
    
    void ScaledBase3D::SetExponent(double n) {
      exponent = n;
    }

    ScaledBase3D::ScaledBase3D() : Module(1), lowerBase(0.0), upperBase(0.0), exponent(1.f) {

    }

  }
}