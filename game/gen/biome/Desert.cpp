/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Desert.cpp
 * Author: Galen Swain
 * 
 * Created on June 9, 2016, 11:03 AM
 */

#include "Desert.h"
namespace Biome {
Desert::Desert() {
  this->setRange(FieldParams::Moisture, 1000);
  //this->setRange(FieldParams::Slope, 10);
  mapColor = ColorRGBA(205, 169, 0, 255);
  id = "desert";
  name = "Desert";
  Register(this);
}

Desert::~Desert() {
}

};
