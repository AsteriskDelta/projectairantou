/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Lake.cpp
 * Author: Galen Swain
 * 
 * Created on June 9, 2016, 11:02 AM
 */

#include "Lake.h"
namespace Biome {
Lake::Lake() {
  this->setRange(FieldParams::Moisture, 65500, 65000);
  //this->setRange(FieldParams::Slope, 10);
  this->setRange(FieldParams::Elevation, 19000);
  mapColor = ColorRGBA(10, 160, 180, 255);
  id = "lake";
  name = "Lake";
  Register(this);
}

Lake::~Lake() {
}

};