/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cliff.cpp
 * Author: Galen Swain
 * 
 * Created on May 25, 2016, 11:40 AM
 */

#include "Cliff.h"
namespace Biome {
Cliff::Cliff() {
  //this->setRange(FieldParams::Moisture, 4000);
  this->setRange(FieldParams::Slope, 76);
  this->setRange(FieldParams::Elevation, 32000, 20000);
  mapColor = ColorRGBA(60, 60, 60, 255);
  id = "cliff";
  name = "Cliff";
  Register(this);
}

Cliff::~Cliff() {
}
};