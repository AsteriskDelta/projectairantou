/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Biome.cpp
 * Author: Galen Swain
 * 
 * Created on May 18, 2016, 1:24 PM
 */

#include "Biome.h"
#include "field/FieldND.h"

namespace Biome {
  std::vector<Biome*> biomes = std::vector<Biome*>();
  //FieldND<Uint16, Uint16> biomeField;
  Uint16 biomeID = 0, postBiomeID = 0;
  
  Biome* Get(Uint16 idx) {
    if(idx >= biomes.size()) return nullptr;
    return biomes[idx];
  }
  
  void Register(Biome *biome) {
    if(biome == nullptr) return;
   // FieldVolume<Uint16> *biomeVol = (FieldVolume<Uint16>*)biome;
    Uint16 newID = biome->getReqID();
    if(newID == 0xFFFF) {
      newID = 256 + (postBiomeID++);//biomes.size();
      /*for(Uint8 i = 0; i < biomeField.size(); i++) {
        if(biomes[i] == nullptr) {
          newID = i;
          break;
        }
      }*/
    }
    
    //biomeField.set(newID, *biome);
    if(newID >= biomes.size()) biomes.resize(newID+1, nullptr);
    biomes[newID] = biome;
    biome->idx = newID;
  }
  
  void Unregister(Biome *biome) {
    std::cerr << "asked to unregister biome " << biome->id << " at " << biome << "\n";
  }
  
  std::list<Weights> Query(const EV& v) {
    float strMod = 1.f;
    Uint8 max = 4;
    /*
    std::list<Weights> ret;
    auto rawLL = biomeField.getWeights(ev, 2, 1.f);
    for(auto &le : rawLL) {
      Biome* biomePtr = (le.key < biomes.size())? biomes[le.key] : nullptr;
      if(biomePtr == nullptr) continue;
      std::cout << "rawll " << biomePtr->id << " = " << le.str << "\n";
      ret.push_back(Weights{biomePtr, le.str});
    }
    return ret;*/
    std::list<Weights> ret;
    if(strMod == 0.f) return ret;
    //TODO: implement actual optimized lookup here
    //std::cout << "searching for compound weights...\n";
    //for(int i = 0; i < v.size(); i++) std::cout << "evi " << i << " = " << v.getComponent(i) << "\n";;
    std::fixed_priority_queue<Weights> lt(max);
    float tot = 0.f;
    for(auto it = biomes.begin(); it != biomes.end(); ++it) {
      if(*it == nullptr) continue;
      float w = (*it)->getWeight<Uint16>(v);
      w = pow(w, 2.f);
      //std::cout << "got " << w << " for " << (*it)->id << "\n";
      if(w > 0.f) {
        lt.push(Weights{*it, w});
        tot += w;
      }
    }
    //std::cout << "contents: \n";
    //We have a total weight of 1.0 to give away- winners take all they can, once we reach 0, nothing else is considered
    float subTotal = 0.f;
    while(!lt.empty()) {
      auto toAdd = lt.top();
      //toAdd.str /= tot;
      //toAdd.weight *= remaining;
      //remaining -= toAdd.weight;
      if(toAdd.weight > 0.0001f) {
        ret.push_back(toAdd);
        subTotal += toAdd.weight;
      }
      //std::cout << toAdd.compound->id << " : " << toAdd.str << " (from " << lt.top().str << "\n";
      lt.pop();
    }
    //Normalize to range
    if(subTotal > 0.f) {
      for(auto it = ret.begin(); it != ret.end(); ++it) {
        it->weight /= subTotal;
      }
    }

    if(strMod != 1.f) {
      for(auto it = ret.begin(); it != ret.end(); ++it) {
        it->weight *= strMod;
      }
    }

    return ret;
  }
  
  std::list<Weights> Query(const Continent::Overmap::Pixel *const p) {
    EV ev;
    ev.set(p);
    return Query(ev);
  }
  
  void EV::set(const Continent::Overmap::Pixel *const p) {
    this->set(FieldParams::Elevation  , p->elevation);
    this->set(FieldParams::Temperature, p->temp);
    this->set(FieldParams::Moisture   , p->moisture);
    this->set(FieldParams::Slope      , p->slopeMagnitude());
    this->set(FieldParams::Wind       , p->windMagnitude());
    this->set(FieldParams::Magic      , p->magic);
    this->set(FieldParams::Divine     , p->divine);
  }
  
  Biome::Biome() {
    id="null";
    name="Unassigned";
    mapColor = ColorRGBA(0,0,0,255);
    idx = 0;
    
    for(Uint8 i = 0; i <= FieldParams::Divine; i++) {
      this->set(i, FieldVolume<Uint16>());
    }
  }

  Biome::~Biome() {
    
  }
 
  
  Uint16 Biome::getReqID() {
    return 0xFFFF;
  }
  
  Uint16 Biome::getID() {
    return idx;
  }
  
  void Biome::setRange(Uint8 paramID, Uint16 center, Uint16 l, Uint16 h) {
    this->set(paramID, FieldVolume<Uint16>(center, l, h));
  }
};

#include "Plains.h"
#include "Forest.h"
#include "Lake.h"
#include "Ocean.h"
#include "Desert.h"
#include "Cliff.h"
namespace Biome {
  void Initialize() {
    new Plains();
    new Forest();
    new Lake();
    new Ocean();
    new Desert();
    new Cliff();
  }
};