/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Forest.cpp
 * Author: Galen Swain
 * 
 * Created on May 25, 2016, 11:40 AM
 */

#include "Forest.h"

namespace Biome {
Forest::Forest() {
  this->setRange(FieldParams::Moisture, 28000);
  //this->setRange(FieldParams::Slope, 30);
  this->setRange(FieldParams::Elevation, 28000);
  mapColor = ColorRGBA(16, 148, 22, 255);
  id = "forest";
  name = "Forest";
  Register(this);
}

Forest::~Forest() {
  Unregister(this);
}

Uint16 Forest::getReqID() {
  return 3;
}

};
