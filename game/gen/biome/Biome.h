/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Biome.h
 * Author: Galen Swain
 *
 * Created on May 18, 2016, 1:24 PM
 */

#ifndef BIOME_H
#define BIOME_H
#include <list>
#include "game.h"
#include <lib/Color.h>
#include "field/ElementVolume.h"
#include "field/ElementVector.h"
#include "gen/continent/Overmap.h"

namespace Biome {
    class Biome;
    enum FieldParams {
        Elevation   = 0,
        Temperature = 1,
        Moisture    = 2,
        Slope       = 3,
        Wind        = 4,
        Magic       = 5,
        Divine      = 7
    };
    
    class EV : public FieldVector<Uint8, Uint16> {
    public:
        using FieldVector::set;
        void set(const Continent::Overmap::Pixel *const p);
    };
    
    class Biome : public FieldVector<Uint8, FieldVolume<Uint16>> {
    public:
        Biome();
        virtual ~Biome();
        
        std::string id, name;
        ColorRGBA mapColor;
        
        Uint16 getID();
        virtual Uint16 getReqID();
        
        void setRange(Uint8 paramID, Uint16 center, Uint16 l = 0, Uint16 h = 0);
        Uint16 idx;
    private:
    };

    void Register(Biome *biome);
    void Unregister(Biome *biome);
    
    void Initialize();
    
    struct Weights {
        Biome *biome;
        float weight;
        inline operator<(const Weights& o) const { return weight < o.weight; };
        inline operator>(const Weights& o) const { return weight > o.weight; };
    };
    std::list<Weights> Query(const EV& ev);
    std::list<Weights> Query(const Continent::Overmap::Pixel *const p);
    
    Biome* Get(Uint16 idx);
};

#endif /* BIOME_H */

