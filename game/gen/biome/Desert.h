/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Desert.h
 * Author: Galen Swain
 *
 * Created on June 9, 2016, 11:03 AM
 */

#ifndef DESERT_H
#define DESERT_H
#include "game.h"
#include "Biome.h"

namespace Biome {

class Desert : public Biome {
public:
    Desert();
    virtual ~Desert();
private:

};

};

#endif /* DESERT_H */

