/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Ocean.cpp
 * Author: Galen Swain
 * 
 * Created on June 9, 2016, 11:03 AM
 */

#include "Ocean.h"
namespace Biome {
Ocean::Ocean() {
  this->setRange(FieldParams::Moisture, 65500, 65000);
  //this->setRange(FieldParams::Slope, 10);
  this->setRange(FieldParams::Elevation, 10000);
  mapColor = ColorRGBA(10, 2, 110, 255);
  id = "ocean";
  name = "Ocean";
  Register(this);
}

Ocean::~Ocean() {
}

};