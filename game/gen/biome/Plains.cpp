/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Plains.cpp
 * Author: Galen Swain
 * 
 * Created on May 25, 2016, 11:40 AM
 */

#include "Plains.h"

namespace Biome {
Plains::Plains() {
  this->setRange(FieldParams::Moisture, 18000);
  //this->setRange(FieldParams::Slope, 10);
  this->setRange(FieldParams::Elevation, 28000);
  mapColor = ColorRGBA(146, 220, 80, 255);
  id = "plains";
  name = "Plains";
  Register(this);
}

Plains::~Plains() {
  Unregister(this);
}

Uint16 Plains::getReqID() {
  return 2;
}

};
