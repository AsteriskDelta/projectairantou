/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cliff.h
 * Author: Galen Swain
 *
 * Created on May 25, 2016, 11:40 AM
 */

#ifndef CLIFF_H
#define CLIFF_H
#include "game.h"
#include "Biome.h"

namespace Biome {

class Cliff : public Biome {
public:
    Cliff();
    virtual ~Cliff();
private:

};
};
#endif /* CLIFF_H */

