/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Ocean.h
 * Author: Galen Swain
 *
 * Created on June 9, 2016, 11:03 AM
 */

#ifndef OCEAN_H
#define OCEAN_H
#include "game.h"
#include "Biome.h"

namespace Biome {

class Ocean : public Biome {
public:
    Ocean();
    virtual ~Ocean();
    
    //virtual Uint16 getReqID() override;
private:

};
};
#endif /* OCEAN_H */

