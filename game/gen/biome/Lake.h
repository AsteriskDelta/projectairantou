/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Lake.h
 * Author: Galen Swain
 *
 * Created on June 9, 2016, 11:02 AM
 */

#ifndef LAKE_H
#define LAKE_H
#include "game.h"
#include "Biome.h"

namespace Biome {

class Lake : public Biome{
public:
    Lake();
    virtual ~Lake();
private:

};
};

#endif /* LAKE_H */

