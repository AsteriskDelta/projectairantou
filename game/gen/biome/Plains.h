/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Plains.h
 * Author: Galen Swain
 *
 * Created on May 25, 2016, 11:40 AM
 */

#ifndef PLAINS_H
#define PLAINS_H
#include "game.h"
#include "Biome.h"

namespace Biome {
class Plains  : public Biome {
public:
    Plains();
    virtual ~Plains();
    
    virtual Uint16 getReqID() override;
private:

};
};
#endif /* PLAINS_H */

