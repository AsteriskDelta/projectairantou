/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Forest.h
 * Author: Galen Swain
 *
 * Created on May 25, 2016, 11:40 AM
 */

#ifndef FOREST_H
#define FOREST_H
#include "game.h"
#include "Biome.h"

namespace Biome {
class Forest : public Biome {
public:
    Forest();
    Forest(const Forest& orig);
    virtual ~Forest();
    
    virtual Uint16 getReqID() override; 
private:

};
};

#endif /* FOREST_H */

