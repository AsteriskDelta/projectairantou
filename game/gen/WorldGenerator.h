/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldGenerator.h
 * Author: Galen Swain
 *
 * Created on June 29, 2016, 9:35 AM
 */

#ifndef WORLDGENERATOR_H
#define WORLDGENERATOR_H
#include "game.h"

namespace WorldGenerator {
    struct Meta {
        Uint8 multi, e,f,g;
        unsigned int sub;
        double x,y,z;
        double sx, sy, sz;
        struct {//12-byte padding
            unsigned int a,b;
        };
        
        template<class Archive>
        void serialize(Archive & archive) {
            archive(CEREAL_NVP(multi),CEREAL_NVP(e),CEREAL_NVP(f),CEREAL_NVP(g),
                    CEREAL_NVP(sub),
                    CEREAL_NVP(x),CEREAL_NVP(y),CEREAL_NVP(z),
                    CEREAL_NVP(sx),CEREAL_NVP(sy),CEREAL_NVP(sz),
                    CEREAL_NVP(a),CEREAL_NVP(b)
                    );
        }
    };
    
    
};

#endif /* WORLDGENERATOR_H */

