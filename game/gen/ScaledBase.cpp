#include "ScaledBase.h"

namespace noise {
  namespace module {

    double ScaledBase::GetValue (double x, double y, double z) const {
      double placement = sqrt(((x - centerX) * (x - centerX)) + ((y - centerY) * (y - centerY))) / maxDist;
      double base = placement * lowerBase + (1.0 - placement) * upperBase;
      double mod = placement * lowerMod + (1.0 - placement) * upperMod;
      
      return (base + (((m_pSourceModule[0])->GetValue(x, y, z) + offset) * mod)) * range;
    }

    void ScaledBase::SetCenter(double cX, double cY) {
      centerX = cX; centerY = cY;
      maxDist = sqrt((cX * cX) + (cY * cY));
    }

    void ScaledBase::SetBases(double lBase, double uBase) {
      lowerBase = lBase; upperBase = uBase;
    }

    void ScaledBase::SetMods(double lMod, double uMod) {
      lowerMod = lMod; upperMod = uMod;
    }

    void ScaledBase::SetRange(double min, double max) {
      range = (max - min) / 2.0;
      offset = min - -1.0;
    }

    ScaledBase::ScaledBase() : Module(1) {

    }

  }
}