/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CompoundList.h
 * Author: Galen Swain
 *
 * Created on June 6, 2016, 11:32 AM
 */

#ifndef COMPOUNDLIST_H
#define COMPOUNDLIST_H
#include "game.h"
#include "Compound.h"
#include <map>

class CompoundList {
public:
    CompoundList();
    CompoundList(const CompoundList& orig);
    virtual ~CompoundList();
    
    struct Entry {
        double volume;
        inline Entry() : volume(0.0) {};
        template<class Archive>
        void serialize(Archive & archive) {
            archive(CEREAL_NVP(volume) );
        }
    };
    std::map<CompoundIdx, Entry> compounds;
    
    double mass();
    double volume();
    
    double massFor(CompoundIdx idx, double v = -1.0);
    double volumeFor(CompoundIdx idx, double m = -1.0);
    //Adds up to limits, returning excess volume
    double add(CompoundIdx idx, double vol, double maxV = -1.0, double maxM = -1.0);
    double remove(CompoundIdx idx, double vol);
    
    
    double canAdd(CompoundIdx idx, double vol, double maxV = -1.0, double maxM = -1.0);
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(
                CEREAL_NVP(compounds)
                );
    }
private:

};

CEREAL_REGISTER_TYPE(CompoundList);

#endif /* COMPOUNDLIST_H */

