/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldFields.cpp
 * Author: Galen Swain
 * 
 * Created on February 10, 2016, 5:44 AM
 */

#include "WorldFields.h"

#include "lib/Image.h"
#include "lib/AppData.h"
#include "lib/ImageFont.h"
#include "lib/FixedPriorityQueue.h"

WorldFields* WorldFields::instance = NULL;

WorldFields::WorldFields() {
  WorldFields::instance = this;
}

WorldFields::~WorldFields() {
}

std::list<CompoundWeight> WorldFields::getCompoundWeights(const ElementVector& v, const Uint8 max, float strMod) { _prsguard();
  std::list<CompoundWeight> ret;
  if(strMod == 0.f) return ret;
  //TODO: implement actual optimized lookup here
  //std::cout << "searching for compound weights...\n";
  //for(int i = 0; i < v.size(); i++) std::cout << "evi " << i << " = " << v.getComponent(i) << "\n";;
  std::fixed_priority_queue<CompoundWeight> lt(max);
  float tot = 0.f;
  for(auto it = compounds.begin(); it != compounds.end(); ++it) {
    float w = it->getWeight(v);
    //std::cout << "got " << w << " for " << it->id << "\n";
    if(w > 0.f) {
      lt.push(CompoundWeight{&(*it), w});
      tot += w;
    }
  }
  //std::cout << "contents: \n";
  //We have a total weight of 1.0 to give away- winners take all they can, once we reach 0, nothing else is considered
  float remaining = 1.f; float subTotal = 0.f;
  while(!lt.empty()) {
    auto toAdd = lt.top();
    //toAdd.str /= tot;
    toAdd.str *= remaining;
    remaining -= toAdd.str;
    if(toAdd.str > 0.0001f) ret.push_front(toAdd);
    //std::cout << toAdd.compound->id << " : " << toAdd.str << " (from " << lt.top().str << "\n";
    lt.pop();
  }
  //Normalize to range
  if(remaining < 1.f) {
    for(auto it = ret.begin(); it != ret.end(); ++it) {
      it->str /= (1.f-remaining);
    }
  }
  
  if(strMod != 1.f) {
    for(auto it = ret.begin(); it != ret.end(); ++it) {
      it->str *= strMod;
    }
  }
  
  return ret;
}


ElementClass* WorldFields::getElementClass(const std::string& str) {
  for(auto it = elementClasses.begin(); it != elementClasses.end(); ++it) {
    if(it->id == str) return &(*it);
  }
  return NULL;
}
Element* WorldFields::getElement(const std::string& str) {
  for(auto it = elements.begin(); it != elements.end(); ++it) {
    if(it->id == str) return &(*it);
  }
  return NULL;
}

void WorldFields::addElementClass(ElementClass * cls) {
  elementClasses.push_back(*cls);
  //Console::DebugLog(4, "Loaded elemental class " << cls->id << "\n");
  delete cls;
}

void WorldFields::addElement(Element *const elem) {
  elements.push_back(*elem);
  elements.rbegin()->idx = elements.size()-1;
  delete elem;
}
Compound* WorldFields::addCompound() {
  compounds.push_back(Compound());
  Compound *ret = &(*compounds.rbegin());
  ret->idx = compounds.size()-1;
  return ret;
}

void WorldFields::addTextureMap(unsigned short tid, Compound *c) {
  auto f = textureLookup.find(tid);
  if(f == textureLookup.end()) textureLookup[tid] = c;
}

void WorldFields::processCompound(Compound* compound) {
  if(compound == NULL) {
    Console::Error("processCompound called on null record!");
    return;
  }
  std::cout << "processing " << compound << ", name=" << compound->name << ", id="<<compound->id << " " << compound->renderInfo.texLayers.size() << " layers\n";
  CompoundIdx idx = compound->idx;
  compoundRemap.resize(std::max(int(compoundRemap.size()),int(idx+1)));
  compounds[idx].ridx = compoundRemap[idx] = idx;
  compoundLookup[compound->id] = &(compounds[idx]);
  if(compound->id.find(".pure") != std::string::npos) compoundLookup[compound->id.substr(0, compound->id.find(".pure"))] = &(compounds[idx]);
}

void WorldFields::loadCompoundResources() {
  Console::Print("Loading compound resources...");
  for(CompoundIdx i = 0; i < compounds.size(); i++) {
    compounds[i].loadResources();
  }
}

void WorldFields::loadCompoundUsed(const CompoundIdx &cid) {
  if(cid > compounds.size()) {
    std::stringstream ss; ss << "Compound resources request for " << cid<<", but no such compound is known!";
    Console::Error(ss.str());
    return;
  }
  compounds[cid].loadResources();
  //compoundResourceQueue.push_back(cid);
  //compounds[cid].resourcesLoaded = true;
}

void WorldFields::loadCompoundQueue(float dt) {
  float xt = 0.f;
  while(xt < dt && compoundResourceQueue.begin() != compoundResourceQueue.end()) {
    CompoundIdx toLoad = *compoundResourceQueue.begin();
    compounds[toLoad].loadResources();
    compoundResourceQueue.pop_front();
  }
}

bool WorldFields::loadElementClasses(std::string dir) {
  elementClasses.reserve(16);
  
  if(dir == "") dir = "fields/class/";
  if(!AppData::base.listCallback((std::function<bool(std::string,std::string)>)([this](std::string file, std::string fullPath)->bool {
    _unused(file);
    ElementClass *n = new ElementClass();
    if(!n->loadFromFile(fullPath)) {
      delete n;
      std::cerr << "Error while loading elemental class at " << fullPath << "\n";
      
      return false;
    } else this->addElementClass(n);
    return true;
  }), true, dir)) {
    std::stringstream ss; ss << "Unable to open " << dir << " for loading elemental classes!";
    Console::Error(ss.str());
    return false;
  }
  
  return true;
}
bool WorldFields::loadElements(std::string dir) {
  elements.reserve(256);
  
  if(dir == "") dir = "fields/elements";
  if(!AppData::base.listCallback((std::function<bool(std::string,std::string)>)([this](std::string file, std::string fullPath)->bool {
    _unused(file);
    Element *n = new Element();
    if(!n->loadFromFile(fullPath)) {
      delete n;
      std::cerr << "Error while loading element at " << fullPath << "\n";
      
      return false;
    } else this->addElement(n);
    return true;
  }), true, dir)) {
    std::stringstream ss; ss << "Unable to open " << dir << " for loading elements!";
    Console::Error(ss.str());
    return false;
  }
  return true;
}
bool WorldFields::loadCompounds(std::string dir) {
  compounds.reserve(512);
  
  if(dir == "") dir = "fields/compounds/";
  if(!AppData::base.listCallback((std::function<bool(std::string,std::string)>)([this](std::string file, std::string fullPath)->bool {
    if(file.find(".dnu.") != std::string::npos || file.find(".xml") != file.size()-4) return false;
    if(!Compound::LoadFromFile(fullPath)) {
      std::cerr << "Error while loading compounds from " << fullPath << "\n";
      
      return false;
    }
    return true;
  }), true, dir)) {
    std::stringstream ss; ss << "Unable to open " << dir << " for loading compounds!";
    Console::Error(ss.str());
    return false;
  }
  return true;
}
bool WorldFields::load(std::string rDir) {
  bool res = this->loadElementClasses ((rDir == "")?"":(rDir+"class/")) && 
         this->loadElements       ((rDir == "")?"":(rDir+"elements/")) && 
         this->loadCompounds      ((rDir == "")?"":(rDir+"compounds/"));
  if(!res) return false;
  
  this->buildClsOrdering();
  return true;
}

void WorldFields::buildClsOrdering() {
  elementsClsOrdered.clear();
  for(auto it = elements.begin(); it != elements.end(); ++it) elementsClsOrdered.push_back(&(*it));
  
  auto cmpFunc = [=](Element *a, Element *b) {
    return a->classID < b->classID || 
          (a->classID == b->classID && a->catID < b->catID) ||
          (a->classID == b->classID && a->catID == b->catID && a->name < b->name);
  };
  std::sort(elementsClsOrdered.begin(), elementsClsOrdered.end(), cmpFunc);
}

//Image generation routines below

static const unsigned short imSize = 1024;
static const unsigned short imAct = (unsigned short)(int(imSize)*39/100);
static const V2T<Int16> imCenter(imSize/2, imSize/2);
static const float imSingu = 0.042f;

V2T<Int16> WorldFields::getElPt(ElementIdx id, float t) {
  float rad = (float(id)/float(elements.size())) * 2.0*M_PI;
  float d = t;
  
  return V2T<Int16>(d*cos(rad), d*sin(rad));
}
V2T<Int16> WorldFields::projectElpt(ElementIdx id, float p) {
  if(id >= elements.size()) id %= elements.size();
  return imCenter+getElPt(id, ((0.5f+imSingu) + p/100.f*(0.5f-imSingu))*float(imAct));
}

static ImageFont* srcFont = NULL;
bool WorldFields::saveGraph(std::string path) {
  if(srcFont == NULL) srcFont = new ImageFont("ui/umonob.ttf", 13);
  ImageRGBA *img = new ImageRGBA(imSize,imSize);
  ColorRGBA borderColor = ColorRGBA(200,200,200, 255), white = ColorRGBA(255,255,255,255), black = ColorRGBA(0,0,0,255);
  //borderColor = ColorRGBA(0,64,0,255); white = ColorRGBA(0,0,0,255); black = ColorRGBA(0,255,0,0);
  img->fill(white);
  
  srcFont->write(img, 5, 5, "Elemental Phase Projection - Toroidal v2");
  
  //Elemental chart
  ElementIdx off = 0;
  for(auto e : elementsClsOrdered) {
    img->drawLine(projectElpt(off, -100.f), projectElpt(off, 100.f), borderColor);
    for(float dm = -1.f; dm <= 1.f; dm += 0.25f) {
      ColorRGBA c = borderColor;
      if(fabs(dm) < 0.05f || 1.f-fabs(dm) < 0.05f) c = black;
      img->drawLine(projectElpt(off, dm*100.f), projectElpt(off+1, dm*100.f), c);
    }
    if(off % 2/*(elements.size()/2)*/ == 0) {
      for(float dm = -0.5; dm <= 1.f; dm += 0.5f) {
        //ColorRGBA c = ((abs(dm) < 0.05f || (abs(dm)-1.f) < 0.05f)?black:borderColor);
        auto p = projectElpt(off, dm*100.f);
        std::stringstream ss; ss << round(dm*100.f) << "%";
        srcFont->write(img, p.x,p.y, ss.str());
      }
    }
    //Outer ring
    if(off < elements.size()-1 && e->classID == this->getElementClsOrdered(off+1)->classID) {
      img->drawLine(imCenter+getElPt(off, (imSize/2)-20), imCenter+getElPt(off+1, (imSize/2)-20), black);
      img->drawLine(imCenter+getElPt(off, (imSize/2)-16), imCenter+getElPt(off+1, (imSize/2)-16), black);
    }
    auto p = projectElpt(off, 120.f);
    srcFont->write(img, p.x + ((p.x < imCenter.x)?-30:0), p.y-8, e->name);
    srcFont->write(img, p.x + ((p.x < imCenter.x)?-30:0), p.y+8, e->id);
    off++;
  }
  srcFont->write(img, imCenter.x-18, imCenter.y-7, "-100%");
  
  //Label and draw each compound ring
  std::cout << "cmpd cnt: " << compounds.size() << "\n";
  
  for(auto it = compounds.begin(); it != compounds.end(); ++it) {
    Compound& c = *it;
    //std::cout << "drawing " << c.name << " at " << &c << "\n";
    ColorRGBA col = ColorRGBA(rand(),rand(),rand(),96);
    off = 0;
    for(auto e : elementsClsOrdered) {
      ElementIdx i = e->idx;
      ElementIdx jo = (off+1)%elementsClsOrdered.size();
      ElementIdx j = elementsClsOrdered[jo]->idx;
      
      auto l = projectElpt(off, c[i].lowf()*100.f);
      auto h = projectElpt(off, c[i].highf()*100.f);
      auto nl = projectElpt(off+1, c[j].lowf()*100.f);
      auto nh = projectElpt(off+1, c[j].highf()*100.f);
      
      float mWidth = std::max(c[i].highf()-c[i].lowf(), c[j].highf()-c[j].lowf())/2.f;
      int mLines = floor(mWidth*200.f);
      float tw = c[i].highf()-c[i].lowf(), tn = c[j].highf()-c[j].lowf();
      //std::cout << "drawing " << mLines << " guides\n";
      for(int y = 0; y < mLines; y++) {
        float md = tw/mLines*y,//tw/2.f - log(tw/mLines*y*50.f)/50.f, 
              mn = tn/mLines*y;//tn/2.f - log(tn/mLines*y*50.f)/50.f;
        auto yl = projectElpt(off, (c[i].lowf()+md)*100.f);
        auto yh = projectElpt(off, (c[i].highf()-md)*100.f);
        auto ynl = projectElpt(off+1, (c[j].lowf()+mn)*100.f);
        auto ynh = projectElpt(off+1, (c[j].highf()-mn)*100.f);
        img->drawLine(yl, ynl, col);
        img->drawLine(yh, ynh, col);
      }
      
      img->drawLine(l, h, col);
      img->drawLine(l, nl, col);
      img->drawLine(h, nh, col);
      off++;
    }
  }
  
  srcFont->write(img, 100, 100, "SIGMA");
  srcFont->write(img, imSize-140, imSize-100, "DELTA");
  srcFont->write(img, imSize-140, 100, "OMEGA");
  
  std::cout << "saving img to " << path << "\n";
  bool res = img->Save(path);
  delete img;
  return res;
}
