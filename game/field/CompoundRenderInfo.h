/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CompoundRenderInfo.h
 * Author: Galen Swain
 *
 * Created on February 27, 2016, 11:52 AM
 */

#ifndef COMPOUNDRENDERINFO_H
#define COMPOUNDRENDERINFO_H
#include "game.h"
#include <Color.h>
#include <vector>
#include <lib/V2.h>

#ifndef TEXTURE_INC
template<bool Arr = false>
class Texture;
#endif
class Compound;

class CompoundRenderInfo {
public:
    friend class Compound;
    struct GPUStruct {
        float heightScale, heightFreq;
        float paddA, paddB;
        V4 col;
    };
    
    CompoundRenderInfo();
    CompoundRenderInfo(void* xmlRenderRoot);
    CompoundRenderInfo(const CompoundRenderInfo& orig);
    virtual ~CompoundRenderInfo();
    
    Compound* parent;
    struct TexEmit {
        Uint16 particleID;
        float density;
        V2 sizeMin, sizeMax;
        std::string texPathParticle;
        
        inline V2 getSize() {
            V2 f = V2((rand()%100)/100.f, (rand()%100)/100.f);
            return sizeMin*f + sizeMax*(V2(1.f)-f);
        }
    };
    
    struct TexLayer {
        ColorRGBA tint, mult;
        //Texture<> *main, *blend;
        Uint16 main, blend;
        float heightScale, heightFreq;
        std::string texPathMain, texPathBlend;
    };
    std::vector<TexLayer> texLayers;
    std::list<TexEmit> emits;
    
    bool loadResources(bool force = false);
    
    bool loadFromXMLTag(void* xmlTag);
    inline Uint16 getTex(Uint16 off) {
        if(off >= texLayers.size()) return 0;
        return texLayers[off].main;
    }
    inline ColorRGBA getTint(Uint16 off) {
        if(off >= texLayers.size()) return ColorRGBA(255,255,255,255);
        return texLayers[off].tint;
    }
    
    TexEmit loadTexEmit(void* xmlTag);
    
    GPUStruct makeUB(std::vector<GPUStruct> *vec = NULL, Uint16 tIdx = 0) const;
private:

};

#endif /* COMPOUNDRENDERINFO_H */

