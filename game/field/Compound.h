/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Compound.h
 * Author: Galen Swain
 *
 * Created on February 10, 2016, 5:39 AM
 */

#ifndef COMPOUND_H
#define COMPOUND_H
#include "game.h"
#include "CompoundRenderInfo.h"
#include "ElementVolume.h"
#include "ElementVector.h"
#include "phys/PhysicalProperties.h"

typedef Uint16 CompoundIdx;

class Compound : public PhysicalProperties {
public:
    Compound();
    Compound(const Compound& orig);
    virtual ~Compound();
    
    std::string id, name;
    CompoundIdx idx, ridx;//Index and remapped index
    Compound* root;
    
    CompoundRenderInfo renderInfo;
    ElementVolume phase;
    
    
    bool resourcesLoaded;
    
    inline FieldVolume<FieldInt>& operator[](ElementIdx off) { return phase[off]; };
    float getWeight(const ElementVector& v);
    
    float getSubdivLength(float epsilon);
    
    bool preprocess();
    bool loadResources();
    
    static bool LoadFromFile(std::string path);
private:
};

#endif /* COMPOUND_H */

