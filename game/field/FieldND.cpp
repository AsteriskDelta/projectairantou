/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FieldND.cpp
 * Author: Galen Swain
 * 
 * Created on February 8, 2016, 2:22 PM
 */
#ifndef FIELDND_CPP
#define FIELDND_CPP
#include "FieldND.h"
#include <algorithm>
#include "lib/FixedPriorityQueue.h"

template<typename KT, typename VT>
FieldND<KT,VT>::FieldND() {
}

template<typename KT, typename VT>
FieldND<KT,VT>::~FieldND() {
}

template<typename KT, typename VT>
std::list<typename FieldND<KT,VT>::Weight> FieldND<KT,VT>::getWeights(const FieldVector<KT,VT> v, Uint8 max, float strMod) {
  std::list<Weight> ret;
  if(strMod == 0.f) return ret;
  //TODO: implement actual optimized lookup here
  //std::cout << "searching for compound weights...\n";
  //for(int i = 0; i < v.size(); i++) std::cout << "evi " << i << " = " << v.getComponent(i) << "\n";;
  std::fixed_priority_queue<Weight> lt(max);
  float tot = 0.f;
  //for(auto it = compounds.begin(); it != compounds.end(); ++it) {
  for(KT i = 0; i < this->size(); i++) {
    if(!this->get(i)) continue;
    float w = this->get(i).getWeight(v);
    //std::cout << "got " << w << " for " << i << "\n";
    if(w > 0.f) {
      lt.push(Weight{w, i});
      tot += w;
    }
  }
  //std::cout << "contents: \n";
  //We have a total weight of 1.0 to give away- winners take all they can, once we reach 0, nothing else is considered
  float remaining = 1.f;// float subTotal = 0.f;
  while(!lt.empty() && remaining > 0.f) {
    auto toAdd = lt.top();
    //toAdd.str /= tot;
    toAdd.str *= remaining;
    remaining -= toAdd.str;
    if(toAdd.str > 0.0001f) ret.push_front(toAdd);
    //std::cout << toAdd.compound->id << " : " << toAdd.str << " (from " << lt.top().str << "\n";
    lt.pop();
  }
  //Normalize to range
  if(remaining < 1.f) {
    for(auto it = ret.begin(); it != ret.end(); ++it) {
      it->str /= (1.f-remaining);
    }
  }
  
  if(strMod != 1.f) {
    for(auto it = ret.begin(); it != ret.end(); ++it) {
      it->str *= strMod;
    }
  }
  
  return ret;
}
#endif