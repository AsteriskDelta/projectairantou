/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FieldVolume.cpp
 * Author: Galen Swain
 * 
 * Created on February 27, 2016, 12:00 PM
 */
#ifndef FIELDVOLUME_CPP_INC
#define FIELDVOLUME_CPP_INC
#include "FieldVolume.h"
#include "XML.h"

inline float getPercent(XMLNode *n, std::string t) {
  std::string rawT = n->get<std::string>(t, "0");
  if(rawT.find("%") != std::string::npos) {
    std::string tmp = n->get<std::string>(t, "0%");
    return std::stof(tmp.substr(0, tmp.find("%")));
  } else if(rawT.find("c") != std::string::npos) {
    std::string tmp = n->get<std::string>(t, "0c");
    return std::stof(tmp.substr(0, tmp.find("c"))) / 80.f;
  } else return 0.f;
}

template<typename FVal>
FieldVolume<FVal>::FieldVolume() {
  low = mid = high = 0;
  lowData.weight = highData.weight = 0.f;
  lowData.sq = highData.sq = 1.f;
}

template<typename FVal>
FieldVolume<FVal>::FieldVolume(FVal m, FVal l, FVal h) {
  mid = m;
  low = l;
  if(h == 0) high = std::numeric_limits<FVal>::max();
  else high = h;
  
  lowData.weight = highData.weight = lowData.sq = highData.sq = 1.f;
}

template<typename FVal>
FieldVolume<FVal>::FieldVolume(const FieldVolume& o) {
  low = o.low; mid = o.mid; high = o.high;
  lowData.weight = o.lowData.weight; lowData.sq = o.lowData.sq;
  highData.weight = o.highData.weight; highData.sq = o.highData.sq;
}

template<typename FVal>
FieldVolume<FVal>::~FieldVolume() {
}

template<typename FVal>
float FieldVolume<FVal>::getWeight(const FVal& v, float *tot) const { _prsguard();
  int delta = int(mid) - int(v);
  
  //if(low == high || delta < 0.05f) return 1.f;
  float dist = 0.f, denom = 1.f;
  try {
    denom = fabs(float(((delta < 0)?high : low)) - float(mid));
    if(denom == 0.f && delta != 0.f) dist = 0.f;
    else if(denom == 0.f) return 0.f;
    else dist = float(delta) / (denom+1.f);
  } catch(...) {
    dist = 0.f;
  };
  
  auto dat = (delta < 0)? (&lowData) : (&highData);
  if(true||dat->sq > 0) dist = pow(fabs(dist), dat->sq);
  //else dist = pow(dist, 1/dat->sq);
  //std::cout << "gw delta: " << delta << " / " << denom << ", dist = " << dist << " -< " << mid << " - " << v << " weight="<<dat->weight<<"\n";
  float ret = clamp(1.f - dist, 0.f, 1.f);
  if(tot != NULL) {
    ret *= dat->weight;
    *tot += dat->weight;
  }
  return ret;
}

template<typename FVal>
bool FieldVolume<FVal>::loadFromXMLTag(void* rt) {
  XMLNode *node = reinterpret_cast<XMLNode*>(rt);
  //std::cout << "lln: " << getPercent(node, "l") << " fmi: " << std::numeric_limits<FVal>::max() << "\n";
  low  = round(getPercent(node, "l")/100.f * float(std::numeric_limits<FVal>::max()));
  mid  = round(getPercent(node, "m") / 100.f * float(std::numeric_limits<FVal>::max()));
  high = round(getPercent(node,"h") / 100.f * float(std::numeric_limits<FVal>::max()));
  //std::cout << "l" << low << " h" << high << "\n";
  lowData.weight    = node->get<float>("lw", 1.f);
  highData.weight   = node->get<float>("hw", 1.f);
  lowData.sq        = node->get<float>("lsq", 1.f);
  highData.sq       = node->get<float>("hsq", 1.f);
  
  return true;
}

#endif