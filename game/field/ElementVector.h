/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ElementVector.h
 * Author: Galen Swain
 *
 * Created on March 3, 2016, 11:13 AM
 */

#ifndef ELEMENTVECTOR_H
#define ELEMENTVECTOR_H
#include "game.h"
#include "FieldVector.h"
#include "Element.h"

class ElementVector  : public FieldVector<ElementIdx, FieldInt> {
public:
    ElementVector();
    ElementVector(const ElementVector& orig);
    inline ElementVector(const FieldVector<ElementIdx, FieldInt>& orig) : FieldVector<ElementIdx, FieldInt>(orig){
        
    }
    virtual ~ElementVector();
    
    template<class Archive>
    void serialize(Archive & archive) {
        _unused(archive);
    }
private:

};

CEREAL_REGISTER_TYPE(FieldVector<ElementIdx, FieldInt>);
CEREAL_REGISTER_TYPE(ElementVector);

#endif /* ELEMENTVECTOR_H */