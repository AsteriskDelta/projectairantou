/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Subfield.h
 * Author: Galen Swain
 *
 * Created on March 3, 2016, 7:39 AM
 */

#ifndef SUBFIELD_H
#define SUBFIELD_H
#include "game.h"
#include "ElementVolume.h"
#include "ElementVector.h"
#include <list>

class Compound;
typedef unsigned char Uint8;

struct CompoundWeight {
    Compound* compound;
    float str;
    
    inline CompoundWeight(Compound *c, float s) : compound(c), str(s) {};

    inline bool operator>(const CompoundWeight& o) const {
        return (str > o.str);
    }

    inline bool operator<(const CompoundWeight& o) const {
        return (str < o.str);
    }
};
struct ElementWeight {
    ElementVector v;
    float str;
    
    inline ElementWeight(ElementVector iv, float s) : v(iv), str(s) {};
    
    inline bool operator>(const ElementWeight& o) const {
        return (str > o.str);
    }

    inline bool operator<(const ElementWeight& o) const {
        return (str < o.str);
    }
};

class IdealContour;
class IdealSlice;
class Subfield {
public:
    struct Feature {
        enum Type {
            Default, Point, Skin, Null = 255
        } Uint8;
        V3 position;
        ElementVector value, second;
        Type type;
        float proportion;
        
        inline Feature() : type(Null), proportion(1.f) {
            
        };
        inline Feature(unsigned char ct, const ElementVector& v, const V3& p = V3(0.f), const ElementVector &s = ElementVector(), float pro = 1.f) : position(p), value(v), second(s), type((Type)ct), proportion(pro) {
            
        };
        
        ElementVector getVector();
        float getStrength(const V3& pos);
        float getVolume(IdealSlice *slice);
        V3 getCO(IdealSlice *slice, int prop);
        
        std::list<CompoundWeight> getCompounds();
        
        template<class Archive>
        void serialize(Archive & archive) {
            archive(
                    CEREAL_NVP(position),
                    CEREAL_NVP(value),
                    CEREAL_NVP(second),
                    CEREAL_NVP(type),
                    CEREAL_NVP(proportion)
                    );
        }
        };
    std::vector<Feature> features;
    
    Subfield();
    Subfield(const ElementVector& v);
    Subfield(const Subfield& orig);
    Subfield(const std::string& src);
    
    Subfield(const std::string& srcA, float strA, const std::string& srcB, float strB);
    virtual ~Subfield();
    
    void addFeature(const Feature& f);
    void delFeature(Uint8 fID);
    
    void setDefault(const ElementVector& v, const ElementVector& s = ElementVector(), float pro = 1.f);
    void setDefault(const std::string& src);
    void setDefault(const std::string& srcA, float strA, const std::string& srcB, float strB);
    
    ElementVector getVector(const V3& pos);
    std::list<ElementWeight> getVectors(const V3& pos, unsigned short listLimit = 32);
    
    std::list<CompoundWeight> getCompounds(const V3& pos, unsigned short listLimit = 32);
    
    std::list<CompoundWeight> getCompounds(IdealSlice *slice, unsigned short listLimit = 32);
    
    V3 getCO(IdealSlice *slice, Uint8 propID);
    
    template<class Archive>
        void serialize(Archive & archive) {
            archive(
                    CEREAL_NVP(features)
                    );
        }
private:

};

#endif /* SUBFIELD_H */

