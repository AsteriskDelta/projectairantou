/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldFields.h
 * Author: Galen Swain
 *
 * Created on February 10, 2016, 5:44 AM
 */

#ifndef WORLDFIELDS_H
#define WORLDFIELDS_H
#include "game.h"

#include "Element.h"
#include "Compound.h"
#include "PhaseField.h"
#include "ElementClass.h"
#include <vector>
#include <V2.h>
#include <unordered_map>
#include <list>
#include "Subfield.h"

class WorldFields {
public:
    WorldFields();
    virtual ~WorldFields();
    
    std::vector<ElementClass> elementClasses;
    ElementClass* getElementClass(const std::string& str);
    
    std::vector<Element> elements;
    std::vector<Element*> elementsClsOrdered;
    inline Element* getElement(const ElementIdx& idx) { return &(elements[idx]); };
    Element* getElement(const std::string& str);
    inline Element* getElementClsOrdered(const ElementIdx& idx) { return elementsClsOrdered[idx]; };
    
    std::vector<Compound> compounds;
    std::unordered_map<std::string, Compound*> compoundLookup;//id -> ptr
    std::unordered_map<unsigned short, Compound*> textureLookup;//tid -> ptr
    std::vector<CompoundIdx> compoundRemap;//Used to map server's IDs -> client's ID
    inline Compound* getCompound(const CompoundIdx& idx) { return &compounds[compoundRemap[idx]]; };
    inline Compound* getCompoundRaw(const CompoundIdx& idx) { return &compounds[idx]; };
    inline Compound* getCompound(std::string id) { return compoundLookup[id]; };
    inline Compound* getCompoundByTexture(unsigned short tid) { return textureLookup[tid]; };
    
    void addTextureMap(unsigned short tid, Compound *c);
    
    std::list<CompoundWeight> getCompoundWeights(const ElementVector& v, const Uint8 max = 4, float strMod = 1.f);
    
    void addElementClass(ElementClass *const cls);
    void addElement(Element *const elem);
    Compound* addCompound();
    void processCompound(Compound* compound);//Builds mapping data
    
    void loadCompoundResources();
    void loadCompoundUsed(const CompoundIdx &cid);
    void loadCompoundQueue(float dt = 1.f);
    
    bool loadElementClasses(std::string dir="");
    bool loadElements(std::string dir = "");
    bool loadCompounds(std::string dir="");
    bool load(std::string rDir = "");
    bool saveGraph(std::string path);
    
    static WorldFields* instance;
    inline static WorldFields* active() {
        return instance;
    }
private:
    void buildClsOrdering();
    std::list<CompoundIdx> compoundResourceQueue;
    
    V2T<Int16> getElPt(ElementIdx id, float t);
    V2T<Int16> projectElpt(ElementIdx id, float p);
};

#endif /* WORLDFIELDS_H */

