/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ElementClass.h
 * Author: Galen Swain
 *
 * Created on February 26, 2016, 4:21 PM
 */

#ifndef ELEMENTCLASS_H
#define ELEMENTCLASS_H
#include <game.h>

typedef Uint8 ElementClassIdx;

class ElementClass {
public:
    ElementClass();
    ElementClass(const ElementClass& orig);
    virtual ~ElementClass();
    
    std::string id, name;
    ElementClassIdx idx;
    
    bool loadFromFile(std::string fullPath);
private:

};

#endif /* ELEMENTCLASS_H */

