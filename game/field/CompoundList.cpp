/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CompoundList.cpp
 * Author: Galen Swain
 * 
 * Created on June 6, 2016, 11:32 AM
 */

#include "CompoundList.h"
#include "field/WorldFields.h"

CompoundList::CompoundList() : compounds() {
}

CompoundList::CompoundList(const CompoundList& orig) : compounds(orig.compounds) {
}

CompoundList::~CompoundList() {
}

double CompoundList::mass() {
  double ret = 0.0;
  for(auto it = compounds.begin(); it != compounds.end(); ++it) {
    Compound *compound = WorldFields::active()->getCompound(it->first);
    if(compound == nullptr) continue;
    ret += it->second.volume * compound->density;
  }
  return ret;
}
double CompoundList::volume() {
  double ret = 0.0;
  for(auto it = compounds.begin(); it != compounds.end(); ++it) {
    ret += it->second.volume;
  }
  return ret;
}

double CompoundList::massFor(CompoundIdx idx, double v) {
  Compound *compound = WorldFields::active()->getCompound(idx);
  if(compound == nullptr) return 0.0;
  
  if(v < 0.0) {
    auto it = compounds.find(idx);
    if(it == compounds.end()) return 0.0;
    else return it->second.volume * compound->density;
  } else {
    return v * compound->density;
  }
}

double CompoundList::volumeFor(CompoundIdx idx, double m) {
  Compound *compound = WorldFields::active()->getCompound(idx);
  if(compound == nullptr) return 0.0;
  
  if(m < 0.0) {
    auto it = compounds.find(idx);
    if(it == compounds.end()) return 0.0;
    else return it->second.volume;
  } else {
    return m / compound->density;
  }
}

double CompoundList::canAdd(CompoundIdx idx, double vol, double maxV, double maxM) {
   Compound *compound = WorldFields::active()->getCompound(idx);
  if(compound == nullptr) return 0.0;
  
  double added = min(vol, min(maxV, maxM/compound->density));
  return added;
}

double CompoundList::add(CompoundIdx idx, double vol, double maxV, double maxM) {
  Compound *compound = WorldFields::active()->getCompound(idx);
  if(compound == nullptr) return 0.0;
  
  double added = min(vol, min(maxV, maxM/compound->density));
  if(compounds.find(idx) == compounds.end()) compounds[idx] = Entry();
  compounds[idx].volume += added;
  
  return max(0.0, vol - added);
}

double CompoundList::remove(CompoundIdx idx, double vol) {
  Compound *compound = WorldFields::active()->getCompound(idx);
  if(compound == nullptr) return 0.0;
  if(compounds.find(idx) == compounds.end()) return 0.0;
  
  double del = min(vol, compounds[idx].volume);
  compounds[idx].volume -= del;
  if(compounds[idx].volume < 0.0001) compounds.erase(idx);
  
  return del;
}