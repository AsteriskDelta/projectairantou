/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Subfield.cpp
 * Author: Galen Swain
 * 
 * Created on March 3, 2016, 7:39 AM
 */

#include "Subfield.h"
#include "WorldFields.h"
#include "Compound.h"
#include "rep/IdealSlice.h"
#include "rep/IdealContour.h"
#include <algorithm>

ElementVector Subfield::Feature::getVector() {
  switch(type) {
    
    case Type::Default:
    default:
      return value;
      break;
  }
}

float Subfield::Feature::getVolume(IdealSlice *slice) {
  switch(type) {
    
    case Type::Default:
    default:
      slice->calculateVolume();
      return slice->volume;
      break;
  }
}


V3 Subfield::Feature::getCO(IdealSlice *slice, int prop) {
  switch(type) {
    
    case Type::Default:
    default:
      return slice->evalCentroid(0.f) * slice->contourRatio + slice->evalCentroid(1.f)* (1.f - slice->contourRatio);
      break;
  }
}

float Subfield::Feature::getStrength(const V3& pos) {
  switch(type) {
    
    case Type::Default:
    default:
      return 1.f;
      break;
  }
}

std::list<CompoundWeight> Subfield::Feature::getCompounds() {
  std::list<CompoundWeight> ret;
  ret.splice(ret.end(), WorldFields::active()->getCompoundWeights(value, 2, proportion));
  ret.splice(ret.end(), WorldFields::active()->getCompoundWeights(second, 2, 1.f-proportion));
  /*for(CompoundWeight& w : ret) {
    std::cout << w.compound->name << " : " << w.str << "\n";
  }*/
  return ret;
}

Subfield::Subfield() {
  
}
Subfield::Subfield(const ElementVector& v) {
  this->setDefault(v);
}
Subfield::Subfield(const std::string& src) {
  Compound* compound = WorldFields::active()->getCompound(src);
  this->setDefault(compound->phase.vec());
}

Subfield::Subfield(const Subfield& orig) {
  features = orig.features;
}

Subfield::Subfield(const std::string& srcA, float strA, const std::string& srcB, float strB) {
  Compound* compoundA = WorldFields::active()->getCompound(srcA),*compoundB = WorldFields::active()->getCompound(srcB);
  const float tot = strA+strB;
  this->setDefault(compoundA->phase.vec()*(strA/tot) + compoundB->phase.vec()*(strB/tot));
}

Subfield::~Subfield() {
  
}

void Subfield::addFeature(const Feature& f) {
  features.push_back(f);
}
void Subfield::delFeature(Uint8 idx) {
  if(features.size() <= idx) return;
  
  for(Uint8 i = idx+1; i < features.size(); i++) {
    features[i-1] = features[i];
  }
  features.pop_back();
}
    
void Subfield::setDefault(const ElementVector& v,const ElementVector& s, float pro) {
  if(features.size() < 1) {
    this->addFeature(Feature(Feature::Type::Default, v, V3(0.f), s, pro));
  } else {
    features[0].value = v;
    features[0].second = s;
    features[0].proportion = pro;
  }
}
void Subfield::setDefault(const std::string& src) { _prsguard();
  Compound* compound = WorldFields::active()->getCompound(src);
  if(compound == NULL) {
    std::stringstream ss; ss << "Requested compound \"" << src << "\" not found or loadable!";
    Console::Error(ss.str());
    return;
  }
  //std::cout << "set default to " << src << "\n";
  this->setDefault(compound->phase.vec());
}

void Subfield::setDefault(const std::string& srcA, float strA, const std::string& srcB, float strB) { _prsguard();
  Compound* compoundA = WorldFields::active()->getCompound(srcA),*compoundB = WorldFields::active()->getCompound(srcB);
  if(compoundA == NULL || compoundB == NULL) {
    std::stringstream ss; ss << "Requested compound \"" << srcA << "\" or \""<<srcB<<" not found or loadable!";
    Console::Error(ss.str());
    return;
  }
  const float tot = strA+strB;
  this->setDefault(compoundA->phase.vec(), compoundB->phase.vec(), strA/tot);
}
    
ElementVector Subfield::getVector(const V3& pos) { _prsguard();
  ElementVector ret = ElementVector(); float mag = 0.f;
  //std::cout << "considering features cnt " << features.size() << "\n";
  for(auto it = features.begin(); it != features.end(); ++it) {
    float str = it->getStrength(pos);
    ElementVector tmpVec = it->getVector();
    tmpVec.orphan();//Ensure resolution of all values to be multiplied
    
    ret = (ret * (1.f-str)) + tmpVec*str;
    mag += str;
  }
  
  return ret;
}

std::list<ElementWeight> Subfield::getVectors(const V3& pos, unsigned short listLimit) { _prsguard();
  std::list<ElementWeight> ret; float mag = 0.f;
  
  for(auto it = features.begin(); it != features.end(); ++it) {
    float str = it->getStrength(pos);
    ElementVector tmpVec = it->getVector();
    tmpVec.orphan();//Ensure resolution of all values to be multiplied
    
    ret.push_back(ElementWeight(tmpVec, str));
    mag += str;
  }
  
  for(auto it = ret.begin(); it != ret.end(); ++it) it->str /= mag;
  ret.sort();
  ret.reverse();
  while(ret.size() > listLimit) ret.pop_back();
  
  return ret;
}

std::list<CompoundWeight> Subfield::getCompounds(const V3& pos, unsigned short listLimit) { _prsguard();
  std::list<CompoundWeight> ret; float mag = 0.f;
  
  for(auto it = features.begin(); it != features.end(); ++it) {
    float str = it->getStrength(pos);
    if(str < 0.01f) continue;
    //ElementVector tmpVec = it->getVector(pos);
    //tmpVec.orphan();//Ensure resolution of all values to be multiplied
    
    std::list<CompoundWeight> subCompounds = it->getCompounds();
    for(auto ct = subCompounds.begin(); ct != subCompounds.end(); ++ct) ct->str *= str;
    ret.splice(ret.end(), subCompounds);
    mag += str;
  }
  
  for(auto it = ret.begin(); it != ret.end(); ++it) it->str /= mag;
  ret.sort();
  ret.reverse();
  while(ret.size() > listLimit) ret.pop_back();
  
  return ret;
}

std::list<CompoundWeight> Subfield::getCompounds(IdealSlice *slice, unsigned short listLimit) { _prsguard();
  std::list<CompoundWeight> ret; float mag = 0.f;
  
  for(auto it = features.begin(); it != features.end(); ++it) {
    float str = it->getVolume(slice);
    if(str < 0.01f) continue;
    //ElementVector tmpVec = it->getVector(pos);
    //tmpVec.orphan();//Ensure resolution of all values to be multiplied
    
    std::list<CompoundWeight> subCompounds = it->getCompounds();
    for(auto ct = subCompounds.begin(); ct != subCompounds.end(); ++ct) ct->str *= str;
    ret.splice(ret.end(), subCompounds);
    mag += str;
  }
  
  for(auto it = ret.begin(); it != ret.end(); ++it) it->str /= mag;
  ret.sort();
  ret.reverse();
  while(ret.size() > listLimit) ret.pop_back();
  
  return ret;
}

V3 Subfield::getCO(IdealSlice *slice, Uint8 propID) { _prsguard();
  slice->calculateCompounds();
  
  V3 ret = V3(0.f); float mag = 0.f;
  
  for(auto it = features.begin(); it != features.end(); ++it) {
    float str = it->getVolume(slice);
    if(str < 0.001f) continue;
    
    ret += it->getCO(slice, propID)*str;
    mag += str;
    std::cout << "feature CO: " << glm::to_string(it->getCO(slice, propID)) << " at str=" << str << "\n";
  }
  
  ret /= mag;
  std::cout << "subfield CO: " << glm::to_string(ret) << " div mag=" << mag << "\n";
  return ret;
}