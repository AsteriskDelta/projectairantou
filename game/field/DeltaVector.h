/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DeltaVector.h
 * Author: Galen Swain
 *
 * Created on April 9, 2016, 6:05 PM
 */

#ifndef DELTAVECTOR_H
#define DELTAVECTOR_H
#include "game.h"
#include "Element.h"

class DeltaVector {
public:
    DeltaVector();
    DeltaVector(const DeltaVector& orig);
    virtual ~DeltaVector();
    union {
        V3 velocity;
        V3 position;
        V3 acceleration;
    };
    float vals[ELEMENT_DELTA_MAX];
    
    static float affinity(const DeltaVector& a, const DeltaVector& b);
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(velocity), CEREAL_NVP(vals));
    }
private:

};

inline DeltaVector min(const DeltaVector& a, const DeltaVector& b) {
    DeltaVector ret;
    ret.velocity = glm::min(a.velocity, b.velocity);
    for(unsigned short i = 0; i < ELEMENT_DELTA_MAX; i++) {
        ret.vals[i] = min(a.vals[i], b.vals[i]);
    }
    return ret;
}
inline DeltaVector max(const DeltaVector& a, const DeltaVector& b) {
    DeltaVector ret;
    ret.velocity = glm::max(a.velocity, b.velocity);
    for(unsigned short i = 0; i < ELEMENT_DELTA_MAX; i++) {
        ret.vals[i] = max(a.vals[i], b.vals[i]);
    }
    return ret;
}

#endif /* DELTAVECTOR_H */