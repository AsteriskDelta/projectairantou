/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ElementClass.cpp
 * Author: Galen Swain
 * 
 * Created on February 26, 2016, 4:21 PM
 */

#include "ElementClass.h"

#include "lib/XML.h"

ElementClass::ElementClass() {
  
}

ElementClass::ElementClass(const ElementClass& orig) {
  id = orig.id; name = orig.name;
  idx = orig.idx;
}

ElementClass::~ElementClass() {
  
}

bool ElementClass::loadFromFile(std::string fullPath) {
  XMLDoc doc;
  if(!doc.load(fullPath, false)) {
    std::stringstream ss;
    ss << "Elemental Class at \"" << fullPath << "\" failed to load due to XML parsing error!";
    Console::Error(ss.str());
    return false;
  }
  
  XMLNode r = doc.child("elementClass");
  id = r.get<std::string>("id");
  idx = r.get<ElementClassIdx>("intrinID");
  //name = r.child("name").get<std::string>();
  //std::cout << "El: id: " << id << " name: " << name << "\n";
  return true;
}
