/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ElementVector.cpp
 * Author: Galen Swain
 * 
 * Created on March 3, 2016, 11:13 AM
 */

#include "ElementVector.h"

ElementVector::ElementVector() : FieldVector<ElementIdx, FieldInt>() {
  
}

ElementVector::ElementVector(const ElementVector& orig) : FieldVector<ElementIdx, FieldInt>(orig) {
  _unused(orig);
}

ElementVector::~ElementVector() {
}

