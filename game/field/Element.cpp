/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Element.cpp
 * Author: Galen Swain
 * 
 * Created on February 8, 2016, 2:31 PM
 */

#include "Element.h"
#include <iostream>
#include "lib/XML.h"
#include "WorldFields.h"

Element::Element() {
}

Element::Element(const Element& o) {
  id = o.id; name = o.name;
  idx = o.idx;
  classPtr = o.classPtr; classID = o.classID;
  catID = 0;
}

Element::~Element() {
}

bool Element::loadFromFile(std::string fullPath) {
  XMLDoc doc;
  if(!doc.load(fullPath, false)) {
    std::stringstream ss;
    ss << "Element at \"" << fullPath << "\" failed to load due to XML parsing error!";
    Console::Error(ss.str());
    return false;
  }
  
  XMLNode r = doc.child("element");
  id = r.get<std::string>("id");
  name = r.child("name").get<std::string>();
  
  if(!this->setClass(r.get<std::string>("class"))) {
    std::stringstream ss;
    ss << "Element at \"" << fullPath << "\" has invalid elemental class: \"" << r.get<std::string>("class") << "\"";
    Console::Error(ss.str());
    return false;
  }
  
  catID = r.get<ElementIdx>("catID", 0xFF);
  
  //std::cout << "El: id: " << id << " name: " << name << ", idx: "<<int(classID)<<"\n";
  
  //for(XMLNode prefNode = rp.child("ui.pref"); prefNode; ++prefNode) {
    //std::cout << prefNode.get<std::string>("id") << " : " << prefNode.get<std::string>() << "\n";
  //}
  return true;
}

bool Element::setClass(std::string cls) {
  classPtr = WorldFields::active()->getElementClass(cls);
  if(classPtr == NULL) {
    classID = -1; return false;
  }
  classID = classPtr->idx;
  return true;
}