/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DeltaVector.cpp
 * Author: Galen Swain
 * 
 * Created on April 9, 2016, 6:05 PM
 */

#include "DeltaVector.h"
#include <cstring>

DeltaVector::DeltaVector() : velocity(0.f) {
  for(unsigned short i = 0; i < ELEMENT_DELTA_MAX; i++) vals[i] = 0.f;
}

DeltaVector::DeltaVector(const DeltaVector& orig) {
  velocity = orig.velocity;
  std::memcpy(vals, orig.vals, sizeof(vals));
}

DeltaVector::~DeltaVector() {
}

float DeltaVector::affinity(const DeltaVector& a, const DeltaVector& b) { _prsguard();
  float tot = 0.f;
  tot += glm::distance(a.velocity, b.velocity);
  for(unsigned short i = 0; i < ELEMENT_DELTA_MAX; i++) {
    tot += fabs(a.vals[i] - b.vals[i]);
  }
  
  return pow(tot, 1.f/3.f);
}