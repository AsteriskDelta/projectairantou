/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FieldND.h
 * Author: Galen Swain
 *
 * Created on February 8, 2016, 2:22 PM
 */

#ifndef FIELDND_H
#define FIELDND_H
#include "game.h"
#include "FieldVector.h"
#include "FieldVolume.h"
#include <list>

//N-dimensional interpolation field over fieldPTs (via voronoi fields and delauny cograph)
template<typename KT, typename VT>
class FieldND : public FieldVector<KT, FieldVector<KT, FieldVolume<VT>>> {
public:
    struct Weight {
        float str;
        KT key;
        
        inline operator<(const Weight& o) const { return str < o.str; };
        inline operator>(const Weight& o) const { return str > o.str; };
    };
    
    FieldND();
    virtual ~FieldND();
    
    std::list<Weight> getWeights(const FieldVector<KT,VT> vec, Uint8 count = 4, float strMod = 1.0);
private:

};
#include "FieldND.cpp"
#endif /* FIELDND_H */

