/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ElementVolume.h
 * Author: Galen Swain
 *
 * Created on February 27, 2016, 12:06 PM
 */

#ifndef ELEMENTVOLUME_H
#define ELEMENTVOLUME_H
#include "game.h"
#include "Element.h"
#include "FieldVolume.h"
#include "FieldVector.h"
#include "ElementVector.h"

class ElementVolume : public FieldVector<ElementIdx, FieldVolume<FieldInt>> {
public:
    ElementVolume();
    ElementVolume(const ElementVolume& orig);
    virtual ~ElementVolume();
    
    ElementVector vec();
    
    bool loadFromXMLTag(void* xmlTag);
    
    template<class Archive>
    void serialize(Archive & archive) {
        
        _unused(archive);
    }
private:

};

CEREAL_REGISTER_TYPE(FieldVector<ElementIdx, FieldVolume<FieldInt>>);
CEREAL_REGISTER_TYPE(ElementVolume);
#endif /* ELEMENTVOLUME_H */

