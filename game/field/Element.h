/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Element.h
 * Author: Galen Swain
 *
 * Created on February 8, 2016, 2:31 PM
 */

#ifndef ELEMENT_H
#define ELEMENT_H
#include "game.h"
#include "ElementClass.h"

typedef Uint8 ElementIdx;
#define ELEMENT_VOLUME (255)
#define ELEMENT_DELTA_MAX (16)

#define DELTA_ELECTRIC (3)
#define DELTA_LIGHT (4)
#define DELTA_MANA (2)
#define DELTA_POTEN (5)
#define DELTA_SPIRIT (1)
#define DELTA_THERMAL (6)

class Element {
public:
    Element();
    Element(const Element& orig);
    virtual ~Element();
    
    bool loadFromFile(std::string fullPath);
    
    std::string id, name;
    
    ElementIdx idx, catID;
    
    Uint8 classID;
    ElementClass *classPtr;
    
    
    inline ElementClass* getClass() { return classPtr; };
private:
    bool setClass(std::string cls);
};

#endif /* ELEMENT_H */

