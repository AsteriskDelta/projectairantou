/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CompoundRenderInfo.cpp
 * Author: Galen Swain
 * 
 * Created on February 27, 2016, 11:52 AM
 */
#include <client/Texture.h>
#include "CompoundRenderInfo.h"
#include "XML.h"
#include "ResourceManager.h"
#include "Compound.h"
#include <lib/AppData.h>
#include "WorldFields.h"

CompoundRenderInfo::CompoundRenderInfo() {
}
CompoundRenderInfo::CompoundRenderInfo(void* xmlRenderRoot) {
  this->loadFromXMLTag(xmlRenderRoot);
}

CompoundRenderInfo::CompoundRenderInfo(const CompoundRenderInfo& orig) {
  parent = orig.parent;
  texLayers = orig.texLayers;
}

CompoundRenderInfo::~CompoundRenderInfo() {
}

bool CompoundRenderInfo::loadResources(bool force) { _prsguard();
  //std::cout << "loadRes"<<texLayers.size()<<" emits=" << emits.size() << "\n";
 // return true;
  if(!force && texLayers.size() > 0 && texLayers[0].main != 0) return true;
  
  for(auto &layer : texLayers) {
    //std::cout << "tpmain: " << layer.texPathMain << "\n";
    //std::cout << "exists: " << AppData::base.exists(layer.texPathMain+"-diff.png") << " at dir:\"" << AppData::base.str() << "\"\n";
    if(layer.texPathMain.size() > 0) {
      layer.main = ResourceManager::GetTexture(layer.texPathMain+"-diff.png");
      WorldFields::active()->addTextureMap(layer.main, parent);
      //std::cout << "got " << layer.main << " for tex " << layer.texPathMain << "\n";
      if(layer.main == 0) {
        std::stringstream ss; ss << "Unable to load main texture at \"" << layer.texPathMain << "\" for compound " << parent->id;
        Console::Error(ss.str());
      }
    }
    if(layer.texPathBlend.size() > 0) {
      layer.blend = ResourceManager::GetTexture(layer.texPathBlend+"-blend.png");
      WorldFields::active()->addTextureMap(layer.blend, parent);
      if(layer.blend == 0) {
        std::stringstream ss; ss << "Unable to load blend texture at \"" << layer.texPathBlend << "\" for compound " << parent->id;
        Console::Error(ss.str());
      }
    }
  }

  for(TexEmit& emit : emits) {
    //std::cout << " getPar " << emit.texPathParticle<<"\n";
    emit.particleID = ResourceManager::GetParticle(emit.texPathParticle);
    if(emit.particleID == 0) {
      std::stringstream ss; ss << "Unable to load emitted particle at \"" << emit.texPathParticle << "\" for compound " << parent->id;
      Console::Error(ss.str());
    }
  }
  return true;
}

bool CompoundRenderInfo::loadFromXMLTag(void* tag) { _prsguard();
  XMLNode *h = reinterpret_cast<XMLNode*>(tag);
  
  XMLNode n = h->child("emit");
    while(n) {
      emits.push_back(this->loadTexEmit(reinterpret_cast<void*>(&n)));
      n = n.sibling("emit");
    }
  
  unsigned short texID = 0;
  XMLNode tex = NULL, tint = NULL, bint = NULL;
  while((tex = h->child("tex"+std::to_string(texID)))) {
    tint = h->child("tint"+std::to_string(texID));
    bint = h->child("blend"+std::to_string(texID));
    float heightScale = tex.get<float>("heightScale", 1.f);
    float heightFreq = tex.get<float>("heightFreq", 1.f);
    texID++;
    
    TexLayer newLayer = TexLayer{
            bool(!tint)?ColorRGBA(255,255,255,255) : ColorRGBA(tint.get<Uint8>("r"),tint.get<Uint8>("g"),tint.get<Uint8>("b"),tint.get<Uint8>("a",255)),
                    bool(!bint)?ColorRGBA(255,255,255,255) : ColorRGBA(bint.get<Uint8>("r"),bint.get<Uint8>("g"),bint.get<Uint8>("b"),bint.get<Uint8>("a", 255)),
                    0,0,
                    heightScale, heightFreq,
                    tex.get<std::string>("path", ""),
                    tex.get<std::string>("blendPath", "")
    };
    texLayers.push_back(newLayer);
    //std::cout << "pushed layer " << newLayer.texPathMain << "\n";
  }
  //std::cout << "tl: " << texLayers.size() << "\n";
  return true;
}

CompoundRenderInfo::TexEmit CompoundRenderInfo::loadTexEmit(void* xmlTag) {
  XMLNode *const n = reinterpret_cast<XMLNode*>(xmlTag);
  TexEmit ret = {0,0.f, V2(0.f), V2(0.f), ""};
  if(n != NULL) {
    ret.density = n->get<float>("density", 0.f);
    ret.sizeMin = V2(n->get<float>("sizeMinX", 0.f), n->get<float>("sizeMinY", 0.f));
    ret.sizeMax = V2(n->get<float>("sizeMaxX", 0.f), n->get<float>("sizeMaxY", 0.f));
    ret.texPathParticle = n->get<std::string>("path", "");
    if( ret.texPathParticle == "" || ret.sizeMin.x == 0.f || ret.sizeMin.y == 0.f || ret.sizeMax.x == 0.f || ret.sizeMax.y == 0.f || ret.density == 0.f) {
      std::stringstream ss; ss << "Particle at path " << ret.texPathParticle << " has incomplete emission information, skipping...";
      Console::Error(ss.str());
      return ret;
    }
    std::cout << "loading PP@P " << ret.texPathParticle << "\n";
  }
  return ret;
}

CompoundRenderInfo::GPUStruct CompoundRenderInfo::makeUB(std::vector<GPUStruct> *vec, Uint16 tIdx) const { _prsguard();
        const TexLayer *l;
        GPUStruct ret;
        if(texLayers.size() < 1) return ret;
        l = &texLayers[0];
        
        ret.heightScale = l->heightScale;
        ret.heightFreq = l->heightFreq;
        ret.paddA = ret.paddB = 0.f;
        ret.col = V4(l->tint.flt(0), l->tint.flt(1),l->tint.flt(2),l->tint.flt(3));
        
        //if(vec != NULL) vec->push_back(ret);
        if(vec != NULL) {
            std::cout << tIdx << " to " << vec->size() << " = (" << ret.heightScale << "," << ret.heightFreq << ")\n";
            if(vec->size() <= tIdx) vec->resize(tIdx+1);
            (*vec)[tIdx] = ret;
        }
        return ret;
    }