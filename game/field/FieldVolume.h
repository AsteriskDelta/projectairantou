/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FieldVolume.h
 * Author: Galen Swain
 *
 * Created on February 27, 2016, 12:00 PM
 */

#ifndef FIELDVOLUME_H
#define FIELDVOLUME_H
#include "game.h"
#include <limits>

template<typename FVal = FieldInt>
class FieldVolume {
public:
    FieldVolume();
    FieldVolume(const FieldVolume& orig);
    FieldVolume(FVal m, FVal l = 0, FVal h = 0);
    virtual ~FieldVolume();
    
    FVal low, mid, high;
    struct {
        FVal weight, sq;
    } lowData, highData;
    
    //Weight output to *tot when supplied, otherwise returns fully normalized
    float getWeight(const FVal& v, float *tot = NULL) const;
    
    bool loadFromXMLTag(void* tag);
    
    inline float lowf() { return float(low)/float(std::numeric_limits<FVal>::max()); }
    inline float highf() { return float(high)/float(std::numeric_limits<FVal>::max()); };
    
    inline operator bool() const {
        return high > low && high > 0;
    }
    
    template<class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(low), CEREAL_NVP(mid), CEREAL_NVP(high),
                CEREAL_NVP(lowData.weight), CEREAL_NVP(lowData.sq), 
                CEREAL_NVP(highData.weight), CEREAL_NVP(highData.sq)
                );
    }
private:

};
#include "FieldVolume.cpp"
#endif /* FIELDVOLUME_H */
