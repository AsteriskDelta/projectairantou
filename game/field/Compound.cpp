/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Compound.cpp
 * Author: Galen Swain
 * 
 * Created on February 10, 2016, 5:39 AM
 */

#include "Compound.h"
#include "lib/XML.h"
#include "WorldFields.h"

Compound::Compound() : PhysicalProperties() {
  root = NULL;
  renderInfo.parent = this;
  resourcesLoaded = false;
}

Compound::Compound(const Compound& orig) : PhysicalProperties(orig) {
  id = orig.id; name = orig.name;
  idx = orig.idx; ridx = orig.ridx;
  root = orig.root;
  renderInfo = orig.renderInfo;
  phase = orig.phase;
  renderInfo.parent = this;
  resourcesLoaded = orig.resourcesLoaded;
}

Compound::~Compound() {
  
}

float Compound::getWeight(const ElementVector& v) { _prsguard();
  ElementIdx maxOffset = std::max(phase.size(), v.size());
  float weight = 1.f, total = 1.f;
  for(ElementIdx i = 0; i < maxOffset; i++) {
    float localWeight = 0.f;
    const float stageWeight = phase[i].getWeight(v.getComponent(i), &localWeight);
    if(localWeight > 0.f) {
      //std::cout << "w"<<int(i)<<" = " << stageWeight << ", tot = " << localWeight << "\n";
      weight *= stageWeight*localWeight;
      total *= 1.f;//localWeight;
    }
  }
  if(total == 0.f) return 0.f;
  return weight / total;
}

float Compound::getSubdivLength(float epsilon) {
  float avgSubdivLength = 0.0f;
  for(auto it = renderInfo.texLayers.begin(); it != renderInfo.texLayers.end(); ++it) {
    avgSubdivLength += it->heightFreq;
  }
  avgSubdivLength /= float(renderInfo.texLayers.size());
  avgSubdivLength = 1.f / avgSubdivLength;
  
  return std::max(epsilon*6.f, avgSubdivLength * clamp(float(pow(epsilon, 0.5f)), 1.f, 100.f) * 4.f);
}

bool Compound::loadResources() { _prsguard();
  if(resourcesLoaded) return true;
  bool ret = renderInfo.loadResources();
  //std::cout <<  "cpmd load: " << renderInfo.getTex(0) << "\n";;
  return ret;
}

bool Compound::LoadFromFile(std::string path) { _prsguard();
  XMLDoc doc;
  if(!doc.load(path, false)) {
    std::stringstream ss;
    ss << "Compounds at \"" << path << "\" failed to load due to XML parsing error!";
    Console::Error(ss.str());
    return false;
  }
  
  XMLNode r = doc.child("compound");
  std::string masterID = r.get<std::string>("id");
  Compound *root = NULL;
  
  for(XMLNode inst = r.child("inst"); inst; ++inst) {
    std::string instanceID = inst.get<std::string>("id");
    bool isRoot = inst.get<bool>("root",false);
    std::string name = inst.child("name").get<std::string>();
    std::string actID = masterID+"."+instanceID;
    
    Compound *c = WorldFields::active()->addCompound();
    if(isRoot) root = c;
    c->id = actID;
    c->name = name;
    
    XMLNode rend = inst.child("render");
    if(rend) c->renderInfo.loadFromXMLTag(reinterpret_cast<void*>(&rend));
    else {
      std::stringstream ss;
      ss << "Compound in \"" << path << "\", " << actID << " has no render tag!";
      Console::Error(ss.str());
      return false;
    }
    
    XMLNode elem = inst.child("elements");
    if(elem) {
      c->phase.loadFromXMLTag(reinterpret_cast<void*>(&elem));
    } else {
      std::stringstream ss;
      ss << "Compound in \"" << path << "\", " << actID << " has no elements tag!";
      Console::Error(ss.str());
      return false;
    }
    
    XMLNode phys = inst.child("physical");
    if(phys) {
      c->loadPhysicalXMLTag((void*)&phys);
    } else {
      std::stringstream ss;
      ss << "Compound in \"" << path << "\", " << actID << " has no physical tag!";
      Console::Error(ss.str());
      return false;
    }
    
    //std::cout << "encountered compound " << actID << " named " << name << " with " << c->renderInfo.texLayers.size() << " layers\n";
    
    WorldFields::active()->processCompound(c);
  }
  return true;
}