/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FieldVec.h
 * Author: Galen Swain
 *
 * Created on February 8, 2016, 2:23 PM
 */

#ifndef FIELDVEC_H
#define FIELDVEC_H
#include "game.h"
#include <vector>
#include <type_traits>
#include <iostream>

template<typename F, typename T>
class FieldVector {
public:
    FieldVector() : defaultValue(), root(NULL) {
        
    }
    FieldVector(const FieldVector<F,T>& orig) {
        offsets = orig.offsets; values = orig.values;
        defaultValue = orig.defaultValue; root = orig.root;
    }
    virtual ~FieldVector() {
        
    }
    
    std::vector<F> offsets;
    std::vector<T> values;
    T defaultValue;
    FieldVector<F,T> *root;
    
    inline void reserve(F cnt) {
        offsets.reserve(cnt);
        values.reserve(cnt);
    }
    
    inline F size() const {
        if(root != NULL) return  std::max(F(root->size()), F(offsets.size()));
        else return F(offsets.size());
    }
    
    inline void orphan() {
        for(auto ot = offsets.begin(); ot != offsets.end(); ++ot) {
            if(*ot == 0xFF) this->setComponent(*ot, (*this)[*ot]);
        }
        root = NULL;
    }
    
    inline FieldVector<F,T> operator*(float m) {
        FieldVector ret = *this;
        ret.orphan();
        
        for(auto it = ret.values.begin(); it != ret.values.end(); ++it) {
            *it = T(float(*it)*m);
        }
        return ret;
    }
    
    inline FieldVector<F,T> operator+(const FieldVector<F,T>& o) {
        F cnt = std::max(this->size(), o.size());
        FieldVector ret; ret.reserve(cnt);
        ret.defaultValue = this->defaultValue;
        
        for(F i = 0; i < cnt; i++) {
            ret.setComponent(i, this->getComponent(i)+o.getComponent(i));
        }
        return ret;
    }
    
    inline bool hasKey(F idx) const {
         if(idx >= offsets.size() || offsets[idx] == 0xFF) {
            if(root != NULL && root->hasKey(idx)) return true;
            else return false;
        } else return true;
    }
    
    inline T& operator[](F idx) {
        //std::cout << idx << " >= " << offsets.size() << "\n";
        if(idx >= offsets.size() || offsets[idx] == 0xFF) {
            if(root != NULL && root->hasKey(idx)) return (*root)[idx];
            else return defaultValue;
        }
        else return values[offsets[idx]];
    }
    
    inline T getComponent(F idx) const {
        if(idx >= offsets.size() || offsets[idx] == 0xFF) {
            if(root != NULL && root->hasKey(idx)) return (*root)[idx];
            else return defaultValue;
        }
        else return values[offsets[idx]];
    }
    
    inline void setComponent(F idx, const T& val) {
        if(idx >= offsets.size()) offsets.resize(idx+1, 0xFF);
        
        if(offsets[idx] != 0xFF) values[offsets[idx]] = val;
        else {
            offsets[idx] = values.size();
            values.push_back(val);
        }
        
    }
    
    inline T get(F idx) const {
        return this->getComponent(idx);
    }
    inline void set(F idx, const T& val) {
        this->setComponent(idx, val);
    }
    
    inline operator bool() const {
        return this->size() > 0;
    }

    template<typename SP, typename E = T>
    typename std::enable_if<!std::is_arithmetic<E>::value, float>::type getWeight(const FieldVector<F, SP> &v) {
        F maxOffset = this->size();
        float weight = 1.f, total = 0.f;
        for (F i = 0; i < maxOffset; i++) {
            float localWeight = 0.f;
            const float stageWeight = this->get(i).getWeight(v.getComponent(i), &localWeight);
            if (localWeight > 0.f) {
                //std::cout << "w"<<int(i)<<" = " << stageWeight << ", tot = " << localWeight << "\n";
                weight *= lerp(1.f, stageWeight, localWeight); //stageWeight*localWeight;
                total = 1.f; //localWeight;
            }
        }
        //std::cout << "gw ret " << weight << "/" << total << "\n";
        if (total == 0.f) return 0.f;
        return weight / total;
    }
    template<typename E = T>
    typename std::enable_if<std::is_arithmetic<E>::value, float>::type getWeight(const FieldVector<F, T> &v) {
        return 0.f;
    }
    
    template<class Archive>
    void serialize(Archive & archive) {
        this->orphan();
        archive(CEREAL_NVP(offsets),
                CEREAL_NVP(values),
                CEREAL_NVP(defaultValue)
                );
    }
private:

};
//CEREAL_REGISTER_TYPE(FieldVector);
#endif /* FIELDVEC_H */

