/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ElementVolume.cpp
 * Author: Galen Swain
 * 
 * Created on February 27, 2016, 12:06 PM
 */

#include "ElementVolume.h"
#include <XML.h>
#include "WorldFields.h"
#include <iostream>
ElementVolume::ElementVolume() {
}

ElementVolume::ElementVolume(const ElementVolume& orig) : FieldVector<ElementIdx, FieldVolume<FieldInt>>(orig) {
}

ElementVolume::~ElementVolume() {
}

ElementVector ElementVolume::vec() {
  ElementVector ret;
  for(unsigned int i = 0; i < this->size(); i++) {
    ret.setComponent(i, this->getComponent(i).mid);
  }
  return ret;
}

bool ElementVolume::loadFromXMLTag(void* tag) {
  XMLNode *h = reinterpret_cast<XMLNode*>(tag);
  
  for(XMLNode n = h->child(); bool(n); n = n.sibling()) {
    std::string elementName = n.name();
    FieldVolume<FieldInt> v; v.loadFromXMLTag(&n);
    ElementIdx elementIdx;
    if(WorldFields::active()->getElement(elementName) != NULL) elementIdx = WorldFields::active()->getElement(elementName)->idx;
    else {
      std::stringstream ss; ss << "Unrecognized element requested in compound, \"" << elementName << "\"";
      Console::Error(ss.str());
      return false;
    }
    //std::cout << "setting " << elementName << "\n";
    this->setComponent(elementIdx, v);
  }
  
  if(h->get<std::string>("default") == "trace") {
    this->defaultValue.low = -2*256;
    this->defaultValue.mid = 0;
    this->defaultValue.high = 2*256;
    this->defaultValue.lowData.weight = this->defaultValue.highData.weight = 0.f;
  }
  return true;
}
