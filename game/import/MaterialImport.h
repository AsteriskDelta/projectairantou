/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MaterialImport.h
 * Author: Galen Swain
 *
 * Created on June 1, 2016, 1:07 PM
 */

#ifndef MATERIALIMPORT_H
#define MATERIALIMPORT_H
#include "game.h"
#include "lib/Image.h"
#include "client/Texture.h"

class ModelImport;

class MaterialImport {
public:
    friend class ModelImport;
    MaterialImport();
    MaterialImport(const MaterialImport& orig);
    virtual ~MaterialImport();
    
    ModelImport *parent;
    Texture2D *texture;
private:

};

#endif /* MATERIALIMPORT_H */

