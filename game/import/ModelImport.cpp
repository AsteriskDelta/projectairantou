/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ModelImport.cpp
 * Author: Galen Swain
 * 
 * Created on May 27, 2016, 4:08 PM
 */

#include "ModelImport.h"
#include "client/PipelineStage.h"
#include "ai/AICritic.h"
#include "client/Mesh.h"
#include "client/Mesh.cpp"
#include "lib/AuxAlgorithm.h"
#include <vector>

ModelImport::IdealMeta* ModelImport::IdealMeta::getNextUnused(std::deque<TraceLayer> *layers) {
  IdealMeta *m = &((*layers)[id.layerID].idealMeta[id.idealID]);
  float bestScore = -1.f; IdealMeta *best = nullptr;
  //std::cout << "getNextUnused considering " << m->conns.size() << "\n";
  for(const LID& cid : m->conns) {
    IdealMeta *other = &((*layers)[cid.layerID].idealMeta[cid.idealID]);
    IdealContour *ideal = ((*layers)[cid.layerID].ideals[cid.idealID]);
    if(other->useCount != 0) continue;
    //std::cout << "layer: " << int(other->useCount) << "uc with score=" << ideal->area << "\n";
    
    const float score = ideal->area;
    if(score > bestScore) {
      bestScore = score;
      best = other;
    }
  }
  
  return best;
}

ModelImport::TraceLayer::~TraceLayer() {
  for(auto it = ideals.begin(); it != ideals.end(); ++it) {
    delete *it;
  }
  if(preIdeal != nullptr) delete preIdeal;
}

void ModelImport::TraceLayer::linkNodes(V3 a, V3 b, V3 ha, V3 hb, V2 ta, V2 tb) {
  if(nodes.find(a) == nodes.end()) nodes[a] = TraceNode(a, ha, ta);
  if(nodes.find(b) == nodes.end()) nodes[b] = TraceNode(b, hb, tb);
  
  nodes[a].links.push_back(&nodes[b]);
  nodes[b].links.push_back(&nodes[a]);
}

void ModelImport::TraceLayer::pruneNodes() {
  for(auto it = nodes.begin(); it != nodes.end(); ) {
    auto newIt = std::next(it);
    ModelImport::TraceNode *n = &(it->second);;
    while(n != nullptr && n->links.size() < 2) {
      ModelImport::TraceNode *o = (n->links.size() > 0)? *(n->links.begin()) : nullptr;
      auto curIt = nodes.find(n->pos);
      if(newIt == curIt) newIt = std::next(curIt);
      
      if(o != nullptr) o->removeLink(n);
      nodes.erase(curIt);
      n = o;
    }
    
    it = newIt;
  }
}

void ModelImport::TraceNode::removeLink(ModelImport::TraceNode* n) {
  links.remove(n);
}

V3 ModelImport::TraceLayer::getNearestCentroid(V3 p) {
  IdealContour *c = this->getNearestIdeal(p);
  if(c == nullptr) return V3(0.f);
  else return c->centroid;
}

bool ModelImport::TraceLayer::nearestContains(V3 p) {
  IdealContour *c = this->getNearestIdeal(p);
  if(c == nullptr) return false;
  else return c->contains(p);
}

IdealContour* ModelImport::TraceLayer::getNearestIdeal(V3 p) {
  float nDist = 999999.f; unsigned int nID = 0;
  for(unsigned int i = 0; i < ideals.size(); i++) {
    float d = glm::distance(p, ideals[i]->centroid);
    if(d < nDist) {
      nDist = d;
      nID = i;
    }
  }
  
  return (nID >= ideals.size())? nullptr : ideals[nID];
}

unsigned int ModelImport::TraceLayer::getIdealID(IdealContour *const c) const {
  for(unsigned int i = 0; i < ideals.size(); i++) {
    if(ideals[i] == c) return i;
  }
  return 0;
}

ModelImport::PathNode::PathNode(ModelImport::IdealMeta *m, ModelImport::TraceVec *v) : str(1.f) {
  id = m->id;
  vec = v;
  meta = m;
  layer = &(v->layers[id.layerID]);
  ideal = layer->ideals[id.idealID];
  pos = layer->wp.resolve(ideal->centroid);
}

void ModelImport::Path::addNode(ModelImport::IdealMeta *m, ModelImport::TraceVec *v, float str) {
  nodes.push_back(PathNode(m, v));
  nodes.rbegin()->str = max(nodes.rbegin()->str, str);
  m->useCount++;
}

void ModelImport::Path::process() {
  float totalArea = 0.f, totalWeight = 0.f, minArea = std::numeric_limits<float>::max(), maxArea = 0.f;
  float totalVal = 0.f;
  for(PathNode &n : nodes) {
    const float ar = n.ideal->area;
    const float mv = n.meta->weight;
    totalArea += ar;
    totalVal += mv;
    totalWeight += 1.f;
    minArea = min(ar, minArea);
    maxArea = max(ar, maxArea);
  }
  avgArea = totalArea / totalWeight;
  float avgVal = 1.f;//totalVal / totalWeight;
  
  float del = (fabs(avgArea - minArea) + fabs(maxArea - avgArea))/avgArea;
  float diff = fabs(maxArea - minArea) / avgArea;
  
  score = pow(avgVal, 0.2f) * (1.f / (pow(1.f+diff, 1.f)));// * (1.f / (pow(diff, 1.f) + pow(1.f + del, 2.f)));
}

ModelImport::ModelImport() : model(nullptr), traceVectors(), dbgMesh(nullptr) {
}
ModelImport::ModelImport(std::string path) : model(nullptr), traceVectors(), dbgMesh(nullptr) {
  this->load(path);
}

ModelImport::~ModelImport() {
  if(model != nullptr) delete model;
}

void ModelImport::load(std::string path) {
  if(model != nullptr) delete model;
  model = new Model(path);
  if(!model) {
    std::stringstream ss; ss << "ModelImport::load on " << path << " wasn't able to open file!";
    Console::Error(ss.str());
  }
  modelCenter = V3(0.f);
}

void ModelImport::useVectors(unsigned short count) {
  if(count == 3) {
    for(unsigned int i = 0; i < model->meshes.size(); i++) {
      Mesh<ModelVert> *mesh = &(model->meshes[i].mesh);
      traceVectors.emplace_back(TraceVec(V3_RIGHT, mesh));
      traceVectors.emplace_back(TraceVec(V3_UP, mesh));
      traceVectors.emplace_back(TraceVec(V3_FORWARD, mesh));
    }
  } else if(count == 7) {
    std::vector<V3> midvs = std::vector<V3>({V3_RIGHT, V3_UP, V3_FORWARD}); Uint16 n = 3;
    std::vector<Uint16> idvs = std::vector<Uint16>({0,1,2});
    for(unsigned int m = 0; m < model->meshes.size(); m++) {
      Mesh<ModelVert> *mesh = &(model->meshes[m].mesh);
      for(Uint16 k = 1; k <= n; k++) {
        do {
          V3 vec = V3(0.f);
          for (int i = 0; i < k; ++i) {
            vec += midvs[i];
          }
          vec = glm::normalize(vec);
          traceVectors.emplace_back(TraceVec(vec, mesh));
       } while(next_combination(idvs.begin(),idvs.begin() + k,idvs.end()));
      }
    }
    /*traceVectors.emplace_back(TraceVec(V3_UP));
    traceVectors.emplace_back(TraceVec(V3_RIGHT));
    traceVectors.emplace_back(TraceVec(V3_FORWARD));
    
    traceVectors.emplace_back(TraceVec((V3_FORWARD+V3_UP)/2.f));
    traceVectors.emplace_back(TraceVec((V3_FORWARD+V3_RIGHT)/2.f));
    traceVectors.emplace_back(TraceVec((V3_RIGHT+V3_UP)/2.f));
    traceVectors.emplace_back(TraceVec((V3_FORWARD+V3_UP+V3_RIGHT)/3.f));*/
    
    //TODO: finish cross vectors if needed
  } else {
    std::stringstream ss; ss << "ModelImport::useVectors is invalid with count=" << count << "!";
    Console::Error(ss.str());
  }
}
void ModelImport::process() {
  for(unsigned short i = 0; i < traceVectors.size(); i++) {
    this->traceVector(i);
  }
  //this->traceVector(2);
}

inline V3 tbRoundVec(V3 in) {
  const float spDivisor = 256.f*4.f*16.f;
  return glm::round(in*spDivisor)/spDivisor;
}
void ModelImport::traceVector(unsigned short i) {
  TraceVec *v = &traceVectors[i];
  //Determine min and max times
  unsigned int newSpace = 384000;
  float areaThreshold = 0.0005f;//01f;
  float fullThreshold = 0.02f, fullMult = 0.8f;
  const bool detailedDebugMesh = false;
  if(dbgMesh == nullptr) dbgMesh = new Mesh<WObjectVert>(MeshType::Lines, newSpace, newSpace * 4, true);
  
  Mesh<ModelVert> *mesh = v->mesh;
  mesh->getProjectedRange(v->normal, &(v->tStart), &(v->tEnd));
  //v->tStart *= 2.f; v->tEnd *= 2.f;
  std::cout << "tracing vector #" << i << ", range is between "<<v->tStart << " and " << v->tEnd << "\n";
  //For each time with epsilon,
  float dt = 1.f/64.f;//(v->tEnd - v->tStart)/64.f;
  float ct = v->tStart;
  while(ct <= v->tEnd) {
    //std::cout << "tracing vector at " << ct << ", eq " << glm::to_string(v->normal*ct) << "\n";
    Workplane workplane = Workplane(modelCenter + v->normal*ct, v->normal);
    v->layers.emplace_back(TraceLayer());
    TraceLayer *layer = &(v->layers.back());
    layer->t = ct;
    layer->wp = workplane;
    
    //Add all intersecting vectors
    const unsigned int maxInd = floor(mesh->getIndCount()/3)*3;
    for(unsigned int ind = 0; ind < maxInd; ind += 3) {
      ModelVert *va = &mesh->getVertFromIndSafe(ind+0),
                *vb = &mesh->getVertFromIndSafe(ind+1),
                *vc = &mesh->getVertFromIndSafe(ind+2);
      unsigned char intersectCount = 0;;
      unsigned int iia, iib;
      V3 ipa, ipb, iha, ihb;
      V2 ita, itb;
      _unused(iia); _unused(iib);
      if(va == nullptr || vb == nullptr || vc == nullptr) continue;
      
      //For all lines comprising the primative
      for(unsigned char ori = 0; ori < 3; ori++) {
        ModelVert *s, *e;
        if(ori == 0) { s = va; e = vb; }
        else if(ori == 1) { s = vb; e = vc; }
        else if(ori == 2) { s = va; e = vc; }
        
        float intersectionTime = -1.f;
        V3 intersection = workplane.intersectLine(s->pos, e->pos, &intersectionTime);
        if(intersectionTime != -1.f) {//Add to the ideal plane
          //std::cout << "intersection at " << glm::to_string(intersection) << "\n";
          if(intersectCount == 0) {
            iia = ind+ori;
            ipa = intersection;
            iha = s->normal*(1.f-intersectionTime) + e->normal*intersectionTime;
            ita = s->texCoord*(1.f-intersectionTime) + e->texCoord*intersectionTime;
          } else if(intersectCount == 1) {
            iib = ind+ori;
            ipb = intersection;
            ihb = s->normal*(1.f-intersectionTime) + e->normal*intersectionTime;
            itb = s->texCoord*(1.f-intersectionTime) + e->texCoord*intersectionTime;
          }
          intersectCount++;
        }
      }
      
      if(intersectCount > 0) {
        if(intersectCount > 2) {
          //std::cout << "IMPORT ERROR: " << intersectCount << " intersections for a triangle!\n";
        } else {
          if(detailedDebugMesh) {
            V3 col = V3(0.15f);
            unsigned int dia = dbgMesh->addVert(WObjectVert(ipa, col));
            unsigned int dib = dbgMesh->addVert(WObjectVert(ipb, col));
            dbgMesh->vert(dia).sourcePos = col;
            dbgMesh->vert(dib).sourcePos = col;

            dbgMesh->addLineInd(dia, dib);
          }
          layer->linkNodes(tbRoundVec(ipa), tbRoundVec(ipb), iha, ihb, ita, itb);
        }
      }
    }
    
    //Prune the layer to remove billboard faces
    layer->pruneNodes();
    
    //Break into contiguous areas, make ideal contours
    Uint16 groupIdx = 0;
    for(auto it = layer->nodes.begin(); it != layer->nodes.end(); ++it ) {
      if(it->second.group != 0xFFFF) continue;
      TraceNode *start = &(it->second), *prev = start;
      TraceNode *next = (start->links.size() > 0)? *(start->links.begin()) : nullptr;
      IdealContour *newIdeal = new IdealContour();
      
      float dotAvg = 0.f, dotWeight = 0.f;
      
      Uint16 escapeCounter = 8192;
      while(next != nullptr && (--escapeCounter) != 0) {
        //Add the new vertex, assign group
        IdealFeature tmpFeature;
        tmpFeature.type = IdealFeature::Line;
        tmpFeature.start = workplane.project(prev->pos); tmpFeature.end = workplane.project(next->pos);
        tmpFeature.smoothness = 0.f;
        tmpFeature.setHermite((prev->hermite == -next->hermite)? prev->hermite : (prev->hermite+next->hermite)/2.f);
        newIdeal->addFeature(tmpFeature);
          
        dotAvg += glm::dot(v->normal, glm::normalize(prev->hermite));
        dotWeight += 1.f;
        
        //Find next sibling, order insensitive
        next->group = groupIdx;
        if(next == start) break;
        TraceNode *newNext = (next->links.size() > 0)? *(next->links.begin()) : nullptr;
        if(newNext == prev || newNext == next) newNext = (next->links.size() > 0)? *(next->links.rbegin()) : nullptr;
        //std::cout << "n->n:\t\t" << next << " ->" << newNext << ", " << next->links.size() << " conns\n";
        if(newNext == next) {//Allows us to repair a 4-vert, but fails (if we're unlucky) on higher artifacts
          TraceNode *bestNext = nullptr; float bestDist = Maxf;
          for(TraceNode*& ln : next->links) {
            if(ln == next || ln == prev || ln == nullptr) continue;
            float d = glm::distance2(next->pos, ln->pos);
            if(d < bestDist) {
              d = bestDist;
              bestNext = ln;
            }
          }
          
          if(bestNext != nullptr) {
            newNext = bestNext;
          } else {
            std::cout << "Import error: unavoidable cyclic on vertex at " << glm::to_string(start->pos) << " with " << next->links.size() << " siblings\n";
            break;
          }
        }
        prev = next; next = newNext;
      }
      
      dotAvg = fabs(dotAvg);
      dotAvg /= dotWeight;
      newIdeal->weight = 1.f - dotAvg;
      
      //Add the new contour
      if(escapeCounter == 0) {
        std::cout << "Import error: cyclic loop on contour, skipping...\n";
        delete newIdeal;
      } else if(newIdeal->size() > 2) {
        newIdeal->quadratic = 1.f;
        newIdeal->prefab = false;
        newIdeal->parametricize();
        newIdeal->integrate();
        newIdeal->getVariance();
        if(newIdeal->area > areaThreshold) {
          layer->ideals.push_back(newIdeal);
        } else delete newIdeal;
      } else delete newIdeal;
      
      groupIdx++;
    }
    
    //Get center of each contour
    /*for(IdealContour *& ideal : layer->ideals) {
      
    }*/
    
    if(ct == v->tEnd) break;
    ct = min(v->tEnd, ct+dt);
    
    layer->idealMeta.resize(layer->ideals.size());
  }
  
  //Draw centroid connections
  V3 layerColor = V3(1.0, 0.0, 1.0);
  if(i%3 == 0) layerColor = V3_RIGHT;
  else if(i%3 == 1) layerColor = V3_UP;
  else if(i%3 == 2) layerColor = V3_FORWARD;
  
  rawPaths.clear();
  
  const float connThreshold = 0.05f;
  for(unsigned int lid = 0; lid < v->layers.size(); lid++) {
    TraceLayer *p, *c = &(v->layers[lid]), *n;
    p = (lid == 0)? nullptr : &v->layers[lid-1];
    n = (lid == v->layers.size()-1)? nullptr : &v->layers[lid+1];
    
    unsigned int idealID = 0;
    for(IdealContour *&ideal : c->ideals) {
      //if(ideal->getIntegral() < areaThreshold || !ideal->isOutwardConvex()) continue;
      unsigned int dim = dbgMesh->addVert(WObjectVert(c->wp.resolve(ideal->centroid), V3(0.f)));
      dbgMesh->vert(dim).sourcePos = layerColor * ideal->weight;
      IdealMeta *const m = &(c->idealMeta[idealID]);
      m->id = LID{lid, idealID};
      //std::cout << " layer " << lid << " has centroid " << glm::to_string(c->wp.resolve(ideal->centroid)) << "\n";
      //ideal->weight = sqrt(ideal->weight);
      
      IdealContour *oi = nullptr;
      if(p != nullptr && (oi = p->getNearestIdeal(ideal->centroid)) != nullptr) {
        const float d = glm::distance(ideal->centroid, oi->centroid);
        const float minA = max(fullThreshold, min(ideal->radius, oi->radius)*fullMult);
        if(d > minA) { //std::cout << "SKIP " << d << " over " << minA << "\n";
        } else {
          // (p->nearestContains(ideal->centroid) ||ideal->contains(p->getNearestIdeal(ideal->centroid)->centroid))
          unsigned int dio = dbgMesh->addVert(WObjectVert(p->wp.resolve(p->getNearestCentroid(ideal->centroid)), V3(0.f)));
          dbgMesh->vert(dio).sourcePos = layerColor * ideal->weight;// * p->getNearestIdeal(ideal->centroid)->weight;
          //dbgMesh->addLineInd(dio, dim);
          const unsigned int oid = p->getIdealID(oi);
          if(ideal->weight > connThreshold) {
            m->addConn(LID{lid-1, oid}, ideal->weight);
            p->idealMeta[oid].addConn(m->id, ideal->weight);
          }
        }
      } else //std::cout << "NO PREV CONTAIN";
      if(n != nullptr && (oi = n->getNearestIdeal(ideal->centroid)) != nullptr ) {
        const float d = glm::distance(ideal->centroid, oi->centroid);
        const float minA = max(fullThreshold, min(ideal->radius, oi->radius)*fullMult);
        if(d > minA) { //std::cout << "SKIP " << d << " over " << minA << "\n";
        } else {
          //&& (n->nearestContains(ideal->centroid) || ideal->contains(n->getNearestIdeal(ideal->centroid)->centroid))
          unsigned int dio = dbgMesh->addVert(WObjectVert(n->wp.resolve(n->getNearestCentroid(ideal->centroid)), V3(0.f)));
          dbgMesh->vert(dio).sourcePos = layerColor * ideal->weight;//n->getNearestIdeal(ideal->centroid)->weight;
          //dbgMesh->addLineInd(dio, dim);
          const unsigned int oid = n->getIdealID(oi);
          
          if(ideal->weight > connThreshold) {
            m->addConn(LID{lid+1, oid}, ideal->weight);
            n->idealMeta[oid].addConn(m->id, ideal->weight);
          }
        }
      }// else std::cout << "NO NEXT CONTAIN";
      
      idealID++;
    }
  }
  
  //Build core paths
  for(unsigned int lid = 0; lid < v->layers.size(); lid++) {
    TraceLayer *c = &(v->layers[lid]);
    unsigned int idealID = 0;
    for(IdealContour *&ideal : c->ideals) {
      _unused(ideal);
      IdealMeta *m = &(c->idealMeta[idealID]);
      
      if(m->useCount == 0) {
        rawPaths.emplace_back(Path(v));
        Path *path = &(*rawPaths.rbegin());
        //Traverse unused ideals to create path
        IdealMeta *nodeMeta = m, *nextMeta;
        path->addNode(nodeMeta, v, 1.f);
        //std::cout << "building path on layer " << lid << "\n";
        while((nextMeta = nodeMeta->getNextUnused(&v->layers)) != nullptr) {
          //std::cout << "adding " << nextMeta << "\n";
          nodeMeta = nextMeta;
          path->addNode(nodeMeta,  v, 1.f);
        }
        
        path->process();
        path->score = clamp(pow(path->score, 2.2f)*5.f, 0.f, 1.f);
        
        //Draw path for debugging
        ColorRGBA pathColor = ColorRGBA(rand()%128+128, rand()%128+128,rand()%128+128,255);
        unsigned int lastNVID = 0;
        float pathWeight = path->score;//1.f;//pow(path->score, 0.5f);
        //std::cout << "begin path rdebug:\n";
        for(auto it = path->nodes.begin(); it != path->nodes.end(); ++it) {
          unsigned int nextNVID = dbgMesh->addVert(WObjectVert(it->pos, V3(0.f)));
          //std::cout << "node at " << glm::to_string(it->pos) << "\n";
          dbgMesh->vert(nextNVID).sourcePos = V3(pathColor.r, pathColor.g, pathColor.b)/255.f * pathWeight;// * p->getNearestIdeal(ideal->centroid)->weight;
          if(lastNVID != 0 || nextNVID == 1) dbgMesh->addLineInd(lastNVID, nextNVID);
          lastNVID = nextNVID;
        }
      }
      idealID++;
    }
  }
  
  //let each vertex vote for their best path
  
  //Blend paths that share vertices together
  
  //Prune
  
  //Calculate contours
  
  //Reduce contour density along path
  
  dbgMesh->upload();
}

void ModelImport::drawRaw() {
  model->draw();
}
void ModelImport::drawPrinciples() {
  
}

void ModelImport::drawDebug() {
  if(dbgMesh != nullptr) {
    //std::cout << "DRAWING DBGMESH of size=" << dbgMesh->getVertCount() << "\n";
    dbgMesh->draw();
  }
}