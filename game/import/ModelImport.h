/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ModelImport.h
 * Author: Galen Swain
 *
 * Created on May 27, 2016, 4:08 PM
 */

#ifndef MODELIMPORT_H
#define MODELIMPORT_H
#include "game.h"
#include <list>
#include <vector>
#include <client/Model.h>
#include "comp/Workplane.h"
#include "rep/IdealContour.h"
#include "rep/BakedContour.h"
#include "uni/WorldObject.h"
#include <unordered_map>
#include <deque>
#include <list>
#include <algorithm>

class Model;
class ModelImport {
public:
    struct TraceNode {
        V3 pos;
        V3 hermite;
        V2 texCoord;
        std::list<TraceNode*> links;
        unsigned short group;
        TraceNode() : pos(0.f), hermite(0.f), texCoord(0.f), links(), group(0xFFFF) {};
        TraceNode(V3 p, V3 h, V2 tc) : pos(p), hermite(h), texCoord(tc), links(), group(0xFFFF) {};
        
        void removeLink(ModelImport::TraceNode* n);
    };
    struct LID {
        unsigned int layerID;
        unsigned int idealID;
        inline bool operator==(const LID& o) const { return layerID == o.layerID && idealID == o.idealID; };
    };
    struct TraceLayer;
    struct IdealMeta {
        LID id;
        std::list<LID> conns;
        unsigned char useCount;
        float weight;
        
        inline IdealMeta() : conns(), useCount(0), weight(0.f) {};
        inline void addConn(LID o, float w) {
            if(std::find(conns.begin(), conns.end(), o) == conns.end()) conns.push_back(o);
            weight = max(weight, w);
        }
        IdealMeta* getNextUnused(std::deque<TraceLayer> *layers);
    };
    struct TraceLayer {
        float t, str;
        std::vector<IdealContour*> ideals;
        std::vector<IdealMeta> idealMeta;
        IdealContour *preIdeal;
        
        struct V3KeyFuncs {
                size_t operator()(const V3& k) const {
                    return std::hash<int>()(int(k.x*100.f)) ^ std::hash<int>()(int(k.y*100.f)) ^ std::hash<int>()(int(k.z*100.f));
                }
                bool operator()(const V3& a , const V3& b) const {
                        return a == b;
                }
            };
        std::unordered_map<V3, TraceNode, V3KeyFuncs, V3KeyFuncs> nodes;
        Workplane wp;
        
        inline TraceLayer() : t(0.f), str(0.f), ideals(), preIdeal(nullptr), nodes(), wp() {};
        ~TraceLayer();
        
        void linkNodes(V3 a, V3 b, V3 ha, V3 hb, V2 ta, V2 tb);
        void pruneNodes();
        
        IdealContour *getNearestIdeal(V3 p);
        V3 getNearestCentroid(V3 p);
        bool nearestContains(V3 p);
        unsigned int getIdealID(IdealContour *const c) const;
    };
    struct TraceVec {
        V3 normal;
        float tStart, tEnd;
        std::deque<TraceLayer> layers;
        Mesh<ModelVert> *mesh;
        inline TraceVec(V3 n, Mesh<ModelVert>* m) : normal(glm::normalize(n)), tStart(99999.f), tEnd(-99999.f), layers(), mesh(m) {};
    };
    
    struct PathNode {
        V3 tangent, srcVector, pos;
        float str;
        TraceLayer *layer;
        IdealContour* ideal;
        IdealMeta *meta;
        TraceVec *vec;
        LID id;
        PathNode(IdealMeta *m, TraceVec *v);
    };
    
    struct Path {
        std::vector<PathNode> nodes;
        float length, score;
        float avgArea;
        TraceVec *vec;
        inline Path(TraceVec *v) : nodes(), length(0.f), vec(v) {};
        void addNode(IdealMeta *m, TraceVec *v, float str);
        void process();
    };
    
    ModelImport();
    ModelImport(std::string str);
    virtual ~ModelImport();
    
    Model *model;
    std::deque<TraceVec> traceVectors;
    V3 modelCenter;
    Mesh<WObjectVert> *dbgMesh;
    
    std::vector<Path> rawPaths;
    std::vector<Path> paths;
    
    void load(std::string path);
    
    void useVectors(unsigned short count = 3);
    void process();
    void traceVector(unsigned short i);
    
    void drawRaw();
    void drawPrinciples();
    void drawDebug();
private:

};

#endif /* MODELIMPORT_H */

