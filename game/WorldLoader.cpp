/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldLoader.cpp
 * Author: Galen Swain
 * 
 * Created on April 9, 2016, 2:34 PM
 */

#include "WorldLoader.h"
#include "uni/WorldObject.h"
#include "WorldLoader.h"
#include "WorldManager.h"
#include <unordered_map>
#include "lib/Encode.h"
#include "lib/AppData.h"

namespace WorldLoader {
  static std::unordered_map<WOID, WorldObjectPtr> objectMap;
  thread_local WorldObjectPtr nullObject = WorldObjectPtr(nullptr);
  
  Directory *storageDir = nullptr;
  
  WorldObjectPtr GetObject(WOID id) { _prsguard();
    auto entry = objectMap.find(id);
    if(entry != objectMap.end()) return entry->second;
    else return LoadObject(id);
  }
  void RemoveObject(WorldObjectPtr &o) { _prsguard();
    if(!o.isRel) return;
    else UnloadObject(o);
  }
  
  WorldObjectPtr LoadObject(WOID id) { _prsguard();
    if(id == 0) {
      if(!nullObject) {
        nullObject = MakeDeferredObject(0);
      }
      return nullObject;
    }
    WorldObjectPtr ret = MakeDeferredObject(id);
    //Queue the world manager to actually load this object
    WorldManager::QueueLoadObject(ret);
    return ret;
  }
  void UnloadObject(WorldObjectPtr &o) { _prsguard();
    //Queue the worldmanager to actually remove
    WorldManager::QueueUnloadObject(o);
  }
  
  WorldObjectPtr MakeDeferredObject(WOID id) { _prsguard();
    WorldObject *newObj = new WorldObject();
    newObj->setDeferred(true);
    newObj->setID(id);
    std::cout << "made deferred object for WO#" << id << "\n";
    return WorldObjectPtr(newObj);
  }
  
  thread_local char* dataStoreBuffer = nullptr;
  thread_local unsigned int dataStoreSize = 0;
  void freeDSBuffer() {
    if(dataStoreBuffer == nullptr) return;
    delete[] dataStoreBuffer;
    dataStoreSize = 0;
  }
  void ensureDSBuffer(unsigned int s = 1024*1024) {
    if(dataStoreSize > s) return;
    
    freeDSBuffer();
    dataStoreBuffer = new char[s];
    dataStoreSize = s;
  }
  
  std::string GetLoadKey(const WorldObjectPtr &o) {
    _prsguard();
    return Encode::FSEncode(o.woID());
  }
  
  
  //The following are ONLY executed in a worker thread
  void DoLoadObject(WorldObjectPtr &o) { _prsguard();
    //Load from file, query server, or query generator
    std::cout << "Requested to load object #" << o->getID() << "\n";
  }
  void DoUnloadObject(WorldObjectPtr &o) { _prsguard();
    
    std::cout << "Requested to UNload object #" << o->getID() << "\n";
  }
};