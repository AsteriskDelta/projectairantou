#ifndef CEREAL_PCH_INC
#define CEREAL_PCH_INC
#define CEREAL_INCLUDED

#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#include <ext/cereal/types/polymorphic.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/queue.hpp>
#include <cereal/types/deque.hpp>
#include <cereal/types/memory.hpp>

#include <ext/cereal/archives/binary.hpp>
#include <ext/cereal/archives/json.hpp>
#include <ext/cereal/archives/xml.hpp>

#define CEREAL_NVP_ID(idt, v) cereal::make_nvp(idt, v)
#define COMMA() ,

#pragma GCC diagnostic pop
#endif