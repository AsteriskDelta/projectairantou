/* 
 * File:   MatManager.h
 * Author: Galen Swain
 *
 * Created on June 18, 2015, 3:56 PM
 */

#ifndef MATMANAGER_H
#define	MATMANAGER_H

#include "game.h"
#include "PhysMat.h"

#define MATERIALS_MAX 638
namespace MatManager {
    void Initialize();//Load all material data
    void LoadRendering();//Load texture data and render-specific stuff

    extern Uint32 materialCount;
    extern PhysMat materials[MATERIALS_MAX];
};

#endif

