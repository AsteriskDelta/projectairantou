/* 
 * File:   PhysMat.h
 * Author: Galen Swain
 *
 * Created on June 13, 2015, 9:07 PM
 */

#ifndef PHYSMAT_H
#define	PHYSMAT_H
#include "game.h"
#include <Texture.h>

#define MAT_MAX_TEX 2

class PhysMat {
public:
    PhysMat();
    virtual ~PhysMat();
    
    Uint16 id;
    Uint8 renderID;
    
    float density;//kg/cubic unit
    float frictionCoeff;
    
    float hardness;//Physical resistance
    float flexibility;//
    float elasticity;
    
    //Liquid
    float permeability, retention;
    
    //Flame- threshold of denaturing and flashing, and resultant energy released at rate
    float denaturePoint, flashPoint, flashEnergy, flashRate;
    
    //Conductance
    float energyCapacity, energyRetention;
    float elecConductance, elecAbsorbance;
    float mageConductance, mageAbsorbance;
    float heatConductance, heatAbsorbance;
    
    //Emmissivity
    
    
    //Visual
    float roughness, metalness;
    float transparency, divinity, simul;
    float bumpScale;
    Texture2D* textures[MAT_MAX_TEX];
    std::string texPath[MAT_MAX_TEX];
    float texWeight[MAT_MAX_TEX];
    
    //Interface
    std::string name;
private:

};

#endif	/* PHYSMAT_H */

