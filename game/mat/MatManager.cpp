/* 
 * File:   MatManager.cpp
 * Author: Galen Swain
 * 
 * Created on June 18, 2015, 3:56 PM
 */

#include "MatManager.h"

namespace MatManager {
  Uint32 materialCount;
  PhysMat materials[MATERIALS_MAX];
  
  char *matBlockBuffer; const unsigned int matBlockMax = 65536; unsigned int matBlockUsed = 0;
  
  void Initialize() {
    materialCount = 2;
    PhysMat *mat = &materials[0];
    mat->hardness = 1.f;
    mat->flexibility = mat->elasticity = 1.f;
    mat->name = "Material";
    
    mat = &materials[1];
    mat->hardness = 1.f;
    mat->flexibility = mat->elasticity = 1.f;
    mat->name = "Bond";
  }
};