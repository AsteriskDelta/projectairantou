/* 
 * File:   WorldObjectLink.h
 * Author: Galen Swain
 *
 * Created on June 20, 2015, 11:05 AM
 */

#ifndef WORLDOBJECTLINK_H
#define	WORLDOBJECTLINK_H
#include "field/DeltaVector.h"
#include "WorldObjectPtr.h"

class WorldObject;
class PhysMat;

class WorldObjectLink {
public:
    enum {
        Instructed = BIT03
    };
    
    WorldObjectLink();
    WorldObjectLink(WorldObject *const a, WorldObject *const b, const DeltaVector& v = DeltaVector());
    WorldObjectLink(const WorldObjectLink& orig);
    virtual ~WorldObjectLink();
    
    inline WorldObjectPtr& getOther(const WorldObjectPtr& self) {
        return ends[0] == self? ends[1] : ends[0];
    }
    inline WorldObjectPtr& operator()(const WorldObjectPtr& self) { return getOther(self); };
    
    WorldObjectPtr ends[2];
    struct {
        Uint8 flags;
    };
    DeltaVector vec;
    
    inline operator bool() const {
        return ends[0] != nullptr && ends[1] != nullptr;
    }
    
    float getWeight();
    
    template<class Archive>
    void serialize(Archive& archive) {
      archive(CEREAL_NVP(ends),
              CEREAL_NVP(flags),
              CEREAL_NVP(vec)
              );
    }
    
    static WorldObjectLink null;
private:
    
    
};

//CEREAL_REGISTER_TYPE(WorldObjectLink);

#endif	/* WORLDOBJECTLINK_H */

