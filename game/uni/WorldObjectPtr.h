/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldObjectPtr.h
 * Author: Galen Swain
 *
 * Created on April 10, 2016, 12:11 PM
 */

#ifndef WORLDOBJECTPTR_H
#define WORLDOBJECTPTR_H
#include "game.h"
class WorldObject;

class WorldObjectPtr {
public:
    WorldObjectPtr();
    WorldObjectPtr(const WorldObjectPtr& orig);
    virtual ~WorldObjectPtr();
    
    WorldObjectPtr(WorldObject *const obj);
    WorldObjectPtr(WOID objID);
    
    WorldObject& operator*();
    operator WorldObject*();
    
    inline WorldObject* rawptr() {
        return (WorldObject*)(*this);
    }
    WOID woID() const;
    
    WorldObject* operator->();
    
    WorldObjectPtr& operator=(const WorldObjectPtr& o);
    bool operator==(const WorldObjectPtr& o) const;
    bool operator!=(const WorldObjectPtr& o) const;
    
    bool operator==(const WorldObject *const o) const;
    bool operator!=(const WorldObject *const o) const;
    
    union {
        struct {
            bool isRel : 1;
            WOID id : 63;
        };
        WorldObject* ptr;
    };

    template<class Archive>
    void save(Archive & archive) const {
        archive(this->woID());
    }

    template<class Archive>
    void load(Archive & archive) {
        WOID newWOID;
        archive(newWOID);
        *this = WorldObjectPtr(newWOID);
    }
    
    inline operator bool() const {
        return id == 0x0;
    }
private:

};
//CEREAL_REGISTER_TYPE(WorldObjectPtr);

#endif /* WORLDOBJECTPTR_H */

