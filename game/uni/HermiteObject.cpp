/* 
 * File:   HermiteObject.cpp
 * Author: Galen Swain
 * 
 * Created on May 15, 2015, 9:41 AM
 */

#include "HermiteObject.h"
#include "Mesh.h"

//Paper-defined QEF and DC constants
const int MATERIAL_AIR = 0;
const int MATERIAL_SOLID = 1;

const float QEF_ERROR = 1e-5f;
const int QEF_SWEEPS = 4;
const hovec CHILD_MIN_OFFSETS[] = {
	// needs to match the vertMap from Dual Contouring impl
	hovec( 0, 0, 0 ),
	hovec( 0, 0, 1 ),
	hovec( 0, 1, 0 ),
	hovec( 0, 1, 1 ),
	hovec( 1, 0, 0 ),
	hovec( 1, 0, 1 ),
	hovec( 1, 1, 0 ),
	hovec( 1, 1, 1 ),
};
const int edgevmap[12][2] =  {
	{0,4},{1,5},{2,6},{3,7},	// x-axis 
	{0,2},{1,3},{4,6},{5,7},	// y-axis
	{0,1},{2,3},{4,5},{6,7}		// z-axis
};
const int edgemask[3] = { 5, 3, 6 } ;
const int vertMap[8][3] = {
	{0,0,0},
	{0,0,1},
	{0,1,0},
	{0,1,1},
	{1,0,0},
	{1,0,1},
	{1,1,0},
	{1,1,1}
};
const int faceMap[6][4] = {{4, 8, 5, 9}, {6, 10, 7, 11},{0, 8, 1, 10},{2, 9, 3, 11},{0, 4, 2, 6},{1, 5, 3, 7}} ;
const int cellProcFaceMask[12][3] = {{0,4,0},{1,5,0},{2,6,0},{3,7,0},{0,2,1},{4,6,1},{1,3,1},{5,7,1},{0,1,2},{2,3,2},{4,5,2},{6,7,2}} ;
const int cellProcEdgeMask[6][5] = {{0,1,2,3,0},{4,5,6,7,0},{0,4,1,5,1},{2,6,3,7,1},{0,2,4,6,2},{1,3,5,7,2}} ;
const int faceProcFaceMask[3][4][3] = {
	{{4,0,0},{5,1,0},{6,2,0},{7,3,0}},
	{{2,0,1},{6,4,1},{3,1,1},{7,5,1}},
	{{1,0,2},{3,2,2},{5,4,2},{7,6,2}}
} ;
const int faceProcEdgeMask[3][4][6] = {
	{{1,4,0,5,1,1},{1,6,2,7,3,1},{0,4,6,0,2,2},{0,5,7,1,3,2}},
	{{0,2,3,0,1,0},{0,6,7,4,5,0},{1,2,0,6,4,2},{1,3,1,7,5,2}},
	{{1,1,0,3,2,0},{1,5,4,7,6,0},{0,1,5,0,4,1},{0,3,7,2,6,1}}
};
const int edgeProcEdgeMask[3][2][5] = {
	{{3,2,1,0,0},{7,6,5,4,0}},
	{{5,1,4,0,1},{7,3,6,2,1}},
	{{6,4,2,0,2},{7,5,3,1,2}},
};
const int processEdgeMask[3][4] = {{3,2,1,0},{7,5,6,4},{11,10,9,8}} ;


static thread_local Mesh<WObjectVert> *dbgMesh;

float Sphere(const vec3& worldPosition, const vec3& origin, float radius) {
  return glm::length(worldPosition - origin) - radius;
}

// ----------------------------------------------------------------------------

float Cuboid(const vec3& worldPosition, const vec3& origin, const vec3& halfDimensions) {
  const vec3& local_pos = worldPosition - origin;
  const vec3& pos = local_pos;

  const vec3& d = glm::abs(pos) - halfDimensions;
  const float m = max(d.x, max(d.y, d.z));
  return min(m, glm::length(max(d, vec3(0.f))));
}

// ----------------------------------------------------------------------------
#include <glm/ext.hpp>

float FractalNoise(
        const int octaves,
        const float frequency,
        const float lacunarity,
        const float persistence,
        const vec2& position) {
  const float SCALE = 1.f / 128.f;
  vec2 p = position * SCALE;
  float noise = 0.f;

  float amplitude = 1.f;
  p *= frequency;

  for (int i = 0; i < octaves; i++) {
    noise += glm::simplex(p) * amplitude;
    p *= lacunarity;
    amplitude *= persistence;
  }

  // move into [0, 1] range
  return 0.5f + (0.5f * noise);
}

// ----------------------------------------------------------------------------

float Density_Func(const vec3& worldPosition) {
  /*const float MAX_HEIGHT = 20.f;
  const float noise = FractalNoise(4, 0.5343f, 2.2324f, 0.68324f, vec2(worldPosition.x, worldPosition.z));
  const float terrain = worldPosition.y - (MAX_HEIGHT * noise);

  const float cube = Cuboid(worldPosition, vec3(-4., 10.f, -4.f), vec3(12.f));
  const float sphere = Sphere(worldPosition, vec3(15.f, 2.5f, 1.f), 16.f);*/

  return Sphere(worldPosition, vec3(15.f, 2.5f, 1.f), 16.f); //max(-cube, min(sphere, terrain));
}

vec3 CalculateSurfaceNormal(const vec3& p) {
  const float H = 0.001f;
  const float dx = Density_Func(p + vec3(H, 0.f, 0.f)) - Density_Func(p - vec3(H, 0.f, 0.f));
  const float dy = Density_Func(p + vec3(0.f, H, 0.f)) - Density_Func(p - vec3(0.f, H, 0.f));
  const float dz = Density_Func(p + vec3(0.f, 0.f, H)) - Density_Func(p - vec3(0.f, 0.f, H));

  return glm::normalize(vec3(dx, dy, dz));
}

vec3 ApproximateZeroCrossingPosition(const vec3& p0, const vec3& p1) {
  // approximate the zero crossing by finding the min value along the edge
  float minValue = 100000.f;
  float t = 0.f;
  float currentT = 0.f;
  const int steps = 8;
  const float increment = 1.f / (float) steps;
  while (currentT <= 1.f) {
    const vec3 p = p0 + ((p1 - p0) * currentT);
    const float density = glm::abs(Density_Func(p));
    if (density < minValue) {
      minValue = density;
      t = currentT;
    }

    currentT += increment;
  }

  return p0 + ((p1 - p0) * t);
}

void GenerateVertexIndices(HObjNode* node, Mesh<WObjectVert> * const mesh) {
  if (!node) {
    return;
  }

  if (node->type != HObjNode::Leaf) {
    for (int i = 0; i < 8; i++) {
      GenerateVertexIndices(node->children[i], mesh);
    }
  }

  if (node->type != HObjNode::Internal) {
    HObjDI* d = node->dInfo;
    if (!d) {
      printf("Error! Could not add vertex!\n");
      exit(EXIT_FAILURE);
    }

    d->index = mesh->addVert(WObjectVert(d->pos, d->norm));
#ifdef DEBUGMESH
    dbgMesh->addVert(WObjectVert(d->pos, d->norm));
#endif
  }
}

// ----------------------------------------------------------------------------

void ContourProcessEdge(HObjNode* node[4], int dir, Mesh<WObjectVert> * const mesh) {
  int minSize = 1000000; // arbitrary big number
  int minIndex = 0;
  int indices[4] = {-1, -1, -1, -1};
  bool flip = false;
  bool signChange[4] = {false, false, false, false};

  for (int i = 0; i < 4; i++) {
    const int edge = processEdgeMask[dir][i];
    const int c1 = edgevmap[edge][0];
    const int c2 = edgevmap[edge][1];

    const int m1 = (node[i]->dInfo->corners >> c1) & 1;
    const int m2 = (node[i]->dInfo->corners >> c2) & 1;

    if (node[i]->size < minSize) {
      minSize = node[i]->size;
      minIndex = i;
      flip = m1 != MATERIAL_AIR;
    }

    indices[i] = node[i]->dInfo->index;

    signChange[i] =
            (m1 == MATERIAL_AIR && m2 != MATERIAL_AIR) ||
            (m1 != MATERIAL_AIR && m2 == MATERIAL_AIR);
  }

  if (signChange[minIndex]) {
    if (!flip) {
      mesh->addTriInd(indices[0], indices[1], indices[3]);

      mesh->addTriInd(indices[0], indices[3], indices[2]);
    } else {
      mesh->addTriInd(indices[0], indices[3], indices[1]);

      mesh->addTriInd(indices[0], indices[2], indices[3]);
    }
#ifdef DEBUGMESH
    dbgMesh->addLineInd(indices[0], indices[1]);
    dbgMesh->addLineInd(indices[1], indices[3]);
    dbgMesh->addLineInd(indices[3], indices[2]);
#endif
  }
}

// ----------------------------------------------------------------------------

void ContourEdgeProc(HObjNode* node[4], int dir, Mesh<WObjectVert> * const mesh) {
  if (!node[0] || !node[1] || !node[2] || !node[3]) {
    return;
  }

  if (node[0]->type != HObjNode::Internal &&
          node[1]->type != HObjNode::Internal &&
          node[2]->type != HObjNode::Internal &&
          node[3]->type != HObjNode::Internal) {
    ContourProcessEdge(node, dir, mesh);
  } else {
    for (int i = 0; i < 2; i++) {
      HObjNode * edgeNodes[4];
      const int c[4] ={
        edgeProcEdgeMask[dir][i][0],
        edgeProcEdgeMask[dir][i][1],
        edgeProcEdgeMask[dir][i][2],
        edgeProcEdgeMask[dir][i][3],
      };

      for (int j = 0; j < 4; j++) {
        if (node[j]->type == HObjNode::Leaf || node[j]->type == HObjNode::Pseudo) {
          edgeNodes[j] = node[j];
        } else {
          edgeNodes[j] = node[j]->children[c[j]];
        }
      }

      ContourEdgeProc(edgeNodes, edgeProcEdgeMask[dir][i][4], mesh);
    }
  }
}

// ----------------------------------------------------------------------------

void ContourFaceProc(HObjNode* node[2], int dir, Mesh<WObjectVert> * const mesh) {
  if (!node[0] || !node[1]) {
    return;
  }

  if (node[0]->type == HObjNode::Internal ||
          node[1]->type == HObjNode::Internal) {
    for (int i = 0; i < 4; i++) {
      HObjNode * faceNodes[2];
      const int c[2] ={
        faceProcFaceMask[dir][i][0],
        faceProcFaceMask[dir][i][1],
      };

      for (int j = 0; j < 2; j++) {
        if (node[j]->type != HObjNode::Internal) {
          faceNodes[j] = node[j];
        } else {
          faceNodes[j] = node[j]->children[c[j]];
        }
      }

      ContourFaceProc(faceNodes, faceProcFaceMask[dir][i][2], mesh);
    }

    const int orders[2][4] ={
      { 0, 0, 1, 1},
      { 0, 1, 0, 1},
    };
    for (int i = 0; i < 4; i++) {
      HObjNode * edgeNodes[4];
      const int c[4] ={
        faceProcEdgeMask[dir][i][1],
        faceProcEdgeMask[dir][i][2],
        faceProcEdgeMask[dir][i][3],
        faceProcEdgeMask[dir][i][4],
      };

      const int* order = orders[faceProcEdgeMask[dir][i][0]];
      for (int j = 0; j < 4; j++) {
        if (node[order[j]]->type == HObjNode::Leaf ||
                node[order[j]]->type == HObjNode::Pseudo) {
          edgeNodes[j] = node[order[j]];
        } else {
          edgeNodes[j] = node[order[j]]->children[c[j]];
        }
      }

      ContourEdgeProc(edgeNodes, faceProcEdgeMask[dir][i][5], mesh);
    }
  }
}

// ----------------------------------------------------------------------------

void ContourCellProc(HObjNode* node, Mesh<WObjectVert> * const mesh) {
  if (node == NULL) {
    return;
  }

  if (node->type == HObjNode::Internal) {
    for (int i = 0; i < 8; i++) {
      ContourCellProc(node->children[i], mesh);
    }

    for (int i = 0; i < 12; i++) {
      HObjNode * faceNodes[2];
      const int c[2] = {cellProcFaceMask[i][0], cellProcFaceMask[i][1]};

      faceNodes[0] = node->children[c[0]];
      faceNodes[1] = node->children[c[1]];

      ContourFaceProc(faceNodes, cellProcFaceMask[i][2], mesh);
    }

    for (int i = 0; i < 6; i++) {
      HObjNode * edgeNodes[4];
      const int c[4] ={
        cellProcEdgeMask[i][0],
        cellProcEdgeMask[i][1],
        cellProcEdgeMask[i][2],
        cellProcEdgeMask[i][3],
      };

      for (int j = 0; j < 4; j++) {
        edgeNodes[j] = node->children[c[j]];
      }

      ContourEdgeProc(edgeNodes, cellProcEdgeMask[i][4], mesh);
    }
  }
}

HObjNode* SimplifyOctree(HObjNode* node, float threshold) {
  if (!node) return NULL;

  if (node->type != HObjNode::Internal) {
    // can't simplify!
    return node;
  }

  svd::QefSolver qef;
  int signs[8] = {-1, -1, -1, -1, -1, -1, -1, -1};
  int midsign = -1;
  int edgeCount = 0;
  bool isCollapsible = true;

  for (int i = 0; i < 8; i++) {
    node->children[i] = SimplifyOctree(node->children[i], threshold);
    if (node->children[i]) {
      HObjNode* child = node->children[i];
      if (child->type == HObjNode::Internal) {
        isCollapsible = false;
      } else {
        qef.add(child->dInfo->qef);

        midsign = (child->dInfo->corners >> (7 - i)) & 1;
        signs[i] = (child->dInfo->corners >> i) & 1;

        edgeCount++;
      }
    }
  }

  if (!isCollapsible) {
    // at least one child is an internal node, can't collapse
    return node;
  }

  svd::Vec3 qefPosition;
  qef.solve(qefPosition, QEF_ERROR, QEF_SWEEPS, QEF_ERROR);
  float error = qef.getError();

  // convert to glm vec3 for ease of use
  vec3 position(qefPosition.x, qefPosition.y, qefPosition.z);

  // at this point the masspoint will actually be a sum, so divide to make it the average
  if (error > threshold) {
    // this collapse breaches the threshold
    return node;
  }

  if (position.x < node->min.x || position.x > (node->min.x + node->size) ||
          position.y < node->min.y || position.y > (node->min.y + node->size) ||
          position.z < node->min.z || position.z > (node->min.z + node->size)) {
    const auto& mp = qef.getMassPoint();
    position = vec3(mp.x, mp.y, mp.z);
  }

  // change the node from an internal node to a 'psuedo leaf' node
  HObjDI* drawInfo = new HObjDI;

  for (int i = 0; i < 8; i++) {
    if (signs[i] == -1) {
      // Undetermined, use centre sign instead
      drawInfo->corners |= (midsign << i);
    } else {
      drawInfo->corners |= (signs[i] << i);
    }
  }

  drawInfo->norm = vec3(0.f);
  for (int i = 0; i < 8; i++) {
    if (node->children[i]) {
      HObjNode* child = node->children[i];
      if (child->type == HObjNode::Pseudo ||
              child->type == HObjNode::Leaf) {
        drawInfo->norm += child->dInfo->norm;
      }
    }
  }

  drawInfo->norm = glm::normalize(drawInfo->norm);
  drawInfo->pos = position;
  drawInfo->qef = qef.getData();

  for (int i = 0; i < 8; i++) {
    delete node->children[i];
    node->children[i] = NULL;
  }

  node->type = HObjNode::Pseudo;
  node->dInfo = drawInfo;

  return node;
}

HObjNode::~HObjNode() {
  for(Uint8 i = 0; i < 8; i++) if(children[i] != NULL) delete children[i];
  if(dInfo != NULL) delete dInfo;
}

//static thread_local HObjNode *newChild = new HObjNode();

bool HObjNode::constructNode() {//Todo: move allocation out after confirming functionality
  if (size == 1) return this->constructLeaf();
  const int childSize = size / 2;
  bool hasChildren = false;
  HObjNode *newChild = new HObjNode();//Throws an exception of made static or thread_local... wat
  
  for (int i = 0; i < 8; i++) {//See if node needs to be expanded- if so, claim the child and move on
    if(newChild == NULL) newChild = new HObjNode();
    newChild->size = childSize;
    newChild->min = min + (CHILD_MIN_OFFSETS[i] * hovec(childSize));
    newChild->type = Internal;
    
    if(newChild->constructNode()) {
      hasChildren = true;
      children[i] = newChild;
      newChild = NULL;
    } else children[i] = NULL;
  }
  if(newChild != NULL) delete newChild;

  if (!hasChildren) return false;
  else return true;
}

bool HObjNode::constructLeaf() {
  int corners = 0;
  for(Uint8 i = 0; i < 8; i++) {
    const hovec cornerPos = min + CHILD_MIN_OFFSETS[i];
    const float density = Density_Func(V3(cornerPos));
    const int material = density < 0.f ? MATERIAL_SOLID : MATERIAL_AIR;
    corners |= (material << i);
  }

  if(corners == 0 || corners == 255) return false;//Superfluous data, delete

  // otherwise the voxel contains the surface, so find the edge intersections
  const int MAX_CROSSINGS = 6;
  int edgeCount = 0;
  vec3 averageNormal(0.f);
  svd::QefSolver qef;

  for (int i = 0; i < 12 && edgeCount < MAX_CROSSINGS; i++) {
    const int c1 = edgevmap[i][0];
    const int c2 = edgevmap[i][1];

    const int m1 = (corners >> c1) & 1;
    const int m2 = (corners >> c2) & 1;

    if ((m1 == MATERIAL_AIR && m2 == MATERIAL_AIR) ||
            (m1 == MATERIAL_SOLID && m2 == MATERIAL_SOLID)) {
      // no zero crossing on this edge
      continue;
    }

    const vec3 p1 = vec3(min + CHILD_MIN_OFFSETS[c1]);
    const vec3 p2 = vec3(min + CHILD_MIN_OFFSETS[c2]);
    const vec3 p = ApproximateZeroCrossingPosition(p1, p2);
    const vec3 n = CalculateSurfaceNormal(p);
    qef.add(p.x, p.y, p.z, n.x, n.y, n.z);

    averageNormal += n;

    edgeCount++;
  }

  svd::Vec3 qefPosition;
  qef.solve(qefPosition, QEF_ERROR, QEF_SWEEPS, QEF_ERROR);

  HObjDI* drawInfo = new HObjDI;
  drawInfo->pos = vec3(qefPosition.x, qefPosition.y, qefPosition.z);
  drawInfo->qef = qef.getData();

  const vec3 vecMin = vec3(min);
  const vec3 max = vec3(this->min + hovec(size));
  if (drawInfo->pos.x < vecMin.x     || drawInfo->pos.x > max.x ||
          drawInfo->pos.y < vecMin.y || drawInfo->pos.y > max.y ||
          drawInfo->pos.z < vecMin.z || drawInfo->pos.z > max.z) {
    const auto& mp = qef.getMassPoint();
    drawInfo->pos = vec3(mp.x, mp.y, mp.z);
  }
  
  drawInfo->norm = glm::normalize(averageNormal / (float) edgeCount);
  drawInfo->corners = corners;

  this->type = Leaf;
  this->dInfo = drawInfo;
  return true;
}

HermiteObject::HermiteObject() {
  
}

//HermiteObject::HermiteObject(const HermiteObject& orig) {
//}

HermiteObject::~HermiteObject() {
  
}

void HermiteObject::construct(float epsilon) {
  hovt size = 64; hovec min = hovec(-size/2);
  float threshold = epsilon;
  root.min = min;
  root.size = size;
  root.type = HObjNode::Internal;

  root.constructNode();
  root = *SimplifyOctree(&root, threshold);
  
  //baseMesh = new Mesh<WObjectVert>(MeshType::Tris, 32000, 60000);
  #ifdef DEBUGMESH
  debugMesh = new Mesh<WObjectVert>(MeshType::Lines, 32000, 60000);
  dbgMesh = debugMesh;
  #endif
  
  //GenerateVertexIndices(&root, baseMesh);
  //ContourCellProc(&root, baseMesh);
  std::cout << "Hermite Object:\tV: " << baseMesh->getVertCount() << ", T: " << (baseMesh->getIndCount()/3) << " I: " << baseMesh->getIndCount() << "\n";
  //baseMesh->upload();
#ifdef DEBUGMESH
  dbgMesh->upload();
#endif
}
    
void HermiteObject::draw() {
  WorldObject::draw();
  //if(baseMesh != NULL) baseMesh->draw();
}

void HermiteObject::drawDebug() {
#ifdef DEBUGMESH
  WorldObject::drawDebug();
  if(debugMesh != NULL) debugMesh->draw();
#endif
}