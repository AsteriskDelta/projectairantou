/* 
 * File:   HermiteObject.h
 * Author: Galen Swain
 *
 * Created on May 15, 2015, 9:41 AM
 * 
 * Description: Non-trivial subdivision geometry, functions on a basic of coherent fractal data and seed data
 * TL;DR: Dirt, rocks, hills, rocks- can describe "natural" phenomena flawlessly. 
 */

#ifndef HERMITEOBJECT_H
#define	HERMITEOBJECT_H
#include "WorldObject.h"
#include <limits>
#include "rep/qef.h"

typedef Int16 hovt;
const hovt hovt_max = std::numeric_limits<hovt>::max();
const hovt hovt_min = std::numeric_limits<hovt>::min();
typedef V3T<hovt> hovec;

struct HObjDI {//Draw info
  HObjDI() : index(-1) , corners(0) { }
  
  Uint16 index;
  int corners;
  V3 pos, norm;
  QEF qef;
};

class HObjNode {
public:
    enum Type : Uint8 {
        None, Internal, Pseudo, Leaf
    };
    Type type;
    hovec min;
    hovt size;
    
    HObjDI *dInfo;
    HObjNode *children[8];
    
    HObjNode() : type(None) , min(0, 0, 0) , size(0) , dInfo(NULL) {
        for (int i = 0; i < 8; i++)  children[i] = NULL;
    }
    HObjNode(const Type _type) : type(_type) , min(0, 0, 0) , size(0) , dInfo(NULL) {
        for (int i = 0; i < 8; i++) children[i] = NULL;
    }
    ~HObjNode();
    
    bool constructNode();
    bool constructLeaf();
    
protected:
    
};

class HermiteObject : public WorldObject {
public:
    HermiteObject();
    //HermiteObject(const HermiteObject& orig);
    virtual ~HermiteObject();
    
    virtual void construct(float epsilon);
    
    virtual void draw();
    virtual void drawDebug();
private:
    HObjNode root;
};

#endif	/* HERMITEOBJECT_H */

