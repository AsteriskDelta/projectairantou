/* 
 * File:   WorldObject.h
 * Author: Galen Swain
 *
 * Created on May 10, 2015, 6:15 PM
 */

#ifndef WORLDOBJECT_H
#define	WORLDOBJECT_H
class Transform;
#include "game.h"
#include <lib/Matrix.h>
#include <lib/Flags.h>
#include <vector>
#include <list>
#include <queue>
#include "Mesh.h"
#include "rep/ContourPlane.h"
#include "rep/IdealContour.h"
#include "rep/Perturbation.h"
#include "rep/WorldMaterial.h"
#include "WorldObjectLink.h"
#include "energy/EnergyStore.h"
#include "comp/IntersectionHull.h"
#include "client/Transform.h"
#include "field/Subfield.h"
#include "field/DeltaVector.h"
#include "WorldObjectPtr.h"
#include "phys/Physical.h"
#include "WorldObjectMeta.h"
#include "gen/WorldGenerator.h"
namespace Soul {
class Soul;
};

//The minimum number of children to batch into a separate worldObject node
#define WO_BATCH_MIN_CHILDREN 2
#define WO_BATCH_PRF_CHILDREN 29
#define WO_BATCH_MAX_CHILDREN 32
#define WO_BUFFER_NULL 0xFFFF

struct WObjectVert {
    V3 pos;
    V3 normal;
    V3 sourceNormal;
    //V2 coordA, coordB;
    V3 sourcePos; float meta;
    //V2T<Uint16> coordA, coordB;
    struct {
        Uint16 matA, matB;
        Uint8 weightA, weightB;
        Uint8 smoothness;
        Uint8 ltID;//transformID;
    };
    V4T<Uint8> transformIDs;
    V4T<Uint8> transformWeights;
    
    WObjectVert() {};
    WObjectVert(const V3 &p, const V3 &n) : pos(p), normal(n) {  };
    
    inline void mapAttributes(MeshContainer *const mesh) {
        Uint8 offset = 0;
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
        
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
        mesh->addAttribute(offset, 1, MeshAttribute::Float, sizeof(float));
        //mesh->addAttribute(offset, 4, MeshAttribute::Float, sizeof(V2)/2);
        //mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof(Uint16), false);
        
        mesh->addAttribute(offset, 2, MeshAttribute::Unsigned, sizeof(Uint16));
        mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof(Uint8), true);
        //mesh->addAttribute(offset, 1, MeshAttribute::Unsigned, sizeof(Uint8), true);
        
        //Transforms
        mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof(Uint8), false);
        mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof(Uint8), true);
    }
    
    inline void retransform(const Mat4& delta) {
        pos = V3(delta * V4(pos, 1.f));
        normal = glm::normalize(V3(delta * V4(pos, 0.f)));
    }
    
    inline WObjectVert lerpTo(const WObjectVert& o, float t) const {
        
    }
};
template class Mesh<WObjectVert>;
template class MeshBuffer<WObjectVert>;
typedef MeshBuffer<WObjectVert> WMMeshType;

typedef unsigned int WMIndType;
struct WMBufferRegionTransform {
    //Mat4 tmat;
    DualQuat dual;
    V4 meta;
    V4 tint;
    
    inline bool operator!=(const WMBufferRegionTransform& o) {
        return //tmat != o.tmat || 
                dual != o.dual || 
                tint != o.tint;
    };
};

class ContourObject;
class WorldGenerator::Meta;

class WorldObject {
    friend class Soul::Soul;
public:
    enum {
        Intrinsic           = BIT00,
        Container           = BIT01,
        Subtractive         = BIT04,
        GroupRoot           = BIT05,
        Deferred            = BIT06,
        Abstracted          = BIT07,
        Prospective         = BIT08,
        
        Prefab              = BIT12,
        DrawDbg             = BIT13,
        UpdateWaiting       = BIT14,
        IsDirty             = BIT15
    };
    
    enum Changed {
        ChangedTransform       = BIT01,
        ChangedChild           = BIT02,
        ChangedContour         = BIT03,
        ChangedCheck           = BIT04,
        ChangedAll             = BIT01 | BIT02 | BIT03
    };
    struct VertCompoundInfo {
        float subdivLength;
    };
    
    
    WorldObject();
    virtual ~WorldObject();
    
    virtual ContourObject *contour();
    virtual const ContourObject *constContour() const;
    
    //Parent-relative transform
    class Transform transform;
    class Physical physical;
    Flags<Uint32> flags;
    unsigned short treeDepth;
    
    WOID id;
    void setID(const WOID& newID);
    WOID getID();
    void setDeferred(bool val);
    void applyDeferred(WorldObjectPtr o);
    
    inline bool hasUpdateWaiting() const { return flags.get(UpdateWaiting); };
    inline void setUpdateWaiting() { return flags.set(UpdateWaiting); };
    inline void clearUpdateWaiting() { return flags.clr(UpdateWaiting); };
    
    //Path and other leading distortions
    IdealContour path;
    //Applied perturbations to subspace
    std::list<Perturbation> perturbations;
    Subfield subfield;
    
    //The very lowest values capable of notably deforming or altering the object
    //If a force less than this is applied, it is attenuated and passed on without regard
    DeltaVector descendThreshold;
    //Deferred forces to be applied as resources or priorities permit
    std::queue<DeltaVector> deferredForces;
    
    Soul::Soul* soul;
    WorldObjectMeta *meta;
    WorldGenerator::Meta *worldMeta;
    
    Uint64 getSoulID() const;
    void useSoulID(Uint64) const;
    
    //WMBootCoord relWMCoord;//If object has no parent, position is relative to root of this coordinate- about 74 total bits of precision per axis
    
    void setWOBufferID(unsigned int woID);
    
    //Recursivly construct own mesh and children's, uploads, and draws accordingly
    virtual void construct(float epsilon, WMMeshType *mbuff, WMMeshType *const dbg = NULL);
    virtual void constructBoundsMesh(float epsilon, WMMeshType *const dbg = NULL);
    
    virtual void upload();
    virtual void drawBounds();
    virtual void drawDebug();
    virtual void draw();
    virtual void refresh();
    
    virtual unsigned short getContourCount();
    virtual IdealContour* getContour(unsigned short i);
    
    //This one's REALLY important: it does the stuff, and, uh, the things.... yeah. xD
    virtual void update();
    void requestUpdate();
    bool needsUpdate();
    void process();
    
    virtual bool isContour() const;
    
    //Contains: With current bounds, could this object possibly contain the request area (in entirety)
    bool boundContains(V3 relPoint);
    bool boundContains(WorldObject *const obj);
    inline bool boundContained(WorldObject *const obj);
    //Does this object's bounds intersect with this point or object (ie, touching))
    bool boundIntersects(V3 relPoint);
    bool boundIntersects(WorldObject *const obj);
    IntersectionHull getBounds(Mat4* mat = NULL);
    virtual IntersectionHull getContourBounds();
    
    //Tree functions
    void organize();
    void optimize();
    void split();//Splits into some number of children
    void reinsert();//Removes free or less-connected children and reinserts them
    void overflow();//Either reinserts or splits
    void merge(WorldObjectPtr& o);//Merges together with other node
    void remove();//Removes this node and resinserts EVERYTHING
    
    WorldObjectLink& getLink(WorldObjectPtr &o);
    WorldObjectLink& getParentLink();
    
    float getEmplaceAffinity(WorldObjectPtr& o) const;
    float getLinkWeight(WorldObjectPtr& o) const;
    float getPosWeight(WorldObjectPtr& o) const;
    
    //Adds specified world object to self, or delegates to parent- it'll get where it needs to go... somehow.
    void emplace(WorldObjectPtr& obj);
    void constructBounds();//Constructs cumulative bounding sphere and box, depending on flags
    inline float getBoundVolume();
    
    virtual void constructPhysical();
    //Calculate descend force
    virtual void constructThresholds();
    //Calculate contour abstractions
    virtual void constructAbstraction();
    
    void addChild(WorldObjectPtr &c);
    void setParent(WorldObjectPtr &p);
    void removeChild(WorldObjectPtr &c);
    void transferChild(WorldObjectPtr& c, const WorldObjectPtr &o);
    bool hasChild(WorldObjectPtr &c);
    
    WorldObjectPtr makeContainerChild();
    
    //Local-space workplane resolution
    ContourPlane resolveWorkplane(float z) const;
    
    //Final resolution from relative to worldspace
    inline V3 resolvePoint(const V3& p) const;
    inline V3 resolveNormal(const V3& n) const;
    
    //Shifts to relative space
    V3 perturbPoint(const V3& p) const;
    V3 perturbNormal(const V3& n, const V3& from, const V3& fromPerturbed) const;
    
    Transform getParentTransform() const;
    Mat4 getParentMatrix();
    Mat4 getGlobalMatrix();
    
    VertCompoundInfo assignTextureCoords(WObjectVert *const v, const float& epsilon);
    
    WMBootCoord getWidePos(Uint16 offset);
    WMBufferRegionTransform getRegionTransform();
    
    V3 getRenderPosition();
    
    unsigned int woBufferID;
    
    inline void setDirty() { flags.set(IsDirty); };
    inline void setClean() { flags.clr(IsDirty); };
    inline bool isDirty() { return flags.get(IsDirty); };
//protected:
    bool doPeriodicChecks();
    
#ifdef DEBUGMESH
    Mesh<WObjectVert> *debugMesh;
#endif
    WMMeshType *baseMesh;
    Mat4 resolveMatrix;
    float prevEpsilon;
    
    WorldObjectPtr parent;//Fully contains and links this object in the world
    std::list<WorldObjectPtr> children;//Entirely lower-order than object
    
    std::list<WorldObjectLink> links;//Physically interacts with object
    
    IntersectionHull bounds;
    
    mutable Mutex mtx;
    
    inline void llLock() { mtx.lock(); };
    inline void llUnlock() { mtx.unlock(); };

public:
    template<class Archive>
    void save(Archive & archive) const {
        archive(
                CEREAL_NVP(id),
                CEREAL_NVP(transform),
                CEREAL_NVP(physical),
                CEREAL_NVP(flags.value),
                CEREAL_NVP(treeDepth),
                CEREAL_NVP(children),
                CEREAL_NVP(links),
                CEREAL_NVP(bounds),
                
                CEREAL_NVP(path),
                CEREAL_NVP(perturbations),
                CEREAL_NVP(subfield),
                
                CEREAL_NVP_ID("descendThreshold", WorldObject::descendThreshold),
                CEREAL_NVP(deferredForces)
                );
        
        if(meta != nullptr) archive(cereal::make_nvp("meta", *meta));
        else archive(cereal::make_nvp("meta", WorldObjectMeta()));
        
        if(soul != nullptr) archive(cereal::make_nvp("soul", this->getSoulID()));
        else archive(cereal::make_nvp("soulID", 0));
        
        if(worldMeta != nullptr) {
            archive(cereal::make_nvp("hasWorldMeta", true));
            archive(cereal::make_nvp("worldMeta", *worldMeta));
        } else archive(cereal::make_nvp("hasWorldMeta", false));
        
        if(this->constContour() != nullptr) {
            archive(CEREAL_NVP_ID("hasMF", true));
            archive(CEREAL_NVP_ID("mf", *this->constContour()));
        } else {
            archive(CEREAL_NVP_ID("hasMF", false));
        }
    }

    template<class Archive>
    void load(Archive & archive) {
        archive(
                CEREAL_NVP(id),
                CEREAL_NVP(transform),
                CEREAL_NVP(physical),
                CEREAL_NVP(flags.value),
                CEREAL_NVP(treeDepth),
                CEREAL_NVP(children),
                CEREAL_NVP(links),
                CEREAL_NVP(bounds),
                
                CEREAL_NVP(path),
                CEREAL_NVP(perturbations),
                CEREAL_NVP(subfield),
                
                CEREAL_NVP_ID("descendThreshold", WorldObject::descendThreshold),
                CEREAL_NVP(deferredForces)
                );
        
        meta = new WorldObjectMeta();
        archive(cereal::make_nvp("meta", *meta));
        if(!(*meta)) {
            delete meta;
            meta = nullptr;
        }
        
        Uint64 soulID;
        archive(cereal::make_nvp("soul", soulID));
        if(soulID != 0) {
            std::cerr << "NO SOUL ID HANDLER\n";
        }
        
        bool hasWorldMeta; archive(cereal::make_nvp("hasWorldMeta", hasWorldMeta));
       if(hasWorldMeta) {
           if(worldMeta == nullptr) worldMeta = new WorldGenerator::Meta();
           archive(cereal::make_nvp("worldMeta", *worldMeta));
        }
        
        bool hasContour;
        archive(cereal::make_nvp("hasMF", hasContour));
        if(hasContour) archive(CEREAL_NVP_ID("mf", *this->contour()));
    }
};

inline bool WorldObject::boundContained(WorldObject *const obj) {
    return (obj != NULL) && obj->boundContains(this);
}

inline V3 WorldObject::resolvePoint(const V3& p) const {
    return V3(V4(p, 1.0) * resolveMatrix);
}
inline V3 WorldObject::resolveNormal(const V3& n) const {
    return V3(V4(n, 0.0) * resolveMatrix);
}
inline float WorldObject::getBoundVolume() {
    return bounds.volume();
}

typedef Flags<Uint8> WOF;

//CEREAL_REGISTER_TYPE(WorldObject);

#endif	/* WORLDOBJECT_H */

