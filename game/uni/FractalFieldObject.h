/* 
 * File:   FractalFieldObject.h
 * Author: Galen Swain
 *
 * Created on May 15, 2015, 9:42 AM
 * 
 * Description:
 * Represents a probalistic distribution of children based on fractal seed and world position
 * Collapsed children are flagged as such, and may be reconstituted on entropy reduction. 
 * TL;DR: Grass, branches, rocks in/on ground, things without absolute state that are important only as a whole
 */

#ifndef FRACTALFIELDOBJECT_H
#define	FRACTALFIELDOBJECT_H

class FractalFieldObject {
public:
    FractalFieldObject();
    FractalFieldObject(const FractalFieldObject& orig);
    virtual ~FractalFieldObject();
private:

};

#endif	/* FRACTALFIELDOBJECT_H */

