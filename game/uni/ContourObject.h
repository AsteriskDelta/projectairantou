/* 
 * File:   ContourObject.h
 * Author: Galen Swain
 *
 * Created on May 15, 2015, 9:41 AM
 * 
 * Extrudes in local space over distance the specified contour. When paired with object-level NURBS deformation, allows for arbitrary complex structures. 
 * TL;DR: Describes uniform, "manmade" structures with ease- walls, bricks, castles, etc. Also represents natural formations (trees, etc.) with the aid of fractal fields
 */

#ifndef CONTOUROBJECT_H
#define	CONTOUROBJECT_H
#include "WorldObject.h"

#include "rep/BakedContour.h"
#include "rep/BakedContourTrans.h"
#include "rep/IdealContour.h"
#include "rep/ContourCache.h"

struct ContourCoeffs {
    struct {
        float c[2] = {0.f, 0.f};
        union {
        struct {
            bool ls : 1;
            Uint16 l : 15;
            bool rs : 1;
            Uint16 r : 15;
        };
        Uint32 rawField = 0x0;
        };
        
        template<class Archive>
        void serialize(Archive& archive) {
          archive(CEREAL_NVP(c),
                  CEREAL_NVP(rawField)
                  );
        }
    } up, down;
    union {
    struct {
        bool ids : 1;
        Uint16 id : 15;
    };
    Uint16 rawID = 0x0;  
    };
    
    template<class Archive>
    void serialize(Archive& archive) {
      archive(CEREAL_NVP(up),
              CEREAL_NVP(down),
              CEREAL_NVP(rawID)
              );
    }
};
//CEREAL_REGISTER_TYPE(ContourCoeffs);

class ContourLevel {
public:
    IdealContour *ideal;
    BakedContour *baked;
    float weights[3];//Linear, quadratic, cubic
    float bakedEpsilon;
    ContourCoeffs coeffs;
    
    inline ContourLevel() : ideal(NULL), baked(NULL) , weights{0.f, 0.f, 0.f}, bakedEpsilon(999999.f) {};
    ~ContourLevel() {
        //if(baked != NULL) delete baked;
        if(ideal != NULL && !ideal->prefab) delete ideal;
    }
    
    inline BakedContour* getBaked(float epsilon) {
        if(baked == NULL || (fabs(epsilon - bakedEpsilon)) > WM_EPSILON_UPDATE_THRESHOLD) {
            if(baked != NULL) delete baked;
            baked = new BakedContour();
            if(ideal == NULL) {
                Console::Error("Null ideal on contour to be baked!");
                
            } else {
                *baked = ideal->bake(epsilon);
                //baked->simplify(epsilon);
            }
            bakedEpsilon = epsilon;
        }
        return baked;
    }
    
    inline void freeBaked() {
        if(baked != NULL) {
            delete baked;
            baked = NULL;
        }
    }
    
    template<class Archive>
    void save(Archive & archive) const {
        archive(CEREAL_NVP_ID("ideal", *ideal),
                CEREAL_NVP(weights),
                CEREAL_NVP(coeffs)); 
    }

    template<class Archive>
    void load(Archive & archive) {
        archive(CEREAL_NVP_ID("ideal", *ideal),
                CEREAL_NVP(weights),
                CEREAL_NVP(coeffs)); 
        bakedEpsilon = -1000.f;
    }
};
//CEREAL_REGISTER_TYPE(ContourLevel);

class ContourObject : public WorldObject {
public:
    ContourObject();
    virtual ~ContourObject();
    
    virtual ContourObject *contour() override;
    virtual const ContourObject *constContour() const override;
    
    inline operator WorldObject&() { return *this; };
    
    virtual bool isContour() const;
    
    virtual void construct(float epsilon, WMMeshType* baseMesh, WMMeshType *const dbg);
    
    virtual void draw();
    virtual void drawDebug();
    
    virtual unsigned short getContourCount();
    virtual IdealContour* getContour(unsigned short i);
    
    void addContour(IdealContour *const c);
    const BakedContour getContour(float d, float epsilon, float* featureTime = NULL);
    
    virtual IntersectionHull getContourBounds();
    
    float extrusionLength;//Local space
    
    void constructPhysical();
    void constructThresholds();
    ContourCache contourCache;
    
    //virtual float integrateSection(IdealContour *f, IdealContour *t, unsigned short physID);
    //virtual float integratePhys(unsigned short physID);
protected:
//#ifdef DEBUGMESH
//    Mesh<WObjectVert> *debugMesh;
//#endif
    //Responsible for deallocating all non-prefab contours
    void constructSlices();
    std::vector<ContourLevel> contours;
    std::vector<IdealSlice> slices;
    
    std::vector<const BakedContourVert*> getContourTris(BakedContour& contour);
    void meshContourTris(const std::vector<const BakedContourVert*> &verts, bool inv = false);
    //Must ALWAYS be called incrementally!
    float getExtrusionIncrement(float curExt, float epsilon);
    float knotExtrusionIncrement;
    
    Uint16 getNextContourID(float epsilon, Uint16 curID = 0);
    
public:
    /*
    template<class Archive>
    void serialize(Archive& archive) {
      archive(CEREAL_NVP(extrusionLength),
              CEREAL_NVP(contours)
              );
    }*/
    template<class Archive>
    void save(Archive & archive) const {
        archive(CEREAL_NVP(extrusionLength),
                CEREAL_NVP(contours)); 
    }

    template<class Archive>
    void load(Archive & archive) {
        archive(CEREAL_NVP(extrusionLength),
                CEREAL_NVP(contours)); 
    }
};

CEREAL_REGISTER_TYPE(ContourObject);

#endif	/* CONTOUROBJECT_H */

