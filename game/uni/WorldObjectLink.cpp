/* 
 * File:   WorldObjectLink.cpp
 * Author: Galen Swain
 * 
 * Created on June 20, 2015, 11:06 AM
 */

#include "WorldObjectLink.h"

WorldObjectLink WorldObjectLink::null = WorldObjectLink();

WorldObjectLink::WorldObjectLink() : ends({WorldObjectPtr(), WorldObjectPtr()}) , flags(0x0) {
  vec = DeltaVector();
}

WorldObjectLink::WorldObjectLink(WorldObject *const a, WorldObject *const b, const DeltaVector& v) {
  ends[0] = a; ends[1] = b;
  flags = 0x0;
  vec = v;
}

WorldObjectLink::WorldObjectLink(const WorldObjectLink& orig) {
  ends[0] = orig.ends[0]; ends[1] = orig.ends[1];
  vec = orig.vec;
  flags = orig.flags;
}

WorldObjectLink::~WorldObjectLink() {
}

