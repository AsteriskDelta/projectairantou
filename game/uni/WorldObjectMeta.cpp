/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldObjectMeta.cpp
 * Author: Galen Swain
 * 
 * Created on June 7, 2016, 9:52 AM
 */

#include "WorldObjectMeta.h"

WorldObjectMeta::WorldObjectMeta() : name(), className(), description() {
}

WorldObjectMeta::WorldObjectMeta(const WorldObjectMeta& orig) : name(orig.name), className(orig.className), description(orig.description) {
}

WorldObjectMeta::~WorldObjectMeta() {
}

