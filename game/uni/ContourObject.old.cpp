/*
void ContourObject::construct(float epsilon, WMMeshType* meshBuff, WMMeshType *const dbg) {
  debugMesh = dbg;
  //This is IMPORTANT, don't delete it!!!
  WorldObject::construct(epsilon, baseMesh);
  this->baseMesh = meshBuff;
  
  float extrusionCurrent = 0.f;
  const float epsilonSkipThresh = pow(epsilon, 1.f);// * (3.f/4.f);
  const bool doCache = flags.get(WorldObject::Subtractive);
  
  #ifdef DEBUGMESH
  //debugMesh = new Mesh<WObjectVert>(MeshType::Lines, 60000, 65000);
  #endif
  //baseMesh = new Mesh<WObjectVert>(MeshType::Tris, 60000, 65000);
  
  std::vector<const BakedContourVert*> contourTris;
  extrusionLength = path.getMaxExtrusion();//WARNING: sets to path's extrusion, NOTE THIS
  
  //For the length to extrusion, recalculate contour at every point where the derivative sum would (likely) exceed epsilon
  BakedContour minContour, prevContour; float minExtrusion = 0.f, prevExtrusion = 0.f;
  while(extrusionCurrent <= extrusionLength && extrusionCurrent <= path.getMaxExtrusion()) {//Must execute AT LEAST twice
    float featureParam = 0.f; BakedContour curContour = this->getContour(extrusionCurrent, epsilon, &featureParam);//Construct contour for current data
    prevContour.clearFlags();
    //Flags for acting upon current contour
    const bool endingContour = extrusionCurrent == 0.f || extrusionCurrent == extrusionLength || extrusionCurrent == path.getMaxExtrusion();
    const bool skipContour = bool(prevContour) && !endingContour && (path.deltaMag(extrusionCurrent, epsilon) < epsilonSkipThresh) && (curContour.calculateDifference(prevContour) < epsilonSkipThresh);
    const bool drawContourTris = (!prevContour || extrusionCurrent == extrusionLength || extrusionCurrent == path.getMaxExtrusion()) && !skipContour;
    
    //If required, triangulate and add face for contour - MUST be called prior to point perturbation! (ie, relative to workplane- z values aren't considered!)
    if(drawContourTris&&false) {
      contourTris = this->getContourTris(curContour);//If required, add starting plane faces
    }
    curContour.splitFilter(epsilon);
    if(debugMesh != NULL) Uint32 firstVertID = debugMesh->getLeadingID();

    if(skipContour) goto contourSkipped;
    
    //First iteration- create verts, assign IDs and parents
    for(unsigned int v = 0; v < curContour.verts.size(); v++) {
      BakedContourVert &vert = curContour.verts.at(v); WObjectVert meshVert;
      
      V3 relPos = V3(vert.pos.x, vert.pos.y, vert.pos.z + extrusionCurrent);
      V3 relNorm = vert.normal;
      
      const V3 pertPos = this->perturbPoint(relPos);
      meshVert.pos = pertPos;
      meshVert.normal = this->perturbNormal(relNorm, relPos, meshVert.pos);
      this->assignTextureCoords(&meshVert);
      vert.pertPos = pertPos;
      
      Uint32 curVertID = baseMesh == NULL? 0 : baseMesh->addVert(meshVert);
      vert.id = curVertID;
      if(debugMesh != NULL) debugMesh->addVert(meshVert);
      
      if(endingContour) {//Thus, on end contours, "upward" data is interleaved at v+1, ensure calculation of perturbation is possible, than offset to hack-calc normal
        const float ecDeltaZ = (extrusionCurrent == 0.f)? 0.12f : -0.12f;
        relPos.z += ecDeltaZ;
        meshVert.normal = this->perturbNormal(V3_FORWARD*(-ecDeltaZ * 5.f/6.f), relPos, this->perturbPoint(relPos));
        if(baseMesh != NULL) baseMesh->addVert(meshVert);
        if(debugMesh != NULL) debugMesh->addVert(meshVert);//Keep the meshes in sync
      }
      
      if(bool(prevContour)) {
        BakedContourVert &parent = prevContour.getNearestXYNCNorm(vert.pos, vert.normal);
        vert.parent = prevContour.getID(&parent);
        vert.parentDist = glm::distance(vert.pos, parent.pos);
        parent.linked = true;
      } else vert.parent = CVNULL;
    }
    
    //Debug draw the contour
    if(curContour.verts.size() > 1) {
    for(unsigned int v = 0; v < curContour.verts.size(); v++) {
      unsigned int vn = (v+1)%curContour.verts.size();
      BakedContourVert *vert = &curContour.verts.at(v), *next = &curContour.verts.at(vn);
      //debugMesh->addLineInd(vert->id, next->id);
    }
    }
    
    //Ensure all prevContours have a marked child-if not, assign them one
    for(auto it = prevContour.verts.begin(); it != prevContour.verts.end(); ++it) {
      if(!it->linked) {
        BakedContourVert *child = &curContour.getNearestXYNCNorm(it->pos, it->normal);
        float dist = glm::distance(it->pos, child->pos);
        if(dist > child->parentDist) {
          //!std::cout << "updating child vert parent to " << prevContour.getID(&(*it)) << "\n";
          child->parent = prevContour.getID(&(*it));
          child->parentDist = dist;
        }
        it->linked = true;
      }
    }
    
    //Second iteration- generate quads and debug lines if required
    if(bool(prevContour)) {
      for(unsigned int v = 0; v < curContour.verts.size(); v++) {
        BakedContourVert *vert = &curContour.verts.at(v); vert->linked = false; if(vert->siblingID[1] == CVNULL) continue;
        BakedContourVert *rSib = &curContour.verts.at(vert->siblingID[1]);
        BakedContourVert *parent = &prevContour.verts.at(vert->parent), *rSibParent = &prevContour.verts.at(rSib->parent);
        BakedContourVert *min = &minContour.getNearestXYNCPNorm(parent->pertPos, parent->normal), *rMin = &minContour.getNearestXYNCPNorm(rSibParent->pertPos, rSibParent->normal);
        
        if(baseMesh != NULL) {
          WObjectVert &meshVert = baseMesh->vert(vert->id);

          //Correction for normal based on parent and child skew
          V3 normRotCenter = glm::normalize(vert->pertPos - rSib->pertPos);//Relative up/down of extrusion
          //V3 normRotUp = glm::normalize(glm::cross(normRotCenter, meshVert.normal));
          V3 normDown = glm::normalize(vert->pertPos - min->pertPos);
          
          V3 normFV = glm::normalize(glm::cross(normRotCenter, normDown));
          meshVert.normal = normFV * glm::dot(normFV, meshVert.normal);
          
          /loat normAngle = glm::orientedAngle(meshVert.normal, normDown, normRotCenter);
          if(normAngle > float(M_PI/2.f)) {
            if(normAngle < float(M_PI*0.75f)) normAngle = normAngle - M_PI/2.f;
            //meshVert.normal = glm::rotate(meshVert.normal, -normAngle, normRotCenter);

          } else if(normAngle > 0.01f) {
            //if(normAngle > float(M_PI*0.25f)) normAngle = M_PI/2.f - normAngle;//This statement is correct
            //else normAngle = normAngle + M_PI/4.f;
            //if(normAngle < float(M_PI/4.f)) normAngle += M_PI/2.f;
            //else normAngle = M_PI*0.25f + normAngle;
            //if(normAngle < float(M_PI*0.25f)) normAngle = 0.f;//normAngle-float(M_PI/2.f);
            //else normAngle = normAngle-float(M_PI/2.f);
            //meshVert.normal = glm::rotate(meshVert.normal, -normAngle, normRotCenter);
            //meshVert.normal = glm::cross(normRotUp, normRotCenter);
          }
          //meshVert.normal = glm::cross(normDown, normRotCenter);//glm::cross(normRotUp, normRotCenter);
          // * 
        }
        //if(debugMesh != NULL) debugMesh->addLineInd(vert->id, rSib->id);
        //  debugMesh->addLineInd(vert->id, min->id);
        
        V3 extVector = glm::normalize(vert->pertPos - parent->pertPos);
        V3 fullVector = glm::normalize(parent->pertPos - min->pertPos);
        float extWeight = glm::distance(vert->pertPos, parent->pertPos);
        float extDelta = glm::length(glm::normalize(extVector - fullVector) * extWeight);
        if(extDelta < epsilon && false) {//Allow batching with parent quad, defer construction
          
        } else {//Commit quads with min->prev and continue
          
          Uint8 sibDir = 255; BakedContourVert *start = min, *end = NULL;
          Uint16 targID = minContour.getID(rMin);
          
          if(min == rMin) end = start;//Set to anything, the loop is skipped anyway
          else if(min->siblingID[0] == CVNULL) sibDir = 1;
          else if(min->siblingID[1] == CVNULL) sibDir = 0;
          else {
            BakedContourVert *lSeek = min, *rSeek = min;
            //!std::cout << "searching for " << targID<<"...\n";
            for(int off = 0; off < 8; off++) {
              if(lSeek->siblingID[0] == CVNULL || !lSeek->lConnect) { sibDir = 1; break; }
              else if(rSeek->siblingID[1] == CVNULL || !rSeek->rConnect) { sibDir = 0; break; }
              
              //!std::cout << "traversing from " << minContour.getID(lSeek) << "->" << lSeek->siblingID[0] << " and " << minContour.getID(rSeek) << "->" << rSeek->siblingID[1] << "\n";
              lSeek = &minContour[lSeek->siblingID[0]];
              rSeek = &minContour[rSeek->siblingID[1]];
              if(minContour.getID(lSeek) == targID) { sibDir = 0; break; }
              else if(minContour.getID(rSeek) == targID) { sibDir = 1; break; }
            }
            if(sibDir == 255) {
              std::cout << "can't find traverse direction!\n";
              continue;
            }
          }
          
          if(start != end) {
            while(start->siblingID[sibDir] != CVNULL && (sibDir == 1? start->rConnect : start->lConnect)) {
              //if(end != rMin) std::cout << "from " << minContour.getID(start) << " to " << start->siblingID[sibDir] << "\n";
              end = &minContour.verts.at(start->siblingID[sibDir]);
              
              BakedContourVert *linkVert = rSib;//(glm::distance2(vert->pos, end->pos) < glm::distance2(rSib->pos, end->pos))? vert : rSib;
              //if(debugMesh != NULL)debugMesh->addLineInd(linkVert->id, end->id);

              //Draw quad from current vert and rSib to both parents, counter-clockwise
              if((sibDir == 1? start->rConnect : start->lConnect) && baseMesh != NULL) addOrientedTris(baseMesh, linkVert, start, end);
              if(end != rMin) ;//addOrientedTris(baseMesh, vert, start, end);
              else break;

              start = end;
            }
          }
          //if(debugMesh != NULL)debugMesh->addLineInd(vert->id, min->id);
          if(baseMesh != NULL) addOrientedTris(baseMesh, vert, rSib, min);
        }
      }
    }
    
    prevContour.clearFlags(); curContour.clearFlags();
    if(baseMesh != NULL && drawContourTris) this->meshContourTris(contourTris, !prevContour);
    if(doCache && prevContour.size() > 0) contourCache.add(prevExtrusion, prevContour);
    prevContour = curContour;
    prevExtrusion = extrusionCurrent;
    if(!bool(minContour)) {
      minContour = prevContour;
      minExtrusion = prevExtrusion;
    }
    
    contourSkipped:
    float extrusionDelta = this->getExtrusionIncrement(extrusionCurrent, epsilon);
    
    if(extrusionCurrent >= extrusionLength || extrusionDelta == 0.f) break;
    else extrusionCurrent = std::min(extrusionLength, extrusionCurrent + extrusionDelta);
  }
  if(doCache) contourCache.add(prevExtrusion, prevContour);
  
  #ifdef DEBUGMESH
  //debugMesh->upload();
  #endif
  if(baseMesh != NULL) std::cout << "Contour Object:\tV: " << baseMesh->getVertCount() << ", T: " << (baseMesh->getIndCount()/3) << " I: " << baseMesh->getIndCount() << "\n";
  //baseMesh->upload();
  
  return;
}
*/
/*void ContourObject::construct(float epsilon) {//TODO: alter sibling assignment, check if we should even bother connecting
  WorldObject::construct(epsilon);
  float extrusionCurrent = 0.f;
  const float epsilonSkipThresh = pow(epsilon, 1.f);// * (3.f/4.f);
  const bool doCache = flags.get(WorldObject::Subtractive);
  
  #ifdef DEBUGMESH
  debugMesh = new Mesh<WObjectVert>(MeshType::Lines, 20000, 25000);
  #endif
  baseMesh = new Mesh<WObjectVert>(MeshType::Tris, 20000, 25000);
  
  std::vector<const BakedContourVert*> contourTris;
  extrusionLength = path.getMaxExtrusion();//WARNING: sets to path's extrusion, NOTE THIS
  //For the length to extrusion, recalculate contour at every point where the derivative sum would (likely) exceed epsilon
  BakedContour prevContour; float prevExtrusion = 0.f;
  while(extrusionCurrent <= extrusionLength && extrusionCurrent <= path.getMaxExtrusion()) {//Must execute AT LEAST twice
    float featureParam = 0.f; BakedContour curContour = this->getContour(extrusionCurrent, epsilon, &featureParam);//Construct contour for current data
    
    //Flags for acting upon current contour
    const bool endingContour = extrusionCurrent == 0.f || extrusionCurrent == extrusionLength || extrusionCurrent == path.getMaxExtrusion();
    const bool skipContour = bool(prevContour) && !endingContour && (path.deltaMag(extrusionCurrent, epsilon) < epsilonSkipThresh) && (curContour.calculateDifference(prevContour) < epsilonSkipThresh);
    const bool drawContourTris = false && (!prevContour || extrusionCurrent == extrusionLength || extrusionCurrent == path.getMaxExtrusion()) && !skipContour;
    
    //If required, triangulate and add face for contour - MUST be called prior to point perturbation! (ie, relative to workplane- z values aren't considered!)
    if(drawContourTris) {
      contourTris = this->getContourTris(curContour);//If required, add starting plane faces
    }
    std::vector<BakedContourVert> *const cVerts = &(curContour.verts);
    Uint32 firstVertID = debugMesh->getLeadingID();
    
    //std::cout << "executing at " << extrusionCurrent << " with " << cVerts->size() << " verts\n";
    if(skipContour) goto contourSkipped;
    
    for(unsigned int v = 0; v < cVerts->size(); v++) {
      BakedContourVert &vert = cVerts->at(v); WObjectVert meshVert;
      
      V3 relPos = V3(vert.pos.x, vert.pos.y, vert.pos.z + extrusionCurrent);
      V3 relNorm = vert.normal;
      
      const V3 pertPos = this->perturbPoint(relPos);
      meshVert.pos = pertPos;
      meshVert.normal = this->perturbNormal(relNorm, relPos, meshVert.pos);
      vert.pertPos = pertPos;
      
      Uint32 curVertID = baseMesh->addVert(meshVert);
      vert.id = curVertID;
#ifdef DEBUGMESH
      debugMesh->addVert(meshVert);
#endif
      if(endingContour) {//Thus, on end contours, "upward" data is interleaved at v+1, ensure calculation of perturbation is possible, than offset to hack-calc normal
        const float ecDeltaZ = (extrusionCurrent == 0.f)? 0.12f : -0.12f;
        relPos.z += ecDeltaZ;
        meshVert.normal = this->perturbNormal(V3_FORWARD*(-ecDeltaZ * 5.f/6.f), relPos, this->perturbPoint(relPos));
        baseMesh->addVert(meshVert);
#ifdef DEBUGMESH
        debugMesh->addVert(meshVert);//Keep the meshes in sync
#endif
      }
      
      //Assigned at contour resolution now
      //vert.siblingID[0] = (v == 0)? cVerts->size()-2 : v-1;//(v == 0)? cVerts->size()-1 : v-1;
      //vert.siblingID[1] = (v == cVerts->size()-1)? 1 : v+1;//(v == cVerts->size()-1)? 0 : v+1;
    }
    
    for(unsigned int v = 0; v < cVerts->size(); v++) {
      BakedContourVert &vert = cVerts->at(v); if(vert.siblingID[1] == CVNULL || !vert.rConnect) continue;
      BakedContourVert &other =cVerts->at(vert.siblingID[1]);
      
       debugMesh->addLineInd(vert.id, other.id);
      //if(glm::distance(vert.pos, other.pos) > 3.f) {
      //  std::cout << "invalid contour line at " << v << "->"<<vert.siblingID[1]<<" of << " << glm::distance(vert.pos, other.pos) <<"\n";
      //}
    }
    
    if(prevContour && cVerts->size() > 0 && prevContour.verts.size() > 0) {//Add faces connecting previous contour and current contour
      //Update normal based on previous contour, later we need to update the one in prevContour
      for(unsigned int v = 0; v < cVerts->size(); v++) {
        BakedContourVert &vert = cVerts->at(v), &vertSibling = cVerts->at(vert.siblingID[1]), &prevVert = prevContour.getNearest(vert.pos);
        BakedContourVert &prevVertSibling = prevContour.verts.at(prevVert.siblingID[0]);
        WObjectVert &vt = baseMesh->vert(vert.id), &vts = baseMesh->vert(vertSibling.id), &vp = baseMesh->vert(prevVert.id), &vps = baseMesh->vert(prevVertSibling.id);
        
        const V3 lowerCross = glm::cross(glm::normalize(vt.pos - vts.pos), glm::normalize(vp.pos -  vt.pos));
        const V3 prevUpperCross = glm::cross(glm::normalize(vp.pos - vps.pos), glm::normalize(vt.pos - vp.pos));
        _unused(prevUpperCross);
        vt.normal = lowerCross;
        //if(prevExtrusion == 0.f||true)vp.normal = prevUpperCross;//prevUpperCross;
        //else vp.normal = (vp.normal + prevUpperCross) / 2.f;
      }
      
      BakedContour *smaller, *larger;
      if(prevContour.size() > curContour.size()) {//Black magic f*cked up naming
        smaller = &prevContour;
        larger = &curContour;
      } else {
        larger = &prevContour;
        smaller = &curContour;
      }
      
      larger->clearFlags();
      
      //Between the two sets, the closest verts are always connected... somehow
      //Rendering the last vert generates z-fighting artifacts? TODO: Do we need to switch this?
      const unsigned int vMax = smaller->size() == 0? 0 : smaller->size();
      for(unsigned int v = 0; v < vMax ; v++) {
        BakedContourVert &vert = smaller->verts.at(v);
        BakedContourVert &prevVert = larger->getNearest(vert.pos);
        prevVert.linked = true;
#ifdef DEBUGMESH
        debugMesh->addLineInd(vert.id, prevVert.id);
        //if(glm::distance(vert.pos, prevVert.pos) > 0.4f) {
        //  std::cout << "invalid prev line at " << v<<" of << " << glm::distance(vert.pos, prevVert.pos) <<"\n";
        // }
#endif
        vert.parent = (prevVert.siblingID[0] == larger->verts.size()-1)? 1 : prevVert.siblingID[0]+1;
        //Create face using verts and rightmost siblings
        BakedContourVert &vertA = smaller->verts.at(vert.siblingID[1]);
        BakedContourVert &vertB = larger->verts.at(prevVert.siblingID[1]);
        
        //Straight feature extrusions have duplicate verts at their extreme, but during interpolation we need to draw this nonetheless
   //(int(v+1) != smaller->size() || (featureParam != 0.f && featureParam != 1.f)) && 
        if(vert.rConnect) {//If we need the face, ensure it "faces" (lol) the right direction
          if(larger == &curContour) {
            baseMesh->addTriInd(vertA.id, vertB.id, prevVert.id);
            baseMesh->addTriInd(prevVert.id,  vert.id, vertA.id);
            //baseMesh->addQuadInd(vertA.id, vertB.id, prevVert.id,  vert.id);
          } else {
            baseMesh->addTriInd(vert.id, prevVert.id, vertB.id);
            baseMesh->addTriInd(vertB.id, vertA.id, vert.id);
            //baseMesh->addQuadInd(vert.id, prevVert.id, vertB.id, vertA.id);
          }
        }
      }
      
      //For all target verts, create faces for all those not selected as nearest, between their nearest and rightmost sibling
      for(unsigned int v = 0; v < larger->size(); v++) {
        const BakedContourVert &vert = larger->verts.at(v);
        if(!vert.linked && vert.rConnect) {
          //Ensure we choose the proper verts- if we loop around, it produces a crazy tri, so clamp it if that's the case.
          const BakedContourVert &prevVert = smaller->getNearest(vert.pos);
          const BakedContourVert &vertA = larger->verts.at(vert.siblingID[1] == 0? v : vert.siblingID[1]);
          const BakedContourVert &vertB = prevVert.siblingID[1] == 0? prevVert : smaller->verts.at(prevVert.siblingID[1]);
#ifdef DEBUGMESH
          debugMesh->addLineInd(vert.id, prevVert.id);
          //if(glm::distance(vert.pos, prevVert.pos) > 0.4f) {
          //std::cout << "invalid n/a nearest line at " << v<<" of << " << glm::distance(vert.pos, prevVert.pos) <<"\n";
          //}
#endif
          if(smaller == &curContour) {//These are both required to form all correct cases- TODO: simplify and case switch?
            baseMesh->addTriInd(prevVert.id,  vert.id, vertA.id);
            baseMesh->addTriInd(vertB.id,  vert.id, vertA.id);
          } else {
            baseMesh->addTriInd(vertB.id, vertA.id, vert.id);//Potential FIXME !!!! Not sure this order is CCW
            baseMesh->addTriInd(prevVert.id, vertA.id, vert.id);
          }
        }
      }
    }
    
    if(drawContourTris) this->meshContourTris(contourTris, !prevContour);
    if(doCache && prevContour.size() > 0) contourCache.add(prevExtrusion, prevContour);
    prevContour = curContour;
    prevExtrusion = extrusionCurrent;
    
    contourSkipped:
    float extrusionDelta = this->getExtrusionIncrement(extrusionCurrent, epsilon);
    
    if(extrusionCurrent >= extrusionLength || extrusionDelta == 0.f) break;
    else extrusionCurrent = std::min(extrusionLength, extrusionCurrent + extrusionDelta);
    //prevExtrusionDelta = extrusionDelta;
  }
  if(doCache) contourCache.add(prevExtrusion, prevContour);
  
  #ifdef DEBUGMESH
  debugMesh->upload();
  #endif
  std::cout << "Contour Object:\tV: " << baseMesh->getVertCount() << ", T: " << (baseMesh->getIndCount()/3) << " I: " << baseMesh->getIndCount() << "\n";
  baseMesh->upload();
  
  return;
}*/