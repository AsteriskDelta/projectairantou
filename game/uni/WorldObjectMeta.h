/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldObjectMeta.h
 * Author: Galen Swain
 *
 * Created on June 7, 2016, 9:52 AM
 */

#ifndef WORLDOBJECTMETA_H
#define WORLDOBJECTMETA_H
#include "game.h"

class WorldObjectMeta {
public:
    WorldObjectMeta();
    WorldObjectMeta(const WorldObjectMeta& orig);
    virtual ~WorldObjectMeta();
    
    std::string name, className;
    std::string description;
    
    inline operator bool() const {
        return name != "" || className != "" || description != "";
    }
    
    template<class Archive>
    void serialize(Archive& archive) {
      archive(CEREAL_NVP(name),
              CEREAL_NVP(className),
              CEREAL_NVP(description)
              );
    }
private:

};

CEREAL_REGISTER_TYPE(WorldObjectMeta);
#endif /* WORLDOBJECTMETA_H */

