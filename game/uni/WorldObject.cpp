/* 
 * File:   WorldObject.cpp
 * Author: Galen Swain
 * 
 * Created on May 10, 2015, 6:15 PM
 */

#include "WorldObject.h"
#include "WorldManager.h"
#include "field/WorldFields.h"
#include "soul/Soul.h"
#include <client/Transform.h>
#include <algorithm>

WorldObject::WorldObject() : parent(), children() {
  flags.set(DrawDbg, 1);
  woBufferID = WO_BUFFER_NULL;
  soul = NULL;
  worldMeta = nullptr;
  bounds = IntersectionHull();
}

WorldObject::~WorldObject() {
  
}

ContourObject* WorldObject::contour() {
  //if(std::type_index(*this) == std::type_index(ContourObject)) return dynamic_cast<ContourObject*>(this);
  return nullptr;
}
const ContourObject* WorldObject::constContour() const {
  return nullptr;
}

void WorldObject::setID(const WOID& newID) {
  id = newID;
}
WOID WorldObject::getID() {
  return id;
}
void WorldObject::setDeferred(bool val) {
  flags.set(Deferred, val);
}
void WorldObject::applyDeferred(WorldObjectPtr o) {
  if(!flags.get(Deferred)) return;
  //Apply all deferred commands to object ptr
  //TODO
}

bool WorldObject::doPeriodicChecks() {
  //std::cout << "gf="<< _globalFrame << "\n";
  return (_globalFrame % 180) == ((woBufferID)%180);
}

void WorldObject::construct(float epsilon, WMMeshType *mbuff, WMMeshType *const dbg) {  _prsguard(); MutexGuard mg(this->mtx);
  _unused(epsilon); _unused(mbuff); _unused(dbg);
  prevEpsilon = epsilon;
  for(WorldObjectPtr child : children) {
    WorldManager::WasUpdated(child, ChangedAll);
  }
  //std::cout << "CONSTRUCT " << this << std::dec <<"\n";
  this->constructBounds();
  return;
}

void WorldObject::constructBoundsMesh(float epsilon, WMMeshType *const dbg) { _prsguard(); MutexGuard mg(this->mtx);
  if(dbg == NULL) return;
  //std::cout << "BEGIN CONSTRUCTBOUNDSMESH\n";
  if(!bounds) this->constructBounds();
  unsigned int dbgVC = dbg->vertCount;
  
  bounds.constructMesh<WMMeshType, WObjectVert>(epsilon, dbg);
  for(unsigned int i = dbgVC; i < dbg->vertCount; i++) {
    WObjectVert *vert= &(dbg->bufferVert[i]);
    vert->sourcePos = V3(1.f, 0.f, 0.f);
  }
  physical.constructMesh(epsilon, dbg);
}

Uint64 WorldObject::getSoulID() const {
  
  return 0;
}
void WorldObject::useSoulID(Uint64) const {
  
}

void WorldObject::setWOBufferID(unsigned int woID) { MutexGuard mg(this->mtx);
  //std::cout << "got woid: " << woID << "\n";
  woBufferID = woID;
}
void WorldObject::upload() {
  return;
}
void WorldObject::drawBounds() {
  return;
}
void WorldObject::drawDebug() {
  return;
}
void WorldObject::draw() {
  return;
}

//----------------------
bool WorldObject::needsUpdate() {
  return true;
}

void WorldObject::requestUpdate() { _prsguard();// MutexGuard mg(this->mtx);
  if(this->hasUpdateWaiting()) return;
  MutexGuard mg(this->mtx);
  //Handle update frequency
  
  if(this->hasUpdateWaiting() || !this->needsUpdate()) return;
  WorldManager::DoUpdate(this);
  
  this->setUpdateWaiting();
}

void WorldObject::update() { _prsguard(); MutexGuard mg(this->mtx);
  this->clearUpdateWaiting();//Allow further updates for this object to be scheduled
  if(!this->needsUpdate()) return;
  
  //Calculate physics upon the given object
  float deltaTime = _deltaTime;
  int physChanges = physical.apply(this, deltaTime);
  if(physChanges == 1) WorldManager::WasUpdated(this, WOF(ChangedTransform));
  else if(physChanges == 2) this->refresh();
  
  //Update children
  //if(children.size() > 0) std::cout << "updating " << children.size() << " children on WorldObject " << this << "\n";
  for(WorldObjectPtr& child : children) {
    //std::cout <<"PTR="<< (WorldObject*)(child) << "\n";
    child->requestUpdate();
   // WorldManager::DoUpdate(child);
  }
  
  //Update mesh
  if(woBufferID != WO_BUFFER_NULL) {
    //this->transform.position += V3(sin(_globalTimef), cos(_globalTimef), 0.f)/20.f;
    //this->transform.position = V3(this->transform.position.x, sin(_globalTimef+this->transform.position.z + this->transform.position.x), this->transform.position.z);
    //WorldManager::WasUpdated(this, WOF(ChangedTransform));
    //std::cout << "Update on object with soul: " << soul << "\n";
    float newEpsilon;
    //if(doPeriodicChecks()) std::cout << "dp: " << doPeriodicChecks() << ", prev=" << prevEpsilon<<", new=" <<WorldManager::GetEpsilon(this) << ", res:" << (fabs((newEpsilon = WorldManager::GetEpsilon(this)) - prevEpsilon))<< "\n";
    if(doPeriodicChecks() && fabs((newEpsilon = WorldManager::GetEpsilon(this)) - prevEpsilon) > WM_EPSILON_UPDATE_THRESHOLD) {//if() {
      this->refresh();
      //std::cout << "refreshing mesh\n";
    }
  }
  
  if(soul != NULL) {
    soul->update(0.02f);
  }
}

void WorldObject::refresh() {
  WorldManager::WasUpdated(this, WOF(ChangedAll));
}
//------------------------


unsigned short WorldObject::getContourCount() {
  return 0;
}
IdealContour* WorldObject::getContour(unsigned short i) {
  _unused(i);
  return nullptr;
}

bool WorldObject::isContour() const {
  return false;
}

bool WorldObject::boundContains(V3 relPoint) {
  return getBounds().contains(relPoint);
}
bool WorldObject::boundContains( WorldObject *const obj){
  return getBounds().contains(obj->getBounds());
}
bool WorldObject::boundIntersects(V3 relPoint) {
  return getBounds().contains(relPoint);
}
bool WorldObject::boundIntersects(WorldObject *const obj) {
  return getBounds().intersectsOrContains3D(obj->getBounds());
}

IntersectionHull WorldObject::getBounds(Mat4* mat) {
  if(!bounds) constructBounds();
  
  if(mat != NULL) return bounds.transform(*mat);
  else return bounds;
}

IntersectionHull WorldObject::getContourBounds() {
  return IntersectionHull();
}

void WorldObject::split() { _prsguard(); MutexGuard mg(this->mtx);
  if(flags.get(Intrinsic)) return;
  //Create at least one child grouping such that affinity is maximized
  /*struct WOAffin {
    float linkWeight, posWeight;
    WorldObjectPtr a, b;
  };
  std::list<WOAffin> affins;
  //Calculate affinities between all children
  for(auto a = children.begin(); a != children.end(); a++) { 
    for(auto b = std::next(a); b != children.end(); b++) {
      affins.push_back(WOAffin{(*a)->getLinkWeight(*b), (*a)->getPosWeight(*b), *a, *b});
    }
  }
  auto cmpFunc = [=](const WOAffin &a, const WOAffin &b) {
    return a.linkWeight < b.linkWeight || (a.linkWeight == b.linkWeight && a.posWeight < b.posWeight);
  };
  affins.sort(cmpFunc);
  
  //Given affinities, choose groups that maximize affinity, 
  //struct WOMeta {
  //  unsigned short newCID;
  //};
  //std::vector<WOMeta> childMeta;
  std::vector<WorldObjectPtr> newChildren;
  for(auto it = affins.begin(); it != affins.end(); ++it) {
    WorldObjectPtr target; float targetWeight = 0.f, tmpWeight;
    //Try to emplace in existing containers
    if((tmpWeight = it->a->getEmplaceAffinity(it->b)) > targetWeight) {
      targetWeight = tmpWeight; target = it->a;
    }
    if((tmpWeight = it->b->getEmplaceAffinity(it->a)) > targetWeight) {
      targetWeight = tmpWeight; target = it->b;
    }
    
    //Try to emplace in new containers
    for(auto ct = newChildren.begin(); ct != newChildren.end(); ++ct) {
      if((*ct)->hasChild(it->a) && (tmpWeight = (*ct)->getEmplaceAffinity(it->b)) > targetWeight) {
        targetWeight = tmpWeight; target = *ct;
      }
      if((*ct)->hasChild(it->b) && (tmpWeight = (*ct)->getEmplaceAffinity(it->a)) > targetWeight) {
        targetWeight = tmpWeight; target = *ct;
      }
    }
    
    //Make a new container
    if(targetWeight <= 0.f) {
      newChildren.push_back(this->makeContainerChild());
      target = *newChildren.rbegin();
    }
    
    if(!target->hasChild(it->a)) {
      //this->removeChild(*ic);
      //target->addChild(it->a);
      this->transferChild(it->a, target);
    }
    if(!target->hasChild(it->b)) {
      //target->addChild(it->b);
      this->transferChild(it->b, target);
    }
  }
  */
  //Add new children, removing delegated ones
  /*for(auto it = newChildren.begin(); it != newChildrene.end(); ++it) {
    //for(auto ic = (*it)->children.begin(); ic != (*it)->children.end(); ++ic) {
    //  this->removeChild(*ic);
    //}
    this->addChild(*it);
  }*/
  
  //Rebuild abstracted linkages and bounds
  this->process();
}

void WorldObject::process() { _prsguard();
  this->constructBounds();
  this->constructPhysical();
  this->constructThresholds();
  this->constructAbstraction();
}
    
void WorldObject::organize() { _prsguard(); MutexGuard mg(this->mtx);//Split children into bound-minimum groupings
/*
  std::cout << "Organizing object with " << children.size() << " children...\n";
  if(children.size() <= 2) return;
  //Temporary really inefficient shit
  WorldObject *con = new WorldObject();
  con->flags.set(Container, true);
  float threshDist = 0.f;
  const V3 cp = (*children.begin())->transform.position; int offs = 0;
  for(auto it = std::next(children.begin()); it != children.end(); it++) {
    float dist = glm::distance((*it)->transform.position, cp);
    std::cout << "has distance: " << dist << "\n";
    if(dist > threshDist && offs > 1) {
      threshDist = dist; offs--;
    } else if(false&&dist < threshDist && offs < 1) {
      threshDist = dist;
      offs++;
    }
    
    if(dist > threshDist) offs++;
    else if(dist < threshDist) offs--;
  }
  std::cout << "threshold distance is: " << threshDist << "\n";
  children.erase(children.begin());
  auto it = children.begin(); 
  while(it != children.end()) {
    bool add = glm::distance(cp, (*it)->transform.position) < threshDist;
    if(add) {
      con->addChild(*it);
      it = children.erase(it);
    } else ++it;
  }
  std::cout << "Organize gave " << children.size() << " children\n";
  this->addChild(con);*/
  if(children.size() < WO_BATCH_PRF_CHILDREN) return;
  //Determine optimal clusters, forcing at most n/2 in each
  
}    
void WorldObject::optimize() {
  
}

WorldObjectLink& WorldObject::getLink(WorldObjectPtr &o) {
  for(auto it = links.begin(); it != links.end(); ++it) if(it->getOther(this) == o) return *it;
  return WorldObjectLink::null;
}

WorldObjectLink& WorldObject::getParentLink() {
  if(!parent) return WorldObjectLink::null;
  else return this->getLink(parent);
}

void WorldObject::emplace(WorldObjectPtr &obj) { _prsguard();
  //Place based upon links
  /*struct WOAffin {
    float linkWeight, posWeight;
    WorldObjectPtr a, b;
  };
  std::list<WOAffin> affins;
  //Calculate affinities between all children
  for(auto a = children.begin(); a != children.end(); a++) { 
    affins.push_back(WOAffin{(*a)->getLinkWeight(obj), (*a)->getPosWeight(obj), *a, WorldObjectPtr()});
  }
  auto cmpFunc = [=](const WOAffin &a, const WOAffin &b) {
    return a.linkWeight < b.linkWeight || (a.linkWeight == b.linkWeight && a.posWeight < b.posWeight);
  };
  affins.sort(cmpFunc);
  */
  //Emplace based upon partitioning
  
  //No significant links, emplace here
  this->addChild(obj);
  //this->organize();
  if(children.size() > WO_BATCH_PRF_CHILDREN) this->split();
}
void WorldObject::constructBounds() { _prsguard(); MutexGuard mg(this->mtx);
  //Compose own children
  this->bounds.clear();
  for(WorldObjectPtr child : children) {
    Mat4 localMatrix = child->getParentMatrix();
    this->bounds.encompass3D(child->getBounds(&localMatrix), &localMatrix);
  }
  
  //Add self (if applicable)
  IntersectionHull localHull = this->getContourBounds();
  if(localHull) this->bounds.encompass3D(localHull);
  
  //std::cout << "got bounds " << glm::to_string(this->bounds.del) << "\n";
}


void WorldObject::constructPhysical() { _prsguard();
  physical.clear();
  
  for(WorldObjectPtr child : children) {
    if(!child->getParentLink()) continue;
    Transform cTransform = child->getParentTransform();
    physical.incorporate(&(child->physical), cTransform);
  }
}

void WorldObject::constructThresholds() {  _prsguard();
  /*this->descendThreshold.clearInfinite();
  
  for(WorldObjectPtr child : children) {
  
  }
  this->descendThreshold = min(this->descendThreshold, )*/
}

void WorldObject::constructAbstraction() { _prsguard();
  
}

void WorldObject::addChild(WorldObjectPtr& c) { _prsguard(); MutexGuard mg(this->mtx);
  /*if(false&&children.size() > WO_BATCH_PRF_CHILDREN) {
    this->organize();
    this->emplace(c);//We may place it into a newly created container
    return;
  }*/
  
  children.push_front(c);
  WorldObjectPtr thisPtr = this;
  c->setParent(thisPtr);
/*
  const bool insertBack = c->flags.get(Subtractive);
  const float childVolume = c->getBoundVolume();
  if(insertBack || children.size() == 0) children.push_back(c);
  else {//Insert ordered by volume to optimize later queries
    bool inserted = false;
    for(auto it = children.begin(); it != children.end(); it++) {
      if((*it)->getBoundVolume() < childVolume) {
        children.insert(it, c);//insert before smaller child, so first are always larger
        inserted = true;
        break;
      }
    }
    if(!inserted) {
      children.push_back(c);
    }
  }
  c->setParent(this);
 * */
}
bool WorldObject::hasChild(WorldObjectPtr &c) {
  return std::find(children.begin(), children.end(), c) != children.end();
}

void WorldObject::setParent(WorldObjectPtr& p) { _prsguard(); MutexGuard mg(this->mtx);
  parent = p;
}
/*
void WorldObject::removeChild(WorldObjectPtr &c) { _prsguard(); MutexGuard mg(this->mtx);
  
}

void WorldObject::transferChild(WorldObjectPtr &c, WorldObjectPtr &to) { _prsguard(); MutexGuard mg(this->mtx);
  
}*/

ContourPlane WorldObject::resolveWorkplane(float z) const { _prsguard(); MutexGuard mg(this->mtx);
  ContourPlane ret;
  ret.origin = resolvePoint(V3(0.f, 0.f, z));
  ret.normal = glm::cross(glm::normalize(resolvePoint(V3(1.f, 0.f, z)) - ret.origin), glm::normalize(resolvePoint(V3(0.f, 1.f, z)) - ret.origin));
  //ret.normal = glm::normalize(resolvePoint(V3(0.f, 0.f, z+0.2f)) - ret.origin);
  return ret;
}

V3 WorldObject::perturbPoint(const V3& p) const {
  //Evaluate perturbations in local space
  V3 ret = p;
  for(auto it = perturbations.begin() ; it != perturbations.end(); ++it) {
    //it->evaluate(ret);
  }
  
  //Then transform along path
  path.evaluate(ret);
  
  return ret;
}
V3 WorldObject::perturbNormal(const V3& n, const V3& from, const V3& fromPerturbed) const {
  return glm::normalize(perturbPoint(n+from) - fromPerturbed);
}

Mat4 WorldObject::getParentMatrix() {
  return transform.inverseMatrix();
}

Mat4 WorldObject::getGlobalMatrix() {
  return this->getParentMatrix() * transform.matrix();
}

inline V2 getTexCoord(const V3& v) {
  const float fac = 1.f / 10.f;
  const float theta = std::atan2(v.y, v.x);
  float xc = fac*(theta), yc = fac*(v.z);
  return V2(xc, yc);
}
WorldObject::VertCompoundInfo WorldObject::assignTextureCoords(WObjectVert *const v, const float& epsilon) { _prsguard();
  VertCompoundInfo ret;
  ret.subdivLength = 9999.f;
  std::list<CompoundWeight> wg = subfield.getCompounds(v->pos, 2);//WorldFields::active()->getCompoundWeights(subfield.getVector(v->pos), 2);
  //std::cout << "subfield list size: " << wg.size() << "\n";
  v->matA = 1; v->matB = 0;
  v->weightA = 128;
  v->weightB = 0;//clamp((int)round(v->pos.y/80.f*255.f), 0, 255);
  //v->smoothness = 0;
  v->transformIDs = V4T<Uint8>(0);
  v->transformWeights = V4T<Uint8>(0);
  //v->coordA = v->coordB = getTexCoord(v->pos);
  v->sourcePos = v->pos;
  v->meta = 0.f;
  
  if(wg.size() > 0) {
    auto it = wg.begin();
    if(!it->compound->resourcesLoaded) WorldFields::active()->loadCompoundUsed(it->compound->idx);
    v->matB = v->matA = it->compound->renderInfo.getTex(0);
    
    v->weightA = round(clamp(it->str, 0.f, 1.f)*255.f);
    ret.subdivLength = it->compound->getSubdivLength(epsilon);
    //std::cout << "ma " << v->matA << " @ " << int(v->weightA) << ", subL=" << ret.subdivLength << "\n";
     wg.pop_front();
  }
  if(wg.size() > 0) {
    auto it = wg.begin();
    if(!it->compound->resourcesLoaded) WorldFields::active()->loadCompoundUsed(it->compound->idx);
    v->matB = it->compound->renderInfo.getTex(0);
    ret.subdivLength = (ret.subdivLength + it->compound->getSubdivLength(epsilon)) / 2.f;
    
    v->weightB = round(clamp(it->str, 0.f, 1.f)*255.f);
     wg.pop_front();
  }
  return ret;
}

/*V3 WorldObject::perturbNormal(const V3& n, const V3& from, const V3& fromPerturbed) const {
  const float epsilon = 0.1f;
  const V3 pLesser = perturbPoint(from - V3(0.f,0.f, epsilon)), pGreater = perturbPoint(from + V3(0.f,0.f, epsilon));
  const V3 pseudoNormal = glm::normalize(perturbPoint(n+from) - fromPerturbed);
  //const V3 deNormal = glm::normalize((fromPerturbed - pLesser) * 0.5f + (pGreater - fromPerturbed) * 0.5f);
  const V3 deNormal = glm::normalize(pGreater - pLesser);
  std::cout << glm::to_string(pGreater) << " - " << glm::to_string(pLesser) << "\n";
  
  const V3 normalPerp = glm::normalize(glm::cross(pseudoNormal, deNormal));
  const V3 normalAxis = glm::cross(normalPerp, deNormal);
  
  return deNormal;//V3(pseudoNormal.x * );
}*/
/*
WMBootCoord WorldObject::getWidePos(Uint16 offset) {
  if(parent != NULL) return parent->getWidePos(offset);
  else {
    return (offset == 0)? WMBootCoord(0,0,0) : WMBootCoord();
  }
}*/

Transform WorldObject::getParentTransform() const {//Transform relative to parent
  //if(!parent) return Transform();
  //else return par
  return transform;
}

WMBufferRegionTransform WorldObject::getRegionTransform() { _prsguard();
  WMBufferRegionTransform ret;
  //ret.tmat = transform.toMatrix();
  //ret.dual = transform.dualQuat();
  ret.tint = V4(1.f, 1.f, 1.f, 1.f);
  ret.dual = transform.dualQuat();
  
  //TODO: DON'T iterate over every parent, ya dumdum
  if(parent) {
    WMBufferRegionTransform tParent = parent->getRegionTransform();
    //ret.tmat *= tParent.tmat;
    ret.dual = ret.dual + tParent.dual;
    ret.tint *= tParent.tint;
  }
  return ret;
}

V3 WorldObject::getRenderPosition() { MutexGuard mg(this->mtx);
  //WMBufferRegionTransform rt = this->getRegionTransform();
  if(!parent) return transform.position;
  else return V3(parent->getRegionTransform().dual * V4(transform.position, 1.f));
}