/* 
 * File:   ObjectCompChain.h
 * Author: Galen Swain
 *
 * Created on May 15, 2015, 10:38 AM
 */

#ifndef OBJECTCOMPCHAIN_H
#define	OBJECTCOMPCHAIN_H

class ObjectCompChain {
public:
    ObjectCompChain();
    ObjectCompChain(const ObjectCompChain& orig);
    virtual ~ObjectCompChain();
private:

};

#endif	/* OBJECTCOMPCHAIN_H */

