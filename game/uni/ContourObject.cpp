/* 
 * File:   ContourObject.cpp
 * Author: Galen Swain
 * 
 * Created on May 15, 2015, 9:41 AM
 */

#include "ContourObject.h"
#include <iostream>
#include <list>
#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wshadow"
#include "engine/ext/polypartition.h"
#pragma GCC diagnostic pop
#include "comp/Workplane.h"
#include "rep/IdealSlice.h"
#include "rep/BakedContourTrans.h"
#define DEBUGMESH
ContourObject::ContourObject() : WorldObject() {
  extrusionLength = 0.f;
  resolveMatrix = Mat4(1.f);
  baseMesh = NULL;
#ifdef DEBUGMESH
  //debugMesh = NULL;
#endif
  knotExtrusionIncrement = FLT_MAX;
}

ContourObject::~ContourObject() {
  for(auto it = contours.begin() ; it != contours.end(); ++it) {
    if(!( it->ideal->prefab )) {
      delete it->ideal;
    }
    if(it->baked != NULL) delete it->baked;
  }
}

ContourObject *ContourObject::contour() {
  return this;
}

const ContourObject *ContourObject::constContour() const {
  return this;
}

bool ContourObject::isContour() const {
  return true;
}

void addOrientedTris(WMMeshType *const m, const BakedContourVert *const a, const BakedContourVert *b, const BakedContourVert *c) { _prsguard();
  if(m == NULL) return;
  V3 normal = m->vert(a->id).normal;
  V3 deltaAB = glm::normalize(a->pertPos - b->pertPos), deltaAC = glm::normalize(a->pertPos - c->pertPos);
  V3 dNorm = glm::cross(deltaAB, deltaAC);;
  
  if(glm::dot(normal, dNorm) > 0.f) m->addTriInd(a->id, b->id, c->id);
  else m->addTriInd(a->id, c->id, b->id);
}

Uint16 ContourObject::getNextContourID(float epsilon, Uint16 curID) { _prsguard();
 // return curID + 1;
  V3 prevCenter = this->perturbPoint(V3(0.f, 0.f, path.getFeatureTime(curID)));
  for(unsigned short i = curID+1; i < contours.size() - 1; i++) {
    bool skipContour = true;
    float contourParam = path.getFeatureTime(i), contourParamNext = path.getFeatureTime(i+1);
    const V3 curCenter = this->perturbPoint(V3(0.f, 0.f, contourParam)), nextCenter = this->perturbPoint(V3(0.f, 0.f, contourParamNext));
    
    if(glm::distance(curCenter, prevCenter) < epsilon) {
      if(glm::distance(nextCenter, curCenter) < epsilon*1.5f) {
        skipContour = true;
      } else {
        skipContour = false;
      }
    } else skipContour = false;
    
    if(!skipContour && i > curID) return i;
  }
  return contours.size() - 1;
}

static thread_local WMMeshType* debugMesh = NULL;
void ContourObject::construct(float epsilon, WMMeshType* meshBuff, WMMeshType *const dbg) { _prsguard(); MutexGuard mg(this->mtx);
  if(contours.size() <= 1) {
    Console::Error("ContourObject::construct called with 1 or fewer valid hulls!");
  }
  debugMesh = dbg;  this->baseMesh = meshBuff;
  //This is IMPORTANT, don't delete it!!!
  WorldObject::construct(epsilon, baseMesh);
  //std::cout << "building with epsilon: " << epsilon << "\n";
  //float extrusionCurrent = 0.f;
  //const float epsilonSkipThresh = pow(epsilon, 1.f);// * (3.f/4.f);
  //const bool doCache = flags.get(WorldObject::Subtractive);
  
  unsigned int meshVC = meshBuff->vertCount, dbgVC = dbg? dbg->vertCount : 0;
  unsigned int meshVI = meshBuff->indCount;
  
  this->constructSlices();
  for(unsigned short sliceID = 0; sliceID < slices.size(); sliceID++) {
    IdealSlice *const slice = &slices[sliceID];
    IdealSlice::Flags sliceFlags = 0x0;
    
    if(sliceID == 0) sliceFlags |= IdealSlice::CapLower;
    if(sliceID == slices.size()-1) sliceFlags |= IdealSlice::CapUpper;
    
    slice->construct(epsilon, meshBuff, sliceFlags, dbg);
  }
  /*
  BakedContourTrans prev;
  for(unsigned short i = 0; i < contours.size() - 1; i = this->getNextContourID(epsilon, i)) {
    //bool skipContour = true;
    Uint16 nextContourID = this->getNextContourID(epsilon, i);
    float contourParam = path.getFeatureTime(i), contourParamNext = path.getFeatureTime(nextContourID);
    std::cout << "building contour for " << i << " ->" << nextContourID << "\n";
//    const V3 curCenter = this->perturbPoint(V3(0.f, 0.f, contourParam)), nextCenter = this->perturbPoint(V3(0.f, 0.f, contourParamNext));
//    
//    if(i == 0 || i == contours.size() - 2) skipContour = false;
//    else if(glm::distance(curCenter, prevCenter) < epsilon) {
//      if(glm::distance(nextCenter, curCenter) < epsilon*1.5f) {
//        skipContour = true;
//      } else {
//        skipContour = false;
//      }
//    }
    //if(skipContour) continue;
    
    BakedContourTrans trans;
    //std::cout << "ext " << contourParam << "->" << contourParamNext << "\n";
    BakedContour* cContour = contours[i].getBaked(epsilon), *nContour = contours[nextContourID].getBaked(epsilon);
    if(cContour == NULL || nContour == NULL || cContour->size() < 3 || nContour->size() < 3) {
      Console::Error("BakedContourTrans::construct(): cContour or nContour is NULL!");
      continue;
    }
    trans.calculate(cContour, nContour,
            Workplane(V3(0.f, 0.f, contourParam), V3_FORWARD), Workplane(V3(0.f, 0.f, contourParamNext), V3_FORWARD), epsilon);
    
    if(i == 0) {
      trans.constructBottom = true;
      trans.capWith(epsilon, false);
    }
    if(i == contours.size()-2) {
      trans.constructTop = true;
      trans.capWith(epsilon, true);
    }
    
    if(i != 0) {
      trans.stitchWith(epsilon, prev);
      prev.construct(this, epsilon, meshBuff, dbg, false);//(glm::dot(V3_FORWARD, path.getDelta(contourParam)) > 0.96f));
      prev.shareWith(epsilon, trans);
    }
    //std::cout << "PATH  dot: " <<glm::to_string(path.getDelta(contourParam)) <<"\n";
    if(i == 0) {
      //auto tris = getContourTris(*contours[i].getBaked(epsilon));
      //this->meshContourTris(tris, false);
    }
    prev = trans;
  }
  prev.construct(this,epsilon, meshBuff, dbg, false);
  */
  
  //Transform and alter new points
  for(unsigned int i = meshVC; i < meshBuff->vertCount; i++) {
    WObjectVert *vert= &(meshBuff->bufferVert[i]);
    //Texture coordinate data is now performed in BakedContourTrans
    //this->assignTextureCoords(vert);
    V3 originalPos = vert->pos;
    vert->pos = this->perturbPoint(vert->pos);
    vert->normal = this->perturbNormal(vert->normal, originalPos, vert->pos);
  }
  //std::cout << "CO INDS:\n";
  for(unsigned int i = meshVI; i < meshBuff->indCount; i++) {
    //std::cout << "ind: " << meshBuff->bufferInd[i] << "\t\t" << meshVI << "\n";
  }
  if(dbg) {
    for(unsigned int i = dbgVC; i < dbg->vertCount; i++) {
      WObjectVert *vert= &(dbg->bufferVert[i]);
      V3 originalPos = vert->pos;
      vert->pos = this->perturbPoint(vert->pos);
      vert->normal = this->perturbNormal(vert->normal, originalPos, vert->pos);
      //std::cout << "mesh has pos " << glm::to_string(originalPos) << "->" << glm::to_string(vert->pos) << "\n";
    }
  }
  
  for(unsigned short i = 0; i < contours.size(); i++) {
    contours[i].freeBaked();//Unload from cache and save memory
  }
  
  this->constructPhysical();
  std::cout << "Physical char: volume=" << physical.volume() << ", mass=" << physical.mass() << ", COM=" << glm::to_string(physical.COM()) << "\n";
}

float ContourObject::getExtrusionIncrement(float curExt, float epsilon) {
  const float maxIncrement = 12000.f;
  return std::min(maxIncrement, std::min(path.getDeltaStep(curExt, epsilon), knotExtrusionIncrement));
}

std::vector<const BakedContourVert*> ContourObject::getContourTris(BakedContour& contour) { _prsguard();
  std::vector<const BakedContourVert*> ret;
  if(contour.size() < 3) return ret;
  //Compose poly structure
  TPPLPoly inPoly; std::list<TPPLPoly> polys;
  inPoly.Init(contour.verts.size());
  //inPoly.SetHole(true);
  for(Uint16 i = 1; i <= contour.verts.size(); i++) {//Temp order hack to get end-faces drawn
    BakedContourVert vert = contour.verts[i-1];
    //if(i == contour.verts.size()-1 && glm::distance(vert.pos,contour.verts[0].pos) < 0.008f) vert.pos = contour.verts[0].pos;
    inPoly[/*contour.verts.size()-*/i-1].x = vert.pos.x;
    inPoly[/*contour.verts.size()-*/i-1].y = vert.pos.y;
    //std::cout << "adding " << inPoly[i].x<<","<<inPoly[i].y<<"\n";
  }
  /*std::list<TPPLPoly> inPolys; std::list<TPPLPoly> polys;
  contour.clearFlags();
  for(auto it = contour.verts.begin(); it != contour.verts.end(); ++it) {//Decompose into polygon groups
    if(it->proc) continue;
    
    inPolys.push_back(TPPLPoly()); unsigned int i = 0;
    TPPLPoly &inPoly = *inPolys.rbegin();
    inPoly.Init(contour.verts.size());
    inPoly.SetHole(true);
    
    for(Uint16 i = 0; i < contour.verts.size(); i++) {
      BakedContourVert vert = contour.verts[i];
      //if(i == contour.verts.size()-1 && glm::distance(vert.pos,contour.verts[0].pos) < 0.008f) vert.pos = contour.verts[0].pos;
      inPoly[i].x = vert.pos.x;
      inPoly[i].y = vert.pos.y;
      //std::cout << "adding " << inPoly[i].x<<","<<inPoly[i].y<<"\n";
    }
    unsigned int drawElimID = it->siblingID[1]; const Uint8 sibOffset = 1;
    unsigned int drawInterFrom = contour.getID(&(*it));
    while(drawElimID != CVNULL) {
      contour.verts[drawElimID].proc = true;
      inPoly[i].x = contour.verts[drawElimID].pos.x;
      inPoly[i].y = contour.verts[drawElimID].pos.y;
      i++;
      
      if (drawElimID == drawInterFrom) break;
      drawElimID = contour.verts[drawElimID].siblingID[sibOffset];
    }
    std::cout << "Poly set of " << i << "verts created\n";
    break;
  }*/
  //std::cout <<"endpoly\n";
  //Pass, monotonize, and triangulate
  TPPLPartition pp; std::list<TPPLPoly> tris; int res;
  //res = pp.ConvexPartition_HM(&inPoly, &polys);
  //polys.push_back(inPoly);
  std::list<TPPLPoly> tmpPoly; polys.push_back(inPoly);
  pp.RemoveHoles(&tmpPoly, &polys);
  //std::cout << " Convex partition: " << res << " returning " << polys.size() << " of " << polys.begin()->GetNumPoints()<<"\n";
  if(polys.size() == 0 || polys.begin()->GetNumPoints() < 3) return ret;
  for(auto it = polys.begin(); it != polys.end(); ++it) {
    res = pp.Triangulate_EC(&(*it), &tris);
    std::cout << " Triangulate :" << res << " returning up to" << tris.size() << "\n";
  }
  _unused(res);
  
  //Recompose to vertex structure - points are unique due to preemptive mesh simplification, so associate and reconstruct
  ret.reserve(tris.size()*3);
  for(auto it = tris.begin(); it != tris.end(); ++it) {
    if(it->GetNumPoints() != 3) {
      Console::Warning("Triangulation returned non-triangle mesh!");
      continue;
    }
    
    ret.push_back( &contour.getNearestXY(V3(it->GetPoint(0).x, it->GetPoint(0).y, 0.f)));
    ret.push_back( &contour.getNearestXY(V3(it->GetPoint(1).x, it->GetPoint(1).y, 0.f)));
    ret.push_back( &contour.getNearestXY(V3(it->GetPoint(2).x, it->GetPoint(2).y, 0.f)));
  }
  
  return ret;
}

void ContourObject::meshContourTris(const std::vector<const BakedContourVert*> &verts, bool inv) {
  for(Uint16 i = 0; i < verts.size(); i += 3) {
    const Uint16 mEndOff = 1;
#ifdef DEBUGMESH
     debugMesh->addLineInd(verts[i+0]->id + mEndOff, verts[i+1]->id + mEndOff);
     debugMesh->addLineInd(verts[i+1]->id + mEndOff, verts[i+2]->id + mEndOff);
     debugMesh->addLineInd(verts[i+2]->id + mEndOff, verts[i+0]->id + mEndOff);
#endif
    if(inv) baseMesh->addTriInd(verts[i+2]->id + mEndOff, verts[i+1]->id + mEndOff,  verts[i+0]->id + mEndOff);
    else baseMesh->addTriInd(verts[i+0]->id + mEndOff, verts[i+1]->id + mEndOff,  verts[i+2]->id + mEndOff);
  }
}

void ContourObject::drawDebug() {
  WorldObject::drawDebug();
#ifdef DEBUGMESH
  //if(debugMesh != NULL) debugMesh->draw();
#endif
}

void ContourObject::draw() {
  WorldObject::draw();
  //if(baseMesh != NULL) baseMesh->draw();
}

void ContourObject::addContour(IdealContour* const c) {
  ContourLevel level;
  level.ideal = c;
  level.baked = NULL;//new BakedContour();
  contours.push_back(level);
}

//Interpolate for level;
const BakedContour ContourObject::getContour(float d, float epsilon, float *featureTime) {
  BakedContourWeights levels = path.getLevelWeights(d); BakedContour ret;
  if(levels.toID == 0xFF) ret = *contours[levels.fromID].getBaked(epsilon);
  else {
    const float t = levels.weight;
    IdealContour *const idealFrom = (contours[levels.fromID].ideal), *const idealTo = (contours[levels.toID].ideal);
    const float tFrom = pow(t*idealFrom->linear, idealFrom->quadratic), tTo = pow( (1.f-t)*idealTo->linear , idealTo->quadratic);
    //Differential parts
    //const float dFrom = idealFrom->quadratic * pow(idealFrom->linear, idealFrom->quadratic) * pow(t, idealFrom->quadratic- 1);
    //const float dTo   = idealTo->quadratic   * pow(idealTo->linear  , idealTo->quadratic  ) * pow(1.f-t, idealTo->quadratic  - 1);
    const float tMax = tFrom  + tTo;
    levels.weight = tFrom / tMax;
    if(featureTime != NULL) *featureTime = levels.thru;

    BakedContour& from = *contours[levels.fromID].getBaked(epsilon);
    BakedContour& to = *contours[levels.toID].getBaked(epsilon); 
    /*
    float quadraticMagnitude = 0.f;
    if(idealFrom->quadratic > 0.f) quadraticMagnitude += tSquare;
    else if(idealFrom->quadratic < 0.f) quadraticMagnitude += 1.f - tSquare;

    if(idealTo->quadratic < 0.f) quadraticMagnitude += 1.f-tSquare;
    else if(idealTo->quadratic > 0.f) quadraticMagnitude += tSquare;

    std::cout << "from " << idealFrom->quadratic << " to " << idealTo->quadratic  << " " <<quadraticMagnitude << " QDMAG at tsq " << tSquare << "\n";
    if(quadraticMagnitude < FLT_EPSILON || levels.length < FLT_EPSILON) knotExtrusionIncrement = FLT_MAX;
    else knotExtrusionIncrement = (pow(epsilon, 1.f) / levels.length) / quadraticMagnitude * 3.2f;
    */
    float quadraticMagnitude = std::min(1.f, abs(idealFrom->quadratic-1.f) * (t+0.5f)/3.f + abs(idealTo->quadratic-1.f) * (1.f-t + 0.5f)/3.f);
    //quadraticMagnitude = pow(quadraticMagnitude, 1.4f);
    knotExtrusionIncrement = pow(epsilon, 0.8f) * levels.length / ((idealFrom->quadratic * t + idealTo->quadratic * (1.f - t))) / pow(quadraticMagnitude, 0.8f);
    //knotExtrusionIncrement = ((dFrom / (dFrom + dTo)) * levels.length) / epsilon / 30.f;// * float(to.size() / from.size());//Simple difference heuristic by size
    //std::cout << knotExtrusionIncrement << "\n";
    if(knotExtrusionIncrement == 0.f) knotExtrusionIncrement = FLT_MAX;

    if(to.size() > from.size()) ret = to.lerpTo(from, 1.f - levels.weight, epsilon);
    else ret = from.lerpTo(to, levels.weight, epsilon);
  }
  
  //std::cout << "debugging d << "<< d << "\n";
  for(auto it = ret.verts.begin(); it != ret.verts.end(); ++it) {
    //std:: cout <<"\t"<< ret.getID(&(*it)) << "->" << it->siblingID[1] << "\n";
  }
  
  
  //For each affecting CSG child, alter the contour
  //if(levels.toID != 0xFF|| !levels.isLast) {
    //std::cout << "proc at " << d <<"(" << int(levels.toID) << ", " << levels.isLast <<  ")\n";
    //std::cout << "EXEC SUBTRACT\n";
    ContourPlane workPlane =  this->resolveWorkplane(d);
    for(auto it = children.begin(); it != children.end(); it++) {
      WorldObject *const child = (*it);
      if(child->flags.get(WorldObject::Subtractive)) {
        ContourObject *cContour = (ContourObject*)child;
        BakedContour intersection = cContour->contourCache.get(workPlane.origin, workPlane.normal, epsilon);
        
        //std::cout << "Subtracting child, " << intersection.size() << " inter\n";
        //if(intersection.size() > 1) ret = intersection;
        //ret.add(intersection);
        if(intersection.size() > 2) ret.csgOperand(intersection, epsilon, BakedContour::Operand::Add);
        //ret.add(intersection);
      }
  //  }
  }
    
  //Enabling this causes an error with sibling-parent traversal on subtraction- TODO: why?!?!
  ret.simplify(epsilon);
  return ret;
  //return contours[levels.fromID].getBaked(epsilon);
}

IntersectionHull ContourObject::getContourBounds() { _prsguard();
  unsigned int featureID = 0;
  IntersectionHull ret;
  for(auto it = contours.begin(); it != contours.end(); ++it) {
    IdealContour *const ideal = it->ideal;
    //std::cout << "encorporating bound with a=" << ideal->bounds.area() << "\n";;
    const float featureTime = path.getFeatureTime(featureID);
    Workplane wp = path.getWorkplane(featureTime);
    //std::cout << "wp V_0 = " << glm::to_string(wp.center) << " pointing to " << glm::to_string(wp.normal) << " at ft=" << featureTime << "\n";
    if(ideal->size() > 0) ret.encompassWorkplane(ideal->bounds, wp);
    featureID++;
  }
  //std::cout << "3D bounds: " << glm::to_string(ret.root) << ", " << glm::to_string(ret.root+ret.del) << "\n";
  //std::cout << "built hull of v=" << ret.volume() << "\n";
  return ret;
}

void ContourObject::constructSlices() { _prsguard();
  slices.clear();
  for(unsigned short i = 0; i < contours.size() - 1; i = this->getNextContourID(0.f, i)) {
    Uint16 nextContourID = this->getNextContourID(0.f, i);
    float contourParam = path.getFeatureTime(i), contourParamNext = path.getFeatureTime(nextContourID);
    std::cout << "building slice data for " << i << " ->" << nextContourID << "\n";
    
    slices.push_back(IdealSlice());
    auto it = slices.rbegin();
    it->s = contours[i].ideal;
    it->e = contours[nextContourID].ideal;
    it->path = &path;
    it->subfield = &subfield;
    
    it->sZ = contourParam;
    it->eZ = contourParamNext;
    
    if(i == 0) it->isBottom = true;
    if(i == contours.size()-2) it->isTop = true;
    
    it->calculateCorrelates();
  }
}

void ContourObject::constructPhysical() { _prsguard();
  WorldObject::constructPhysical();
  this->constructSlices();
  
  for(unsigned short i = 0; i < slices.size(); i++) {
    IdealSlice *slice = &slices[i];
    Physical phy = slice->calculatePhysical();
    Transform trans;
    std::cout << "Slice char: volume=" << phy.volume() << ", mass=" << phy.mass() << ", COM=" << glm::to_string(phy.COM()) << "\n";
    physical.incorporate(&phy, trans);
  }
}
void ContourObject::constructThresholds() { _prsguard();
  WorldObject::constructThresholds();
  
}

unsigned short ContourObject::getContourCount() {
  return contours.size();
}
IdealContour* ContourObject::getContour(unsigned short i) {
  return contours[i].ideal;
}