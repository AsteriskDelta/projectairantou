/* 
 * File:   DualObject.h
 * Author: Galen Swain
 *
 * Created on May 23, 2015, 1:05 PM
 * 
 * Uniform grid dual-contouring, allows for greater rendering and physics speeds
 */

#ifndef DUALOBJECT_H
#define	DUALOBJECT_H
#include "WorldObject.h"
#include "lib/Flags.h"

typedef V3T<Int8> DOVec;

struct DOPoint {
    enum {
        External    = BIT0,
        DistBound   = BIT1,
        Marked      = BIT2
    };
    
    WMatID matID;
    WMatParam matParams;
    Int4x2 skew;
    Flags<Uint8> flags;
    
    inline bool isTransparent() const {
        return matID.id != 0;
    }
};

struct DOPointComp {
    WMatID matA, matB;
    WMatParam matParamA, matParamB;
    float matRatio;
    V3 localPoint;
};

class DualGridObject : public WorldObject {
public:
    DualGridObject();
    //DualObject(const DualGridObject& orig);
    virtual ~DualGridObject();
    
    DOPointComp getPoint(V3 pos);//Local position query
    DOPoint& getPoint(const DOVec& rawPos);//Raw query
    
    void preprocess();
    bool allocateFor(DOVec size);
private:
    Uint16 calcIdx(const DOVec &c) const;
    
    DOPoint *gridData;
    Uint16 gridLength;
    DOVec maxSize;
    
    static DOPoint dummy;
};

#endif	/* DUALOBJECT_H */

