/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WorldObjectPtr.cpp
 * Author: Galen Swain
 * 
 * Created on April 10, 2016, 12:11 PM
 */

#include "WorldObjectPtr.h"
#include "WorldObject.h"
#include "WorldLoader.h"

WorldObjectPtr::WorldObjectPtr() : ptr(nullptr) {
  isRel = true;
}

WorldObjectPtr::WorldObjectPtr(const WorldObjectPtr& orig) {
  ptr = orig.ptr;
}

WorldObjectPtr::~WorldObjectPtr() {
}

WorldObjectPtr::WorldObjectPtr(WorldObject *const obj) {
  ptr = obj;
  //std::cout << "set PTR to : " << ptr << "\n";
  isRel = true;
}
WorldObjectPtr::WorldObjectPtr(WOID objID) {
  id = objID;
  isRel = false;
}

WorldObject& WorldObjectPtr::operator*(){
  return *((WorldObject*)(*this));
}
WorldObjectPtr::operator WorldObject*() { _prsguard();
  if(isRel) {//Return the pointer
    return reinterpret_cast<WorldObject*>(PtrInt(ptr) & (~BIT00));
  } else {//Allocate a deferred WO
    WorldObject *const newPtr = WorldLoader::LoadObject(id);
    ptr = newPtr;
    isRel = true;
    return newPtr;
  }
}
    
WorldObject* WorldObjectPtr::operator->() {
  return (WorldObject*)(*this);
}
    
WorldObjectPtr& WorldObjectPtr::operator=(const WorldObjectPtr& o) {
  ptr = o.ptr;
  //std::cout << "set PTR to : " << ptr << "\n";
  isRel = o.isRel;
  return *this;
}
bool WorldObjectPtr::operator==(const WorldObjectPtr& o ) const {
  return ptr == o.ptr;
}
bool WorldObjectPtr::operator!=(const WorldObjectPtr& o) const {
  return ptr != o.ptr;
}

bool WorldObjectPtr::operator==(const WorldObject *const o) const {
  return isRel && reinterpret_cast<WorldObject*>(PtrInt(ptr) & (~BIT00)) == o;
}
bool WorldObjectPtr::operator!=(const WorldObject *const o) const {
  return !isRel || reinterpret_cast<WorldObject*>(PtrInt(ptr) & (~BIT00)) != o;
}

WOID WorldObjectPtr::woID() const {
  if(isRel) return reinterpret_cast<WorldObject *const>(PtrInt(ptr) & (~BIT00))->getID();
  else return id;
}