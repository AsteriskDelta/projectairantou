/* 
 * File:   DualObject.cpp
 * Author: Galen Swain
 * 
 * Created on May 23, 2015, 1:05 PM
 */

#include "DualGridObject.h"

DOPoint DualGridObject::dummy = DOPoint();

const DOVec siblingOffsets[6] = {
  DOVec( 1, 0, 0), DOVec(-1, 0 , 0),//Differs X
  DOVec( 0, 1, 0), DOVec( 0,-1 , 0),//Differs Y
  DOVec( 0, 0, 1), DOVec( 0, 0 ,-1),//Differs Z
};

DualGridObject::DualGridObject() {
  gridData = NULL;
}
//DualObject::DualObject(const DualObject& orig) {
//}
DualGridObject::~DualGridObject() {
  if(gridData != NULL) delete gridData;
  
}

void DualGridObject::preprocess() {//Determine if internal/external, prepare for CW iteration, etc.
  for(Int8 z = 0; z < maxSize.z; z++) {
    for(Int8 y = 0; y < maxSize.y; y++) {
      for(Int8 x = 0; x < maxSize.x; x++) {
        const DOVec pt = DOVec(x,y,z);
        DOPoint self = this->getPoint(pt);
        bool external = false;
        
        for(Uint8 sib = 0; sib < 6; sib++) {
          const DOPoint& sibling = this->getPoint(pt + siblingOffsets[sib]);
          external |= sibling.isTransparent();
        }
        
        self.flags.set(DOPoint::External  , external);
        self.flags.set(DOPoint::Marked    , false);
      }
    }
  }
  
  //Determine clockwise manner
}

bool DualGridObject::allocateFor(DOVec size) {
  const unsigned int allocSize = size.x * size.y * size.z;
  if(allocSize > 65535) return false;
  
  gridData = new DOPoint[allocSize];
  gridLength = allocSize;
  maxSize = size;
}

DOPoint& DualGridObject::getPoint(const DOVec& rawPos) {
  return gridData[calcIdx(rawPos)];
}

Uint16 DualGridObject::calcIdx(const DOVec &c) const {
  const Uint16 idx = c.z * (maxSize.x * maxSize.y) + c.y * maxSize.x + c.x;
  return idx >= gridLength? 0 : idx;
}