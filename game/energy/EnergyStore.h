/* 
 * File:   EnergyStore.h
 * Author: Galen Swain
 *
 * Created on June 20, 2015, 12:38 PM
 */

#ifndef ENERGYSTORE_H
#define	ENERGYSTORE_H
#include <list>
#include "EnergyInstance.h"

class EnergyStore {
public:
    EnergyStore();
    EnergyStore(const EnergyStore& orig);
    virtual ~EnergyStore();
    
    std::list<EnergyInstance> forces;
private:

};

#endif	/* ENERGYSTORE_H */

