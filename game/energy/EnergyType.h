/* 
 * File:   EnergyType.h
 * Author: Galen Swain
 *
 * Created on June 20, 2015, 12:35 PM
 */

#ifndef ENERGYTYPE_H
#define	ENERGYTYPE_H
#include "game/game.h"

#define ENERGYTYPE_MAX 64

class EnergyType {
public:
    EnergyType();
    EnergyType(const EnergyType& orig);
    virtual ~EnergyType();
    
    Uint8 id;
    
    static EnergyType types[ENERGYTYPE_MAX];
private:

};

#endif	/* ENERGYTYPE_H */

