/* 
 * File:   EnergyInstance.h
 * Author: Galen Swain
 *
 * Created on June 20, 2015, 12:36 PM
 */

#ifndef ENERGYINSTANCE_H
#define	ENERGYINSTANCE_H
#include "game/game.h"
#include <lib/V3.h>

class EnergyInstance {//Must fit within 16 bytes
public:
    EnergyInstance();
    EnergyInstance(const EnergyInstance& orig);
    virtual ~EnergyInstance();
    
    struct {
        Uint8 energyID;
        Uint8 paddA, paddB, paddC;
    };
    V3 dataVec;
private:

};

#endif	/* ENERGYINSTANCE_H */

